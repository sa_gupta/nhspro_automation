package com.ex2.nhspro.client_testcases;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;

public class Common_testcases extends Initializations{

@BeforeMethod(groups = {"smokeTest","regressionTest"})
public void beforeMethod() {
	openClientApplication();
}

/*	@Test
	public void verify_Login_Into_Application() {
		commonPage.clickOnSingIn_link();
		commonPage.enterEmailAddress();
		commonPage.clickOnSubmit_btn();
		boolean LoginStatus=commonPage.isUserLoggedIn();
		Assert.assertTrue(LoginStatus,"User not logged In");
	}*/
	
	@Test(enabled=false,groups = {"regressionTest"})
	public void verify_Registration_Into_Client_Application() {
		commonPage.clickOnSingIn_link();
		int randomInt = new Random().nextInt(1000);
		String emailId = "nhs" + randomInt + "@newhomesource.com";
		commonPage.enterEmailAddress(emailId);
		commonPage.clickOnSubmit_btn();
		commonPage.enterFirstName_in_FirstName_field(commonData.getJsonData("clientData","FirstName"));
		commonPage.enterLastName_in_LastName_field(commonData.getJsonData("clientData","LastName"));
		commonPage.enterPhone_in_Phone_field(commonData.getJsonData("clientData","PhoneNo"));
		commonPage.clickOnSave_button();
		boolean LoginStatus=commonPage.isUserLoggedIn();
		Assert.assertTrue(LoginStatus,"User not logged In");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void validate_register_of_a_newclient_clientside() {
		commonPage.clickOnSingIn_link();
		int randomInt = new Random().nextInt(1000);
		String emailId = "nhspro_qa" + randomInt + "@newhomesource.com";
		commonPage.enterEmailAddress(emailId);
		commonPage.clickOnSubmit_btn();
		ActionsUtility.staticWait(2); 
		commonPage.register_a_newclient(commonData.getJsonData("clientData","FirstName"),commonData.getJsonData("clientData","LastName"),commonData.getJsonData("clientData","PhoneNo"));
		boolean value=commonPage.isUserLoggedIn();
		Assert.assertTrue(value, "Unable to Register A New Client");
		System.out.println("New Client Register Successfully");
		ActionsUtility.staticWait(2); 
		openAgentApplication();
		commonPage.loginIntoApplication(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
		commonPage.clickOnMyClientsLink();
		myclient.deleteClient(commonData.getJsonData("clientData","ClientName"));
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verifyNavigationOnStaticPages() {		
		commonPage.clickOnPrivacy_link();
		Assert.assertTrue(driver.getTitle().contains("Privacy Policy"));
		commonPage.clickOnCookie_link();
		Assert.assertTrue(driver.getTitle().contains("Cookie Policy"));
		commonPage.clickOnTerms_link();
		Assert.assertTrue(driver.getTitle().contains("Terms of Use"));
		commonPage.clickOnDoNotSellInfo_link();
		Assert.assertTrue(driver.getTitle().contains("User Privacy"));
	}

}
