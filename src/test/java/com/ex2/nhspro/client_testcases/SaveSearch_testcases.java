package com.ex2.nhspro.client_testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;

public class SaveSearch_testcases extends Initializations{

	String SaveSearchesName;
	String CurrentUrl="";
	
	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openClientApplication();
	}
	

	
	@Test(groups = {"regressionTest"})
	public void verify_Save_Search_functionality() {
	loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.clickOnSave_btn();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    ActionsUtility.staticWait(5);
	    String savedSearchesHomeCount = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCount);
	    driver.navigate().back();
	    savedSearchesPage.select_Perticular_checkbox(SaveSearchesName);
	    savedSearchesPage.clickOn_Delete_btn();
	    savedSearchesPage.click_On_Delete_btn_from_Delete_popup();
	    
	}

	@Test(groups = {"smokeTest","regressionTest"})
	public void verify_Save_Search_loaded_properly_on_clientsite() {
	//	commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
		loginOrNavigate();
		 commonPage.click_On_SavedSearches();
		 ActionsUtility.staticWait(2);
		 Assert.assertTrue(driver.getCurrentUrl().contains("savedsearches"), "Saved Search page unable to load properly");
			System.out.println("Saved Search page on client site is loaded properly"); 
		 
	}
	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			homePage.clickOnSearch_icon();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);			
	}

}
