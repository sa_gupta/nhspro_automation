package com.ex2.nhspro.client_testcases;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SaveListing_testcases extends Initializations {
	
	String CurrentUrl="";
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openClientApplication();
	}
		
	
	@Test(description="Verify Client Perform Saved Listing",groups = {"regressionTest"})
	public void verify_saveListing_working_clientSide() {
		loginOrNavigate();
	//	commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
		commonPage.clickOnSaveListing();
		saveListingPage.clearSaveListing();
		saveListingPage.clickOnBack_link();
//		commonPage.clickOnSearchNewHomes_link();
//		ActionsUtility.staticWait(2);
//		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
//		homePage.clickOnSearch_icon();
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomesInPhotoView(1);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			srpPage.selectHome(1);
		else
			photoView.slectHomeInPhotoView(1);
		srpPage.clickOn_Main_SaveListing();
		srpPage.clickOnOk_saveListing();
		srpPage.clickOnOk_button();
		ActionsUtility.staticWait(2);
		commonPage.clickOnSaveListing();
		ActionsUtility.staticWait(5);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListing);
		System.out.println("Saved Listing by Client Verified");
	}
	
	@Test(description="Verify Saved Listing Page on Client site is loaded properly",groups = {"SmokeTest","regressionTest"})
	public void verify_SavedListing_Page_loaded_properly_on_clientsite()
	{
		//commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
		loginOrNavigate();
		commonPage.clickOnSaveListing();
		Assert.assertTrue(driver.getCurrentUrl().contains("savedlistings"), "Saved Listing page unable to load properly");
		System.out.println("Saved Listing page on client site is loaded properly");
	}
	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
			homePage.clickOnSearch_icon();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);			
	}
}
