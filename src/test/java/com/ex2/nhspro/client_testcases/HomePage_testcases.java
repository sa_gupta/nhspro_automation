package com.ex2.nhspro.client_testcases;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.GetCommonData;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class HomePage_testcases extends Initializations{

	String currentUrl="";
	
	
	  @BeforeClass (groups= {"smokeTest","regressionTest","HappyPath"})
	  public void beforeStarting() 
	  { 
		  openClientApplication(); 
		  }
	 
		@Test(groups = {"HappyPath"},priority=1)
		public void validateElementsOnHomePage_BeforeLogin() {
			Assert.assertEquals(homePage.getSearchBoxElementsExistOnThePage(), GetCommonData.getSearchData(getCurrentUrl()));
			Assert.assertEquals(homePage.getHomeBuilderSectionData(),GetCommonData.getHomebuildersData(getCurrentUrl()));
		}
		
		@Test(groups = {"HappyPath"},priority=2)
		public void validateElementsOnHomePageAfterLogin() {
			loginOrNavigate();
			Assert.assertEquals(homePage.getSearchBoxElementsExistOnThePage(), GetCommonData.getSearchData(getCurrentUrl()));
			Assert.assertEquals(homePage.getHomeBuilderSectionData(),GetCommonData.getHomebuildersData(getCurrentUrl()));
		}
	
	/**
	 * @author sagupta
	 */
	@Test(description="To verify client is able to do the searching without filters from homepage",groups= {"smokeTest","ClientFlow"})
	public void validate_Searching_Without_filter_at_client_Side() {
		loginOrNavigate();
		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		homePage.clickOnSearch_icon();
		String locationtext=srpPage.getSearchLocationText();
		System.out.println(locationtext);
		Assert.assertEquals(locationtext, homeData.getJsonData("Search_data","Matching_Searched_data"),"Mismatch");
		logger.log(Status.PASS, "Assertion Passed:- Actual Text is same as Expected");
		System.out.println("Assertion Passed:- Actual Text is same as Expected");
	}
	
	/**
	 * @author sagupta
	 * @descrition:- 
	 */
	@Test(description="To verify searching is working from location handler agent side",groups= {"ClientFlow","regressionTest"})
	public void validate_Searching_from_location_handler_at_client_side() {
		loginOrNavigate();
		homePage.enterSearchData_field(homeData.getJsonData("invalidData","SearchData"));
		homePage.clickOnSearch_icon();	
		Assert.assertTrue(locationHandler.isUserOn_Location_HandlerPage(), "User is on "+driver.getCurrentUrl());
		locationHandler.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		locationHandler.clickOnSearch_icon();
		
		  String locationtext=srpPage.getSearchLocationText();
		 Assert.assertEquals(locationtext, homeData.getJsonData("Search_data","Matching_Searched_data"),"Mismatch"); 
			logger.log(Status.PASS, "Assertion Passed:- Actual Text is same as Expected");
			System.out.println("Assertion Passed:- Actual Text is same as Expected");

	}
	
	/**
	 * @author sagupta
	 */
	@Test(groups = {"smokeTest","regressionTest"})
	public void validate_searching_from_homePage_with_filter_clientside() {
		commonPage.clickOnSearchNewHomes_link();
		String minPrice="100000";
		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
	    homePage.select_minprice("100000");	
	    homePage.clickOnSearch_icon();
	    String locationtext=driver.getCurrentUrl();
	    System.out.println(locationtext);
	    String MinAmount=minPrice.replace(",", "").replace("$", "");
	    Assert.assertTrue(locationtext.contains("pricelow="+MinAmount),"Min Price Filter Does Not Matched");
	    Assert.assertTrue(driver.getTitle().contains(homeData.getJsonData("Search_data","Matching_Searched_data")));
	    Assert.assertTrue(driver.getCurrentUrl().contains(homeData.getJsonData("Search_data","containtext")));
	    System.out.println("Agent Search From Home Page With Filters Verified");   
	}
	
	/* Author Shankit */
	@Test(groups = {"smokeTest","regressionTest"})
	public void validateSeeWhatsNewToday() {
		loginOrNavigate();
		homePage.isWhatsNewTodayLinkExist();
		System.out.println("See What's New Today Link is verified");
		homePage.clickOnWhatsNewTodayLink();
		srpPage.switchTowindow();
		homePage.isSeeAllQuickMoveInsLinkExist();
		homePage.isSeeAllNewCommunitiesLinkExist();
		homePage.isSeeAllFeaturedCommunitiesLinkExist();
		System.out.println("See What's New Today Page is verified");
		ActionsUtility.staticWait(5);
	}
	
	@Test(description="Validate clarity script for client site",groups = { "smokeTest" })
	public void validate_clarity_script_for_client()
	{
		loginOrNavigate();
		String sourceCode=driver.getPageSource();
		Assert.assertTrue(sourceCode.contains(homeData.getJsonData("ClarityScriptForClient", "Clientclarityscript")), "Clarity script for clientsite is not available");
		System.out.println("Clarity Script for Clientsite is Verfied Successfully");
	}  

	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	
	
}
