package com.ex2.nhspro.client_testcases;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SRPPage_testcases  extends Initializations {
	String CurrentUrl="";
	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openClientApplication();
	}
	


	
	@Test(description="Verify Client Perform PrintReport From SRP",groups = {"regressionTest"})
	public void verify_client_perform_printreport_from_SRPPage() {
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("List");
		srpPage.selectParticularHome(3);
		 String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnPrintReportButton();
		ActionsUtility.staticWait(2);
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
		srpPage.clickOnOkPrintReportForClient();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		String currentlurl=driver.getCurrentUrl();
		System.out.println(currentlurl);
		 Assert.assertTrue(currentlurl.contains("pdfgenerator"),"Client Print Report From SRP Page Functionality Not Verified");
		  System.out.println("Client Print Report From SRP Page Functionality Verified");
		    }
		    else {
		    	Assert.assertTrue(srpPage.isOK_btn_exist(), "Ok Button is Not Present");
		    	  System.out.println("Client Print Report From SRP Page Functionality Verified");
		    	  logger.log(Status.INFO, "Client Print Report From SRP Page Functionality Verified");
		    }
		driver.close();
		driver.switchTo().window(parentWinHandle);
	}
	
	@Test(description="To verify navigation of home and community tab should take place on agent side",groups = {"smokeTest","regressionTest"})
	public void verify_navigationOfTabOnSRPFromClientSide() {
		loginOrNavigate();
		Assert.assertTrue(commonPage.isHomeTabSelected());
		ActionsUtility.printLog("Assertion Paased:- Home tab is selected");
		commonPage.clickOnCommunities_tab();
		Assert.assertTrue(commonPage.isCommunitiesSelected());	
		ActionsUtility.printLog("Assertion Paased:- Communities tab is selected");
	}
	
	@Test(groups = {"regressionTest"})
	public void verifySearchingFromSRPPage_ClientSide() {
		loginOrNavigate();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			srpPage.clickOnFilter_link();
		srpPage.enterSearchedData_in_location_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		srpPage.clickOnSearchIcon();
		String currecturl = driver.getCurrentUrl();
		Assert.assertTrue(currecturl.contains(homeData.getJsonData("Search_data","containtext")), "User is unable to search from the SRP Page");
		ActionsUtility.printLog("Searching From the SRP Page is Verified");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_NearByCity_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
//		srpPage.closeSRPEnageClientPopUp();
		filter.clickOnLocationField();
		filter.clickNearByCities();
		filter.selectCity_from_NearBy("Austin");
		filter.selectCity_from_NearBy("Bastrop");
		filter.clickOnUpdate_btn_forlocation();
		ActionsUtility.staticWait(5);
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("NearByCity", "Bastrop");
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "Bastrop, TX", noOfHomesWithSublocation);
			}
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_ZipCode_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
//		srpPage.closeSRPEnageClientPopUp();
		filter.clickOnLocationField();
		filter.clickZipcode();
		filter.selectZipCode_from_NearBy("78612");
		filter.clickOnUpdate_btn_forlocation();
		ActionsUtility.staticWait(5);
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("Zipcode", "78612");
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "78612", noOfHomesWithSublocation);
			}
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_SchoolDistrict_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
		filter.clickOnLocationField();
		filter.clickSchoolDistricts();
		String idOfTheDistrict = filter.getIdOfTheDistrict("Bastrop ISD");
		filter.selectSchool_from_SchoolDistricts("Bastrop ISD");
		filter.clickOnUpdate_btn_forlocation();
		ActionsUtility.staticWait(5);
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("SchoolDistricts",idOfTheDistrict);
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "Bastrop ISD", noOfHomesWithSublocation);
			}
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_County_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
//		srpPage.closeSRPEnageClientPopUp();
		filter.clickOnLocationField();
		filter.clickCounties();
		filter.selectCounty_from_Counties("Burnet");
		filter.clickOnUpdate_btn_forlocation();
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("Counties", "Burnet");
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "Burnet County, TX", noOfHomesWithSublocation);
			}
	}
	
	/* Author Shankit */
	@Test(groups = {"regressionTest"})
	public void verify_ContactMe_Home_Preview_FromSRP() {
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("List");
		srpPage.clickOnContactMeFormTabPreview();
		srpPage.enterTextInContactMeTextArea("Contact Me Client");
		srpPage.clickOnSend_button_on_ContactMe_Preview();
		System.out.println("ContactMe form from Home Preview has been verified");
	}
	
	/* Author Shankit */
	@Test(groups = {"regressionTest"})
	public void verify_ContactMe_Community_Preview_FromSRP() {
		loginOrNavigate();
		commonPage.clickOnCommunities_tab();
		commonPage.clickOnView("List");
		srpPage.clickOnContactMeFormTabPreview();
		srpPage.enterTextInContactMeTextArea("Contact Me Client");
		srpPage.clickOnSend_button_on_ContactMe_Preview();
		System.out.println("ContactMe form from Community Preview has been verified");
	}

	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
			homePage.clickOnSearch_icon();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);	
		ActionsUtility.staticWait(3);
	}

}
