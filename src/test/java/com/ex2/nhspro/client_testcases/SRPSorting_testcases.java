package com.ex2.nhspro.client_testcases;

import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SRPSorting_testcases extends Initializations {

	String currentUrl="";
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openClientApplication();
	}
		
//	@Test(description="To verify sorting is working at client side.")
	public void verify_Price_Sorting_WorkingOn_PhotoView() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Skip this scenario on mobile");
			throw new SkipException("Skipping this exception");
		} else {
//			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
//			commonPage.clickOnSearchNewHomes_link();
//			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
//			homePage.clickOnSearch_icon();
			loginOrNavigate();
			commonPage.clickOnView("Photo");
			srpPage.clickOnPriceSorting();
			ActionsUtility.staticWait(5);
			List<String> priceList = srpPage.getPriceList();
			System.out.println(srpPage.getPriceList());
			for (int i = 0; i < priceList.size() - 1; i++) {
				// System.out.println(priceList.get(i)+"=="+priceList.get(i).compareTo(priceList.get(i+1)));
				if ((priceList.get(i).compareTo(priceList.get(i + 1))) <= 0) {
					Assert.assertTrue(true);
				} else {
					Assert.assertTrue(false);
				}
			}

		}

	}
	
	@Test(groups = {"regressionTest"})
	public void verifySortingFor_HomeTab_ListView_OnSRPPage() {
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("List");
		// srpPage.clickOnListIcon();
		List<String> allSortingdropdownValues = srpPage.getAlldropdownValues();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			Assert.assertEquals(allSortingdropdownValues.toString(), srpData.getJsonData("Sorting", "desktop_HomeTab"));
		else
			System.out.println("I am on mobile");
		for (String eachSortValue : allSortingdropdownValues) {
			if (!eachSortValue.equalsIgnoreCase("Featured")) {
				srpPage.selectSortByValue(eachSortValue);
				ActionsUtility.staticWait(5);
				// commonPage.focusOnFacebook_icon();
				Assert.assertTrue(srpPage.verifyColumnIsSelcted(eachSortValue));
				List<WebElement> allListElements = srpPage.verifyListIsSortedAccordingToSelectedValues(eachSortValue,
						"HomeTab");
				List<String> actualResult = allListElements.stream().map(s -> s.getText()).collect(Collectors.toList());
				 srpPage.sortingVerification(eachSortValue,actualResult);
			}
		}

	}

	@Test(groups = {"smokeTest"})
	public void verifySortingFor_CommunityTab_ListView_OnSRPPage() {
		loginOrNavigate();
		commonPage.clickOnView("List");
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(5);
		List<String> allSortingdropdownValues = srpPage.getAlldropdownValues();
		System.out.println(allSortingdropdownValues);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			 Assert.assertEquals(allSortingdropdownValues.toString(),
					srpData.getJsonData("Sorting", "desktop_CommunityTab"),"Asssertion has been failed");
		
		else {
			System.out.println("I am on mobile and Mobile doesn't have list View");
		}
		for (String eachSortValue : allSortingdropdownValues) {
			// In below line Price High-low is excluded from Verification because of a bug in functionality,
			// But after the bug gets fixed, Price High-Low again should be include for verification.
			if (!eachSortValue.matches("(?i)Featured|Confirm CoOp First|Price High - Low")) {
				srpPage.selectSortByValue(eachSortValue);
				ActionsUtility.staticWait(5);
				List<WebElement> allListElements = srpPage.verifyListIsSortedAccordingToSelectedValues(eachSortValue,
						"Community");
				List<String> actualResult = allListElements.stream().map(s -> s.getText()).collect(Collectors.toList());
				if (eachSortValue.startsWith("Price") || eachSortValue.startsWith("Sq.Ft")
						|| eachSortValue.startsWith("Beds") || eachSortValue.startsWith("Baths"))
					srpPage.sortingBasedOnRange(eachSortValue, actualResult);
				else
					srpPage.sortingVerification(eachSortValue, actualResult);
			}
		}
//List<String> collect = srpPage.getAllHomeColumnsInSRP().stream().map(s->s.getText()).collect(Collectors.toList());
	}
//================================================== Photo View ======================================================		



	@Test(groups = {"regressionTest"})
	public void verifySortingForPhotoView_SRP() {
		loginOrNavigate();
		ActionsUtility actionPerform = new ActionsUtility(driver, logger);
		commonPage.clickOnView("Photo");
		commonPage.clickOnHomes_tab();
		List<WebElement> allPhotoVIewSortingOptions = srpPage.getAllPhotoViewSortingOptions(); // all sorting values on
																								// photoview
		List<String> allPhotoVIewSortingOptions_data = allPhotoVIewSortingOptions.stream().map(s -> s.getText())
				.collect(Collectors.toList()); // text of sorting values
		Assert.assertEquals(allPhotoVIewSortingOptions_data.toString(),
				srpData.getJsonData("Sorting", "desktop_HomeTab_photoView")); // assertion with expected values
		for (WebElement sortingElement : allPhotoVIewSortingOptions) {
			String selectedValue = sortingElement.getText();
			if(!selectedValue.equalsIgnoreCase("Home")) {
			actionPerform.clickOnElement(sortingElement, " First Clicked on " + selectedValue);
			ActionsUtility.staticWait(5);
			srpPage.verifyBasedOnClicked(selectedValue, "Low to High");
			actionPerform.clickOnElement(sortingElement, " Second Clicked on " + selectedValue);
			ActionsUtility.staticWait(5);
			srpPage.verifyBasedOnClicked(selectedValue, "High to Low");
			}
		}

	}

	private void loginOrNavigate() {
		if (currentUrl.equals("")) {
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			homePage.clickOnSearch_icon();
			currentUrl = driver.getCurrentUrl();
		} else
			driver.navigate().to(currentUrl);
	}
	
	
	
}
