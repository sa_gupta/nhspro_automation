package com.ex2.nhspro.client_testcases;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.SkipException;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class HomeDetail_testcases extends Initializations {


	String CurrentUrl="";
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openClientApplication();
	}
	
	

	@Test(description = "Verify Client Perform PrintReport From Home Detail Page", groups = { "smokeTest",
			"regressionTest" })
	public void verify_client_perform_printreport_from_HomeDetailPage() {

		loginOrNavigate();
		commonPage.clickOnView("List");
		srpPage.clickOnHome(4);
		homeDetailPage.clickOnPrintReportButtonOnHomeDetail();
		String parentWinHandle = driver.getWindowHandle();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			homeDetailPage.clickOnOkBtn_PrintReport();
			ActionsUtility.staticWait(5);
			srpPage.switchTowindow();
			String currentlurl = driver.getCurrentUrl();
			System.out.println(currentlurl);
			Assert.assertTrue(currentlurl.contains("pdfgenerator"),
					"Client Print Report From Home Page Functionality Not Verified");
			System.out.println("Client Print Report From Home Page Functionality Verified");
			driver.close();
			driver.switchTo().window(parentWinHandle);
		} else {
			Assert.assertTrue(srpPage.isOK_btn_exist(), "Ok Button is Not Present");
			System.out.println("Client Print Report From SRP Page Functionality Verified");
			logger.log(Status.INFO, "Client Print Report From SRP Page Functionality Verified");
		}
	}

	@Test(description = "Verify Client Perform Saved Listing From Home Detail Page",groups = {"smokeTest","regressionTest"})
	public void verify_client_perform_savedlisting_from_HomeDetailPage() {
	//	commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
		loginOrNavigate();
		commonPage.clickOnSaveListing();
		saveListingPage.clearSaveListing();
//		if (saveListingPage.getCountOfHome() > 0) {
//			//saveListingPage.clickOnSelectAll_checkbox();
//			saveListingPage.clickOnDelete_btn();
//			saveListingPage.clickOnDeleteOnConfirmationPopp();
//		}
		saveListingPage.clickOnBack_link();
		commonPage.clickOnView("List");
		srpPage.clickOnHome(4);
		homeDetailPage.clickOn_SaveListing();
		srpPage.clickOnOk_saveListing();
		ActionsUtility.staticWait(2);
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		commonPage.clickOnSaveListing();
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		System.out.println(idOfHomesFromSaveListing.get(0));
		System.out.println(homeIdFromDetailsPage);
	//	Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), saveListingPage.getHomeIdFromSavedListingPage(idOfHomesFromSaveListing));

	}

	@Test(description = "Validate the Back to Search Result Functionality", groups = { "smokeTest" })
	public void validate_BackToSearchResult_FromHomeDetailPage_ClientApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Back to Search Result Link");
			throw new SkipException("Skipping this exception");

		} else {
			loginOrNavigate();
			String beforemovetohomepage = driver.getCurrentUrl();
			ActionsUtility.staticWait(3);
			srpPage.clickOnHomeFromSRPPhotoView(1);
			ActionsUtility.staticWait(2);
			System.out.println("Home Detail Page Url " + driver.getCurrentUrl());
			homeDetailPage.clickOnBackToSearchResultOnHomeDetail();
			String aftermovetohomepage = driver.getCurrentUrl();
			Assert.assertEquals(beforemovetohomepage, aftermovetohomepage);
			System.out.println("Back To Search Result Functionality Verified");
		}
	}
	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
			homePage.clickOnSearch_icon();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);			
	}
}
