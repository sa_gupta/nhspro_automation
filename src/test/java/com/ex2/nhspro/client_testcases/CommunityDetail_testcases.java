package com.ex2.nhspro.client_testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class CommunityDetail_testcases extends Initializations{
	
	String CurrentUrl="";
	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openClientApplication();
	}
	

	
	@Test(description = "Verify Client Perform PrintReport From Community Detail Page", groups = { "smokeTest",
			"regressionTest" })
	public void verify_client_perform_printreport_from_CommunityDeatilPage() {
		loginOrNavigate();
		commonPage.clickOnView("List");
		ActionsUtility.staticWait(2);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(5);
		srpPage.clickOnHome(2);
		communityDetailPage.clickOnPrintReportButtonOnCommunityDetail();
		String parentWinHandle = driver.getWindowHandle();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			ActionsUtility.staticWait(2);
			srpPage.clickOnOkPrintReportForClient();
			ActionsUtility.staticWait(5);
			srpPage.switchTowindow();
			String currentlurl = driver.getCurrentUrl();
			System.out.println(currentlurl);
			Assert.assertTrue(currentlurl.contains("pdfgenerator"),
					"Client Print Report From Community Detail Page Functionality Not Verified");
			System.out.println("Client Print Report From Home Page Functionality Verified");
			driver.close();
			driver.switchTo().window(parentWinHandle);
		} else {
			Assert.assertTrue(srpPage.isOK_btn_exist(), "Ok Button is Not Present");
			System.out.println("Client Print Report From Community Detail Page Functionality Verified");
			logger.log(Status.INFO, "Client Print Report From Community Detail Page Functionality Verified");
		}
	}
	
	@Test(description = "Verify Client Perform Saved Listing From Community Detail Page", groups = { "smokeTest",
			"regressionTest" })
	public void verify_client_perform_savedlisting_from_CommunityDetailPage() {
		loginOrNavigate();
		commonPage.clickOnSaveListing();
		saveListingPage.clickOnCommunityTab_saveListing();
		saveListingPage.clearSaveListing();
		saveListingPage.clickOnBack_link();
		commonPage.clickOnView("List");
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(2);
		String CommunityId = communityDetailPage.getHomeId();
		System.out.println("Community Id " + CommunityId);
		communityDetailPage.clickOn_SaveListing();
		communityDetailPage.clickOnOkButton();
		ActionsUtility.staticWait(2);
		communityDetailPage.clickOnConfirmOk();
		commonPage.clickOnSaveListing();
		saveListingPage.clickOnCommunityTab_saveListing();
		ActionsUtility.staticWait(3);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		System.out.println("Community Id in Saved Listing " + idOfHomesOnSaveListing.get(0));
		Assert.assertEquals(CommunityId, saveListingPage.getHomeIdFromSavedListingPage(idOfHomesOnSaveListing));
	//	Assert.assertEquals(CommunityId, idOfHomesOnSaveListing.get(0));

	}
	
	@Test(description = "Validate the Back to Search Result Functionality", groups = { "regressionTest" })
	public void validate_BackToSearchResult_FromCommunityDetailPage_ClientApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Back to Search Result Link");
			throw new SkipException("Skipping this exception");

		} else {
			loginOrNavigate();
			commonPage.clickOnCommunities_tab();
			String noOfCommunities=srpPage.getHomeCountFromSRP();
			srpPage.clickOnCommunityFromSRPPhotoView(1);
			ActionsUtility.staticWait(2);
			communityDetailPage.clickOnBackToSearchResultOnCommunityDetail();
			String noOfCommunitesAfterBack=srpPage.getHomeCountFromSRP();			
			Assert.assertEquals(noOfCommunitesAfterBack, noOfCommunities);
			System.out.println("Back To Search Result Functionality Verified From Community Detail Page");
		}
	}
	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			homePage.clickOnSearch_icon();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);			
	}
}
