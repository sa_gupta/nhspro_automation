package com.ex2.nhspro.agent_testcases;


import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SaveSearch_testcases extends Initializations{

	String SaveSearchesName,currentUrl="";
	

	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();
	}
	

/*
 * @Test(priority=1,alwaysRun = true) public void
 * verify_Agent_Send_a_Save_SearchSearch_To_New_Client_And_Perform_Like_dislike_Comment
 * () { loginOrNavigate(); srpPage.clickOnSaveSearch_btn(); SaveSearchesName =
 * ActionsUtility.nameWithDate();
 * srpPage.enterNameOfSearcNameThisSearch_fieldh(SaveSearchesName);
 * srpPage.selectNewClientAndEnterDetailInSavedSearch(commonData.getJsonData(
 * "clientData", "FirstName"), commonData.getJsonData("clientData",
 * "LastName"),commonData.getJsonData("clientData", "PhoneNo"),
 * commonData.getJsonData("clientData", "Email"));
 * srpPage.clickOnSaveandEmailofSaveSearch(); srpPage.clickOnOk_button();
 * commonPage.click_On_SavedSearches();
 * if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
 * saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing); }
 * savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
 * (5);  
 * savedSearchesPage.clickOnComment();
 * savedSearchesPage.EnterTextOnCommentBox("Message", "Entered Message");
 * savedSearchesPage.clickOnAdd(); (2); String
 * Message=commonOperation.getCommentFromCommentBox();
 * System.out.println(Message); savedSearchesPage.clickOnCloseOfCommentPopUp();
 * savedSearchesPage.clickOnLike(); savedSearchesPage.clickOnDislike();
 * driver.navigate().back(); String originalwindow=driver.getWindowHandle();
 * commonPage.openNewTab(); openClientApplication();
 * commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
 * "NewClient")); commonPage.click_On_SavedSearches(); boolean
 * status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
 * Assert.assertTrue(status,"Save Search not found");
 * savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
 * (5);  
 * savedSearchesPage.clickOnComment(); (1); String
 * ShowingnewMessage=commonOperation.getCommentFromCommentBox();
 * savedSearchesPage.clickOnCloseOfCommentPopUp();
 * Assert.assertEquals(ShowingnewMessage, Message); String
 * LikeImagePathAgent=srpData.getJsonData("SaveListing","LikeImage"); String
 * DisLikeImagePathAgent=srpData.getJsonData("SaveListing","DisLikeImage");
 * String likeImagepathClient=savedSearchesPage.getLikeImagePath(); String
 * DislikeImagepathClient=savedSearchesPage.getDisLikeImagePath();
 * Assert.assertTrue(likeImagepathClient.contains(LikeImagePathAgent),
 * "Like image url Does not match");
 * Assert.assertTrue(DislikeImagepathClient.contains(DisLikeImagePathAgent),
 * "DisLike image url Does not match"); driver.navigate().back();
 * deletePerticularSavedSearches(SaveSearchesName);
 * (1); driver.navigate().refresh();
 * (2); boolean
 * afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName
 * ); savedSearchesPage.verifySaveSearchDeletedOrNot(
 * afterdeleteclientsite,"Client Site"); commonPage.logout(); driver.close();
 * driver.switchTo().window(originalwindow); driver.navigate().refresh();
 * boolean
 * afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName)
 * ; savedSearchesPage.verifySaveSearchDeletedOrNot(
 * afterdeleteagentsite,"Agent Site"); commonPage.clickOnMyClientsLink();
 * (3);
 * myclient.deleteClient(commonData.getJsonData("clientData", "ClientName"));;
 * 
 * }
 */

@Test(groups = {"smokeTest"})
public void verify_Save_Search_loaded_properly_on_agentsite() {
	loginOrNavigate();
	 commonPage.click_On_SavedSearches();	 
	 
	 Assert.assertTrue(driver.getCurrentUrl().contains("savedsearches"), "Saved Search page unable to load properly");
		System.out.println("Saved Search page on agent site is loaded properly"); 
}
	
	@Test(groups = {"smokeTest"})
	public void verify_Save_Search_functionality_For_Me() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	//    srpPage.closeBatchProcessingPopUp();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup("Me");
	    savedSearchesPage.getAttributeValueOfPreview();
	    srpPage.clickOnSave_btn();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage("Me");
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCount = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCount);
	    savedSearchesPage.clickOnUpdateSearchButtonFromSaveSearch();
	    savedSearchesPage.getAttributeValueOfUpdateAndEmailButton();
	    savedSearchesPage.getAttributeValueOfPreview();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	commonOperation.clickOnClose_icon();
	    	driver.navigate().back();
	    }
	    driver.navigate().back();
	   deletePerticularSavedSearches(SaveSearchesName);
	}
	


	@Test(groups = {"regressionTest"})
	public void verify_Send_a_Particular_Save_Search_from_SaveSearchPage() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSave_btn();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    srpPage.closeSRPEnageClientPopUp();
	    savedSearchesPage.clickOnSendButtonInSavedSearchPage();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab();
		  openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,"EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().back();
	    deletePerticularSavedSearches(SaveSearchesName);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	    commonPage.switchtoWindow();
	    driver.navigate().refresh();
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	   
	}
	@Test(groups = {"regressionTest"})
	public void verify_Update_and_Email_of_Save_Search_from_SaveSearchPage() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSave_btn();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    savedSearchesPage.clickOnUpdateSearchButtonFromSaveSearch();
	    savedSearchesPage.clickOnUpdateAndEmailButtonFromSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    deletePerticularSavedSearches(SaveSearchesName);
	    driver.navigate().refresh();
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_send_a_SaveSearch_From_SRP_Page_NoCase() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    deletePerticularSavedSearches(SaveSearchesName);
	    driver.navigate().refresh();
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_send_a_SaveSearch_From_SRP_Page_YesCase() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    srpPage.clickOnYes_btn();
	    savedSearchesPage.clickOnSendButtonInTemplateOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		 "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    deletePerticularSavedSearches(SaveSearchesName);
	    driver.navigate().refresh();
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	@Test(groups = {"smokeTest"})
	public void verify_Only_Save_a_Search_For_A_Client() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	//    srpPage.closeBatchProcessingPopUp();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSave_btn();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().back();
	    deletePerticularSavedSearches(SaveSearchesName);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_send_a_SaveSearch_From_SRP_Page_From_Preview_of_SaveSearch() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOn_SaveSearch_Preview_link();
	   
	    savedSearchesPage.clickOnSendButtonInTemplateOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    deletePerticularSavedSearches(SaveSearchesName);
	   
	    driver.navigate().refresh();
	
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_rename_a_SaveSearch_From_Saved_Search_Result_Page() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOnRenameIcon(SaveSearchesName);
	    String rename=ActionsUtility.nameWithDate();
	    savedSearchesPage.renameSavedSearch(rename);
	    savedSearchesPage.clickOn_Perticular_SearchName(rename);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(rename);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(rename);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    savedSearchesPage.select_Perticular_checkbox(rename);
	    savedSearchesPage.clickOn_Delete_btn();
	    savedSearchesPage.click_On_Delete_btn_from_Delete_popup();
	
	    driver.navigate().refresh();
	
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(rename);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(rename);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_update_a_SaveSearch_From_Particular_Saved_Search_Page() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    String rename=ActionsUtility.nameWithDate();
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	     
	    savedSearchesPage.clickOnUpdateSearchButtonFromSaveSearch();
	    savedSearchesPage.onlyUpdateSavedSeachFromSavedSeachPage(rename); 
	    srpPage.clickOnOk_button();
	  
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    String SearchNameFromDropDown=savedSearchesPage.getNameofFirstItemIntheDropDownOfSavedSearch();
	    String SearchNameFromBreadcrum=savedSearchesPage.getSearchNameFromBreadCrum();
	    String SearchNameFromUpdateSearchPopUp=savedSearchesPage.getSaveSearchNameFromUpdateSearchPopUp();
	    Assert.assertEquals(SearchNameFromDropDown, rename);
	    Assert.assertEquals(SearchNameFromBreadcrum, rename);
	    Assert.assertEquals(SearchNameFromUpdateSearchPopUp, rename);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(rename);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(rename);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    savedSearchesPage.select_Perticular_checkbox(rename);
	    savedSearchesPage.clickOn_Delete_btn();
	    savedSearchesPage.click_On_Delete_btn_from_Delete_popup();
	    driver.navigate().refresh();
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(rename);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(rename);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_Edit_a_SaveSearch_From_Particular_Saved_Search_Page() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    String rename=ActionsUtility.nameWithDate();
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    
	    savedSearchesPage.clickOnEditSearchIcon();
	    savedSearchesPage.renameSavedSearch(rename);
	
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    String SearchNameFromDropDown=savedSearchesPage.getNameofFirstItemIntheDropDownOfSavedSearch();
	    String SearchNameFromBreadcrum=savedSearchesPage.getSearchNameFromBreadCrum();
	    String SearchNameFromUpdateSearchPopUp=savedSearchesPage.getSaveSearchNameFromUpdateSearchPopUp();
	    Assert.assertEquals(SearchNameFromDropDown, rename);
	    Assert.assertEquals(SearchNameFromBreadcrum, rename);
	    Assert.assertEquals(SearchNameFromUpdateSearchPopUp, rename);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(rename);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(rename);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    savedSearchesPage.select_Perticular_checkbox(rename);
	    savedSearchesPage.clickOn_Delete_btn();
	    savedSearchesPage.click_On_Delete_btn_from_Delete_popup();

	    driver.navigate().refresh();

	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(rename);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(rename);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_select_different_search_and_update_search_From_the_dropDown() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    srpPage.clickOnSaveSearch_btn();
	    String SaveSearchesName2 = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName2);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    String rename=ActionsUtility.nameWithDate();
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	  
	    savedSearchesPage.clickOnDropDownAndSelectSearch();
	    savedSearchesPage.clickOnUpdateSearchButtonFromSaveSearch();
	    savedSearchesPage.onlyUpdateSavedSeachFromSavedSeachPage(rename); 
	    srpPage.clickOnOk_button();
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    String SearchNameFromDropDown=savedSearchesPage.getNameofFirstItemIntheDropDownOfSavedSearch();
	    String SearchNameFromBreadcrum=savedSearchesPage.getSearchNameFromBreadCrum();
	    String SearchNameFromUpdateSearchPopUp=savedSearchesPage.getSaveSearchNameFromUpdateSearchPopUp();
	    Assert.assertEquals(SearchNameFromDropDown, rename);
	    Assert.assertEquals(SearchNameFromBreadcrum, rename);
	    Assert.assertEquals(SearchNameFromUpdateSearchPopUp, rename);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(rename);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(rename);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    savedSearchesPage.select_Perticular_checkbox(rename);
	    savedSearchesPage.select_Perticular_checkbox(SaveSearchesName);
	    savedSearchesPage.clickOn_Delete_btn();
	    savedSearchesPage.click_On_Delete_btn_from_Delete_popup();
	    driver.navigate().refresh();
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(rename);
	    boolean afterdeleteclientsite2=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite2,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(rename);
	    boolean afterdeleteagentsite2=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite2,"Agent Site");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_select_different_search_and_Edit_search_From_the_dropDown() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    srpPage.clickOnSaveSearch_btn();
	    String SaveSearchesName2 = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName2);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    String rename=ActionsUtility.nameWithDate();
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    srpPage.closeSRPEnageClientPopUp();
	    savedSearchesPage.clickOnDropDownAndSelectSearch();
	    savedSearchesPage.clickOnEditSearchIcon();
	    savedSearchesPage.renameSavedSearch(rename);

	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    String SearchNameFromDropDown=savedSearchesPage.getNameofFirstItemIntheDropDownOfSavedSearch();
	    String SearchNameFromBreadcrum=savedSearchesPage.getSearchNameFromBreadCrum();
	    String SearchNameFromUpdateSearchPopUp=savedSearchesPage.getSaveSearchNameFromUpdateSearchPopUp();
	    Assert.assertEquals(SearchNameFromDropDown, rename);
	    Assert.assertEquals(SearchNameFromBreadcrum, rename);
	    Assert.assertEquals(SearchNameFromUpdateSearchPopUp, rename);
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		  commonPage.openNewTab(); 
		  openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(rename);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(rename);
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    savedSearchesPage.select_Perticular_checkbox(rename);
	    savedSearchesPage.select_Perticular_checkbox(SaveSearchesName);
	    savedSearchesPage.clickOn_Delete_btn();
	    savedSearchesPage.click_On_Delete_btn_from_Delete_popup();

	    driver.navigate().refresh();
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(rename);
	    boolean afterdeleteclientsite2=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite2,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(rename);
	    boolean afterdeleteagentsite2=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite2,"Agent Site");
	}
	

	@Test(groups = {"regressionTest"})
	public void verify_Agent_Send_a_Save_SearchSearch_To_New_Client_And_Perform_Like_dislike_Comment() {
		loginOrNavigate();
		commonPage.clickOnMyClientsLink();
	    myclient.deleteClientIfExist(commonData.getJsonData("clientData", "Email"));
	    driver.navigate().back();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectNewClientAndEnterDetailInSavedSearch(commonData.getJsonData("clientData", "FirstName"), commonData.getJsonData("clientData", "LastName"),commonData.getJsonData("clientData", "PhoneNo"), commonData.getJsonData("clientData", "Email"));
	    srpPage.clickOnSaveandEmailofSaveSearch();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    savedSearchesPage.clickOnComment();
	    savedSearchesPage.EnterTextOnCommentBox("Message");
	   savedSearchesPage.clickOnAdd();
	   String Message=commonOperation.getCommentFromCommentBox();
	   savedSearchesPage.clickOnCloseOfCommentPopUp();
	   savedSearchesPage.clickOnLike();
	   savedSearchesPage.clickOnDislike();
	    driver.navigate().back();
	    String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab(); 
		 openClientApplication();
		commonPage.loginIfNotInShowingNew(commonData.getJsonData("clientData", "Email"));
		commonPage.click_On_SavedSearches(); 
		boolean status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
		 Assert.assertTrue(status,"Save Search not found");
		  savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName); 
		  savedSearchesPage.clickOnComment();
		  String ShowingnewMessage=commonOperation.getCommentFromCommentBox();
		  savedSearchesPage.clickOnCloseOfCommentPopUp();
		  Assert.assertEquals(ShowingnewMessage, Message);
		  String LikeImagePathAgent=srpData.getJsonData("SaveListing","LikeImage");
		  String DisLikeImagePathAgent=srpData.getJsonData("SaveListing","DisLikeImage");
		  String likeImagepathClient=savedSearchesPage.getLikeImagePath();
		  String DislikeImagepathClient=savedSearchesPage.getDisLikeImagePath();
		 Assert.assertTrue(likeImagepathClient.contains(LikeImagePathAgent), "Like image url Does not match");
		 Assert.assertTrue(DislikeImagepathClient.contains(DisLikeImagePathAgent), "DisLike image url Does not match");
		 driver.navigate().back(); 
		 deletePerticularSavedSearches(SaveSearchesName); 
		  driver.navigate().refresh();
		 boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName); 
		 savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site"); 
		 commonPage.logout();
		   driver.close();
		  driver.switchTo().window(originalwindow); 
		  driver.navigate().refresh();
		 boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName); savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
		 commonPage.clickOnMyClientsLink();
		 myclient.deleteClient(commonData.getJsonData("clientData", "ClientName"));;
		 
	} 

	
	@Test(groups = {"regressionTest"})
	public void verify_Send_a_Save_Search_from_SaveSearchPage_ResultPage() {
		loginOrNavigate();
	    String srpHomeCount = srpPage.getHomeCountFromSRP();
	    srpPage.clickOnSaveSearch_btn();
	    SaveSearchesName = ActionsUtility.nameWithDate();
	    srpPage.enterNameOfSearcNameThisSearch_field(SaveSearchesName);
	    srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	    srpPage.clickOnSave_btn();
	    srpPage.clickOnOk_button();
	    commonPage.click_On_SavedSearches();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing);
	    }
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	    String savedSearchesHomeCountAgentsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(srpHomeCount, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    savedSearchesPage.select_Perticular_checkbox(SaveSearchesName);
	    savedSearchesPage.clickOnSendButtonOfSavedSearchResultPage();
	    savedSearchesPage.clickOnNoButtonOfEditSearchPopUp();
	    srpPage.clickOnOk_button();
	    String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	    commonPage.click_On_SavedSearches();
	    boolean status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    Assert.assertTrue(status,"Save Search not found");
	    savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	   
	    String savedSearchesHomeCountClientsite = savedSearchesPage.getHomeCountFromSavedSearches();
	    Assert.assertEquals(savedSearchesHomeCountClientsite, savedSearchesHomeCountAgentsite);
	    driver.navigate().back();
	    deletePerticularSavedSearches(SaveSearchesName);

	    driver.navigate().refresh();
	
	    boolean afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteclientsite,"Client Site");
	    driver.close();
	    driver.switchTo().window(originalwindow);
	    driver.navigate().refresh();
	    boolean afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	    savedSearchesPage.verifySaveSearchDeletedOrNot(afterdeleteagentsite,"Agent Site");
	}
	
	
	/*  @Test public void
	  verify_Agent_Send_a_Save_Multiple_SearchSearch_To_New_Client_And_Perform_Like_dislike_Comment
	  () { loginOrNavigate(); srpPage.clickOnSaveSearch_btn(); SaveSearchesName =
	 ActionsUtility.nameWithDate();
	 * srpPage.enterNameOfSearcNameThisSearch_fieldh(SaveSearchesName);
	 * srpPage.selectNewClientAndEnterDetailInSavedSearch(commonData.getJsonData(
	 * "clientData", "FirstName"), commonData.getJsonData("clientData",
	 * "LastName"),commonData.getJsonData("clientData", "PhoneNo"),
	 * commonData.getJsonData("clientData", "Email"));
	 * srpPage.clickOnSaveandEmailofSaveSearch(); srpPage.clickOnOk_button();
	 * srpPage.clickOnSaveSearch_btn(); String Multi =
	 * ActionsUtility.nameWithDate();
	 * srpPage.enterNameOfSearcNameThisSearch_fieldh(Multi);
	 * srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
	 * srpPage.selectClientFromClientPopup(commonData.getJsonData("clientData",
	 * "ClientName")); srpPage.clickOnSaveandEmailofSaveSearch();
	 * srpPage.clickOnYes_btn(); (1);
	 * savedSearchesPage.clickOnSendButtonInTemplateOfEditSearchPopUp();
	 * srpPage.clickOnOk_button(); commonPage.click_On_SavedSearches();
	 * if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	 * saveListingPage.clickOnClientOnSaveSearchPage(ClientNameforSaveListing); }
	 * savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	 * (5);  
	 * savedSearchesPage.clickOnComment();
	 * savedSearchesPage.EnterTextOnCommentBox(myclientData.getJsonData(environment,
	 * "Message"), "Entered Message"); savedSearchesPage.clickOnAdd();
	 * (2); String
	 * Message=commonOperation.getCommentFromCommentBox();
	 * System.out.println(Message); savedSearchesPage.clickOnCloseOfCommentPopUp();
	 * savedSearchesPage.clickOnLike(); savedSearchesPage.clickOnDislike();
	 * driver.navigate().back(); String originalwindow=driver.getWindowHandle();
	 * commonPage.openNewTab(); openClientApplication();
	 * commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
	 * "NewClient")); commonPage.click_On_SavedSearches(); boolean
	 * status=savedSearchesPage.verifySaveSearchName(SaveSearchesName);
	 * Assert.assertTrue(status,"Save Search not found");
	 * savedSearchesPage.clickOn_Perticular_SearchName(SaveSearchesName);
	 * (5);  
	 * savedSearchesPage.clickOnComment(); (1); String
	 * ShowingnewMessage=commonOperation.getCommentFromCommentBox();
	 * savedSearchesPage.clickOnCloseOfCommentPopUp();
	 * Assert.assertEquals(ShowingnewMessage, Message); String
	 * LikeImagePathAgent=srpData.getJsonData("SaveListing","LikeImage"); String
	 * DisLikeImagePathAgent=srpData.getJsonData("SaveListing","DisLikeImage");
	 * String likeImagepathClient=savedSearchesPage.getLikeImagePath(); String
	 * DislikeImagepathClient=savedSearchesPage.getDisLikeImagePath();
	 * Assert.assertTrue(likeImagepathClient.contains(LikeImagePathAgent),
	 * "Like image url Does not match");
	 * Assert.assertTrue(DislikeImagepathClient.contains(DisLikeImagePathAgent),
	 * "DisLike image url Does not match"); driver.navigate().back();
	 * deletePerticularSavedSearches(SaveSearchesName);
	 * (1); driver.navigate().refresh();
	 * (2); boolean
	 * afterdeleteclientsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName
	 * ); savedSearchesPage.verifySaveSearchDeletedOrNot(
	 * afterdeleteclientsite,"Client Site"); driver.close();
	 * driver.switchTo().window(originalwindow); driver.navigate().refresh();
	 * boolean
	 * afterdeleteagentsite=savedSearchesPage.verifySaveSearchName(SaveSearchesName)
	 * ; savedSearchesPage.verifySaveSearchDeletedOrNot(
	 * afterdeleteagentsite,"Agent Site"); commonPage.clickOnMyClientsLink();
	 * (3);
	 * myclient.deleteClient(commonData.getJsonData("clientData", "ClientName"));;
	 * 
	 * }
	 */
	
	private void deletePerticularSavedSearches(String saveSearchesName2) {
		 savedSearchesPage.select_Perticular_checkbox(SaveSearchesName);
		    savedSearchesPage.clickOn_Delete_btn();
		    savedSearchesPage.click_On_Delete_btn_from_Delete_popup();
		
	}
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
			homePage.clickOnSearch_icon();
			currentUrl=driver.getCurrentUrl();
			srpPage.closeSRPEnageClientPopUp();
		}
		else
			driver.navigate().to(currentUrl);	
	}
}
