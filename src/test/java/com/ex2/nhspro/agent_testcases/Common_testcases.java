package com.ex2.nhspro.agent_testcases;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ex2.nhspro.actions.CommonPage_actions;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;


public class Common_testcases extends Initializations{
	
	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();
	}
	
	@Test(groups = {"smokeTest"})
	public void verify_Login_Into_Application() {
		commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
		boolean LoginStatus=commonPage.isUserLoggedIn();
		Assert.assertTrue(LoginStatus,"User not logged In");
	}
	
	@Test(enabled=false,groups = {"smokeTest"})
	public void verify_Registration_Into_Agent_Application() {
		commonPage.clickOnSingIn_link();
		int randomInt = new Random().nextInt(1000);
		String emailId = "nhspro_testagent" + randomInt + "@newhomesource.com";
		commonPage.enterEmailAddress(emailId);
		commonPage.clickOnSubmit_btn();
		commonPage.enterFirstName_in_FirstName_field("Test");
		commonPage.enterLastName_in_LastName_field("Agent");
		commonPage.enterZipCode_in_ZipCode_field("11111");
		commonPage.enterLicense_in_License_field("111111");
		commonPage.enterPassowrd_in_Password_field("111111");
		commonPage.enterConfirmPassword_in_ConfirmPassword_field("111111");
		commonPage.clickOnCreateAccount_button();
		boolean LoginStatus=commonPage.isUserLoggedIn();
		Assert.assertTrue(LoginStatus,"User not logged In");
	    commonPage.logoutIflogin();
	    ActionsUtility.staticWait(2);
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_NavigationOnStaticPages() {
		commonPage.clickOnAboutUs_link();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles", "About_US"));
		commonPage.clickOnContactUs_link();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles", "Contact_US"));
		commonPage.clickOnHelp_link();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles", "Help"));
		commonPage.clickOnAccessibilityStatement();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles", "Accessibility_Statement"));
		commonPage.clickOnPrivacy_link();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles", "Privacy"));
		commonPage.clickOnCookie_link();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles","Cookies"));
		commonPage.clickOnTerms_link();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles","Terms"));
		commonPage.clickOnDoNotSellInfo_link();
		Assert.assertEquals(driver.getTitle(), title.getJsonData("Titles","Do_not_sell"));
	}
	

}
