package com.ex2.nhspro.agent_testcases;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ex2.nhspro.actions.CommonOperations_actions;
import com.ex2.nhspro.utility.Initializations;

public class SpeedCurveTestCases extends Initializations{
	

	String currentUrl="";
	
	CommonOperations_actions commonOperation;

	
	@BeforeClass
	public void beforeStarting() {
		openAgentApplication();
	}
	
@AfterClass
public void afterExecution() {
	driver.quit();
}	
	
	@Test
	public void verify_CommunityDetailPage() {
		commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
		boolean LoginStatus=commonPage.isUserLoggedIn();
		Assert.assertTrue(LoginStatus,"User not logged In");
		String communitydeatilUrl=driver.getCurrentUrl()+"/community/texas/austin/east-village/121965?token=LetMeIn";
		driver.navigate().to(communitydeatilUrl);
		homePage.weAreSorry();;
		
	}
	
		@Test
		public void verify_CommunityResults() {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
			boolean LoginStatus=commonPage.isUserLoggedIn();
			Assert.assertTrue(LoginStatus,"User not logged In");
			String communitydeatilUrl=driver.getCurrentUrl()+"/communities/texas/austin?token=LetMeIn";
			driver.navigate().to(communitydeatilUrl);
			homePage.weAreSorry();
		}
		
		@Test
		public void verify_HomeResults() {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
			boolean LoginStatus=commonPage.isUserLoggedIn();
			Assert.assertTrue(LoginStatus,"User not logged In");
			String HomeresultUrl=driver.getCurrentUrl()+"/homes/texas/austin?token=LetMeIn";
			driver.navigate().to(HomeresultUrl);
			homePage.weAreSorry();
			
		}
		
		@Test
		public void verify_PlanDetail() {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
			boolean LoginStatus=commonPage.isUserLoggedIn();
			Assert.assertTrue(LoginStatus,"User not logged In");
			String HomedetailUrl=driver.getCurrentUrl()+"home/texas/austin/princeton-ii/p1778338?token=LetMeIn";
			driver.navigate().to(HomedetailUrl);
			homePage.weAreSorry();
			
		}
		
		@Test
		public void verify_SpecDetail() {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
			boolean LoginStatus=commonPage.isUserLoggedIn();
			Assert.assertTrue(LoginStatus,"User not logged In");
			String HomedetailUrl=driver.getCurrentUrl()+"home/texas/austin/plan-bentley/624-pecan-bottom-trail/s1930316?token=LetMeIn";
			driver.navigate().to(HomedetailUrl);
			homePage.weAreSorry();
			
		}
		
}
