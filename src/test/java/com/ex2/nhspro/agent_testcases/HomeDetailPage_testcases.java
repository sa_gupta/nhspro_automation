package com.ex2.nhspro.agent_testcases;

import java.util.List;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;


public class HomeDetailPage_testcases extends Initializations {
	
	String currentUrl="";
	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();	
	}
	
	@Test(groups= {"HappyPath"},description = "Need to change again for jumplinks")
	public void validateHomeDetailsPage() {
		loginOrNavigate();	
		srpPage.clickOnHome(1);	
		homeDetailPage.validateJumpLinks();
		
	}
	
	public void validate_HomePage_DetailsPage_HeaderData() {
		
	}
	
	
	@Test(description = "Validate the Client Version Report From Home Detail Page", groups = { "smokeTest" })
	public void validate_ClientVersionPrintPdf_FromHomeDetailPage_On_AgentApplication() {
		loginOrNavigate();	
		srpPage.clickOnHome(1);
//		ActionsUtility.staticWait(4);
		homeDetailPage.clickOnPrintReportButtonOnHomeDetail();
		String parentWinHandle = driver.getWindowHandle();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			srpPage.clickOnClientVersionPrintReport();
			ActionsUtility.staticWait(5);
			srpPage.switchTowindow();
			String currentlurl = driver.getCurrentUrl();
			System.out.println(currentlurl);
			Assert.assertTrue(currentlurl.contains("pdfgenerator"), "Unable to generate pdf Report");
			System.out.println("Client Version PrintPdf Verified");
		} else {
			Assert.assertTrue(srpPage.isClientVersion_btn_exist(), "Client version not exist");
			System.out.println("Client Version button should exist");
			logger.log(Status.INFO, "Client version button should exist");
			srpPage.clickOnClientVersionPrintReport();
		}
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
		}
	}

	@Test(description = "Validate the Agent Version Report From Home Detail Page", groups = { "smokeTest","regressionTest" })
	public void validate_AgentVersionPrintPdf_FromHomeDetailPage_On_AgentApplication() {
		loginOrNavigate();
		srpPage.clickOnHome(1);
		homeDetailPage.clickOnPrintReportButtonOnHomeDetail();
		ActionsUtility.staticWait(2);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
		String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnAgentVersionPrintReport();
		srpPage.switchTowindow();
		String currentlurl = driver.getCurrentUrl();
		System.out.println(currentlurl);
		Assert.assertTrue(currentlurl.contains("pdfgenerator"), "Unable to generate pdf Report");
		System.out.println("Agent Version PrintPdf Verified");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
		}
		else {
			Assert.assertTrue(srpPage.isAgentVersion_btn_exist(), "Agent version not exist");
			System.out.println("Agent Version button should exist");
			logger.log(Status.INFO, "Agent version button should exist");
			srpPage.clickOnAgentVersionPrintReport();
		}
		
	}

	@Test(description = "Validate the Back to Search Result Functionality", groups = { "smokeTest" })
	public void validate_BackToSearchResult_FromHomeDetailPage_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Back to Search Result Link");
			throw new SkipException("Skipping this exception");

		} else {
			loginOrNavigate();
			String beforemovetohomepage = driver.getCurrentUrl();
		//	srpPage.closeBatchProcessingPopUp();
			srpPage.clickOnHome(2);
			ActionsUtility.staticWait(2);
			System.out.println("Home Detail Page Url " + driver.getCurrentUrl());
			homeDetailPage.clickOnBackToSearchResultOnHomeDetail();
			String aftermovetohomepage = driver.getCurrentUrl();
			Assert.assertEquals(beforemovetohomepage, aftermovetohomepage);
			System.out.println("Back To Search Result Functionality Verified");
		}
	}
	
	/**
	 * @author sagupta
	 * @category Sprint 06
	 */
	@Test(groups = { "smokeTest" })
	public void validate_saveListingFromHomeDetailsPage() {
		loginOrNavigate();
		String ClientNameforSaveListing = srpData.getJsonData("SaveListing", "ClientName");
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		saveListingPage.clearSaveListing();
		saveListingPage.clickOnBack_link();
		commonOperation.HomeTabtabSelection(currentUrl);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		System.out.println(homeIdFromDetailsPage);
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), saveListingPage.getHomeIdFromSavedListingPage(idOfHomesFromSaveListing));
	//	Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
	}

	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
			homePage.clickOnSearch_icon();
			currentUrl=driver.getCurrentUrl();
			srpPage.closeSignMeUpPopUp();
		}
		else
			driver.navigate().to(currentUrl);	
		commonPage.clickOnHomes_tab();
	}
}
