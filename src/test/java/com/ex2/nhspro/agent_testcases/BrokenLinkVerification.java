package com.ex2.nhspro.agent_testcases;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

import com.ex2.nhspro.actions.CommonPage_actions;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;


public class BrokenLinkVerification extends Initializations{

	
	@Test(priority=1)
	public void validateUrlsOfHomepage_OfAgentSide() {
		openAgentApplication();
		brokenLink.validateInvalidLink();	
	}

	@Test(priority=2)
	public void validateUrlofSRPPage_AgentSide() {
	     openAgentApplication();
		commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
		homePage.enterSearchData_field("Macon, GA Area");
		homePage.clickOnSearch_icon();
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			commonPage.focusOnFacebook_icon();		
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();	
	}
	
	@Test(priority=3)
	public void validateUrlofDetailsPage_AgentSide() {
		openAgentApplication();
		commonPage.clickOnLogo();
		homePage.enterSearchData_field("Macon, GA Area");
		homePage.clickOnSearch_icon();
		srpPage.clickOnHome(1);
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			commonPage.focusOnFacebook_icon();		
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();			
	}
	@Test(priority=4)
	public void validateUrlOnMyAccountPage_AgentSide() {
		openAgentApplication();
		commonPage.clickOnMyAccount();
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			commonPage.focusOnFacebook_icon();		
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();			
	}
	@Test(priority=5)
	public void validateUrlOnManage_MyActivityPage_On_AgentSide() {
		openAgentApplication();
		commonPage.clickOnManagMyClientActivity();
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			commonPage.focusOnFacebook_icon();		
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();	
	}
	
	@Test(priority=6)
	public void validateUrlOnMyConsumerPortalPage_AgentSide() {
		openAgentApplication();
		commonPage.clickOnMyConsumerPortal();
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			commonPage.focusOnFacebook_icon();		
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();			
	}
	@Test(priority=7)
	public void validateUrlOnWidgetPage_AgentSide() {
		openAgentApplication();
		commonPage.clickOnWidget();
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			commonPage.focusOnFacebook_icon();		
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();			
	}
	
	@Test(priority=8)
	public void validateUrlOnFooterPage_AgentSide() {
		openAgentApplication();
		commonPage.clickOnAboutUs_link();
		brokenLink.validateInvalidLink();
		commonPage.clickOnCookie_link();
		brokenLink.validateInvalidLink();
		commonPage.clickOnHelp_link();
		brokenLink.validateInvalidLink();
		commonPage.clickOnPrivacy_link();
		brokenLink.validateInvalidLink();	
		commonPage.clickOnTerms_link();
		brokenLink.validateInvalidLink();
		driver.switchTo().defaultContent();
	}
	
	@Test(priority=9)
	public void validateUrlOnWebinarPage_On_AgentSide() {
		openAgentApplication();
		commonPage.clickOnWebinarLink();
		brokenLink.validateInvalidLink();
	}
	
	@Test(priority=10)
	public void validateUrlOnBuildersPage_On_AgentSide() {
		openAgentApplication();
		commonPage.clickOnTrustBuilder_Link();
		brokenLink.validateInvalidLink();
	}
	
	@Test(priority=11)
	public void validateUrlOnResourcesPage_AgentSide() {
		openAgentApplication();
	//	commonPage.clickOnTrainingVideo();
	//	brokenLink.validateInvalidLink();
	//	driver.navigate().back();
		commonPage.clickOnHowToSell();
		brokenLink.validateInvalidLink();
		driver.navigate().back();
	}
	@Test(priority=12)
	public void newsletter()
	{
		openAgentApplication();
		driver.get(getAgentBaseUrl()+"/newsletteremail/marketid-269");
		brokenLink.validateInvalidLink();
		driver.navigate().back();
	}
	
	@Test(priority=13)
	public void validateUrlsOfHomepage_OfClientSide() {
		openClientApplication();
		commonPage.clickOnSingIn_link();
		commonPage.enterEmailAddress(commonData.getJsonData(environment, "EmailId_client"));
		commonPage.clickOnSubmit_btn();
		brokenLink.validateInvalidLink();	
	}
	
	@Test(priority=14)
	public void validateUrlofSRPPage_ClientSide() {
		homePage.enterSearchData_field("Macon, GA Area");
		homePage.clickOnSearch_icon();
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();	
	}
	
	@Test(priority=15)
	public void validateUrlofDetailsPage_ClientSide() {
		commonPage.clickOnView("List");
		srpPage.clickOnHome(1);
		ActionsUtility.staticWait(5);
		brokenLink.validateInvalidLink();			
	}
	
	@Test(priority=16)
	public void validateUrlofMyAccountPage_ClientSide() {
		commonPage.clickOnMyAccount();
		brokenLink.validateInvalidLink();			
	}
}
