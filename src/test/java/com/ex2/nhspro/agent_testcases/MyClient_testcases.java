package com.ex2.nhspro.agent_testcases;

import java.io.File;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class MyClient_testcases extends Initializations {


	String currentUrl = "";

	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();
	}

	public void validateElementsOnMyClientPage() {
		loginOrNavigate();
		
	}
	
	@Test(description = "Add New Client From Agent Side", groups = { "smokeTest", "regressionTest", "AgentFlow" })
	public void validate_AddNewClient_On_AgentApplication() {
		loginOrNavigate();
		myclient.clickOnNewClientButton();
		String emailId=myclient.EnterValuesInAddNewClientForm(myclientData.getJsonData(environment, "FirstName"),
				myclientData.getJsonData(environment, "LastName"), myclientData.getJsonData(environment, "Phone"),
				myclientData.getJsonData(environment, "Message"));
		System.out.println(emailId);
		String actual = myclient.AddClientSuccessMessage();
		System.out.println(actual);
		Assert.assertEquals("Successfully added the new client!", actual, "Add New Client functionality Failed");
		System.out.println("New Client Added Sucessfully!");
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
			myclient.clickOnAddNewClientSucessOkButton();
			myclient.clickOnClientonMobile(myclientData.getJsonData(environment, "ClientName"));
		}
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		{
		myclient.clickOnAddNewClientSucessOkButton();
		}
		Assert.assertTrue(myclient.isClientExist(emailId));
		myclient.deleteClient(myclientData.getJsonData(environment, "ClientName"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		{
		Assert.assertFalse(myclient.isClientExist(emailId));
		}
		
	}

	@Test(description = "Verify buttons available on my client page on Agent Side", groups = { "regressionTest",
			"AgentFlow" })
	public void verify_Buttons_on_MyClient_page_on_AgentApplication() {
		loginOrNavigate();
		myclient.verifyButtonsAvailableOnMyClientPage();
	}

	@Test(description = "Edit Client Detail From Agent Side", groups = { "regressionTest", "AgentFlow" })
	public void validate_EditClientDetail_On_AgentApplication() {
		loginOrNavigate();
		myclient.clickOnNewClientButton();
		String email = myclient.EnterValuesInAddNewClientForm(myclientData.getJsonData(environment, "FirstName"),
				myclientData.getJsonData(environment, "LastName"), myclientData.getJsonData(environment, "Phone"),
				myclientData.getJsonData(environment, "Message"));
		String actual = myclient.AddClientSuccessMessage();
		Assert.assertTrue(myclient.isClientExist(email));
		Assert.assertEquals("Successfully added the new client!", actual, "Add New Client functionality Failed");
		System.out.println("New Client Added Sucessfully!");
		myclient.clickOnAddNewClientSucessOkButton();
		myclient.clickOnEditIcon(myclientData.getJsonData(environment, "ClientName"));
		myclient.EditClientInformation(email, myclientData.getJsonData(environment, "FirstName"),
				myclientData.getJsonData(environment, "LastName"), myclientData.getJsonData(environment, "Phone"),
				myclientData.getJsonData(environment, "Message"));
		String message = myclient.AddClientSuccessMessage();
		System.out.println(message);
		Assert.assertTrue(message.contains("Successfully edited the client!"), "Edit client functionality failed");
		myclient.clickOnAddNewClientSucessOkButton();
		System.out.println("Edit Client Functionality Verified");
		myclient.backMyClientPageInMobile();
		myclient.deleteClient(myclientData.getJsonData(environment, "ClientName"));
		Assert.assertFalse(myclient.isClientExist(email));

	}

	@Test(description = "Export Client From Agent Side", groups = { "regressionTest", "AgentFlow" })
	public void validate_ExportClient_On_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Export Client Feature");
			throw new SkipException("Skipping this exception");
		} else {
			loginOrNavigate();
			String downloadPath = System.getProperty("user.home") + "\\Downloads";
			deleteFileIfExist(downloadPath);
			myclient.clickOnNewClientButton();
			String emailId=myclient.EnterValuesInAddNewClientForm(myclientData.getJsonData(environment, "FirstName"),
					myclientData.getJsonData(environment, "LastName"), myclientData.getJsonData(environment, "Phone"),
					myclientData.getJsonData(environment, "Message"));
			String actual = myclient.AddClientSuccessMessage();
			Assert.assertTrue(myclient.isClientExist(emailId));
			Assert.assertEquals("Successfully added the new client!", actual, "Add New Client functionality Failed");
			System.out.println("New Client Added Sucessfully!");
			myclient.clickOnAddNewClientSucessOkButton();
			myclient.exportClients();
			myclient.deleteClient(myclientData.getJsonData(environment, "ClientName"));
			Assert.assertTrue(!myclient.isClientExist(emailId));
			File getLatestFile = myclient.getLatestFilefromDir(downloadPath);
			String fileName = getLatestFile.getName();
			Assert.assertTrue(fileName.equals("My Clients.csv"),
					"Downloaded file name is not matching with expected file name");
			deleteFileIfExist(downloadPath);
		}
	}

	private void deleteFileIfExist(String downloadPath) {
		File file = new File(downloadPath + "\\My Clients.csv");
		if (file.delete())
			System.out.println("file deleted");
		else
			System.out.println("file not deleted");

	}   

	@Test(description = "Update Notes Of Client From Agent Side", groups = { "regressionTest",
			"AgentFlow" }, enabled = false)
	public void validate_UpdateNotes_Of_Client_On_AgentApplication() {
		loginOrNavigate();
		myclient.clickOnNewClientButton();
		String emailId=myclient.EnterValuesInAddNewClientForm(myclientData.getJsonData(environment, "FirstName"),
				myclientData.getJsonData(environment, "LastName"), myclientData.getJsonData(environment, "Phone"),
				myclientData.getJsonData(environment, "Message"));
		String actual = myclient.AddClientSuccessMessage();
		Assert.assertTrue(myclient.isClientExist(emailId));
		Assert.assertEquals("Successfully added the new client!", actual, "Add New Client functionality Failed");
		System.out.println("New Client Added Sucessfully!");
		myclient.clickOnAddNewClientSucessOkButton();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			myclient.clickOnEditIcon(myclientData.getJsonData(environment, "ClientName"));
			myclient.clickOnNotes(myclientData.getJsonData(environment, "ClientName"),
					myclientData.getJsonData(environment, "Notes"));
			myclient.clickOnAddNewClientSucessOkButton();
			String notesText = myclient.notesMessageOnMyClient(myclientData.getJsonData(environment, "ClientName"));
			Assert.assertTrue(notesText.equalsIgnoreCase(myclientData.getJsonData(environment, "Notes")),
					"Notes Does Not Match");
			System.out.println("Notes Update Functionality Verified Successfully");
			myclient.backMyClientPageInMobile();
			myclient.deleteClient(myclientData.getJsonData(environment, "ClientName"));
		} else {
			myclient.clickOnNotes(myclientData.getJsonData(environment, "ClientName"),
					myclientData.getJsonData(environment, "Notes"));
			String notesText = myclient.notesMessageOnMyClient(myclientData.getJsonData(environment, "ClientName"));
			Assert.assertTrue(notesText.equalsIgnoreCase(myclientData.getJsonData(environment, "Notes")),
					"Notes Does Not Match");
			System.out.println("Notes Update Functionality Verified Successfully");
			myclient.deleteClient(myclientData.getJsonData(environment, "ClientName"));

		}
		Assert.assertTrue(!myclient.isClientExist(emailId));

	}

	@Test(description = "Verify Pagination on My Client Page On Agent Side", groups = { "regressionTest", "AgentFlow" })
	public void validate_Pagination_Of_MyClientPage_On_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Import Client Feature");
			throw new SkipException("Skipping this exception");
		} else {
			loginOrNavigate();
			myclient.clickOnImport();
			myclient.uploadClientCSVFile();
			String importedClient = myclient.importedClient();
			String notImportedClient = myclient.notImportedClient();
			System.out.println(importedClient + "  " + notImportedClient);
			myclient.closeImportPopUp();
			Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 24);
			srpPage.selectItemsPerPage("48");
			Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 48);
			srpPage.selectItemsPerPage("96");
			Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 96);
			myclient.deleteAllClient_Update();

		}

	}

	private void loginOrNavigate() {
		if (currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "ImportId"),
					commonData.getJsonData(environment, "Password"));
			commonPage.clickOnMyClientsLink();
			currentUrl = driver.getCurrentUrl();
		} else
			driver.navigate().to(currentUrl);
	}

}
