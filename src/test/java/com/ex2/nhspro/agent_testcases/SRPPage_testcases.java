package com.ex2.nhspro.agent_testcases;


import java.util.stream.Collectors;

import org.testng.Assert;
import org.testng.SkipException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.GetCommonData;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;


public class SRPPage_testcases extends Initializations {

	String currentUrl="";
	
	@BeforeClass(groups = {"smokeTest","regressionTest","HappyPath"})
	public void beforeStarting() {
		openAgentApplication();
		
	}
	
	@Test(groups = {"HappyPath"})
	public void validateSRPPageOfListView() {
		loginOrNavigate();	
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("List");
		System.out.println(filter.getBreadCrumbData(homeData.getJsonData("Search_data", "Matching_Searched_data")));
		Assert.assertEquals(filter.getFilterData(), GetCommonData.expectedgetFilterData());
		Assert.assertEquals(srpPage.getAlldropdownValues().toString(), srpData.getJsonData("Sorting", "desktop_HomeTab"));
		Assert.assertEquals(srpPage.getAllHomeColumnsInSRP().stream().map(s->s.getText()).collect(Collectors.toList()).toString(), srpData.getJsonData("Sorting", "homeTab_columns"));
		Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 20);
		Assert.assertEquals(srpPage.getDefaultPaginationValue(), "20");
		Assert.assertEquals(commonOperation.getPaginationValuesOnListView().toString(), commonData.getJsonData("PaginationData", "DropdownValues"));
		Assert.assertEquals(srpPage.getDefaultSelectedSortingValue(), "Featured");		
		commonPage.clickOnCommunities_tab();
		
	}
	
	@Test(groups = {"HappyPath"})
	public void validateSRPPagePhotoView() {
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("Photo");
		ActionsUtility.staticWait(2);
		Assert.assertEquals(photoView.getAllColumnsOnPhotoView().toString(), srpData.getJsonData("Sorting","desktop_HomeTab_photoView"));
		Assert.assertTrue(photoView.isMainButtonsExistOnPage());
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		Assert.assertEquals(photoView.getAllColumnsOnPhotoView().toString(), srpData.getJsonData("Sorting","desktop_CommunityTab_photoView"));
		Assert.assertTrue(photoView.isMainButtonsExistOnPage());
	}
	
	@Test(groups = {"HappyPath"})
	public void validateSRPPageMapView() {
		loginOrNavigate();
		commonPage.clickOnView("Map");
		Assert.assertTrue(srpPage.isDrawInMapOptionExist());
		Assert.assertTrue(srpPage.isSearchWithInMapExist());
		Assert.assertEquals(srpPage.getMapOptions().toString(), srpData.getJsonData("MapData", "Options"));
		Assert.assertTrue(srpPage.isZoomInButtonExist());
		Assert.assertTrue(srpPage.isZoomOutButtonExist());	
	}
	
	
	
	
	public void validateSRPPageOfListViewCommunityTab() {
		loginOrNavigate();
		
	}
	
	
	
	@Test(description="To verify navigation of home and community tab should take place on agent side",groups = {"smokeTest"})
	public void verify_navigationOfTabOnSRPFromAgentSide() {
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		Assert.assertTrue(commonPage.isHomeTabSelected());
		commonPage.clickOnCommunities_tab();
		Assert.assertTrue(commonPage.isCommunitiesSelected());		
	}
	
	@Test(description = "", groups = {"smokeTest","regressionTest"})
	public void validate_AgentVersionPrintPdf_On_From_SRP_AgentApplication() {		
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("List");
		srpPage.selectParticularHome(1);
		srpPage.clickOnPrintReportButton();
		String parentWinHandle = driver.getWindowHandle();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			ActionsUtility.staticWait(4);
			srpPage.clickOnAgentVersionPrintReport();
			ActionsUtility.staticWait(5);
			srpPage.switchTowindow();
			String currentlurl = driver.getCurrentUrl();
			System.out.println(currentlurl);
			Assert.assertTrue(currentlurl.contains("pdfgenerator"), "Unable to generate pdf Report");
			System.out.println("Agent Version PrintPdf Verified");
			driver.close();
			driver.switchTo().window(parentWinHandle);
			commonOperation.clickOnClose_icon();
		} else {
			Assert.assertTrue(srpPage.isAgentVersion_btn_exist(), "Agent version not exist");
			System.out.println("Agent Version button should exist");
			logger.log(Status.INFO, "Agent version button should exist");
			srpPage.clickOnAgentVersionPrintReport();
		}
		
	}

	/* Author Ritesh */
	@Test(description = "", groups = { "smokeTest" })
	public void validate_ClientVersionPrintPdf_On_From_SRP_AgentApplication() {
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("List");
		srpPage.selectParticularHome(2);
		ActionsUtility.staticWait(1);
		srpPage.clickOnPrintReportButton();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			srpPage.clickOnClientVersionPrintReport();
			ActionsUtility.staticWait(5);
			srpPage.switchTowindow();
			String currentlurl = driver.getCurrentUrl();
			System.out.println(currentlurl);
			Assert.assertTrue(currentlurl.contains("pdfgenerator"), "Unable to generate pdf Report");
			System.out.println("Client Version PrintPdf Verified");
			driver.close();
			driver.switchTo().window(parentWinHandle);
			commonOperation.clickOnClose_icon();
		} else {
			Assert.assertTrue(srpPage.isClientVersion_btn_exist(), "Client version not exist");
			System.out.println("Client Version button should exist");
			logger.log(Status.INFO, "Client version button should exist");
			srpPage.clickOnClientVersionPrintReport();
		}
		
	}
	
	
	/* Author Shankit */
	/*
	@Test(description = "Validation of Home Preview on Agent Application", groups= {"smokeTest", "regressionTest"})
	public void validate_Home_Preview_On_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Home/ Community Preview");
		} else {
			loginOrNavigate();
			String oldTab = driver.getWindowHandle();
			String gridText = srpPage.focusOnSecondHome_Community_OfGrid();
			String previewText = srpPage.getHomePreview_Title();
			System.out.println("Home Preview Text is "+ previewText);
	        commonPage.scroll(0,2000);
	        commonPage.focusOnFacebook_icon();
            Assert.assertEquals(previewText, gridText, "Grid Home is not matched with preview");
            srpPage.clickOn_RequestToRegisterClient_btn_Preview();
			srpPage.clickOn_RequestToRegister_from_popup();
			srpPage.clickOnOk_button();
			System.out.println("Request to register client from preview has been verified");
			srpPage.clickOn_PrintReport_btn_Preview();
			ActionsUtility.staticWait(5);
			srpPage.clickOnClientVersionPrintReport();
			srpPage.switchTowindow();
			String agentPdfCurrentlUrl = driver.getCurrentUrl();
			System.out.println(agentPdfCurrentlUrl);
			Assert.assertTrue(agentPdfCurrentlUrl.contains("pdfgenerator"), "Unable to generate pdf Report");
			System.out.println("Agent Version PrintPdf Verified");
			driver.switchTo().window(oldTab);
			srpPage.clickOnAgentVersionPrintReport();
			srpPage.switchTowindow();
			ActionsUtility.staticWait(2);
			String clientPdfCurrentUrl = driver.getCurrentUrl();
			System.out.println(clientPdfCurrentUrl);
			Assert.assertTrue(clientPdfCurrentUrl.contains("pdfgenerator"), "Unable to generate pdf Report");
			System.out.println("Client Version PrintPdf Verified");
			driver.switchTo().window(oldTab);
			commonOperation.clickOnClose_icon();
			System.out.println("Print Report of both agent and client from preview has been verified");
			srpPage.clickOn_SaveListing_btn_Preview();
			Assert.assertTrue(srpPage.isSavdListing_popup_exist());
			srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
			ActionsUtility.staticWait(2);
			srpPage.clickOnSaveandEmail_btn();
			srpPage.clickOnNo_btn();
			srpPage.clickOnOk_button();
			System.out.println("Save listing from preview has been verified");
		}
	}
	*/
	

	
	/* Author Shankit */
	/*
	@Test(description = "Validation of Home Preview ContactTheBuilder on Agent Application", groups= {"smokeTest", "regressionTest"})
	public void validate_Home_Preview_ContactTheBuilder_On_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Home/ Community Preview");
		} else {
			loginOrNavigate();
			String gridText = srpPage.focusOnSecondHome_Community_OfGrid();
			String previewText = srpPage.getHomePreview_Title();
			System.out.println("Home Preview Text is "+ previewText);
			Assert.assertEquals(previewText, gridText, "Grid Home is not matched with preview");
			srpPage.clickOnContactTheBuilderTabFromPreview();
			commonPage.scroll(0,3000);
	        commonPage.focusOnFacebook_icon();
			srpPage.enterTextInContactBuilderTextArea("Contact Sales agent");
			srpPage.clickOnContactAgentSales_btnPreview();
			System.out.println("Contact sales agent form preview has been verified");
			srpPage.clickOn_RequestToRegisterClient_btn_BuilderPreview();
			srpPage.clickOn_RequestToRegister_from_popup();
			srpPage.clickOnOk_button();
			System.out.println("Request to register client from preview has been verified");
			
		}
	}
	*/
	

	/* Author Shankit */
	/*
	@Test(description = "Validation of Community Preview on Agent Application", groups= {"smokeTest", "regressionTest"})
	public void validate_Community_Preview_On_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Home/ Community Preview");
		} else {
			loginOrNavigate();
			String oldTab = driver.getWindowHandle();
			commonPage.clickOnCommunities_tab();
			String gridText = srpPage.focusOnSecondHome_Community_OfGrid();
			String previewText = srpPage.getCommunityPreview_Title();
			System.out.println("Community Preview Text is "+ previewText);
			commonPage.scroll(0,3000);
	        commonPage.focusOnFacebook_icon();
            Assert.assertEquals(previewText, gridText, "Grid Community is not matched with preview");
            srpPage.clickOn_RequestToRegisterClient_btn_Preview();
			srpPage.clickOn_RequestToRegister_from_popup();
			srpPage.clickOnOk_button();
			System.out.println("Request to register client from preview has been verified");
			srpPage.clickOn_PrintReport_btn_Preview();
			ActionsUtility.staticWait(5);
			srpPage.clickOnClientVersionPrintReport();
			srpPage.switchTowindow();
			String agentPdfCurrentlUrl = driver.getCurrentUrl();
			System.out.println(agentPdfCurrentlUrl);
			Assert.assertTrue(agentPdfCurrentlUrl.contains("pdfgenerator"), "Unable to generate pdf Report");
			System.out.println("Agent Version PrintPdf Verified");
			driver.switchTo().window(oldTab);
			srpPage.clickOnAgentVersionPrintReport();
			srpPage.switchTowindow();
			ActionsUtility.staticWait(2);
			String clientPdfCurrentUrl = driver.getCurrentUrl();
			System.out.println(clientPdfCurrentUrl);
			Assert.assertTrue(clientPdfCurrentUrl.contains("pdfgenerator"), "Unable to generate pdf Report");
			System.out.println("Client Version PrintPdf Verified");
			driver.switchTo().window(oldTab);
			commonOperation.clickOnClose_icon();
			commonPage.focusOnFacebook_icon();
			srpPage.clickOn_SaveListing_btn_Preview();
			System.out.println("Print Report of both agent and client from preview has been verified");
			Assert.assertTrue(srpPage.isSavdListing_popup_exist());
			srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
			ActionsUtility.staticWait(2);
			srpPage.clickOnSaveandEmail_btn();
			srpPage.clickOnNo_btn();
			srpPage.clickOnOk_button();
			System.out.println("Save listing from preview has been verified");
		}
	}
	*/
	
	/* Author Shankit */
	/*
	@Test(description = "Validation of Community Preview ContactTheBuilder on Agent Application", groups= {"smokeTest", "regressionTest"})
	public void validate_Community_Preview_ContactTheBuilder_On_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Home/ Community Preview");
		} else {
			loginOrNavigate();
			commonPage.clickOnCommunities_tab();
			String gridText = srpPage.focusOnSecondHome_Community_OfGrid();
			String previewText = srpPage.getCommunityPreview_Title();
			System.out.println("community Preview Text is "+ previewText);
			Assert.assertEquals(previewText, gridText, "Grid Community is not matched with preview");
			srpPage.clickOnContactTheBuilderTabFromPreview();
			commonPage.scroll(0,3000);
	        commonPage.focusOnFacebook_icon();
			srpPage.enterTextInContactBuilderTextArea("Contact Sales agent");
			srpPage.clickOnContactAgentSales_btnPreview();
			System.out.println("Contact sales agent form preview has been verified");
			srpPage.clickOn_RequestToRegisterClient_btn_BuilderPreview();
			srpPage.clickOn_RequestToRegister_from_popup();
			srpPage.clickOnOk_button();
			System.out.println("Request to register client from preview has been verified");
			
		}
	}
	*/

//	/* Author Ritesh */
//	@Test(description = "", groups = { "smokeTest" }, enabled = false)
//	public void validate_Home_Community_Preview_On_AgentApplication() {
//		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
//			System.out.println("Mobile does not have Home/ Community Preview");
//		} else {
//
//			loginOrNavigate();
////			srpPage.closeSRPEnageClientPopUp();
//			String gridtext = srpPage.focusOnFirstHome_Community_OfGrid();
//			ActionsUtility.staticWait(3);
//			String PreviewText = srpPage.focusOnHomePreview();
//			Assert.assertEquals(PreviewText, gridtext, "Grid Home is not matched with preview");
//			System.out.println("Grid Home Preview Functionality Verified.");
//			commonPage.clickOnCommunities_tab();
//			String CommunityGridtext = srpPage.focusOnFirstHome_Community_OfGrid();
//			ActionsUtility.staticWait(3);
//			String CommuntiyPreviewText = srpPage.focusOnCommunityPreview();
//			Assert.assertEquals(CommunityGridtext, CommuntiyPreviewText, "Grid Community is not matched with preview");
//			System.out.println("Grid Community Preview Functionality Verified.");
//		}
//	}

	/* Author Ritesh */
//	@Test(description = "", groups = { "smokeTest" })
//	public void validate_Home_Community_HoverPreview_On_AgentApplication() {
//		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
//			System.out.println("Mobile does not have Home/ Community Preview");
//			throw new SkipException("Skipping this exception");
//
//		} else {
//
//			loginOrNavigate();
////			srpPage.closeSRPEnageClientPopUp();
//			srpPage.closeSignMeUpPopUp();
//			String gridtext = srpPage.focusOnSecondHome_Community_OfGrid();
//			String PreviewText = srpPage.getHomePreview_Title();
//			Assert.assertEquals(PreviewText, gridtext, "Grid Home is not matched with preview");
//			System.out.println("Grid Home Preview Functionality Verified.");
//			commonPage.clickOnCommunities_tab();
//			String Communitygridtext = srpPage.focusOnSecondHome_Community_OfGrid();
//			
//			String CommuntiyPreviewText = srpPage.getCommunityPreview_Title();
//			Assert.assertEquals(Communitygridtext, CommuntiyPreviewText, "Grid Community is not matched with preview");
//			System.out.println("Grid Community Preview Functionality Verified.");
//		}
//	}

//	@Test(description = "Validate the Searching From the SRP Page", groups = { "smokeTest" })
	public void validate_searchingFrom_SRPPage_On_AgentApplication() {

		loginOrNavigate();
//		srpPage.closeSRPEnageClientPopUp();
		String before = driver.getCurrentUrl();
		System.out.println(before);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			srpPage.clickOnFilter_link();
		srpPage.enterSearchedData_in_location_field("Austin, TX Area");
		srpPage.clickOnSearchIcon();
		String currecturl = driver.getCurrentUrl();
		Assert.assertTrue(currecturl.contains("austin"), "User is unable to search from the SRP Page");
		System.out.println("Searching From the SRP Page is Verified");

	}
	
	@Test(groups = {"smokeTest","regressionTest"})
	public void verifyResetFilterFunctionality() {
		loginOrNavigate();
		srpPage.closeSRPEnageClientPopUp();
		String homeCountFromSRP = srpPage.getHomeCountFromSRP();
		System.out.println(homeCountFromSRP);
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			String defaultBedrooms = filter.getSelectedBedrooms();
			String defaultBathrooms = filter.getSelectedBathrooms();
			filter.clickOnBedsFilter();
			filter.selectBedrooms("2");
			filter.selectBathrooms("3");
			filter.clickOnUpdateBedBath_btn();
			Assert.assertNotEquals(srpPage.getHomeCountFromSRP(), homeCountFromSRP);
			filter.clickOnResetFilter();		
			srpPage.waitForHomeCount(homeCountFromSRP);
			Assert.assertEquals(filter.getSelectedBedrooms(), defaultBedrooms);
			Assert.assertEquals(filter.getSelectedBathrooms(), defaultBathrooms);			
		}
		else {
			srpPage.clickOnFilter_link();
			filter.selectBedrooms("2");
			filter.selectBathrooms("3");
			filter.clickOnApply_btn();
			ActionsUtility.staticWait(5);
			 Assert.assertNotEquals(srpPage.getHomeCountFromSRP(), homeCountFromSRP);
			srpPage.clickOnFilter_link();
			filter.clickOnResetFilter();
		}
		
//		srpPage.waitForHomeCount(homeCountFromSRP);
		Assert.assertTrue(srpPage.getHomeCountFromSRP().equalsIgnoreCase(homeCountFromSRP));
		ActionsUtility.printLog("Assertion Passed :- Reset functionality is working fine");	
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_NearByCity_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
		filter.clickOnLocationField();
		filter.clickNearByCities();
		filter.selectCity_from_NearBy("Austin");
		filter.selectCity_from_NearBy("Bastrop");
		filter.clickOnUpdate_btn_forlocation();
		ActionsUtility.staticWait(5);
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("NearByCity", "Bastrop");
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "Bastrop, TX", noOfHomesWithSublocation);
			}
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_ZipCode_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
//		srpPage.closeSRPEnageClientPopUp();
		filter.clickOnLocationField();
		filter.clickZipcode();
		filter.selectZipCode_from_NearBy("78612");
		filter.clickOnUpdate_btn_forlocation();
		ActionsUtility.staticWait(5);
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("Zipcode", "78612");
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "78612", noOfHomesWithSublocation);
			}
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_SchoolDistrict_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
		filter.clickOnLocationField();
		filter.clickSchoolDistricts();
		String idOfTheDistrict = filter.getIdOfTheDistrict("Bastrop ISD");
		filter.selectSchool_from_SchoolDistricts("Bastrop ISD");
		filter.clickOnUpdate_btn_forlocation();
		ActionsUtility.staticWait(5);
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("SchoolDistricts",idOfTheDistrict);
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "Bastrop ISD", noOfHomesWithSublocation);
			}
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_County_SelectionFunctionality_fromSRP() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{
		loginOrNavigate();
//		srpPage.closeSRPEnageClientPopUp();
		filter.clickOnLocationField();
		filter.clickCounties();
		filter.selectCounty_from_Counties("Burnet");
		filter.clickOnUpdate_btn_forlocation();
		String noOfHomesWithSublocation = srp_api.getNoOfHomesWithSublocation("Counties", "Burnet");
		srpPage.verifyBreadCrumb("My Search Results", "Texas", "Austin Area", "Burnet County, TX", noOfHomesWithSublocation);
			}
	}
	
	@Test(description="Validate the Pagination From the SRP Page",groups= {"smokeTest","regressionTest"})
	public void validate_pagination_on_SRPPage_On_AgentApplication() {
	     if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not have Pagination Section");
				throw new SkipException("Skipping this exception");				
			}
			else
			{

	   loginOrNavigate();
	   commonPage.clickOnView("List");
	   commonPage.clickOnHomes_tab();
	//   srpPage.enterSearchedData_in_location_field("Austin, TX Area");
	//	srpPage.clickOnSearchIcon();
	    String IdOffirstPage = srpPage.getIdOfPerticularHome_InList(1);
	    srpPage.clickOnPaginationNoListView("2");
	    ActionsUtility.staticWait(5);
	    String IdOf_SecondPage = srpPage.getIdOfPerticularHome_InList(1);
	 //   ActionsUtility.staticWait(5);
	    Assert.assertNotEquals(IdOf_SecondPage, IdOffirstPage);
	 //   Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 10);
	    srpPage.selectItemsPerPage("20");
	    Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 20);
	    srpPage.selectItemsPerPage("40");
	    Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 40);
	    srpPage.selectItemsPerPage("60");
	    Assert.assertEquals(srpPage.getNoOfHomesOnPage(), 60);
	    
	}
	}
	
/*	@Test(groups = {"regressionTest","smokeTest"})
	public void verify_AdOn_SRP_Page() {
		loginOrNavigate();
		String FirstAdtitle=srpPage.getAdTitle();
		String FirstAdDescription=srpPage.getAdDescription();
		ActionsUtility.staticWait(5);
		 srpPage.selectItemsPerPage("60");
		 int Adcount=srpPage.getAdCountOnGrid().size();
		System.out.println(FirstAdtitle);
		System.out.println(FirstAdDescription);
		System.out.println("Total number of Ad on the page is "+Adcount);
		srpPage.AdtextVerification(FirstAdtitle);
		srpPage.AdtextVerification(FirstAdDescription);
		srpPage.clickOnSponserBy();
		String href=srpPage.gethrefOfAd();
		System.out.println(href);
		ActionsUtility.staticWait(5);
		String originalWindow=driver.getWindowHandle();
		commonPage.switchtoWindow();
		System.out.println(driver.getCurrentUrl());
		Assert.assertTrue(href.equalsIgnoreCase(driver.getCurrentUrl()), "Ad URL does not match");
		driver.close();
		driver.switchTo().window(originalWindow);
		ActionsUtility.staticWait(5);
		
	} 
	*/

	
	@Test(description="Validate Share Search Functionality",groups = {"regressionTest"})
	public void validate_share_search_functionality()
	{
	//	loginOrNavigate();
		  if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
				
				System.out.println("Mobile does not Share Search Functionality");
				throw new SkipException("Skipping this exception");
				
			}
			else
			{ 
		  String minPrice=homeData.getJsonData("Search_data","Min_Price");
		  String maxPrice=homeData.getJsonData("Search_data","Max_Price");
		  String beds=homeData.getJsonData("Search_data","Bedrooms");
		  String bathrooms=homeData.getJsonData("Search_data","Bathrooms");
		  String Squarefeet=homeData.getJsonData("Search_data","SquareFeet");
		  commonPage.clickOnLogo();
		homePage.enterSearchData_field("Dallas, TX Area");
		 homePage.select_minprice(minPrice);
		  homePage.select_maxprice(maxPrice);
		 homePage.select_beds(beds);
		  homePage.select_bath(bathrooms);
		  homePage.select_squareFeet(Squarefeet);
	    homePage.clickOnSearch_icon();
	    String locationtext=driver.getCurrentUrl();
//	    srpPage.closeSRPEnageClientPopUp();
	    System.out.println(locationtext);
	    srpPage.clickOnShareSearch();
	    String parentWinHandle = driver.getWindowHandle();
	    srpPage.verification_Of_ShareSearchFunctionlity(); 
	    driver.close();
		driver.switchTo().window(parentWinHandle);
		srpPage.closeShareSearchPopup();
	}
		  
		  
	}
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
			homePage.clickOnSearch_icon();
			srpPage.closeSRPEnageClientPopUp();
			
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);
		srpPage.closeSignMeUpPopUp();
	}
	
	
}
