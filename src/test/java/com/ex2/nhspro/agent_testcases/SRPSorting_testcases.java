package com.ex2.nhspro.agent_testcases;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;


public class SRPSorting_testcases extends Initializations {

	String currentUrl = "";


	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();
	}


	@Test(groups = {"smokeTest"})
	public void verifySortingFor_HomeTab_ListView_OnSRPPage() {
		loginOrNavigate();
		ActionsUtility actionPerform = new ActionsUtility(driver, logger);
		commonPage.clickOnHomes_tab();
		commonPage.clickOnView("List");
		ActionsUtility.staticWait(3);
		List<String> allSortingdropdownValues = srpPage.getAlldropdownValues();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			Assert.assertEquals(allSortingdropdownValues.toString(), srpData.getJsonData("Sorting", "desktop_HomeTab"));
		else
			System.out.println("I am on mobile");
		for (String eachSortValue : allSortingdropdownValues) {
			if (!eachSortValue.equalsIgnoreCase("Featured") && !eachSortValue.equalsIgnoreCase("Confirm CoOp First")) {
				srpPage.selectSortByValue(eachSortValue);
				ActionsUtility.staticWait(5);
				Assert.assertTrue(srpPage.verifyColumnIsSelcted(eachSortValue));
				List<WebElement> allListElements = srpPage.verifyListIsSortedAccordingToSelectedValues(eachSortValue,"HomeTab");
				List<String> actualResult = allListElements.stream().map(s -> actionPerform.getTextData(s).replace(",", "")).collect(Collectors.toList());
				 srpPage.sortingVerification(eachSortValue,actualResult);
			}
		}
	}

	@Test(groups = {"regressionTest"})
	public void verifySortingFor_CommunityTab_ListView_OnSRPPage() {
		loginOrNavigate();
		ActionsUtility actionPerform = new ActionsUtility(driver, logger);
		ActionsUtility.staticWait(2);
		commonPage.clickOnView("List");
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(5);
		List<String> allSortingdropdownValues = srpPage.getAlldropdownValues();
		System.out.println(allSortingdropdownValues);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		
			Assert.assertEquals(allSortingdropdownValues.toString(),
					srpData.getJsonData("Sorting", "desktop_CommunityTab"));
		else
			System.out.println("I am on mobile");
		for (String eachSortValue : allSortingdropdownValues) {
			if (!eachSortValue.matches("(?i)Featured|Confirm CoOp First|Price High - Low")) {
				srpPage.selectSortByValue(eachSortValue);
				ActionsUtility.staticWait(5);
				List<WebElement> allListElements = srpPage.verifyListIsSortedAccordingToSelectedValues(eachSortValue,
						"Community");
				List<String> actualResult = allListElements.stream().map(s -> actionPerform.getTextData(s)).collect(Collectors.toList());
				if (eachSortValue.startsWith("Price") || eachSortValue.startsWith("Sq.Ft")
						|| eachSortValue.startsWith("Beds") || eachSortValue.startsWith("Baths"))
					srpPage.sortingBasedOnRange(eachSortValue, actualResult);
				else
					srpPage.sortingVerification(eachSortValue, actualResult);
			}
		}
//List<String> collect = srpPage.getAllHomeColumnsInSRP().stream().map(s->s.getText()).collect(Collectors.toList());
	}
//================================================== Photo View ======================================================		



	@Test(groups = {"regressionTest"})
	public void verifySortingForPhotoView_HomeTab_SRP() {
		loginOrNavigate();	
		ActionsUtility.staticWait(2);
		ActionsUtility actionPerform = new ActionsUtility(driver, logger);
		commonPage.clickOnView("Photo");
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(5);
		List<WebElement> allPhotoVIewSortingOptions = srpPage.getAllPhotoViewSortingOptions(); // all sorting values on	photoview
		List<String> allPhotoVIewSortingOptions_data = allPhotoVIewSortingOptions.stream().map(s -> actionPerform.getTextData(s))
				.collect(Collectors.toList()); // text of sorting values
		Assert.assertEquals(allPhotoVIewSortingOptions_data.toString(),
				srpData.getJsonData("Sorting", "desktop_HomeTab_photoView")); // assertion with expected values
		for (WebElement sortingElement : allPhotoVIewSortingOptions) {
			String selectedValue =actionPerform.getTextData(sortingElement);
			actionPerform.clickOnElement(sortingElement, " First Clicked on " + selectedValue);
			ActionsUtility.staticWait(5);
			srpPage.verifyBasedOnClicked(selectedValue, "Low to High");
			actionPerform.clickOnElement(sortingElement, " Second Clicked on " + selectedValue);
			ActionsUtility.staticWait(5);
			srpPage.verifyBasedOnClicked(selectedValue, "High to Low");

		}

	}
	
	@Test(groups = {"smokeTest","regressionTest"})
	public void verifySortingForPhotoView_CommunityTab_SRP() {
		loginOrNavigate();		
		ActionsUtility actionPerform = new ActionsUtility(driver, logger);
		commonPage.clickOnView("Photo");
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		List<WebElement> allPhotoVIewSortingOptions = srpPage.getAllPhotoViewSortingOptions(); // all sorting values on	photoview
		List<String> allPhotoVIewSortingOptions_data = allPhotoVIewSortingOptions.stream().map(s -> actionPerform.getTextData(s))
				.collect(Collectors.toList()); // text of sorting values
		Assert.assertEquals(allPhotoVIewSortingOptions_data.toString(),
				srpData.getJsonData("Sorting", "desktop_CommunityTab_photoView")); // assertion with expected values
		for (WebElement sortingElement : allPhotoVIewSortingOptions) {
			String selectedValue =actionPerform.getTextData(sortingElement);
			actionPerform.clickOnElement(sortingElement, " First Clicked on " + selectedValue);
			ActionsUtility.staticWait(5);
			srpPage.verifyBasedOnClicked(selectedValue, "Low to High");
			actionPerform.clickOnElement(sortingElement, " Second Clicked on " + selectedValue);
			ActionsUtility.staticWait(5);
			srpPage.verifyBasedOnClicked(selectedValue, "High to Low");
		}
	}

	private void loginOrNavigate() {
		if (currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),
					commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			homePage.clickOnSearch_icon();
			srpPage.closeSRPEnageClientPopUp();
			srpPage.closeSignMeUpPopUp();
			currentUrl = driver.getCurrentUrl();
		} else
			driver.navigate().to(currentUrl);
	}
	
	

}
