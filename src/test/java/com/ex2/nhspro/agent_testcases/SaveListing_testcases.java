package com.ex2.nhspro.agent_testcases;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SaveListing_testcases extends Initializations {

	
	String Agentcomment;
	String Clientcomment;
	String currentUrl="";

	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();
		
	}
	

	@Test(groups = {"smokeTest"})
	public void verify_saveListing_working() {
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		homeTabSelection();
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSave_btn();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		ActionsUtility.staticWait(3);
		commonPage.clickOnHomes_tab();
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		Assert.assertEquals(idOfHomesOnSRP.get(0), saveListingPage.getHomeIdFromSavedListingPage(idOfHomesOnSaveListing));
	//	Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListing);
	}
	
	@Test(groups = {"regressionTest","smokeTest"})
	public void verify_Commenting_From_AgentSide_And_Verifying_On_Client_Site() {
		loginOrNavigate();
		Agentcomment= "Comment From Agent "+ActionsUtility.nameWithDate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		homeTabSelection();
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		commonPage.clickOnHomes_tab();
		commonOperation.clickOnCommentIconFromSaveListing(1);
		commonOperation.enterOnCommentBox(Agentcomment);
		commonOperation.clickOnAddButton();
		String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
			ActionsUtility.staticWait(5);
			validateComment(Agentcomment);
			Clientcomment= "Comment from Client "+ActionsUtility.nameWithDate();
			commonOperation.enterOnCommentBox(Clientcomment);
			commonOperation.clickOnAddButton();
			driver.switchTo().window(originalwindow);
			validateComment(Clientcomment);
			commonPage.switchtoWindow();
			  driver.close();
			  driver.switchTo().window(originalwindow);
			  ActionsUtility.staticWait(3);
		
	}


	@Test(description="Verify Saved Listing Page on Agent site is loaded properly",groups = {"smokeTest"})
	public void verify_SavedListing_Page_loaded_properly_on_agentsite()
	{
		loginOrNavigate();		
		commonPage.clickOnSaveListing();
		Assert.assertTrue(driver.getCurrentUrl().contains("savedlistings"), "Saved Listing page unable to load properly");
		System.out.println("Saved Listing page on agent site is loaded properly");
	}
	
	@Test(description="Verify Saved Listing Page Send, Print Report, Delete button are displayed on Agent site.",groups = {"smokeTest"})
	public void verify_SavedListing_Page_Send_Print_Report_Delete_Are_Displayed_agentsite()
	{
		loginOrNavigate();		
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		homeTabSelection();
		srpPage.closeSRPEnageClientPopUp();
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSave_btn();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		ActionsUtility.staticWait(3);
		commonPage.clickOnHomes_tab();
		assertTrue(saveListingPage.getCssValueOfSendButton().contains("rgba(245, 130, 32, 0.5)"), "Send Button is not disabled");
		assertTrue(saveListingPage.getCssValueOfPrintReportButton().contains("rgba(245, 130, 32, 0.5)"), "Print Report Button is not disabled");
		assertTrue(saveListingPage.getCssValueOfDeleteButton().contains("rgba(245, 130, 32, 0.5)"), "Delete Button is not disabled");
		ActionsUtility.staticWait(2);
		saveListingPage.selectParticluarHome(1);
//		saveListingPage.clickOnSelectAll_checkbox();
		assertTrue(saveListingPage.getCssValueOfSendButton().contains("rgba(245, 130, 32, 1)"), "Send Button is disabled");
		assertTrue(saveListingPage.getCssValueOfPrintReportButton().contains("rgba(245, 130, 32, 1)"), "Print Report Button is disabled");
		assertTrue(saveListingPage.getCssValueOfDeleteButton().contains("rgba(245, 130, 32, 1)"), "Delete Button is disabled");
		System.out.println("Send Print_Report And Delete Buttons Are Displayed On Agentsite Verified Successfully");
		
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_Agent_Send_Home_saveListing_And_Verify_from_Client_Site_NoCase() {
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		ActionsUtility.staticWait(4);
		homeTabSelection();
		ActionsUtility.staticWait(2);
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		ActionsUtility.staticWait(3);
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(2);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListing);
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      
	      commonPage.clickOnHomes_tab();
	      ActionsUtility.staticWait(3);
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListingClientSite);
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("Agent Sends A Home SaveListing And Verify from Client Site NoCase Verified Successfully");
	}
	
	@Test(groups = {"regressionTest","smokeTest"})
	public void verify_Agent_Clicks_On_Next_And_Previous_Button() {
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		ActionsUtility.staticWait(4);
		homeTabSelection();
		ActionsUtility.staticWait(2);
		List<String> idOfHome1stOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectParticularHome(1);
		List<String> idOfHome2ndOnSRP = srpPage.getIdOfHomes(2);
		srpPage.selectParticularHome(2);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		ActionsUtility.staticWait(3);
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		String currentUrl=driver.getCurrentUrl();
		System.out.println("Before clicking on next "+currentUrl);
		homeDetailPage.clickOnNextListing();
		ActionsUtility.staticWait(3);
		String nextListingUrl=driver.getCurrentUrl();
		System.out.println("After clicking on next "+nextListingUrl);
		homeDetailPage.validatePreviousAndNextListingUrls(currentUrl, nextListingUrl);
		homeDetailPage.clickOnPrevious();
		ActionsUtility.staticWait(3);
		homeDetailPage.validatePreviousAndNextListingUrls(nextListingUrl, currentUrl);
		
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_Agent_Send_Home_saveListing_And_Verify_from_Client_Site_YesCase() {
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		ActionsUtility.staticWait(4);
		homeTabSelection();
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnYes_btn();
		srpPage.clickOnSend_button_on_PreviewEmail_popup();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		ActionsUtility.staticWait(3);
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(2);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListing);
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(3);
	      commonPage.clickOnHomes_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListingClientSite);
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("Agent Sends A Home SaveListing And Verify from Client Site YesCase Verified Successfully");
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_Agent_Send_Community_saveListing_And_Verify_from_Client_Site_YesCase() {
		loginOrNavigate();
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		navigateToSRP();
		ActionsUtility.staticWait(4);
		commonPage.clickOnCommunities_tab();
		srpPage.selectHome(1);
		List<String> idOfcommunityOnSRP = srpPage.getIdOfHomes(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnYes_btn();
		srpPage.clickOnSend_button_on_PreviewEmail_popup();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		ActionsUtility.staticWait(3);
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(idOfcommunityOnSRP, idOfHomesOnSaveListing);
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(3);
	      commonPage.clickOnCommunities_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(idOfcommunityOnSRP, idOfHomesOnSaveListingClientSite);
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("Agent Sends A Community SaveListing And Verify from Client Site YesCase Verified Successfully");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_Agent_Send_Community_saveListing_And_Verify_from_Client_Site_NoCase() {
		loginOrNavigate();
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		navigateToSRP();
		ActionsUtility.staticWait(4);
		commonPage.clickOnCommunities_tab();
		List<String> idOfcommunityOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		ActionsUtility.staticWait(3);
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(idOfcommunityOnSRP, idOfHomesOnSaveListing);
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(3);
	      commonPage.clickOnCommunities_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(idOfcommunityOnSRP, idOfHomesOnSaveListingClientSite);
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("Agent Sends A Community SaveListing And Verify from Client Site NoCase Verified Successfully");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void verify_Agent_Save_Home_saveListing_And_Sent_From_SavedListing_Page_And_Verify_from_Client_Site() {
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		ActionsUtility.staticWait(3);
		homeTabSelection();
		ActionsUtility.staticWait(2);
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSave_btn();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		ActionsUtility.staticWait(4);
		commonPage.clickOnHomes_tab();
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListing);
		saveListingPage.selectParticluarHome(1);
		saveListingPage.clickOnSend_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(2);
	      commonPage.clickOnHomes_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListingClientSite);
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Save Home saveListing And Sent From SavedListing Page And Verify from Client_Site Verified Successfully");
	}
	
	@Test(groups = {"regressionTest"})
	public void verify_Agent_Save_Community_saveListing_And_Sent_From_SavedListing_Page_And_Verify_from_Client_Site() {
		loginOrNavigate();
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		navigateToSRP();
		ActionsUtility.staticWait(5);
		commonPage.clickOnCommunities_tab();
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSave_btn();
		srpPage.clickOnOk_button();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			driver.navigate().refresh();
		commonPage.hoverToSaveListing();
		srpPage.openSaveListing_perClient(ClientNameforSaveListing);
		ActionsUtility.staticWait(3);
		commonPage.clickOnCommunities_tab();
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListing);
		saveListingPage.selectParticluarHome(1);
		saveListingPage.clickOnSend_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	  	ActionsUtility.staticWait(3);
	  	commonPage.clickOnCommunities_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(idOfHomesOnSRP, idOfHomesOnSaveListingClientSite);
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Save Commumity saveListing And Sent From SavedListing Page And Verify from Client_Site Verified Successfully");
	}
	
	
	@Test(groups = {"regressionTest"})
	public void validate_saveListing_From_HomeDetailsPage_And_Verify_Home_At_Client_Site_NoCase() {	
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		ActionsUtility.staticWait(3);
		homeTabSelection();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		commonPage.clickOnHomes_tab();
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(3);
	      commonPage.clickOnHomes_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(homeIdFromDetailsPage, idOfHomesOnSaveListingClientSite.get(0));
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Send the Saved Listing From Home Detail Page No Case And Verify from Client_Site Verified Successfully");
	
	}
	
	@Test(groups = {"regressionTest"})
	public void validate_saveListing_From_HomeDetailsPage_And_Verify_Home_At_Client_Site_YesCase() {	
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		ActionsUtility.staticWait(3);
		homeTabSelection();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnYes_btn();
		srpPage.clickOnSend_button_on_PreviewEmail_popup();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		commonPage.clickOnHomes_tab();
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(3);
	      commonPage.clickOnHomes_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(homeIdFromDetailsPage, idOfHomesOnSaveListingClientSite.get(0));
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Send the Saved Listing From Home Detail Page Yes Case And Verify from Client_Site Verified Successfully");
	
	}

	

	@Test(groups = {"regressionTest"})
	public void validate_saveListing_From_Community_DetailsPage_And_Verify_Home_At_Client_Site_NoCase() {	
		loginOrNavigate();
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		navigateToSRP();
		ActionsUtility.staticWait(5);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		commonPage.clickOnCommunities_tab();
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(2);
	      commonPage.clickOnCommunities_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(homeIdFromDetailsPage, idOfHomesOnSaveListingClientSite.get(0));
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Send the Saved Listing From Community Detail Page No Case And Verify from Client_Site Verified Successfully");
	
	}
	
	@Test(groups = {"regressionTest"})
	public void validate_saveListing_From_Community_DetailsPage_And_Verify_Home_At_Client_Site_YesCase() {	
		loginOrNavigate();
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		navigateToSRP();
		ActionsUtility.staticWait(5);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnYes_btn();
		srpPage.clickOnSend_button_on_PreviewEmail_popup();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		commonPage.clickOnCommunities_tab();
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
		 String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(2);
	      commonPage.clickOnCommunities_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(homeIdFromDetailsPage, idOfHomesOnSaveListingClientSite.get(0));
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Send the Saved Listing From Community Detail Page Yes Case And Verify from Client_Site Verified Successfully");
	
	}

	@Test(groups = {"regressionTest"})
	public void validate_Agent_Save_Home_From_HomeDetailsPage_And_Sent_Home_From_Saved_Listing_Page_And_Verify_Home_At_Client_Site() {	
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		navigateToSRP();
		ActionsUtility.staticWait(3);
		homeTabSelection();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSave_btn();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		 commonPage.clickOnHomes_tab();
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
		saveListingPage.selectParticluarHome(1);
		saveListingPage.clickOnSend_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button(); 
		String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(3);
	      commonPage.clickOnHomes_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(homeIdFromDetailsPage, idOfHomesOnSaveListingClientSite.get(0));
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Save the Home From Home Detail Page And Send it From Saved Listing Page And Verify from Client_Site Verified Successfully");
	
	}
	
	@Test(groups = {"regressionTest"})
	public void validate_Agent_Save_Community_From_CommunityDetailsPage_And_Sent_Community_From_Saved_Listing_Page_And_Verify_Community_At_Client_Site(){	
		loginOrNavigate();
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		navigateToSRP();
		ActionsUtility.staticWait(5);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSave_btn();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		commonOperation.navigateToSaveListingData(ClientNameforSaveListing);
		commonPage.clickOnCommunities_tab();
		List<String> idOfHomesFromSaveListing = saveListingPage.getIdOfHomes(1);
		Assert.assertEquals(homeIdFromDetailsPage.replace("#m", ""), idOfHomesFromSaveListing.get(0));
		saveListingPage.selectParticluarHome(1);
		saveListingPage.clickOnSend_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button(); 
		String originalwindow=driver.getWindowHandle();
		 commonPage.openNewTab();
		 openClientApplication();
		  commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment,
		  "EmailId_client"));
	      commonPage.clickOnSaveListing();
	      ActionsUtility.staticWait(2);
	      commonPage.clickOnCommunities_tab();
	      List<String> idOfHomesOnSaveListingClientSite = saveListingPage.getIdOfHomes(1);
	      Assert.assertEquals(homeIdFromDetailsPage, idOfHomesOnSaveListingClientSite.get(0));
	      driver.close();
	      driver.switchTo().window(originalwindow);
	      System.out.println("verify Agent Save Community From Community Detail Page And Send From Saved Listing Page And Verify from Client_Site Verified Successfully");
	
	}
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field("Dallas, TX Area");
			homePage.clickOnSearch_icon();
			srpPage.closeSRPEnageClientPopUp();
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	
	private void validateComment(String commentedText) {
		commonOperation.clickOnCommentIconFromSaveListing(1);
		String lastcomment=commonOperation.getLastComment();
		Assert.assertEquals(lastcomment, commentedText);
		System.out.println("Comment verified successfully");
	//	String commentFromCommentBox = commonOperation.getCommentFromCommentBox();
		//Assert.assertEquals(commentFromCommentBox, commentedText);
	}
	
	private void homeTabSelection() {
		if(!driver.getCurrentUrl().equalsIgnoreCase(currentUrl)) {
			driver.navigate().to(currentUrl);	
		}
		commonPage.clickOnHomes_tab();
	}
	
	private void navigateToSRP()
	{
		if(!driver.getCurrentUrl().equalsIgnoreCase(currentUrl))
		{
			driver.navigate().to(currentUrl);
		}
	}
	
	}

