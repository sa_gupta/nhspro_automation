package com.ex2.nhspro.agent_testcases;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;

public class MyConsumerPortal_testcases extends Initializations{
	
	String currentUrl="";
	int notfoundcount;
	int servererrorcount;
	int okurls;
	
	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();
	}
  
	@Test(description="To Verify My Consumer Portal Functionality On Agent Side",groups= {"AgentFlow","smokeTest"})
	public void validate_myconsumerportal_functionality_on_agent_side() {
		loginOrNavigate();
    System.out.println(myconsumerportalData.getJsonData(environment, "SiteUrl"));
    myconsumerportal.EditSiteUrl(myconsumerportalData.getJsonData(environment, "SiteUrl"));
    String FirstName=myconsumerportalData.getJsonData(environment, "FirstName");
    String LastName=myconsumerportalData.getJsonData(environment, "LastName");
    String Email =myconsumerportalData.getJsonData(environment, "Email");
    String Designation=myconsumerportalData.getJsonData(environment, "Designation");
    String OfficeName=myconsumerportalData.getJsonData(environment, "OfficeName");
    String Licence=myconsumerportalData.getJsonData(environment, "Licence");
    String BrokerWebsite=myconsumerportalData.getJsonData(environment, "BrokerWebsite");
    String Editor=myconsumerportalData.getJsonData(environment, "Editor");
    String Phone= myconsumerportalData.getJsonData(environment, "Phone");
    myconsumerportal.enterMyConsumerPortalAccountDetails(FirstName, LastName, Designation, Email, OfficeName, Licence, BrokerWebsite,Phone);
    myconsumerportal.enterTextInEditor(Editor);
    myconsumerportal.clickOnSaveButton();
    String messagetext=myconsumerportal.sucessMessagetext();
    Assert.assertEquals(messagetext, "Your ShowingNew.com site information was updated.","User is unable to update the information");
    myconsumerportal.clickOnOkButton();
    System.out.println("My ShowingNew Information Updated Sucessfully");
    
	}

	@Test(description="To Verify In My Consumer Portal Agent Is able to upload Images",groups= {"AgentFlow","regressionTest"})
	public void validate_myconsumerportal_uploadimage() {
		loginOrNavigate();
	myconsumerportal.uploadHeadShotImage();
    ActionsUtility.staticWait(2);
    myconsumerportal.uploadBrokerageImages();
    ActionsUtility.staticWait(2);
    myconsumerportal.selectBackgroundImage();
    myconsumerportal.clickOnSaveButton();
    myconsumerportal.validateImages();
	}
	
	
	@Test(description="To Verify My Consumer Portal Preview Functionality On Agent Side",groups= {"AgentFlow","regressionTest"})
	public void validate_myconsumerportal_preview_on_agent_side() {
		loginOrNavigate();
    String parentWinHandle = driver.getWindowHandle();
    myconsumerportal.clickOnPreview();
    ActionsUtility.staticWait(3);
    myconsumerportal.switchtopreviewPage();
    Assert.assertTrue(driver.getCurrentUrl().contains("myconsumerportalpreview"),"Preview Functionality On My Consumerportal Failed");
    System.out.println("Preview Functionality On My Consumerportal Verified");
    driver.close();
	driver.switchTo().window(parentWinHandle);
	}
	
/*	@Test(description="Restrict Urls",groups= {"AgentFlow","regressionTest"})
	public void validate_Restrict_Urls_On_Client_Side() {
		String sitemanger=driver.getCurrentUrl()+"sitemanager/";
	    driver.navigate().to(sitemanger);
	    myconsumerportal.loginSiteManager(commonData.getJsonData(environment, "SiteManagerEmail"),commonData.getJsonData(environment, "SiteManagerPassword"));
	    myconsumerportal.clickOnRestrictUrl();
	   List<WebElement> restrict= myconsumerportal.listOfRestrictUrls();
		int size=restrict.size();
		System.out.println("Total number of restrict urls are "+size);
		List<String> alltext = new ArrayList<String>();
		 myconsumerportal.printAllRestrictedUrls(restrict,alltext);
		  commonPage.openNewTab(); 
		 openClientApplication();
		 String showingnew= myconsumerportal.returnShowingNewUrl();
		  for(String urls:alltext) {
			  String url=showingnew+urls;
			  driver.navigate().to(url);
			  brokenLinks(url);
			  
		  }
		  System.out.println("404 URLS are = "+notfoundcount+" Ok URLS are = "+okurls+" Internal 500 Server error URLS ="+servererrorcount);
	     
	}
	*/
	
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
		    commonPage.clickOnMyConsumerPortal();
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	private void brokenLinks(String linkUrl)
	{
	        try 
	        {
	           URL url = new URL(linkUrl);
	           
	           HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
	           
	           httpURLConnect.setConnectTimeout(3000);
	           
	           httpURLConnect.connect();
	           
	           
	           if(httpURLConnect.getResponseCode()==200)
	           {
	        	   okurls++;
	               System.out.println(linkUrl+" - "+httpURLConnect.getResponseCode()+" "+httpURLConnect.getResponseMessage()+"-> "+myconsumerportal.getKeyActiveText());
	                
	           }
	           if(httpURLConnect.getResponseCode()==500)
	           {
	        	   servererrorcount++;
	               System.out.println(linkUrl+" - "+httpURLConnect.getResponseCode()+" "+httpURLConnect.getResponseMessage()+"-> "+myconsumerportal.getKeyActiveText());
	               
	           }
	          if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)  
	           {
	        	  notfoundcount++;
	               System.out.println(linkUrl+" - "+httpURLConnect.getResponseCode()+" "+httpURLConnect.getResponseMessage()+"-> "+myconsumerportal.getKeyActiveText());
	              
	           }
	          
	        } catch (Exception e) {
	           
	        }
			
	    } 
	
	
}
