package com.ex2.nhspro.agent_testcases;

import org.testng.Assert;

import org.testng.annotations.Test;

import com.ex2.nhspro.utility.Initializations;

public class MyAccount_testcases extends Initializations{
	
	
	
	/*
	 * @BeforeClass public void beforeStarting() { openAgentApplication(); }
	 */
	
	

	
	@Test(description="To Verify My Account Functionality On Agent Side",groups= {"regressionTest","smokeTest"})
	public void validate_myaccount_functionality_on_agent_side() {
		openAgentApplication();
	commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"), commonData.getJsonData(environment, "Password"));
    commonPage.clickOnMyAccount();
    myaccount.enterMyAccountDetails(myaccountData.getJsonData(environment, "FirstName"),myaccountData.getJsonData(environment, "LastName"),myaccountData.getJsonData(environment, "BrokerageName"),myaccountData.getJsonData(environment, "Email"),myaccountData.getJsonData(environment, "BussinessPhone"),myaccountData.getJsonData(environment, "Phone"),myaccountData.getJsonData(environment, "ZipCode"));
    myaccount.myAccount_titletext();
//    myaccount.selectEmailCheckbox();
//    myaccount.selectTextCheckbox();
    myaccount.selectNewsletterCheckbox();
    myaccount.clickOnSaveButton();
    String text= myaccount.sucessMessagetext();
    System.out.println(text);
    Assert.assertEquals(text, "Your personal information was updated.","User is unable to update the information");
    myaccount.clickOnOkButton();
    System.out.println("My Account Information Updated Sucessfully");
	}

}
