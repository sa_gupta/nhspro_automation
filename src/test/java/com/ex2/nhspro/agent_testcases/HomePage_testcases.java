package com.ex2.nhspro.agent_testcases;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.GetCommonData;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;


public class HomePage_testcases extends Initializations{
	
	String currentUrl="";
	@BeforeClass(groups = {"smokeTest","regressionTest","HappyPath"}, dependsOnMethods ={"intializeData"})
	public void beforeStarting() {
		openAgentApplication();
	}
	
	@Test(groups = {"HappyPath"},priority=1)
	public void validateElementsOnHomePage_BeforeLogin() {
		Assert.assertEquals(homePage.getSearchBoxElementsExistOnThePage(), GetCommonData.getSearchData(getCurrentUrl()));
		HashMap<String, String> actualbannerDetails = homePage.getBannerDetails();
		HashMap<String, String> expectedbannerDetails = GetCommonData.getBannerDetails();
		Assert.assertEquals(homePage.getNavigationToConsumerPortallink(), homeData.getJsonData("Navigation", "ConsumerBeforelogin"));
		Assert.assertTrue(homePage.isSignUpForWbinarLinkExist());
		Assert.assertEquals(homePage.getsGuideSectionData(), GetCommonData.getGuideSectionDetails());
		Assert.assertEquals(homePage.getHomeBuilderSectionData(),GetCommonData.getHomebuildersData(getCurrentUrl()));
		homePage.validateAgentsSection();
		Assert.assertEquals(homePage.getStartFreshSection(), GetCommonData.getStartFreshDetails());
		Assert.assertEquals(homePage.getLearnHowToSellData(), GetCommonData.getLearnHowToSellDetails());		
	//	Assert.assertEquals(actualbannerDetails, expectedbannerDetails);
	}
	
	@Test(groups = {"HappyPath"},priority=2)
	public void validateElementsOnHomePageAfterLogin() {
		loginOrNavigate();
		Assert.assertEquals(homePage.getSearchBoxElementsExistOnThePage(), GetCommonData.getSearchData(getCurrentUrl()));
		HashMap<String, String> actualbannerDetails = homePage.getBannerDetails();
		HashMap<String, String> expectedbannerDetails = GetCommonData.getBannerDetails();
//		Assert.assertEquals(actualbannerDetails, expectedbannerDetails);
		Assert.assertEquals(homePage.getNavigationToConsumerPortallink(), homeData.getJsonData("Navigation", "ConsumerAfterLogin"));
		Assert.assertTrue(homePage.isSignUpForWbinarLinkExist());
		Assert.assertEquals(homePage.getsGuideSectionData(), GetCommonData.getGuideSectionDetails());
		Assert.assertEquals(homePage.getHomeBuilderSectionData(),GetCommonData.getHomebuildersData(getCurrentUrl()));
		homePage.validateAgentsSection();
		Assert.assertEquals(homePage.getStartFreshSection(), GetCommonData.getStartFreshDetails());
		Assert.assertEquals(homePage.getLearnHowToSellData(), GetCommonData.getLearnHowToSellDetails());
	}
	
	@Test
	public void bottomeLabel() {
	System.out.println(homePage.getSearchBoxElementsExistOnThePage());
	System.out.println(homePage.getBannerDetails());
	System.out.println(homePage.getNavigationToConsumerPortallink());
	System.out.println(homePage.isSignUpForWbinarLinkExist());
	System.out.println(homePage.getsGuideSectionData());
	System.out.println(homePage.getHomeBuilderSectionData());
	homePage.validateAgentsSection();
	System.out.println(homePage.getStartFreshSection());
	System.out.println(homePage.getLearnHowToSellData());
	}
	
	
	/**
	 * @author sagupta
	 *
	 */
	@Test(description="To verify searching is working from location handler agent side",groups= {"AgentFlow","regressionTest"})
	public void validate_Searching_from_location_handler_at_agent_side() {
		loginOrNavigate();
		homePage.enterSearchData_field("wrongData");
		homePage.clickOnSearch_icon();	
		Assert.assertTrue(locationHandler.isUserOn_Location_HandlerPage(), "User is on "+driver.getCurrentUrl());
		locationHandler.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		locationHandler.clickOnSearch_icon();
		ActionsUtility.staticWait(4);
		  String locationtext=srpPage.getSearchLocationText();
		 Assert.assertEquals(locationtext, homeData.getJsonData("Search_data","Matching_Searched_data"),"Mismatch");
		 ActionsUtility.printLog("Assertion Passed:- Actual Text is same as Expected");
	}

	/**
	 * @author ritkumar
	 */
	@Test(description="Validate Agent Search From Home Page Without Filters", groups = { "smokeTest" })
	public void validate_agent_search_from_home_page_without_filters()
	{
		loginOrNavigate();
		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
	    homePage.clickOnSearch_icon();
	    srpPage.closeSRPEnageClientPopUp();
	    String locationtext=srpPage.getSearchLocationText();
	    System.out.println(locationtext);
	    Assert.assertTrue(locationtext.contains(homeData.getJsonData("Search_data","Matching_Searched_data")),"Location is not correct");
	    Assert.assertTrue(driver.getTitle().contains(homeData.getJsonData("Search_data","Matching_Searched_data")));
	    System.out.println("Agent Search From Home Page Without Filters Verified");   
	}
	
	@Test(description="Validate clarity script for Agent site",groups = { "smokeTest" })
	public void validate_clarity_script_for_agent()
	{
		loginOrNavigate();
		String sourceCode=driver.getPageSource();
		Assert.assertTrue(sourceCode.contains(homeData.getJsonData("ClarityScriptForAgent", "Agentclarityscript")), "Clarity script for agentsite is not available");
		System.out.println("Clarity Script for Agentsite is Verfied Successfully");
	} 

	/**
	 * @author ritkumar
	 */
	@Test(description="Validate Agent Search From Home Page With Filters", groups = { "smokeTest" })
	public void validate_agent_search_from_home_page_with_filters()
	{
		  String minPrice="100000";
		  loginOrNavigate();
		  homePage.enterSearchData_field("Dallas, TX Area");
	    homePage.select_minprice(minPrice);
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	minPrice=homePage.getSelectedMinPrice();
	    }
	    homePage.clickOnSearch_icon();
	    String locationtext=driver.getCurrentUrl();
	    String MinAmount=minPrice.replace(",", "").replace("$", "");
	    System.out.println(locationtext);
	    Assert.assertTrue(locationtext.contains("pricelow="+MinAmount),"Min Price Filter Does Not Matched");
	    Assert.assertTrue(driver.getTitle().contains("Dallas, TX"));
	    Assert.assertTrue(driver.getCurrentUrl().contains("dallas"));
	    System.out.println("Agent Search From Home Page With Filters Verified");   
	
	}
	
	
	@Test(description="Validate Agent Search From Home Page With Bedrooms Filter" ,groups = { "regressionTest" })
	public void validate_agent_search_from_home_page_with_bedrooms_filters()
	{
		loginOrNavigate();
		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		 String beds=homeData.getJsonData("Search_data","Bedrooms");
	    homePage.select_beds(beds);
	    homePage.clickOnSearch_icon();
	    String locationtext=driver.getCurrentUrl();
	    System.out.println(locationtext);
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	filter.verifyBedroomsAndColorInMobile(beds);
	    	Assert.assertTrue(locationtext.contains("bedrooms="+beds),"Bedrooms Filter Does Not Matched On SRP URL");
	    	
	    }
	    else
	    {
	    srpPage.closeSRPEnageClientPopUp();
	    String bedroomtext=filter.getExactSelectedBedrooms();
	    System.out.println("Number Of Bedrooms on SRP "+bedroomtext);
	    Assert.assertTrue(locationtext.contains("bedrooms="+beds),"Bedrooms Filter Does Not Matched On SRP URL");
	    Assert.assertTrue(bedroomtext.equalsIgnoreCase(beds),"Bedrooms Filter Does Not Matched On SRP Page");  
	    }
	    System.out.println("Agent Search From Home Page With Bedsrooms Filter Verified");
	}
	
	@Test(description="Validate Agent Search From Home Page With Bathrooms Filter",groups = {"regressionTest"})
	public void validate_agent_search_from_home_page_with_bathrooms_filters()
	{
		loginOrNavigate();
		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		 String bathrooms=homeData.getJsonData("Search_data","Bathrooms");
	    homePage.select_bath(bathrooms);
	    homePage.clickOnSearch_icon();
	    String locationtext=driver.getCurrentUrl();
	    System.out.println(locationtext);
		  if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
		  filter.verifyBathroomsAndColorInMobile(bathrooms);
		  Assert.assertTrue(locationtext.contains("bathrooms="+bathrooms),"Bathrooms Filter Does Not Matched On SRP URL");
		 } 
		  else
		  {
	    srpPage.closeSRPEnageClientPopUp();
	    String bathroomtext=filter.getSelectedBathrooms();
	    System.out.println(bathroomtext);
	    Assert.assertTrue(locationtext.contains("bathrooms="+bathrooms),"Bathrooms Filter Does Not Matched On SRP URL");
	    Assert.assertTrue(bathroomtext.contains(bathrooms),"Bathrooms Filter Does Not Matched On SRP Page");   
	    }
		  System.out.println("Agent Search From Home Page With Bathrooms Filter Verified"); 
	}
	
	
	@Test(description="Validate Agent Search From Home Page With Min And Max Filter",groups = {"regressionTest"})
	public void validate_agent_search_from_home_page_with_min_max_filters()
	{
		loginOrNavigate();
		 String minPrice=homeData.getJsonData("Search_data","Min_Price");
		 String maxPrice=homeData.getJsonData("Search_data","Max_Price");
		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		String minValuetext=homePage.select_minprice(minPrice);
		String maxvaluetext=homePage.select_maxprice(maxPrice);
	    homePage.clickOnSearch_icon();
	    String locationtext=driver.getCurrentUrl();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println(locationtext);
			String minvalue=minValuetext.replace("$", "").replace(",","");
			String maxvalue=maxvaluetext.replace("$", "").replace(",","");
			 Assert.assertTrue(locationtext.contains("pricelow="+minvalue+"&pricehigh="+maxvalue),"Min and Max Price Filter Does Not Matched On SRP URL" );	
			 filter.verifyMin_Max_PriceInMobile(minValuetext,maxvaluetext);
	    } 
	    else
	    {
	    srpPage.closeSRPEnageClientPopUp();
	    String Price=filter.getPriceValue();
	    String replaceText=Price.replace("$","").replace("K","000");
		String[] split= replaceText.split(" ");
		String minprice=split[0];
		String maxprice=split[2];
		System.out.println("Min Price On SRP "+minprice);
		System.out.println("Max Price On SRP "+maxprice);
	    System.out.println(Price);
	    System.out.println(locationtext);
	   Assert.assertTrue(locationtext.contains("pricelow="+minPrice+"&pricehigh="+maxPrice),"Min and Max Price Filter Does Not Matched On SRP URL" );
	   Assert.assertTrue(minprice.equalsIgnoreCase(minPrice),"Min Price Filter Does Not Matched On SRP");
	   Assert.assertTrue(maxprice.equalsIgnoreCase(maxPrice),"Max Price Filter Does Not Matched On SRP"); 
	    }
	    System.out.println("Agent Search From Home Page With Min And Max Filter Verified");  
	}
	
	@Test(description="Validate Agent Search From Home Page With Square Feet Filter",groups = {"regressionTest"})
	public void validate_agent_search_from_home_page_with_square_feet_filters()
	{
		loginOrNavigate();
		String Squarefeet=homeData.getJsonData("Search_data","SquareFeet");
		homePage.enterSearchData_field(homeData.getJsonData("Search_data","Matching_Searched_data"));
		String sqtmobile=homePage.select_squareFeet(Squarefeet);
	    homePage.clickOnSearch_icon();
	    String locationtext=driver.getCurrentUrl();
	    if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
	    	  System.out.println(locationtext);
	    	  Assert.assertTrue(locationtext.contains("sqftlow="+sqtmobile),"Square Feet Filter Does Not Matched On SRP URL");
	    	  filter.verifySqaureFeetInMobile(sqtmobile);	 
	    } 
			  else
			  {
	    srpPage.closeSRPEnageClientPopUp();
	    String Sqtfeet=filter.getExactSquareFeet();
	    System.out.println(locationtext);
       Assert.assertTrue(locationtext.contains("sqftlow="+Squarefeet),"Square Feet Filter Does Not Matched On SRP URL");
       Assert.assertTrue(Sqtfeet.contains(Squarefeet),"Square Feet Filter Does Not Matched On SRP Page");
	}
	    System.out.println("Agent Search From Home Page With Square Feet Filter Verified");
	}
	
	/* Author Shankit */
	@Test(description="Validation of a Webinar Page", groups= {"regressionTest","smokeTest"})
	public void validate_Webinar_Page() {
		loginOrNavigate();
		commonPage.clickOnWebinarLink();
		String title = homePage.getWebinarPageTitle();
		Assert.assertEquals("NewHomeSource Professional Events & Webinars", title, "Webinar Page is not available");
		System.out.println("Webinar Page is verified");
	}
	
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			currentUrl=driver.getCurrentUrl();
			
		}
		else
			driver.navigate().to(currentUrl);			
	}
	
}
