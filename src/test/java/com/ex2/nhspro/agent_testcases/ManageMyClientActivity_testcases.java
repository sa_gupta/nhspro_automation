package com.ex2.nhspro.agent_testcases;


import org.testng.SkipException;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class ManageMyClientActivity_testcases extends Initializations{
	
	

	
	@Test(description = "Manage My Client Activity At Agent Site", groups = { "smokeTest" })
	public void validate_ManageClientActivity_AgentApplication() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			System.out.println("Mobile does not have Manage my client activity feature");
			throw new SkipException("Skipping this exception");

		} else {
		openAgentApplication();
		commonPage.loginIntoApplication(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));	
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
//	   int intial_notificationCount = managemyclientactivity.getNotificationCount();
	    commonPage.clickOnMyConsumerPortal();
	    ActionsUtility.staticWait(2);
	    myconsumerportal.clickShowingAgentLink();
	    managemyclientactivity.switchtoWindowAndLogin(commonData.getJsonData(environment, "EmailId_client"),"Austin, TX Area");
	//    ActionsUtility.staticWait(60);
//	    int after_notificationCount = managemyclientactivity.getNotificationCount();
	    ActionsUtility.staticWait(2);
	    managemyclientactivity.markAsRead();
//	    Assert.assertEquals(after_notificationCount, intial_notificationCount+1);
	    
	}
	}
	

}
