package com.ex2.nhspro.agent_testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;

public class Widget_testcases extends Initializations {

	String currentUrl = "";

	@BeforeClass(groups = {"smokeTest","regressionTest"})
	public void beforeStarting() {
		openAgentApplication();	
	}
	

	@Test(description = "To verify Widget Page Loaded Properly Or Not on agent side", groups = { "smokeTest", "regressionTest", "AgentFlow" })
	public void verifyWidgetPageLoadedProperlyOrNot() {
		loginOrNavigate();
		String widgetText = widget.getWidgetTitleText();
		Assert.assertTrue(widgetText.contains("Create Widgets For Your Site"));
		Assert.assertTrue(driver.getCurrentUrl().contains("widget"), "Widget page unable to load");
		ActionsUtility
				.printLog("---------------------Assertion Passed:- Widget Page loaded properly------------------");
	}

	@Test(description = "To verify User Should Be Able To Search From Widget Page on agent side")
	public void verify_User_Should_Be_Able_To_Search_From_Widget_Page() {
		loginOrNavigate();
		widget.enterLocationAndClickOnSearchIcon(homeData.getJsonData("Search_data", "Matching_Searched_data"),
				homeData.getJsonData("Search_data", "Min_Price"), homeData.getJsonData("Search_data", "Max_Price"),
				homeData.getJsonData("Search_data", "Bedrooms"), homeData.getJsonData("Search_data", "Bathrooms"),
				homeData.getJsonData("Search_data", "SquareFeet"));
		commonPage.switchtoWindow();
		ActionsUtility.staticWait(4);
		String locationSearchedtextFromSRP = srpPage.getSearchLocationText();
		Assert.assertEquals(locationSearchedtextFromSRP, homeData.getJsonData("Search_data", "Matching_Searched_data"),
				"SRP location text is not similar from widget text ");
		ActionsUtility.printLog("Assertion Passed:- SRP text is same as from Widget Searched text");
		String minPrice = homeData.getJsonData("Search_data", "Min_Price");
		String maxPrice = homeData.getJsonData("Search_data", "Max_Price");
		String Squarefeet = homeData.getJsonData("Search_data", "SquareFeet");
		String bathrooms = homeData.getJsonData("Search_data", "Bathrooms");
		String beds = homeData.getJsonData("Search_data", "Bedrooms");
		String Price = filter.getPriceValue();
		String replaceText = Price.replace("$", "").replace("K", "000");
		String[] split = replaceText.split(" ");
		String minprice = split[0];
		String maxprice = split[2];
		Assert.assertTrue(driver.getCurrentUrl().contains("pricelow=" + minPrice + "&pricehigh=" + maxPrice),
				"Min and Max Price Filter Does Not Matched On SRP URL");
		Assert.assertTrue(minprice.equalsIgnoreCase(minPrice), "Min Price Filter Does Not Matched On SRP");
		Assert.assertTrue(maxprice.equalsIgnoreCase(maxPrice), "Max Price Filter Does Not Matched On SRP");
		ActionsUtility.printLog("Min And Max Price Verified Successfully On SRP Page Of Showing New");
		String bedroomtext = filter.getExactSelectedBedrooms();
		Assert.assertTrue(driver.getCurrentUrl().contains("bedrooms=" + beds),
				"Bedrooms Filter Does Not Matched On SRP URL");
		Assert.assertTrue(bedroomtext.equalsIgnoreCase(beds), "Bedrooms Filter Does Not Matched");
		ActionsUtility.printLog("Bedrooms Verified Successfully On SRP Page Of Showing New");
		String bathroomtext = filter.getSelectedBathrooms();
		Assert.assertTrue(driver.getCurrentUrl().contains("bathrooms=" + bathrooms),
				"Bathrooms Filter Does Not Matched On SRP URL");
		Assert.assertTrue(bathroomtext.contains(bathrooms), "Bathrooms Filter Does Not Matched On SRP Page");
		ActionsUtility.printLog("Bathrooms Verified Successfully On SRP Page Of Showing New");
		String Sqtfeet = filter.getExactSquareFeet();
		Assert.assertTrue(driver.getCurrentUrl().contains("sqftlow=" + Squarefeet),
				"Square Feet Filter Does Not Matched On SRP URL");
		Assert.assertTrue(Sqtfeet.contains(Squarefeet), "Square Feet Filter Does Not Matched On SRP Page");
		ActionsUtility.printLog("SquareFeet Verified Successfully On SRP Page Of Showing New");
	}

	private void loginOrNavigate() {
		if (currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "ImportId"),
					commonData.getJsonData(environment, "Password"));
			commonPage.clickOnWidget();
			currentUrl = driver.getCurrentUrl();
		} else
			driver.navigate().to(currentUrl);
	}

}
