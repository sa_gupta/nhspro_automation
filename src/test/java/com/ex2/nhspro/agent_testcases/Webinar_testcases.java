package com.ex2.nhspro.agent_testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.GetCommonData;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class Webinar_testcases extends Initializations{
	
	String currentUrl="";
	@BeforeClass(groups = {"smokeTest","regressionTest","HappyPath"}, dependsOnMethods = {"intializeData"})
	public void beforeStarting() {
		openAgentApplication();
	}
	
	@Test(description = "To verify webinar page loaded properly or not on agent side", groups = {"smokeTest","regressionTest"}, priority=1)
	public void verifyWebinarPageLoadedProperlyOrNot() {
		loginOrNavigate();
		System.out.println(driver.getCurrentUrl());
		Assert.assertTrue(driver.getCurrentUrl().contains("webinars"), "---------------------Assertion Failed:- Webinar is not able to load properly.------------------");
		String webinarTitleText = webinar.getWebinarTitleText();
		Assert.assertEquals(webinarTitleText, webinarData.getJsonData("Webinar_Main_Page", "Title"), "Webinar Page Title doesn't match requirements");
		System.out.println("Webinar Title Text Assertion is Passed");
		ActionsUtility.printLog("---------------------Assertion Passed:- Webinar Page loaded properly------------------");
	}
	
	@Test(description = "To verify whether first webinar image is present or not", groups = {"regressionTest"} )
	public void verifyFirstWebinarImage() {
		loginOrNavigate();
		WebElement firstWebinarImage = webinar.getFirstWebinarImage();
		ActionsUtility.printLog("Getting first Image of the Webinar");
		Boolean p = (Boolean) ((JavascriptExecutor)driver) .executeScript("return arguments[0].complete " + "&& typeof arguments[0].naturalWidth != \"undefined\" " + "&& arguments[0].naturalWidth > 0", firstWebinarImage);
		ActionsUtility.printLog("Verifying the first Image of the Webinar");
		 if (p) {
	         ActionsUtility.printLog("---------------------Assertion Passed:- First Webinar Image is verified------------------");
	      } else {
	         System.out.println("---------------------Assertion Failed:- First Webinar Image is not verified------------------");
	      }
	}
	
	@Test(description = "To verify Whether "+"'"+"Register Now"+"'"+" button is present or not on first webinar", groups = {"smokeTest", "regressionTest"})
	public void verifyRegisterNowButtonPresentOrNot() {
		loginOrNavigate();
		WebElement registerNowButton = webinar.getRegisterNowButton();
		boolean registerNowButtonDisplayStatus = registerNowButton.isDisplayed();
		ActionsUtility.printLog("Verifying whether Register Now Button is displayed or not");
		boolean registerNowButtonEnabledStatus = registerNowButton.isEnabled();
		ActionsUtility.printLog("Verifying whether Register Now Button is Enabled or not");
		if(registerNowButtonDisplayStatus && registerNowButtonEnabledStatus) {
			ActionsUtility.printLog("---------------------Assertion Passed:- Register Now Button is displayed and enabled------------------");
		} else {
			System.out.println("---------------------Register Now Button is not verified------------------");
		}
	}
	
	@Test(description = "To Verify whether first detail webinar page is loaded or Not", groups = {"smokeTest", "regressionTest"})
	public void verifyWebinarDetailPageIsLoadedOrNot() {
		loginOrNavigate();
		ActionsUtility.printLog("Clicking on the Main Webinar Page \"Register Now\" button");
		webinar.getRegisterNowButton().click();
		System.out.println("printing the current URL :- "+ driver.getCurrentUrl());
		Assert.assertTrue(driver.getCurrentUrl().contains("webinars/live"), "---------------------Assertion Failed:- Webinar Detail Page is not able to load properly.------------------");
		String detailPageWebinarTitleText = webinar.getDetailPageWebinarTitleText();
		ActionsUtility.printLog("printing the detail Webinar page title text :- "+detailPageWebinarTitleText);
		Assert.assertEquals(detailPageWebinarTitleText, webinarData.getJsonData("Webinar_Detail_Page", "Title"), "Webinar Detail Page Title doesn't match requirements");
		System.out.println("Webinar Title Text Assertion is Passed");
		ActionsUtility.printLog("---------------------Assertion Passed:- Detail Webinar Page loaded properly------------------");
	}
	
	@Test(description = "To Verify Registeration From on detail webinar page", groups = {"smokeTest", "regressionTest"})
	public void verifyWebinarDetailPageRegisterationForm() {
		loginOrNavigate();
		ActionsUtility.printLog("Clicking on the Main Webinar Page \"Register Now\" button");
		webinar.getRegisterNowButton().click();
		String registerationFormTitleText = webinar.getRegisterationFormTitleText();
		ActionsUtility.printLog("Detail Page webinar Title Text is :- "+ registerationFormTitleText);
		Assert.assertEquals(registerationFormTitleText, webinarData.getJsonData("Webinar_Detail_Page", "RegisterationFormTitle"), "Webinar Registeration Form Title doesn't match requirements");
		ActionsUtility.printLog("Webinar detail Page RegisterationForm Title Text has been verified.");
		WebElement detailWebinarPageRegisterNowButton = webinar.getDetailWebinarPageRegisterNowButton();
		ActionsUtility.printLog("Checking \" Register Now \" button status");
		boolean detailWebinarPageRegisterNowButtonEnabledStatus = detailWebinarPageRegisterNowButton.isEnabled();
		System.out.println(" \" Register Now \" button status is "+detailWebinarPageRegisterNowButtonEnabledStatus);
		ActionsUtility.printLog("---------------------Assertion Passed:- Webinar Detail Page Registeration Form has been verified------------------");
	}
	
	@Test(description = "To verify Registering on a webinar", groups= {"smokeTest", "regressionTest"})
	public void verifyRegisterationOnWebinar() {
		if(PropertyFileOperation.getPropertyValue("environment").equalsIgnoreCase("production")) {
			System.out.println("There is no qa webinar available as we are in production environment.");
		}else {
		loginOrNavigate();
		commonPage.scroll(0, 1000);
		webinar.getAllWebinar();
		webinar.getTestingWebinar().click();
		System.out.println("you are on testing webinar");
		ActionsUtility.printLog("AutoFill first Name in the field is "+webinar.getRegistrationFormFirstName());
		Assert.assertEquals(webinar.getRegistrationFormFirstName(), commonData.getJsonData(environment, "agentFirstName"), "First Name doesn't match requirement");
		ActionsUtility.printLog("AutoFill FirstName Assertion has been Passed");
		ActionsUtility.printLog("AutoFill Last Name in the field is "+webinar.getRegistrationFormLastName());
		Assert.assertEquals(webinar.getRegistrationFormLastName(), commonData.getJsonData(environment, "agentLastName"), "Last Name doesn't match requirement");
		ActionsUtility.printLog("AutoFill LastName Assertion has been Passed");
		ActionsUtility.printLog("AutoFill EMail in the field is "+webinar.getRegistrationFormEMail());
		Assert.assertEquals(webinar.getRegistrationFormEMail(), commonData.getJsonData(environment, "EmailId_agent"), "Email Id doesn't match requirement");
		ActionsUtility.printLog("AutoFill EMail Assertion has been Passed");
		ActionsUtility.staticWait(2);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", webinar.registerationFormCheckbox());
		ActionsUtility.printLog("CheckBox has been Clicked");
	    ActionsUtility.staticWait(2);
	    executor.executeScript("arguments[0].click();", webinar.getRegisterNowButtonWebinarDetail());
		ActionsUtility.printLog("RegisterNow Button has been Clicked");
        Assert.assertEquals(webinar.getSuccessMessageText(), webinarData.getJsonData("Webinar_Detail_Page", "SuccessMessageText").replaceFirst("â€™", "’"), "Webinar Registration doesn't went successfull");
        System.out.println("----------------------------Registeration on the QA Webinar has been verified successfully------------------------------------");
		}
	}
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			commonPage.clickOnWebinarLink();
			currentUrl = driver.getCurrentUrl();
		} else {
			driver.navigate().to(currentUrl);
		}
		    ActionsUtility.printLog("User is on the Webinar Page");
	}
}