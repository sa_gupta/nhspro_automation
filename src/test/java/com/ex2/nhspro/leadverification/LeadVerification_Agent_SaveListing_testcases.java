package com.ex2.nhspro.leadverification;

import java.util.List;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class LeadVerification_Agent_SaveListing_testcases extends Initializations{

	
	
	String currentUrl="";


	@BeforeClass
	public void beforeStarting() {
		openAgentApplication();
	}

	@BeforeMethod
	public void beforeEveryTest() {
		database.createDBConnection();
		
	}

	@AfterMethod
	public void DBclose() {
		database.closeConnection();
	}
	
	@Test(description = "To verify lead is generating from Send button on home tab", groups = {"Regression"})
	public void verifyLeadGenerating_HomeTab_Send_SaveListingPage_AgentSide() {
			loginOrNavigate();		
		  database.executeLeadQuery(commonData.getJsonData(environment,"EmailId_agent")); 
		  String BeforeLeadGeneration_RequestId =database.RequestId;
		  commonPage.clickOnHomes_tab();
		  saveListingPage.selectParticluarHome(1);
		  String sentInfo = saveListingPage.getSentInfo(1);
		  ActionsUtility.staticWait(3);
		  saveListingPage.clickOnSend_btn();
		  saveListingPage.clickOnSendAgain();
		  saveListingPage.performSendOperation(sentInfo);
		  List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		  ActionsUtility.staticWait(2);
		  database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs","slfc", idOfHomesOnSaveListing.get(0), "NULL");
			else
				database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs","m - slfc", idOfHomesOnSaveListing.get(0), "NULL");

	}
	
	@Test
	public void verifyLeadGenerating_HomeTab_CopyToClipboard_SaveListing_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnHomes_tab();
		 ActionsUtility.staticWait(3);
		saveListingPage.selectParticluarHome(1);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		  String sentInfo = saveListingPage.getSentInfo(1);
		  ActionsUtility.staticWait(3);
		  saveListingPage.clickOnSend_btn();
		  ActionsUtility.staticWait(2);
		  saveListingPage.openPreviewBasedOnSendStatus(sentInfo);
		  ActionsUtility.staticWait(2);
		  commonOperation.performCopyToClipboardOperationFromPreview();
		  ActionsUtility.staticWait(3);
		  database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"), "HM",
					"hrs", "slfc", idOfHomesOnSaveListing.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"), "HM",
					"hrs", "m - slfc", idOfHomesOnSaveListing.get(0), "NULL");
	}
	
	@Test
	public void verifyLeadGenerating_HomeTab_PrintReport_SaveListing_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnHomes_tab();
		saveListingPage.selectParticluarHome(1);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		saveListingPage.clickOnPrintReport_btn();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "hrs", "prfc",
				idOfHomesOnSaveListing.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "hrs", "m - prfc",
					idOfHomesOnSaveListing.get(0), "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
	}
	
	@Test(enabled=false)
	public void verifyLeadGenerating_HomeTab_RequestToRegisterClient_SaveListing_AgentSide() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
			System.out.println("Mobile does not Share Search Functionality");
			throw new SkipException("Skipping this exception");
			
		}
		else
		{ 
			loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnHomes_tab();
		 List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		 saveListingPage.hoverToHome(1);
		 ActionsUtility.staticWait(5);
		 saveListingPage.clickOn_RequestToRegisterClient_btn_Preview();
		 srpPage.clickOn_RequestToRegister_from_popup();
		 ActionsUtility.staticWait(1);
		 srpPage.clickOnOk_button();
		 ActionsUtility.staticWait(3);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "HM", "prev_home", "rtrc",idOfHomesOnSaveListing.get(0), "NULL");

		}
		
	}
	
	@Test(description = "To verify lead is generating from Send button on Community tab", groups = {"regressionTest"})
	public void verifyLeadGenerating_CommunityTab_Send_SaveListingPage_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;		
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		saveListingPage.selectParticluarHome(1);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		  String sentInfo = saveListingPage.getSentInfo(1);
		  ActionsUtility.staticWait(2);
		  saveListingPage.clickOnSend_btn();
		  saveListingPage.clickOnSendAgain();
		  ActionsUtility.staticWait(3);
		  saveListingPage.performSendOperation(sentInfo);
		  ActionsUtility.staticWait(2);
		  database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"), "COM",
					"crs", "slfc", idOfHomesOnSaveListing.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"), "COM",
					"crs", "m - slfc", idOfHomesOnSaveListing.get(0), "NULL");
	}
	
	@Test
	public void verifyLeadGenerating_CommunityTab_CopyToClipboard_SaveListing_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(4);
		saveListingPage.selectParticluarHome(1);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		 System.out.println("Id Of Homes on Saved Listing "+idOfHomesOnSaveListing);
		String SendInfo = saveListingPage.getSentInfo(1);
		  saveListingPage.clickOnSend_btn();
		  saveListingPage.openPreviewBasedOnSendStatus(SendInfo);
		  ActionsUtility.staticWait(2);
		  commonOperation.performCopyToClipboardOperationFromPreview();
		//DB validation		
		  ActionsUtility.staticWait(2);
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
					"slfc", idOfHomesOnSaveListing.get(0), "NULL");
			else
				database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
						"m - slfc", idOfHomesOnSaveListing.get(0), "NULL");
		
	}
	
	@Test
	public void verifyLeadGenerating_CommunityTab_PrintReport_SaveListing_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(4);
		saveListingPage.selectParticluarHome(1);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomesFromSavedListing(1);
		saveListingPage.clickOnPrintReport_btn();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "crs", "prfc",
				idOfHomesOnSaveListing.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "crs", "m - prfc",
					idOfHomesOnSaveListing.get(0), "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
	}
	
	@Test(enabled=false)
	public void verifyLeadGenerating_CommunityTab_RequestToRegisterClient_SaveListing_AgentSide() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
			System.out.println("Mobile does not Share Search Functionality");
			throw new SkipException("Skipping this exception");
			
		}
		else
		{ 
			loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnCommunities_tab();
			ActionsUtility.staticWait(4);
		 List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		 saveListingPage.hoverToCommunity(1);
		 ActionsUtility.staticWait(5);
		 saveListingPage.clickOn_RequestToRegisterClient_btn_Preview();
		 srpPage.clickOn_RequestToRegister_from_popup();
		 srpPage.clickOnOk_button();
		 ActionsUtility.staticWait(2);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "COM", "prev_com", "rtrc",idOfHomesOnSaveListing.get(0), "NULL");
		}
	}
	
	@Test(enabled = false)
	public void verifyLeadGenerating_CommunityTab_ScheduleTour_MessageLightBox_SaveListing_AgentSide() {
		String listingType = "normal";
		loginOrNavigateforMessageLightBox(listingType);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnCommunities_tab();
			ActionsUtility.staticWait(4);
		 List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		 commonOperation.clickOnCommentIconFromSaveListing(1);
		 commonOperation.clickOnScheduleTourButton();
		 homeDetailPage.enterMessageInRequestAnApprointment(leadData.getJsonData("Details_agent", "Request_an_AppointmentData"));
		 homeDetailPage.clickOnRequest_an_appointment_btn();
		 ActionsUtility.staticWait(5);
		 database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		 if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
				database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("Details_agent", "Request_an_Appointment")+leadData.getJsonData("Details_agent", "Request_an_AppointmentData"), "COM", "comdetail", "ra",idOfHomesOnSaveListing.get(0), "NULL");
				}
		 else
			 database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("Details_agent", "Request_an_Appointment")+leadData.getJsonData("Details_agent", "Request_an_AppointmentData"), "COM", "comdetail", "ra",idOfHomesOnSaveListing.get(0), "NULL");
	}

	@Test(enabled = false)
	public void verifyLeadGenerating_CommunityTab_ContactSalesAgent_MessageLightBox_SaveListing_AgentSide() {
		String listingType = "comingSoon";
		loginOrNavigateforMessageLightBox(listingType);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnCommunities_tab();
			ActionsUtility.staticWait(4);
		 List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		 commonOperation.clickOnCommentIconFromSaveListing(1);
		 commonOperation.clickOnScheduleTourButton();
		 homeDetailPage.enterMessageInContactToBuilderMessageLightBox(leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"));
			homeDetailPage.clickOnContactAgentSales_btnInMessageLightBox();
			ActionsUtility.staticWait(5);
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
				database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "COM", "comdetail", "cb", idOfHomesOnSaveListing.get(0), "NULL");
			else
				database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "COM", "comdetail", "cb", idOfHomesOnSaveListing.get(0), "NULL");
		
	}
	
	@Test(enabled = false)
	public void verifyLeadGenerating_CommunityTab_PrintReport_MessageLightBox_SaveListing_AgentSide() {
		String listingType = "notBilled";
		loginOrNavigateforMessageLightBox(listingType);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnCommunities_tab();
			ActionsUtility.staticWait(4);
		 List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		 commonOperation.clickOnCommentIconFromSaveListing(1);
		 commonOperation.clickOnPrintReportMessageLightBox();
		 String parentWinHandle = driver.getWindowHandle();
		 srpPage.clickOnClientVersionPrintReport();
		 srpPage.switchTowindow();
		 ActionsUtility.staticWait(5);
		 database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
				database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "PrintPDF"),
						"COM", "comdetail", "prfc", idOfHomesOnSaveListing.get(0), "NULL");
			else
				database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "PrintPDF"),
						"COM", "comdetail", "m - prfc", idOfHomesOnSaveListing.get(0), "NULL");
			driver.close();
			driver.switchTo().window(parentWinHandle);
			commonOperation.clickOnClose_icon();
	}
	
	public void loginOrNavigateforMessageLightBox(String listingType) {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));	
			commonOperation.clearCompleteSaveListingData(ClientNameforSaveListing);
		//	saveListingPage.clickOnBack_link();		
			commonPage.clickOnLogo();
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			homePage.clickOnSearch_icon();
			srpPage.clickOn_MoreFilterButton();
		    srpPage.clickOn_CommunityStatus(listingType);
		    srpPage.clickOn_UpdateButtonUnderMoreFilterSection();
			commonPage.clickOnCommunities_tab();
			ActionsUtility.staticWait(4);
			int homeNo = 1;
		    communityDetailPage.Save_Listing(homeNo,listingType);
		    Assert.assertTrue(srpPage.isSavdListing_popup_exist());
			srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
			ActionsUtility.staticWait(2);
			srpPage.clickOnSave_btn();
			srpPage.clickOnOk_button();
			driver.navigate().back();
			if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
				driver.navigate().refresh();
			commonPage.hoverToSaveListing();
			ActionsUtility.staticWait(3);
			srpPage.openSaveListing_perClient(ClientNameforSaveListing);
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));	
			commonOperation.clickOnNoThanksOnNotificationPopUp();
			commonOperation.clearCompleteSaveListingData(ClientNameforSaveListing);
		//	saveListingPage.clickOnBack_link();		
			commonPage.clickOnLogo();
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			homePage.clickOnSearch_icon();
			commonPage.clickOnHomes_tab();
			commonPage.scroll(300, 0);
			srpPage.selectParticularHome(4);
			srpPage.clickOn_Main_SaveListing();
			Assert.assertTrue(srpPage.isSavdListing_popup_exist());
			srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
			ActionsUtility.staticWait(2);
			srpPage.clickOnSave_btn();
			srpPage.clickOnOk_button();
			commonPage.clickOnCommunities_tab();
			ActionsUtility.staticWait(4);
			commonPage.scroll(300, 0);
			srpPage.selectParticularHome(4);
			srpPage.clickOn_Main_SaveListing();
			Assert.assertTrue(srpPage.isSavdListing_popup_exist());
			srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
			ActionsUtility.staticWait(2);
			srpPage.clickOnSave_btn();
			srpPage.clickOnOk_button();
			if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
				driver.navigate().refresh();
			commonPage.hoverToSaveListing();
			ActionsUtility.staticWait(3);
			srpPage.openSaveListing_perClient(ClientNameforSaveListing);
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	
	
}
