package com.ex2.nhspro.leadverification;

import java.util.List;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;


public class LeadVerification_Agent_SRP_Home_testcases extends Initializations {
	String currentUrl="";

	@BeforeClass
	public void beforeStarting() {
		openAgentApplication();
	}

	@BeforeMethod
	public void beforeEveryTest() {
		
		database.createDBConnection();
		System.out.println(currentUrl);
		
	}


	@AfterMethod
	public void DBclose() {
		database.closeConnection();
	}
	
	@Test
	public void verify_LeadOf_SaveListing_FromSRP_HomeTab_ListView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		HometabSelection();
		commonPage.clickOnView("List");
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		performSaveListing();
		ActionsUtility.staticWait(2);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs",
				"slfc", idOfHomesOnSRP.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs",
					"m - slfc", idOfHomesOnSRP.get(0), "NULL");
	}
	

	@Test(enabled = true)
	public void validate_LeadIsgeneratingOnCopyToClipBoard__HomeTab_ListView_AgentSide() {	
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		HometabSelection();
		ActionsUtility.staticWait(3);
		commonPage.clickOnView("List");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(2);
		srpPage.selectParticularHome(2);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnPreviewEmail_link();
		Assert.assertTrue(srpPage.isPreviewEmail_popup_exist(), "Popup not exist");
		srpPage.clickOnCopyEmailToClipBoard();
		Assert.assertTrue(srpPage.isCopyToClipboard_title_exist(), "Edit popup confirmation popup not exist"); // bug
		ActionsUtility.staticWait(1);
		srpPage.clickOn_EditMessageCopied_Ok_btn();
		ActionsUtility.staticWait(1);
		srpPage.clickOnOkbuttonforConfirmation();
//DB validation		
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs",
				"slfc", idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs",
					"m - slfc", idOfHomesOnSRP, "NULL");

	}

	@Test(enabled = true)
	public void validate_Lead_Verfification_Send_btn__HomeTab_ListView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		HometabSelection();
		commonPage.clickOnView("List");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(3);
		System.out.println(idOfHomesOnSRP);
		srpPage.selectParticularHome(3);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnPreviewEmail_link();
		Assert.assertTrue(srpPage.isPreviewEmail_popup_exist(), "Popup not exist");
		srpPage.clickOnSend_button_on_PreviewEmail_popup();
		srpPage.clickOnOkbuttonforConfirmation();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs",
				"slfc", idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "hrs",
					"m - slfc", idOfHomesOnSRP, "NULL");
	}

	@Test(description = "", groups = { "smokeTest" }, enabled = true)
	public void validate_ClientVersionPrintPdf_On_HomeTab_ListView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		HometabSelection();
		commonPage.clickOnView("List");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(1);
		srpPage.selectParticularHome(1);
		srpPage.clickOnPrintReportButton();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "hrs", "prfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "hrs", "m - prfc",
					idOfHomesOnSRP, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
		srpPage.selectParticularHome(1);
	}

	
//-------------------------------------- Preview Home Tab-------------------------------------------------------	
	@Test(enabled=true)
	public void verify_LeadOf_SaveListing_fromPreview__HomeTab_ListView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		driver.navigate().back();
		HometabSelection();
		commonPage.clickOnView("List");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(4);
		srpPage.hoverToHome("4");
		ActionsUtility.staticWait(2);
		srpPage.hovorOnPreviewHome();
		srpPage.clickOn_SaveListing_btn_Preview();
		performSaveListing();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "prev_home", "slfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "prev_home", "m - prfc",
					idOfHomesOnSRP, "NULL");
	}
	

	@Test(enabled=true)
	public void verify_LeadOf_PrintReport_fromPreview_HomeTab_ListView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		HometabSelection();
		commonPage.clickOnView("List");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(4);
		srpPage.hoverToHome("4");
		ActionsUtility.staticWait(2);
		srpPage.hovorOnPreviewHome();
		srpPage.clickOn_PrintReport_btn_Preview();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "prev_home", "prfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "prev_home", "m - prfc",
					idOfHomesOnSRP, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
		
	}
	@Test(enabled=false)
	public void verify_LeadOf_RequestToRegister_fromPreview__HomeTab_ListView_AgentSide() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
			System.out.println("Mobile does not Share Search Functionality");
			throw new SkipException("Skipping this exception");
			
		}
		else
		{ 
			loginOrNavigate();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			String BeforeLeadGeneration_RequestId = database.RequestId;
			HometabSelection();
			commonPage.clickOnView("List");
			String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(4);
		srpPage.hoverToHome("4");
		ActionsUtility.staticWait(2);
		srpPage.hovorOnPreviewHome();
		srpPage.clickOn_RequestToRegisterClient_btn_Preview();
		srpPage.clickOn_RequestToRegister_from_popup();
		srpPage.clickOnOk_button();
		ActionsUtility.staticWait(4);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "HM", "prev_home", "rtrc",idOfHomesOnSRP, "NULL");
		}
	}
	
	@Test
	public void verify_LeadOf_ContactBuilder_fromPreview_HomeTab_AgentSide() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
			System.out.println("Mobile does not Share Search Functionality");
			throw new SkipException("Skipping this exception");
			
		}
		else
		{ 
			loginOrNavigate();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			String BeforeLeadGeneration_RequestId = database.RequestId;
			commonOperation.HomeTabtabSelection(currentUrl);
			String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(1);
		srpPage.hoverToHome("1");
		ActionsUtility.staticWait(5);
		srpPage.clickOnContactTheBuilderTabFromPreview();
		srpPage.enterTextInContactBuilderTextArea(leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"));
		srpPage.clickOn_ContactBuilderAgent();
		ActionsUtility.staticWait(3);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "HM", "prev_home", "cb",idOfHomesOnSRP, "NULL");

		}
	}
	
	
//==================================== Photo View =====================================================================
	
	@Test(priority=1)
	public void verify_LeadOf_SaveListing_HomeTab_Photoview_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		HometabSelection();
		commonPage.clickOnView("Photo");
		String idOfHomesOnSRP = photoView.getIdOfHome_photoView(6);		
		photoView.clickOnSaveListingforAnyHome_photoView(6);
		performSaveListing();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "prev_home",
				"slfc", idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "prev_home",
					"m - slfc", idOfHomesOnSRP, "NULL");
		
	}
	
	@Test(priority=2)
public void verify_LeadOf_PrintReport_HomeTab_PhotoView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		HometabSelection();
		commonPage.clickOnView("Photo");
		String idOfHomesOnSRP = photoView.getIdOfHome_photoView(3);
		photoView.clickOn_PrintReport_HomeTab_PhotoView(3);
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "prev_home", "prfc",idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "prev_home", "m - prfc",idOfHomesOnSRP, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();

	}
	
	
	@Test(enabled=false)
	public void verify_LeadOf_RequestToRegister_HomeTab_PhotoView_AgentSide() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
			System.out.println("Mobile does not Share Search Functionality");
			throw new SkipException("Skipping this exception");
			
		}
		else
		{ 
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		HometabSelection();
		commonPage.clickOnView("Photo");	
		String idOfHomesOnSRP = photoView.getIdOfHome_photoView(3);
		photoView.clickOn_RequestToRegister_HomeTab_PhotoView(3);
		srpPage.clickOn_RequestToRegister_from_popup();
		srpPage.clickOnOk_button();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "HM", "prev_home", "rtrc",idOfHomesOnSRP, "NULL");

		}
			
		
	}
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field("Dallas, TX Area");
			commonOperation.clickOnNoThanksOnNotificationPopUp();
			homePage.clickOnSearch_icon();
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	private void HometabSelection() {
		if(!driver.getCurrentUrl().equalsIgnoreCase(currentUrl)) {
			driver.navigate().to(currentUrl);	
		}
		commonPage.clickOnHomes_tab();
	}
	private void performSaveListing() {
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
	}

}
