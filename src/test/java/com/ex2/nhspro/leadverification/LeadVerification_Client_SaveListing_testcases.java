package com.ex2.nhspro.leadverification;

import java.util.List;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class LeadVerification_Client_SaveListing_testcases extends Initializations{

	String CurrentUrl="";
	@BeforeClass
	public void beforeStarting() {
		openClientApplication();
	}
	


@BeforeMethod
public void beforeEveryTest() {
	database.createDBConnection();
}

	@AfterMethod
	public void DBclose() {
		database.closeConnection();
	}
	
	@Test(groups = {"regressionTest"})
	public void verifyLead_for_PrintReport_SaveListing_HomeTab_ClientSide() {
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		saveListingPage.selectParticluarHome(1);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		saveListingPage.clickOnPrintReport_btn();
		srpPage.clickOnOk_saveListing();
		ActionsUtility.staticWait(5);
		String parentWinHandle = driver.getWindowHandle();
		srpPage.switchTowindow();
		driver.close();
		driver.switchTo().window(parentWinHandle);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_client", "PrintPDF"), "HM", "hrs", "prbc",idOfHomesOnSaveListing.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_client", "PrintPDF"), "HM", "hrs", "m - prbc",idOfHomesOnSaveListing.get(0), "NULL");

		
	}
	
	@Test(groups = {"regressionTest"})
	public void verifyLead_for_PrintReport_SaveListing_CommunityTab_ClientSide() {
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		loginOrNavigate();
		commonPage.clickOnCommunities_tab();
		saveListingPage.selectParticluarHome(1);
		List<String> idOfHomesOnSaveListing = saveListingPage.getIdOfHomes(1);
		saveListingPage.clickOnPrintReport_btn();
		srpPage.clickOnOk_saveListing();
		ActionsUtility.staticWait(5);
		String parentWinHandle = driver.getWindowHandle();
		srpPage.switchTowindow();
		driver.close();
		driver.switchTo().window(parentWinHandle);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_client", "PrintPDF"), "COM", "crs", "prbc",idOfHomesOnSaveListing.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_client", "PrintPDF"), "COM", "crs", "m - prbc",idOfHomesOnSaveListing.get(0), "NULL");

		
	}
	
	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			commonOperation.clickOnNoThanksOnNotificationPopUp();
			homePage.clickOnSearch_icon();
			commonPage.clickOnSaveListing();
			commonPage.clickOnHomes_tab();
			saveListingPage.clearSaveListing();
			commonPage.clickOnCommunities_tab();
			saveListingPage.clearSaveListing();
			saveListingPage.clickOnBack_link();			
			commonPage.clickOnView("List");
			commonPage.clickOnHomes_tab();
			ActionsUtility.staticWait(2);
			doSaveListing();
			commonPage.clickOnCommunities_tab();
			doSaveListing();
			commonPage.clickOnSaveListing();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);			
	}
	
	private void doSaveListing() {
		srpPage.selectParticularHome(3);
		srpPage.clickOn_Main_SaveListing();
		srpPage.clickOnOk_saveListing();
		srpPage.clickOnOk_button();
	}
}
