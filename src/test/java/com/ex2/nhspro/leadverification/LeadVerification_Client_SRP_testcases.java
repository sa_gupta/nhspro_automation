package com.ex2.nhspro.leadverification;

import java.util.List;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class LeadVerification_Client_SRP_testcases extends Initializations {

String CurrentUrl="";
//appium --chromedriver-executable C:\Users\sagupta\Downloads\chromedriver.exe
	@BeforeClass
	public void beforeStarting() {
		openClientApplication();
	}
	
	@AfterMethod
	public void DBclose() {
		database.closeConnection();
	}
	
     @BeforeMethod
     public void beforeEveryTest() {
	
	database.createDBConnection();
    }



	@Test(enabled = true , groups = {"regressionTest"})
	public void verifyLeadOf_SaveListing_fromSRP_HomeTab_ClientSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		
		commonPage.clickOnSaveListing();
		saveListingPage.clearSaveListing();
		saveListingPage.clickOnBack_link();
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(5);
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomesInPhotoView(1);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			srpPage.selectHome(1);
		else
			ActionsUtility.staticWait(2);
			photoView.slectHomeInPhotoView(1);
		srpPage.clickOn_Main_SaveListing();
		srpPage.clickOnOk_saveListing();
		srpPage.clickOnOk_button();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_client", "SaveListing"), "HM",
					"hrs", "m - slbc", idOfHomesOnSRP.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_client", "SaveListing"), "HM",
					"hrs", "slbc", idOfHomesOnSRP.get(0), "NULL");
	}
	
	@Test
	public void verifyLeadOf_SaveListing_fromSRP_CommunityTab_ClientSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnSaveListing();
		commonPage.clickOnCommunities_tab();
		saveListingPage.clearSaveListing();
		saveListingPage.clickOnBack_link();
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		commonPage.clickOnView("Photo");
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomesInPhotoView(1);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			srpPage.selectHome(1);
		else
			ActionsUtility.staticWait(2);
			photoView.slectHomeInPhotoView(1);
		srpPage.clickOn_Main_SaveListing();
		srpPage.clickOnOk_saveListing();
		ActionsUtility.staticWait(2);
		srpPage.clickOnOk_button();
		ActionsUtility.staticWait(5);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_client", "SaveListing"), "COM",
					"crs", "m - slbc", idOfHomesOnSRP.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_client", "SaveListing"), "COM",
					"crs", "slbc", idOfHomesOnSRP.get(0), "NULL");
	}
	
	@Test(description = "", groups = { "smokeTest" }, enabled = true)
	public void validate_LeadOf_PrintPdf__OnSRP_HomeTab_ClientSide() {
		loginOrNavigate();
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(3);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomesInPhotoView(1);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			srpPage.selectHome(1);
		else
			photoView.slectHomeInPhotoView(1);
		String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnPrintReportButton();
		srpPage.clickOnOk_PrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		driver.close();
		driver.switchTo().window(parentWinHandle);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_client", "PrintPDF"), "HM", "hrs", "m - prbc",
					idOfHomesOnSRP.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_client", "PrintPDF"), "HM", "hrs", "prbc",
					idOfHomesOnSRP.get(0), "NULL");
	}
	

	@Test(description = "", groups = { "regressionTest" }, enabled = true)
	public void validate_LeadOf_PrintPdf__OnSRP_CommunityTab_ClientSide() {
		loginOrNavigate();
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(2);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;

		List<String> idOfHomesOnSRP = srpPage.getIdOfHomesInPhotoView(1);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			srpPage.selectHome(1);
		else
			photoView.slectHomeInPhotoView(1);
		String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnPrintReportButton();
		srpPage.clickOnOk_PrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		driver.close();
		driver.switchTo().window(parentWinHandle);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_client", "PrintPDF"), "COM", "crs", "m - prbc",
					idOfHomesOnSRP.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_client", "PrintPDF"), "COM", "crs", "prbc",
					idOfHomesOnSRP.get(0), "NULL");
	}
	
	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			commonOperation.clickOnNoThanksOnNotificationPopUp();
			homePage.clickOnSearch_icon();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);			
	}

}
