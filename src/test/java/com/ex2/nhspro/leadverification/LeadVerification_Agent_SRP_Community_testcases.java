package com.ex2.nhspro.leadverification;

import java.util.List;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class LeadVerification_Agent_SRP_Community_testcases extends Initializations {

	
	String currentUrl="";


	@BeforeClass
	public void beforeStarting() {
		openAgentApplication();
	}

	@BeforeMethod
	public void beforeEveryTest() {
		database.createDBConnection();	
	}



	@AfterMethod
	public void DBclose() {
		database.closeConnection();
	}
	
	@Test
	public void verify_LeadOf_SaveListing_FromSRP_CommunityTab_ListView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonOperation.CommunitytabSelection(currentUrl);
		List<String> idOfHomesOnSRP = srpPage.getIdOfHomes(1);
		srpPage.selectHome(1);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		ActionsUtility.staticWait(2);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
				"slfc", idOfHomesOnSRP.get(0), "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
					"m - slfc", idOfHomesOnSRP.get(0), "NULL");
	}

	@Test(enabled = true)
	public void validate_LeadIsgeneratingOnCopyToClipBoard_ListView_AgentSide() {	
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonOperation.CommunitytabSelection(currentUrl);
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(3);
		System.out.println(idOfHomesOnSRP);
		srpPage.selectParticularHome(3);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnPreviewEmail_link();
		Assert.assertTrue(srpPage.isPreviewEmail_popup_exist(), "Popup not exist");
		srpPage.clickOnCopyEmailToClipBoard();
		Assert.assertTrue(srpPage.isCopyToClipboard_title_exist(), "Edit popup confirmation popup not exist"); // bug
		srpPage.clickOn_EditMessageCopied_Ok_btn();
		srpPage.clickOnOkbuttonforConfirmation();
//DB validation		
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
				"slfc", idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
					"m - slfc", idOfHomesOnSRP, "NULL");

	}

	@Test(enabled = true)
	public void validate_Lead_Verification_Send_btn_ListView_CommunityTab_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonOperation.CommunitytabSelection(currentUrl);
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(3);
		System.out.println(idOfHomesOnSRP);
		srpPage.selectParticularHome(3);
		srpPage.clickOn_Main_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnPreviewEmail_link();
		Assert.assertTrue(srpPage.isPreviewEmail_popup_exist(), "Popup not exist");
		srpPage.clickOnSend_button_on_PreviewEmail_popup();
		srpPage.clickOnOkbuttonforConfirmation();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
				"slfc", idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "crs",
					"m - slfc", idOfHomesOnSRP, "NULL");
	}

	@Test(description = "", groups = { "smokeTest" }, enabled = true)
	public void validate_ClientVersionPrintPdf_On_AgentApplication_ListView_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(4);
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(1);
		srpPage.selectParticularHome(1);
		srpPage.clickOnPrintReportButton();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "crs", "prfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "crs", "m - prfc",
					idOfHomesOnSRP, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
		srpPage.selectParticularHome(1);
	}
	


	
//-------------------------------------- Preview Community Tab-------------------------------------------------------	
	@Test(enabled=true)
	public void verify_LeadOf_SaveListing_fromPreview_CommunityTab_SRP_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonOperation.CommunitytabSelection(currentUrl);
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(3);
		srpPage.hoverToHome("3");
		ActionsUtility.staticWait(5);
		commonPage.focusOnFacebook_icon();
		srpPage.clickOn_SaveListing_btn_Preview();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "prev_com", "slfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "prev_com", "m - prfc",
					idOfHomesOnSRP, "NULL");
	}
	

	@Test(enabled=true)
	public void verify_LeadOf_PrintReport_fromPreview_CommunityTab_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.CommunitytabSelection(currentUrl);
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(3);
		srpPage.hoverToHome("3");
		ActionsUtility.staticWait(5);
		commonPage.focusOnFacebook_icon();
		srpPage.clickOn_PrintReport_btn_Preview();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "prev_com", "prfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "prev_com", "m - prfc",
					idOfHomesOnSRP, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
		
	}
	@Test(enabled=false)
	public void verify_LeadOf_RequestToRegister_fromPreview_CommunityTab_AgentSide() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
			System.out.println("Mobile does not Share Search Functionality");
			throw new SkipException("Skipping this exception");
			
		}
		else
		{ 
			loginOrNavigate();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			String BeforeLeadGeneration_RequestId = database.RequestId;
			commonOperation.CommunitytabSelection(currentUrl);
			String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(3);
		srpPage.hoverToHome("3");
		ActionsUtility.staticWait(5);
		commonPage.focusOnFacebook_icon();
		srpPage.clickOn_RequestToRegisterClient_btn_Preview();
		srpPage.clickOn_RequestToRegister_from_popup();
		srpPage.clickOnOk_button();
	//DB Verification	
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "COM", "prev_com", "rtrc",idOfHomesOnSRP, "NULL");
		}
	}
	
	@Test
	public void verify_LeadOf_ContactBuilder_fromPreview_Community_AgentSide() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
			System.out.println("Mobile does not Share Search Functionality");
			throw new SkipException("Skipping this exception");
			
		}
		else
		{ 
			loginOrNavigate();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			String BeforeLeadGeneration_RequestId = database.RequestId;
			commonOperation.CommunitytabSelection(currentUrl);
			String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(1);
		srpPage.hoverToHome("1");
		ActionsUtility.staticWait(5);
		srpPage.clickOnContactTheBuilderTabFromPreview();
		srpPage.enterTextInContactBuilderTextArea(leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"));
		srpPage.clickOn_ContactBuilderAgent();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "COM", "prev_com", "cb",idOfHomesOnSRP, "NULL");

		}
	}
	//==================================== Photo View =====================================================================
	
		@Test(priority=1)
		public void verify_LeadOf_SaveListing_CommunityTab_Photoview_AgentSide() {
			loginOrNavigate();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			String BeforeLeadGeneration_RequestId = database.RequestId;
			commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
			saveListingPage.clickOnBack_link();
			commonOperation.CommunitytabSelection(currentUrl);
			commonPage.clickOnView("Photo");
			String idOfHomesOnSRP = photoView.getIdOfHome_photoView(3);		
			photoView.clickOnSaveListingforAnyHome_photoView(3);
			performSaveListing();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "COM", "prev_com",
					"slfc", idOfHomesOnSRP, "NULL");
			else
				database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "SaveListing"), "HM", "prev_com",
						"m - slfc", idOfHomesOnSRP, "NULL");
			
		}
		
		@Test(priority=2)
	public void verify_LeadOf_PrintReport_HomeTab_PhotoView_AgentSide() {
			loginOrNavigate();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			String BeforeLeadGeneration_RequestId = database.RequestId;
			commonOperation.CommunitytabSelection(currentUrl);
			commonPage.clickOnView("Photo");	
			String idOfHomesOnSRP = photoView.getIdOfHome_photoView(5);
			photoView.clickOn_PrintReport_HomeTab_PhotoView(5);
			String parentWinHandle = driver.getWindowHandle();
			ActionsUtility.staticWait(2);
			srpPage.clickOnClientVersionPrintReport();
			ActionsUtility.staticWait(5);
			srpPage.switchTowindow();
			// DB validation
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "prev_com", "prfc",idOfHomesOnSRP, "NULL");
			else
				database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "PrintPDF"), "COM", "prev_com", "m - prfc",idOfHomesOnSRP, "NULL");
			driver.close();
			driver.switchTo().window(parentWinHandle);
			commonOperation.clickOnClose_icon();

		}
		@Test(enabled=false)
		public void verify_LeadOf_RequestToRegister_CommunityTab_PhotoView_AgentSide() {
			if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {	
				System.out.println("Mobile does not Share Search Functionality");
				throw new SkipException("Skipping this exception");
				
			}
			else
			{ 
			loginOrNavigate();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			String BeforeLeadGeneration_RequestId = database.RequestId;
			commonOperation.CommunitytabSelection(currentUrl);
			commonPage.clickOnView("Photo");		
			String idOfHomesOnSRP = photoView.getIdOfHome_photoView(5);
			photoView.clickOn_RequestToRegister_HomeTab_PhotoView(5);
			srpPage.clickOn_RequestToRegister_from_popup();
			srpPage.clickOnOk_button();
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "COM", "prev_com", "rtrc",idOfHomesOnSRP, "NULL");

			}
			

			
		}
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			commonOperation.clickOnNoThanksOnNotificationPopUp();
			homePage.clickOnSearch_icon();
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	private void performSaveListing() {
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
	}
	
	/*
	 * private void CommunitytabSelection() {
	 * if(!driver.getCurrentUrl().equalsIgnoreCase(currentUrl)) {
	 * driver.navigate().to(currentUrl); } srpPage.clickOnCommunities_tab(); }
	 */

}
