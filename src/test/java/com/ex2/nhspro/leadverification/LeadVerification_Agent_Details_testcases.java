package com.ex2.nhspro.leadverification;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class LeadVerification_Agent_Details_testcases extends Initializations {

	String currentUrl="";

	@BeforeClass
	public void beforeStarting() {
		openAgentApplication();
	}
	
	@BeforeMethod
	public void beforeEveryTest() {
		database.createDBConnection();	
	}

	@AfterMethod
	public void DBclose() {
		database.closeConnection();
	}

	@Test
	public void verify_LeadVerification_SaveListing_HomeDetails_AgentSide() {
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		loginOrNavigate();
		commonOperation.clearSaveListingHomeTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonOperation.HomeTabtabSelection(currentUrl);
		ActionsUtility.staticWait(4);
		srpPage.clickOnHome(4);
//		srpPage.clickOnFirstHome();
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "homedetail", "slfc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "homedetail", "m - slfc", homeIdFromDetailsPage, "NULL");

	}

	@Test
	public void verify_LeadVerification_PrintReport_HomeDetails_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(5);
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		homeDetailPage.clickOn_PrintReport();
		String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "PrintPDF"),
					"HM", "homedetail", "prfc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "PrintPDF"),
					"HM", "homedetail", "m - prfc", homeIdFromDetailsPage, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
	}
	
	@Test
	public void verify_LeadVerification_ContactToBuilder_HomeDetails_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		homeDetailPage.enterMessageInContactToBuilderTextBox(leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"));
		homeDetailPage.clickOnContactAgentSales_btn();
		ActionsUtility.staticWait(2);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "HM", "homedetail", "cb", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "HM", "homedetail", "cb", homeIdFromDetailsPage, "NULL");
	
		
	}
	
	@Test
	public void verify_LeadVerification_ContactToBuilder_CommunityDetails_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.clickOnHome(1);
		String communityIdFromDetailsPage = homeDetailPage.getHomeId();
		homeDetailPage.enterMessageInContactToBuilderTextBox(leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"));
		homeDetailPage.clickOnContactAgentSales_btn();
		ActionsUtility.staticWait(3);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "COM", "comdetail", "cb", communityIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("SRP_agent", "ContactToBuilder")+leadData.getJsonData("SRP_agent", "ContactToBuilder_Data"), "COM", "comdetail", "cb", communityIdFromDetailsPage, "NULL");
		
	}
	
	//Community detail Home Preview Test Case************************
	
	@Test(enabled=false)
	public void verify_LeadVerification_CommunityDetails_Homes_Preview_Request_To_Register_Client_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.clickOnHome(2);
		communityDetailPage.scrollTo_Home_SavedListing_In_CommunityDetailPage();
		ActionsUtility.staticWait(3);
		srpPage.hoverToHome("1");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(1);
		communityDetailPage.click_On_Preview_RequestToRegister_Client_Button();
		communityDetailPage.click_On_RequestToRegister_Client_Popup_Button();
		communityDetailPage.clickOnConfirmOk();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "HM", "prev_home", "rtrc",idOfHomesOnSRP, "NULL");
		
	}
	/*
	@Test
	public void verify_LeadVerification_CommunityDetails_Homes_Preview_Saved_Listing_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.clickOnHome(2);
		communityDetailPage.scrollTo_Home_SavedListing_In_CommunityDetailPage();
		ActionsUtility.staticWait(3);
		srpPage.hoverToHome("2");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(2);
		communityDetailPage.click_On_Preview_Saved_Listing_Button();
		performSaveListing();
		ActionsUtility.staticWait(2);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "prev_home", "slfc", idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "prev_home", "m - slfc", idOfHomesOnSRP, "NULL");
	}
	@Test
	public void verify_LeadVerification_CommunityDetails_Homes_Preview_Print_Report_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.clickOnHome(2);
		communityDetailPage.scrollTo_Home_SavedListing_In_CommunityDetailPage();
		ActionsUtility.staticWait(3);
		srpPage.hoverToHome("2");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(2);
		communityDetailPage.click_On_Preview_Print_Report_Button();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "prev_home", "prfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "prev_home", "m - prfc",
					idOfHomesOnSRP, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
	}
	
	
	@Test
	public void verify_LeadVerification_CommunityDetails_Homes_Grid_Print_Report_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.clickOnHome(2);
		communityDetailPage.scrollTo_Home_SavedListing_In_CommunityDetailPage();
		ActionsUtility.staticWait(3);
		srpPage.hoverToHome("1");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(1);
		srpPage.selectParticularHome(1);
		communityDetailPage.click_On_Above_Home_grid_Print_Report_Button();
		String parentWinHandle = driver.getWindowHandle();
		ActionsUtility.staticWait(2);
		srpPage.clickOnClientVersionPrintReport();
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,
				leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "comdetail", "prfc",
				idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,
					leadData.getJsonData("SRP_agent", "PrintPDF"), "HM", "comdetail", "m - prfc",
					idOfHomesOnSRP, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
		srpPage.selectParticularHome(1);
	}
	
	
	@Test
	public void verify_LeadVerification_CommunityDetails_Homes_Grid_Saved_Listing_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.clickOnHome(2);
		communityDetailPage.scrollTo_Home_SavedListing_In_CommunityDetailPage();
		ActionsUtility.staticWait(3);
		srpPage.hoverToHome("1");
		String idOfHomesOnSRP = srpPage.getIdOfPerticularHome_InList(1);
		srpPage.selectParticularHome(1);
		communityDetailPage.click_On_Home_Grid_SavedListing_In_CommunityDetailPage();
		performSaveListing();
		ActionsUtility.staticWait(2);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "comdetail", "slfc", idOfHomesOnSRP, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "comdetail", "m - slfc", idOfHomesOnSRP, "NULL");
	}
	
	*/
	
	
	
	// ************************** End **********************************
	
	
	@Test
	public void verify_LeadVerification_Request_an_Appointment_HomeDetails_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(2);
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		homeDetailPage.enterMessageInRequestAnApprointment(leadData.getJsonData("Details_agent", "Request_an_AppointmentData"));
		homeDetailPage.clickOnRequest_an_appointment_btn();
		ActionsUtility.staticWait(4);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "Request_an_Appointment")+leadData.getJsonData("Details_agent", "Request_an_AppointmentData"), "HM", "homedetail", "ra", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "Request_an_Appointment")+leadData.getJsonData("Details_agent", "Request_an_AppointmentData"), "HM", "homedetail", "ra", homeIdFromDetailsPage, "NULL");
	
		
	} 
	
	@Test
	public void verify_LeadVerification_Request_an_Appointment_From_CommunityDetails_AgentSide(){
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		System.out.println(BeforeLeadGeneration_RequestId);
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.clickOnHome(2);
		String communityIdFromDetailsPage = homeDetailPage.getHomeId();
		homeDetailPage.enterMessageInRequestAnApprointment(leadData.getJsonData("Details_agent", "Request_an_AppointmentData"));
		homeDetailPage.clickOnRequest_an_appointment_btn();
		ActionsUtility.staticWait(4);
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "Request_an_Appointment")+leadData.getJsonData("Details_agent", "Request_an_AppointmentData"), "COM", "comdetail", "ra", communityIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "Request_an_Appointment")+leadData.getJsonData("Details_agent", "Request_an_AppointmentData"), "COM", "comdetail", "ra", communityIdFromDetailsPage, "NULL");
	
		
	} 
	
	
	@Test
	public void verify_LeadVerification_RequestToRegisterClient_HomeDetails_AgentSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(5);
		srpPage.clickOnHome(2);
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		homeDetailPage.clickOnRequestToRegister_btn();
		srpPage.clickOn_RequestToRegister_from_popup();
		 srpPage.clickOnOk_button();
		 ActionsUtility.staticWait(3);
			database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "HM", "homedetail", "rtrc",homeIdFromDetailsPage, "NULL");

	}
	public void verify_LeadVerification_CopyToClipBoard_HomeDetails_AgentSide() {
		
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonPage.clickOnHomes_tab();
//		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnPreviewEmail_link();
		Assert.assertTrue(srpPage.isPreviewEmail_popup_exist(), "Popup not exist");
		srpPage.clickOnCopyEmailToClipBoard();
		Assert.assertTrue(srpPage.isCopyToClipboard_title_exist(), "Edit popup confirmation popup not exist"); // bug
		srpPage.clickOn_EditMessageCopied_Ok_btn();
		srpPage.clickOnOkbuttonforConfirmation();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "homedetail", "slfc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "homedetail", "m - slfc", homeIdFromDetailsPage, "NULL");
	}
	
	@Test
	public void verify_LeadVerification_SaveListing_CommunityDetails_AgentSide() {

		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonOperation.CommunitytabSelection(currentUrl);
		srpPage.clickOnHome(1);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"), "COM",
					"comdetail", "slfc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"), "COM",
					"comdetail", "m - slfc", homeIdFromDetailsPage, "NULL");
	}



	@Test
	public void verify_LeadVerification_PrintReport_CommunityDetails_AgentSide() {
	
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.CommunitytabSelection(currentUrl);
		ActionsUtility.staticWait(2);
		srpPage.clickOnHome(1);
//		srpPage.clickOnFirstHome();
		ActionsUtility.staticWait(2);
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		homeDetailPage.clickOn_PrintReport();
		String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnClientVersionPrintReport();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "PrintPDF"),
					"COM", "comdetail", "prfc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "PrintPDF"),
					"COM", "comdetail", "m - prfc", homeIdFromDetailsPage, "NULL");
		driver.close();
		driver.switchTo().window(parentWinHandle);
		commonOperation.clickOnClose_icon();
	}

	@Test
public void verify_LeadVerification_RequestToRegisterClient_CommunityDetails_AgentSide() {
	loginOrNavigate();
	database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
	String BeforeLeadGeneration_RequestId = database.RequestId;
	commonOperation.CommunitytabSelection(currentUrl);
	ActionsUtility.staticWait(5);
	srpPage.clickOnHome(2);
	String homeIdFromDetailsPage = homeDetailPage.getHomeId();
	homeDetailPage.clickOnRequestToRegister_btn();
	srpPage.clickOn_RequestToRegister_from_popup();
	 srpPage.clickOnOk_button();
	 ActionsUtility.staticWait(4); 
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId,leadData.getJsonData("SRP_agent", "RequestToRegisterClient"), "COM", "comdetail", "rtrc",homeIdFromDetailsPage, "NULL");

	}
	public void verify_LeadVerification_CopyToClipBoard_CommunityDetails_AgentSide() {
		
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonOperation.clearSaveListingCommunityTabData(ClientNameforSaveListing);
		saveListingPage.clickOnBack_link();
		commonOperation.CommunitytabSelection(currentUrl);
		srpPage.clickOnHome(3);
		homeDetailPage.clickOn_SaveListing();
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnPreviewEmail_link();
		Assert.assertTrue(srpPage.isPreviewEmail_popup_exist(), "Popup not exist");
		srpPage.clickOnCopyEmailToClipBoard();
		Assert.assertTrue(srpPage.isCopyToClipboard_title_exist(), "Edit popup confirmation popup not exist"); // bug
		srpPage.clickOn_EditMessageCopied_Ok_btn();
		srpPage.clickOnOkbuttonforConfirmation();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "homedetail", "slfc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_agent", "SaveListing"),
					"HM", "homedetail", "m - slfc", homeIdFromDetailsPage, "NULL");
	}
	
	
	private void loginOrNavigate() {
		if(currentUrl.equals("")) {
			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
			commonOperation.clickOnNoThanksOnNotificationPopUp();
			homePage.clickOnSearch_icon();
			currentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(currentUrl);			
	}
	
	private void performSaveListing() {
		Assert.assertTrue(srpPage.isSavdListing_popup_exist());
		srpPage.selectClientFromClientPopup(ClientNameforSaveListing);
		ActionsUtility.staticWait(2);
		srpPage.clickOnSaveandEmail_btn();
		srpPage.clickOnNo_btn();
		srpPage.clickOnOk_button();
	}

}
