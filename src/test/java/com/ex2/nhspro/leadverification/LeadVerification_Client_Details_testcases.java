package com.ex2.nhspro.leadverification;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class LeadVerification_Client_Details_testcases extends Initializations{

	String CurrentUrl="";
	@BeforeClass
	public void beforeStarting() {
		openClientApplication();
	}
	


	
@BeforeMethod
public void beforeEveryTest() {
	database.createDBConnection();
	ClientNameforSaveListing = srpData.getJsonData("SaveListing", "ClientName");
	
}
	@AfterMethod
	public void DBclose() {
		database.closeConnection();
	}
	
	@Test
	public void verify_LeadVerification_SaveListing_HomeDetails_clientSite() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnSaveListing();
		saveListingPage.clearSaveListing();
		saveListingPage.clickOnBack_link();
		commonPage.clickOnView("List");
		commonPage.clickOnHomes_tab();
		srpPage.clickOnHome(5);
		homeDetailPage.clickOn_SaveListing();
		homeDetailPage.clickOnOkButton();
		homeDetailPage.clickOnConfirmOk();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();		
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "SaveListing"), "HM", "homedetail",
				"slbc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "SaveListing"), "HM", "homedetail",
					"m - slbc", homeIdFromDetailsPage, "NULL");
		
	}
	
	@Test
	public void verify_Lead_PrintReport_HomeDetailsPage_ClientSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;		
		commonPage.clickOnHomes_tab();
		ActionsUtility.staticWait(2);
		commonPage.clickOnView("List");
		srpPage.clickOnHome(5);
		String HomeId = homeDetailPage.getHomeId();
		homeDetailPage.clickOn_PrintReport();
		String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnOkPrintReportForClient();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		driver.close();
		driver.switchTo().window(parentWinHandle);
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "PrintPDF"),
					"HM", "homedetail", "prbc", HomeId, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "PrintPDF"),
					"HM", "homedetail", "m - prbc", HomeId, "NULL");

		
	}
	
//-------------------------------- Community Detail page-----------------------------------------------------------------------------
	
	@Test
	public void verify_LeadVerification_SaveListing_CommunityDetails() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnSaveListing();
		saveListingPage.clickOnCommunityTab_saveListing();
		saveListingPage.clearSaveListing();
		saveListingPage.clickOnBack_link();
		commonPage.clickOnView("List");
		commonPage.clickOnCommunities_tab();
		srpPage.clickOnHome(3);		
//		ActionsUtility.staticWait(2);
		homeDetailPage.clickOn_SaveListing();
		homeDetailPage.clickOnOkButton();
		homeDetailPage.clickOnConfirmOk();
		String homeIdFromDetailsPage = homeDetailPage.getHomeId();		
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "SaveListing"), "COM", "comdetail",
				"slbc", homeIdFromDetailsPage, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "SaveListing"), "COM", "comdetail",
					"m - slbc", homeIdFromDetailsPage, "NULL");
	}
	
	@Test ()
	public void verify_Lead_PrintReport_CommunityDetailsPage_ClientSide() {
		loginOrNavigate();
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		String BeforeLeadGeneration_RequestId = database.RequestId;
		commonPage.clickOnView("List");
		commonPage.clickOnCommunities_tab();
		srpPage.clickOnHome(5);
		String HomeId = homeDetailPage.getHomeId();
		homeDetailPage.clickOn_PrintReport();
		String parentWinHandle = driver.getWindowHandle();
		srpPage.clickOnOkPrintReportForClient();
		ActionsUtility.staticWait(5);
		srpPage.switchTowindow();
		driver.close();
		driver.switchTo().window(parentWinHandle);	
		// DB validation
		database.executeLeadQuery(commonData.getJsonData(environment, "EmailId_agent"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "PrintPDF"),
					"COM", "comdetail", "prbc", HomeId, "NULL");
		else
			database.validateValueFromDB(BeforeLeadGeneration_RequestId, leadData.getJsonData("Details_client", "PrintPDF"),
					"COM", "comdetail", "m - prbc", HomeId, "NULL");
			
	}	
	
	
	//Lead Type: Listing report downloaded by agent's client -
	
	private void loginOrNavigate() {
		if(CurrentUrl.equals("")) {
			commonPage.loginIfNotInShowingNew(commonData.getJsonData(environment, "EmailId_client"));
			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "SearchingDallasMarket"));
			commonOperation.clickOnNoThanksOnNotificationPopUp();
			homePage.clickOnSearch_icon();
			CurrentUrl=driver.getCurrentUrl();
		}
		else
			driver.navigate().to(CurrentUrl);			
	}
	

}
