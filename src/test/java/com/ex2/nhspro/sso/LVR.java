package com.ex2.nhspro.sso;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBase;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;

public class LVR extends SSOBase{
	SSOBase obj=new SSOBase();
	
	String glvarUid="testnhs1";
	String glvarPwd="password1";
	
	@FindBy(xpath = "(//a[text()='Sign In'])[1]")
	static WebElement CommonSignIn;
	@FindBy(xpath = "//a[@class='btn btn-orange']")
	static WebElement SignIn_button;
	@FindBy(xpath="//input[@name='username']")
	static WebElement glvarUsername;
	@FindBy(xpath="//input[@name='password']")
	static WebElement glvarPassword;
	@FindBy(xpath="//button[@id='loginbtn']")
	static WebElement glvarLoginButton;
	@FindBy(xpath="//span[@id='user-name-menu']")
	static WebElement Welcome;
	@FindBy(xpath="//img[@alt='newhomesource']")
	static WebElement glvarDashboard;
	@FindBy(xpath = "//meta[@name='ServerName']")
	static WebElement ServerName;
	int flag=0;
	
	@Test(description="Validate the LVR login")
	public void Validate_LVR_login()
	{
		PageFactory.initElements(driver,LVR.class);
		driver.get("https://www.newhomesourceprofessional.com/lvr");
		driver.manage().window().maximize();
		JsonUtility json=new JsonUtility("SSOData");
		System.out.println("Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO, "Server Name is "+ServerName.getAttribute("content"));
		CommonSignIn.click();
		logger.log(Status.INFO, "Clicked on SignIn link");
		System.out.println("Clicked on SignIn link");
		String actual=obj.SSO_Login_text(driver);
		System.out.println(json.getJsonData("SSO_text","ALL_SSO_Text"));
		String expectedtext=json.getJsonData("SSO_text","ALL_SSO_Text")+"voilà! You'll be signed in and ready to take action.";
		Assert.assertTrue(actual.equalsIgnoreCase(expectedtext),"Login text Does not match");
		logger.log(Status.INFO,"Login text = "+actual);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(SignIn_button));
		SignIn_button.click();
		logger.log(Status.INFO, "Clicked on SignIn button");
		System.out.println("Clicked on SignIn button");
		wait.until(ExpectedConditions.visibilityOf(glvarUsername));
		glvarUsername.sendKeys(glvarUid);
		logger.log(Status.INFO, "GLVAR Username Entered");
		System.out.println("GLVAR Username Entered");
		wait.until(ExpectedConditions.visibilityOf(glvarPassword));
		glvarPassword.click();
		ActionsUtility.staticWait(1);
		glvarPassword.sendKeys(glvarPwd);
		logger.log(Status.INFO, "GLVAR Password Entered");
		System.out.println("GLVAR Password Entered");
		ActionsUtility.staticWait(1);
		glvarLoginButton.click();
		logger.log(Status.INFO, "GLVAR Submit Button Clicked");
		System.out.println("GLVAR Submit Button Clicked");
		JavascriptExecutor jsc=(JavascriptExecutor) driver;
		/*
		 * for(int sec=0;;sec++) { if(sec>=4) { break; }
		 * jsc.executeScript("window.scrollBy(0,400)",""); ActionsUtility.staticWait(1);
		 * }
		 */
		jsc.executeScript("arguments[0].scrollIntoView(false);", glvarDashboard);
		
		wait.until(ExpectedConditions.visibilityOf(glvarDashboard));
		glvarDashboard.click();
		logger.log(Status.INFO, "Select NHSPro From The Dashboard");
		System.out.println("Select NHSPro From The Dashboard");
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		
		try {
			wait.until(ExpectedConditions.visibilityOf(Welcome));
			ActionsUtility.staticWait(3);
			String actualtext=Welcome.getText();
			System.out.println(actualtext);
			Assert.assertTrue(actualtext.contains("Paul"),"GLVAR User Login Failed");
			logger.log(Status.INFO, "GLVAR User Login LoggedIn Sucessfully");
			System.out.println("GLVAR User Login LoggedIn Sucessfully");
			obj.clickOnMyAccount(driver);
			ActionsUtility.staticWait(3);
			logger.log(Status.INFO,"FirstName : "+obj.getFirstName(driver));
			logger.log(Status.INFO,"LastName : "+obj.getLastName(driver));
			logger.log(Status.INFO, "ZipCode : "+obj.getZipCode(driver));
			logger.log(Status.INFO,"Email : "+obj.getEmail(driver));
			logger.log(Status.INFO,"AgencyName : "+obj.getAgencyName(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
			if(obj.getAgencyName(driver).equals(""))
			{
				Assert.fail("Agency is missing");
			}  

		}
		catch(Exception e)
		{
			driver.findElement(By.xpath("//h1[text()='Welcome!']")).isDisplayed();
			logger.log(Status.INFO,"Confirm account detail pop up displayed");
			System.out.println("Confirm account detail pop up displayed");
			  logger.log(Status.INFO,"FirstName : "+obj.getFirstNameFromConfirmPopUp(driver)); 
			  logger.log(Status.INFO,"LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			  logger.log(Status.INFO, "ZipCode : "+obj.getZipCodeFromConfirmPopUp(driver));
			  logger.log(Status.INFO,"Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("First Name: "+obj.getFirstNameFromConfirmPopUp(driver));
			System.out.println("LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			System.out.println("Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("ZipCode "+obj.getZipCodeFromConfirmPopUp(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
		} 
	}

}
