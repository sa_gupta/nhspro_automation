package com.ex2.nhspro.sso;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBase;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;

public class GAAR extends SSOBase{
	SSOBase obj=new SSOBase();
	
	String gaarUid="abq.vendortest";
	String gaarPwd="bdxpro1";
	@FindBy(xpath = "(//a[text()='Sign In'])[1]")
	static WebElement CommonSignIn;
	@FindBy(xpath = "//a[@class='btn btn-orange']")
	static WebElement SignIn_button;
	@FindBy(xpath="//input[@id='user']")
	static WebElement gaarUsername;
	@FindBy(xpath="//input[@name='login']")
	static WebElement gaarNext;
	@FindBy(xpath="//input[@id='password']")
	static WebElement gaarPassword;
	@FindBy(xpath="//button[@id='btn-login']")
	static WebElement gaarLoginButton;
	@FindBy(xpath="//div[@class='col-md-6 col-sm-6 col-xs-12']/a")
	static WebElement NHSPoption;
	@FindBy(xpath="//span[@id='user-name-menu']")
	static WebElement Welcome;
	@FindBy(xpath = "//meta[@name='ServerName']")
	static WebElement ServerName;
	
	
	  @Test(description="Validate the GAAR login")  
	  public void Validate_GAAR_login()
	  {
	  PageFactory.initElements(driver,GAAR.class);
	  driver.get("https://www.newhomesourceprofessional.com/gaar");
	  driver.manage().window().maximize();
		JsonUtility json=new JsonUtility("SSOData");
		System.out.println("Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO, "Server Name is "+ServerName.getAttribute("content"));
		CommonSignIn.click();
		logger.log(Status.INFO, "Clicked on SignIn link");
		System.out.println("Clicked on SignIn link");
		String actual=obj.SSO_Login_text(driver);
		System.out.println(json.getJsonData("SSO_text","ALL_SSO_Text"));
		String expectedtext=json.getJsonData("SSO_text","ALL_SSO_Text")+"voilà! You'll be signed in and ready to take action.";
		Assert.assertTrue(actual.equalsIgnoreCase(expectedtext),"Login text Does not match");
		logger.log(Status.INFO,"Login text = "+actual);
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(SignIn_button));
		SignIn_button.click();
		logger.log(Status.INFO, "Clicked on SignIn button");
		System.out.println("Clicked on SignIn button");
		wait.until(ExpectedConditions.visibilityOf(gaarUsername));
		 gaarUsername.sendKeys(gaarUid);
		logger.log(Status.INFO, "GAAR Username Entered");
		System.out.println("GAAR Username Entered");
		gaarNext.click();
		logger.log(Status.INFO, "GAAR Next Clicked");
		System.out.println("GAAR Next Clicked");
		wait.until(ExpectedConditions.visibilityOf(gaarPassword));
		gaarPassword.sendKeys(gaarPwd);
		logger.log(Status.INFO, "GAAR Password Entered");
		System.out.println("GAAR Password Entered");
		gaarLoginButton.click();
		logger.log(Status.INFO, "GAAR Submit Button Clicked");
		System.out.println("GAAR Submit Button Clicked");
		wait.until(ExpectedConditions.visibilityOf(NHSPoption));
		NHSPoption.click();
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		try {
			wait.until(ExpectedConditions.visibilityOf(Welcome));
			ActionsUtility.staticWait(3);
			String actualtext=Welcome.getText();
			System.out.println(actualtext);
			Assert.assertTrue(actualtext.contains("Test"),"GAAR User Login Failed");
			logger.log(Status.INFO, "GAAR User LoggedIn Sucessfully");
			System.out.println("GAAR User LoggedIn Sucessfully");
			obj.clickOnMyAccount(driver);
			ActionsUtility.staticWait(3);
			logger.log(Status.INFO,"FirstName : "+obj.getFirstName(driver));
			logger.log(Status.INFO,"LastName : "+obj.getLastName(driver));
			logger.log(Status.INFO, "ZipCode : "+obj.getZipCode(driver));
			logger.log(Status.INFO,"Email : "+obj.getEmail(driver));
			logger.log(Status.INFO,"AgencyName : "+obj.getAgencyName(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
		}
		catch(Exception e)
		{
			driver.findElement(By.xpath("//h1[text()='Welcome!']")).isDisplayed();
			logger.log(Status.INFO,"Confirm account detail pop up displayed");
			System.out.println("Confirm account detail pop up displayed");
			  logger.log(Status.INFO,"FirstName : "+obj.getFirstNameFromConfirmPopUp(driver)); 
			  logger.log(Status.INFO,"LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			  logger.log(Status.INFO, "ZipCode : "+obj.getZipCodeFromConfirmPopUp(driver));
			  logger.log(Status.INFO,"Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("First Name: "+obj.getFirstNameFromConfirmPopUp(driver));
			System.out.println("LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			System.out.println("Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("ZipCode "+obj.getZipCodeFromConfirmPopUp(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
		} 
	}

}

