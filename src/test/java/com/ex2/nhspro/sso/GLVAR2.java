package com.ex2.nhspro.sso;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBase;
import com.ex2.nhspro.utility.ActionsUtility;


public class GLVAR2 extends SSOBase{
	
	String glvarUid="236739";
	String glvarPwd="st2019";
	String url="https://www.lasvegasrealtor.com/sso/saml2/idp/SSOService.php?spentityid=https://www.newhomesourceprofessional.com/SPEntityDescriptors/glvar.xml";
	
	@FindBy(xpath="//input[@id='username']")
	static WebElement glvarUsername;
	@FindBy(xpath="//input[@id='password']")
	static WebElement glvarPassword;
	@FindBy(xpath="//button[text()='Log in']")
	static WebElement glvarLoginButton;
	@FindBy(xpath="//a[text()='Welcome ']")
	static WebElement Welcome;
	@FindBy(xpath="(//div[@class='cover'])[5]")
	static WebElement glvarDashboard;

	@Test(description="Validate the GLVAR2 login")
	public void Validate_GLVAR2_login()
	{
		PageFactory.initElements(driver,GLVAR2.class);
		driver.get(url);
		driver.manage().window().maximize();
		WebDriverWait wait= new WebDriverWait(driver,Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(glvarUsername));
		glvarUsername.sendKeys(glvarUid);
		logger.log(Status.INFO, "GLVAR Username Entered");
		System.out.println("GLVAR Username Entered");
		wait.until(ExpectedConditions.visibilityOf(glvarPassword));
		glvarPassword.click();
		ActionsUtility.staticWait(1);
		glvarPassword.sendKeys(glvarPwd);
		logger.log(Status.INFO, "GLVAR Password Entered");
		System.out.println("GLVAR Password Entered");
		ActionsUtility.staticWait(1);
		glvarLoginButton.click();
		logger.log(Status.INFO, "GLVAR Submit Button Clicked");
		System.out.println("GLVAR Submit Button Clicked");
		wait.until(ExpectedConditions.visibilityOf(glvarDashboard));
		glvarDashboard.click();
		logger.log(Status.INFO, "Select NHSPro From The Dashboard");
		System.out.println("Select NHSPro From The Dashboard");
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		wait.until(ExpectedConditions.visibilityOf(Welcome));
		ActionsUtility.staticWait(3);
		String actualtext=Welcome.getText();
		System.out.println(actualtext);
		Assert.assertTrue(actualtext.contains("Welcome Tom"),"GLVAR User Login Failed");
		logger.log(Status.INFO, "GLVAR User Login LoggedIn Sucessfully");
		System.out.println("GLVAR User Login LoggedIn Sucessfully");
	}

}
