package com.ex2.nhspro.sso;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBase;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;

public class BrightMLS extends SSOBase{
	SSOBase obj=new SSOBase();
	
	String brightmlsUid="3274069";
	String brightmlsPwd="BDXmls2023!";
	
	@FindBy(xpath = "(//a[text()='Sign In'])[1]")
	static WebElement CommonSignIn;
	@FindBy(xpath = "//a[@class='btn btn-orange']")
	static WebElement SignIn_button;
	@FindBy(xpath="//input[@id='username']")
	static WebElement brightmlsUsername;
	@FindBy(xpath="//input[@id='password']")
	static WebElement brightmlsPassword;
	@FindBy(xpath="//span[text()='LOG IN']")
	static WebElement brightmlsLogin;
	@FindBy(xpath="//input[@class='btn pro_CRM_Button']")
	static WebElement DisclaimerButton;
	@FindBy (xpath="//a[@class='close']")
	static WebElement close;
	@FindBy(xpath="//span[@id='user-name-menu']")
	static WebElement Welcome;
	@FindBy(xpath = "//meta[@name='ServerName']")
	static WebElement ServerName;
	@FindBy(xpath = "//div[@id='TelerikWindow']")
	static WebElement Popup;
	@FindBy(xpath = "(//div[@class='pro_CRM_ClientsControl']/p)[1]")
	static WebElement BrightmlsText1Locator;
	@FindBy(xpath = "(//div[@class='pro_CRM_ClientsControl']/p)[2]")
	static WebElement BrightmlsText1Locator2;
	@FindBy(xpath = "//div[@class='pro_CRM_Footer']/p")
	static WebElement BrightmlsDisclaimer;
	
	
	@Test(description="Validate the BrightMls login")
	public void Validate_Brightmls_login()
	{
		PageFactory.initElements(driver,BrightMLS.class);
		driver.get("https://www.newhomesourceprofessional.com/brightmls");
		driver.manage().window().maximize();
		JsonUtility json=new JsonUtility("SSOData");
		System.out.println("Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO, "Server Name is "+ServerName.getAttribute("content"));
		CommonSignIn.click();
		logger.log(Status.INFO, "Clicked on SignIn link");
		System.out.println("Clicked on SignIn link");
		String actual=obj.SSO_Login_text(driver);
		System.out.println(json.getJsonData("SSO_text","ALL_SSO_Text"));
		String expectedtext=json.getJsonData("SSO_text","ALL_SSO_Text")+"voilà! You'll be signed in and ready to take action.";
		Assert.assertTrue(actual.equalsIgnoreCase(expectedtext),"Login text Does not match");
		logger.log(Status.INFO,"Login text = "+actual);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(SignIn_button));
		SignIn_button.click();
		logger.log(Status.INFO, "Clicked on SignIn button");
		System.out.println("Clicked on SignIn button");
		wait.until(ExpectedConditions.visibilityOf(brightmlsUsername));
		brightmlsUsername.sendKeys(brightmlsUid);
		logger.log(Status.INFO, "BrightMls Username Entered");
		System.out.println("BrightMls Username Entered");
		wait.until(ExpectedConditions.visibilityOf(brightmlsPassword));
		brightmlsPassword.sendKeys(brightmlsPwd);
		logger.log(Status.INFO, "BrightMls Password Entered");
		System.out.println("BrightMls Password Entered");
		brightmlsLogin.click();
		logger.log(Status.INFO, "BrightMls Submit Button Clicked");
		System.out.println("BrightMls Submit Button Clicked");
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		try {
			wait.until(ExpectedConditions.visibilityOf(Popup));
			String Brightmlstext1fromWeb=BrightmlsText1Locator.getText();
			String Brightmlstext2fromWeb=BrightmlsText1Locator2.getText();
			String BrightmlsDisclaimertextFromweb=BrightmlsDisclaimer.getText();
			System.out.println(Brightmlstext1fromWeb);
			 logger.log(Status.INFO,Brightmlstext1fromWeb);
			System.out.println(Brightmlstext2fromWeb);
			logger.log(Status.INFO,Brightmlstext2fromWeb);
			System.out.println(BrightmlsDisclaimertextFromweb);
			logger.log(Status.INFO,BrightmlsDisclaimertextFromweb);
			Assert.assertEquals(Brightmlstext1fromWeb, json.getJsonData("SSO_Brightmls","Brightmls_Text1"));
			Assert.assertEquals(Brightmlstext2fromWeb, json.getJsonData("SSO_Brightmls","Brightmls_Text2"));
			Assert.assertTrue(BrightmlsDisclaimertextFromweb.contains(json.getJsonData("SSO_Brightmls","Brightmls_Disclaimer")));
			  wait.until(ExpectedConditions.visibilityOf(close)); close.click();
			  logger.log(Status.INFO, "BrightMls Disclaimer Closed");
			  System.out.println("BrightMls Disclaimer Closed");
			wait.until(ExpectedConditions.visibilityOf(Welcome));
			ActionsUtility.staticWait(3);
			String actualtext=Welcome.getText();
			System.out.println(actualtext);
			Assert.assertTrue(actualtext.contains("Paul"),"BrightMls User Login Failed");
			logger.log(Status.INFO, "BrightMls User Login LoggedIn Sucessfully");
			System.out.println("BrightMls User Login LoggedIn Sucessfully");
			obj.clickOnMyAccount(driver);
			ActionsUtility.staticWait(3);
			logger.log(Status.INFO,"FirstName : "+obj.getFirstName(driver));
			logger.log(Status.INFO,"LastName : "+obj.getLastName(driver));
			logger.log(Status.INFO, "ZipCode : "+obj.getZipCode(driver));
			logger.log(Status.INFO,"Email : "+obj.getEmail(driver));
			logger.log(Status.INFO,"AgencyName : "+obj.getAgencyName(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}  
		}
		catch(Exception e)
		{
			driver.findElement(By.xpath("//h1[text()='Welcome!']")).isDisplayed();
			logger.log(Status.INFO,"Confirm account detail pop up displayed");
			System.out.println("Confirm account detail pop up displayed");
			  logger.log(Status.INFO,"FirstName : "+obj.getFirstNameFromConfirmPopUp(driver)); 
			  logger.log(Status.INFO,"LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			  logger.log(Status.INFO, "ZipCode : "+obj.getZipCodeFromConfirmPopUp(driver));
			  logger.log(Status.INFO,"Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("First Name: "+obj.getFirstNameFromConfirmPopUp(driver));
			System.out.println("LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			System.out.println("Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("ZipCode "+obj.getZipCodeFromConfirmPopUp(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
		} 
	}
}
