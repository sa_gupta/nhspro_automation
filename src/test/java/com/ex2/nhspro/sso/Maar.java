package com.ex2.nhspro.sso;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBase;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;

public class Maar extends SSOBase {
	SSOBase obj=new SSOBase();
	String MaarUid = "V000NHS";
	String MaarPwd = "CR8fL8RI_5St";

	@FindBy(xpath = "(//a[text()='Sign In'])[1]")
	static WebElement CommonSignIn;
	@FindBy(xpath = "//a[@class='btn btn-orange']")
	static WebElement SignIn_button;
	@FindBy(xpath = "//input[@name='LoginName']")
	static WebElement MaarUserName;
	@FindBy(xpath = "//input[@id='Password']")
	static WebElement MaarPassword;
	@FindBy(xpath = "//input[@name='Enter']")
	static WebElement MaarLogin;
	@FindBy(xpath = "//span[@id='resources-nav']")
	static WebElement MaarResource;
	@FindBy(xpath = "//a[text()='NewHomeSource Pro']")
	static WebElement NhsProOption;
	@FindBy(xpath = "//button[text()='Close']")
	static WebElement Close;
	@FindBy(xpath = "//span[@id='user-name-menu']")
	static WebElement Welcome;
	@FindBy(xpath = "//span[text()='NewHomeSource Pro']")
	static WebElement Backtowindow;
	@FindBy(xpath = "//span[text()='LOG OUT']")
	static WebElement LogOut;
	@FindBy(xpath = "//meta[@name='ServerName']")
	static WebElement ServerName;

	@Test(description="Validate the Maar Login")
	public void Validate_Maar_login() {
		PageFactory.initElements(driver, Maar.class);
		driver.get("https://www.newhomesourceprofessional.com/maar");
		driver.manage().window().maximize();
		JsonUtility json=new JsonUtility("SSOData");
		System.out.println("Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO, "Server Name is "+ServerName.getAttribute("content"));
		CommonSignIn.click();
		logger.log(Status.INFO, "Clicked on SignIn link");
		System.out.println("Clicked on SignIn link");
		String actual=obj.SSO_Login_text(driver);
		System.out.println(json.getJsonData("SSO_text","ALL_SSO_Text"));
		String expectedtext=json.getJsonData("SSO_text","ALL_SSO_Text")+"voilà! You'll be signed in and ready to take action.";
		Assert.assertTrue(actual.equalsIgnoreCase(expectedtext),"Login text Does not match");
		logger.log(Status.INFO,"Login text = "+actual);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(SignIn_button));
		SignIn_button.click();
		logger.log(Status.INFO, "Clicked on SignIn button");
		System.out.println("Clicked on SignIn button");
		wait.until(ExpectedConditions.visibilityOf(MaarUserName));
		MaarUserName.sendKeys(MaarUid);
		logger.log(Status.INFO, "Maar Username Entered");
		System.out.println("Maar Username Entered");
		wait.until(ExpectedConditions.visibilityOf(MaarPassword));
		MaarPassword.sendKeys(MaarPwd);
		logger.log(Status.INFO, "Maar Password Entered");
		System.out.println("Maar Password Entered");
		/*
		 * Robot r = null; try { r = new Robot(); } catch (AWTException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * r.keyPress(KeyEvent.VK_ENTER); r.keyRelease(KeyEvent.VK_ENTER);
		 */
		MaarLogin.click();
		ActionsUtility.staticWait(3);
		try {
		if(driver.findElements(By.xpath("//button[text()='Close']")).size() > 0); {
		Close.click(); }
		}
		catch(Exception e)
		{
			System.out.println("Message pop up does not display");
		} 
		logger.log(Status.INFO, "Maar Submit Button Clicked");
		System.out.println("Maar Submit Button Clicked");
		wait.until(ExpectedConditions.visibilityOf(MaarResource));
		logger.log(Status.INFO, "Maar User Redirected To The Dashboard");
		System.out.println("Maar User Redirected To The Dashboard");
		MaarResource.click();
		ActionsUtility.staticWait(1);
		NhsProOption.click();
		logger.log(Status.INFO, "Select NHS Pro From The Dashboard");
		System.out.println("Select NHS Pro From The Dashboard");
		ActionsUtility.staticWait(3);
		String original=driver.getWindowHandle();
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		try {
			wait.until(ExpectedConditions.visibilityOf(Welcome));
			ActionsUtility.staticWait(3);
			String actualtext=Welcome.getText();
			System.out.println(actualtext);
			Assert.assertTrue(actualtext.contains("NewHomeSource"),"Maar User Login Failed");
			logger.log(Status.INFO, "Maar User Login LoggedIn Sucessfully");
			System.out.println("Maar User Login LoggedIn Sucessfully");
			obj.clickOnMyAccount(driver);
			ActionsUtility.staticWait(3);
			logger.log(Status.INFO,"FirstName : "+obj.getFirstName(driver));
			logger.log(Status.INFO,"LastName : "+obj.getLastName(driver));
			logger.log(Status.INFO, "ZipCode : "+obj.getZipCode(driver));
			logger.log(Status.INFO,"Email : "+obj.getEmail(driver));
			logger.log(Status.INFO,"AgencyName : "+obj.getAgencyName(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
			if(obj.getAgencyName(driver).equals(""))
			{
				Assert.fail("Agency is missing");
			}   
			driver.switchTo().window(original);
			ActionsUtility.staticWait(2);
			Backtowindow.click();
			ActionsUtility.staticWait(2);
			LogOut.click();
			System.out.println("User Logged out");
			logger.log(Status.INFO, "Maar User Logged Out Sucessfully");
			System.out.println("Maar Maar User Logged Out Sucessfully");
		}
		catch(Exception e)
		{
			driver.findElement(By.xpath("//h1[text()='Welcome!']")).isDisplayed();
			logger.log(Status.INFO,"Confirm account detail pop up displayed");
			System.out.println("Confirm account detail pop up displayed");
			  logger.log(Status.INFO,"FirstName : "+obj.getFirstNameFromConfirmPopUp(driver)); 
			  logger.log(Status.INFO,"LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			  logger.log(Status.INFO, "ZipCode : "+obj.getZipCodeFromConfirmPopUp(driver));
			  logger.log(Status.INFO,"Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("First Name: "+obj.getFirstNameFromConfirmPopUp(driver));
			System.out.println("LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			System.out.println("Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("ZipCode "+obj.getZipCodeFromConfirmPopUp(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
		}
	}

}
