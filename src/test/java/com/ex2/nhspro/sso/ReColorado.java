package com.ex2.nhspro.sso;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBase;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;


public class ReColorado extends SSOBase{
	SSOBase obj=new SSOBase();
	//Recolorado TestCase Locators.
	//Recolorado Credentail	
	String RecoloradoUid="Beta22";
	String RecoloradoPwd="Luckydog1";
	
	@FindBy(xpath = "(//a[text()='Sign In'])[1]")
	static WebElement CommonSignIn;
	@FindBy(xpath = "//a[@class='btn btn-orange']")
	static WebElement SignIn_button;
	@FindBy(xpath="//a[text()='Click to remain on this page']")
	static WebElement Toremainthispage;
	@FindBy(xpath="//input[@id='username']")
	static WebElement RecoloradoUsername;
	@FindBy(xpath="//input[@id='password']")
	static WebElement RecoloradoPassword;
	@FindBy(xpath="//button[@id='loginbtn']")
	static WebElement RecoloradoLogin;
	@FindBy(xpath = "//span[@id='user-name-menu']")
	static WebElement Welcome;
	@FindBy(xpath = "//meta[@name='ServerName']")
	static WebElement ServerName;
	
	@Test(description="Validate the ReColorado Login")
	public void Validate_ReColorado_login()
	{
		PageFactory.initElements(driver,ReColorado.class);
		driver.get("https://www.newhomesourceprofessional.com/ReColorado");
		driver.manage().window().maximize();
		JsonUtility json=new JsonUtility("SSOData");
		System.out.println("Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO, "Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO,"SAML Request ");
		CommonSignIn.click();
		logger.log(Status.INFO, "Clicked on SignIn link");
		System.out.println("Clicked on SignIn link");
		String actual=obj.SSO_Login_text(driver);
		System.out.println(json.getJsonData("SSO_text","ALL_SSO_Text"));
		String expectedtext=json.getJsonData("SSO_text","ALL_SSO_Text")+"voilà! You'll be signed in and ready to take action.";
		Assert.assertTrue(actual.equalsIgnoreCase(expectedtext),"Login text Does not match");
		logger.log(Status.INFO,"Login text = "+actual);
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(SignIn_button));
		SignIn_button.click();
		logger.log(Status.INFO, "Clicked on SignIn Button");
		System.out.println("Clicked on SignIn Button");
		wait.until(ExpectedConditions.visibilityOf(Toremainthispage));
		Toremainthispage.click();
		logger.log(Status.INFO, "Clicked on to remain on this page");
		System.out.println("Clicked on to remain on this page");
		wait.until(ExpectedConditions.visibilityOf(RecoloradoUsername));
		ActionsUtility.staticWait(1);
		RecoloradoUsername.sendKeys(RecoloradoUid);
		logger.log(Status.INFO, "Recolorado Username Entered");
		System.out.println("Recolorado Username Entered");
		wait.until(ExpectedConditions.visibilityOf(RecoloradoPassword));
		RecoloradoPassword.sendKeys(RecoloradoPwd);
		logger.log(Status.INFO, "Recolorado Password Entered");
		System.out.println("Recolorado Password Entered");
		RecoloradoLogin.click();
		ActionsUtility.staticWait(4);
		logger.log(Status.INFO, "Recolorado Submit Button Clicked");
		System.out.println("Recolorado Submit Button Clicked");
		try {	String actualtext=Welcome.getText();
		System.out.println(actualtext);
		Assert.assertTrue(actualtext.contains("Beta"),"ReColorado User Login Failed");
		logger.log(Status.INFO, "ReColorado User Login LoggedIn Sucessfully");
		System.out.println("ReColorado User Login LoggedIn Sucessfully");
		obj.clickOnMyAccount(driver);
		ActionsUtility.staticWait(3);
		logger.log(Status.INFO,"FirstName : "+obj.getFirstName(driver));
		logger.log(Status.INFO,"LastName : "+obj.getLastName(driver));
		logger.log(Status.INFO, "ZipCode : "+obj.getZipCode(driver));
		logger.log(Status.INFO,"Email : "+obj.getEmail(driver));
		logger.log(Status.INFO,"AgencyName : "+obj.getAgencyName(driver));
		if(obj.getFirstName(driver).equals(""))
		{
			Assert.fail("FirstName is missing");
		}
		if(obj.getLastName(driver).equals(""))
		{
			Assert.fail("LastName is missing");
		}
		if(obj.getZipCode(driver).equals(""))
		{
			Assert.fail("ZipCode is missing");	
		}
		if(obj.getEmail(driver).equals(""))
		{
			Assert.fail("Email is missing");
		}
		if(obj.getAgencyName(driver).equals(""))
		{
			Assert.fail("Agency is missing");
		} 
		}
		catch(Exception e)
		{
			driver.findElement(By.xpath("//h1[text()='Welcome!']")).isDisplayed();
			logger.log(Status.INFO,"Confirm account detail pop up displayed");
			System.out.println("Confirm account detail pop up displayed");
			  logger.log(Status.INFO,"FirstName : "+obj.getFirstNameFromConfirmPopUp(driver)); 
			  logger.log(Status.INFO,"LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			  logger.log(Status.INFO, "ZipCode : "+obj.getZipCodeFromConfirmPopUp(driver));
			  logger.log(Status.INFO,"Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("First Name: "+obj.getFirstNameFromConfirmPopUp(driver));
			System.out.println("LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			System.out.println("Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("ZipCode "+obj.getZipCodeFromConfirmPopUp(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
		} 
	}
}

