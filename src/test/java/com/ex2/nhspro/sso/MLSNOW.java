package com.ex2.nhspro.sso;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBase;

import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;


public class MLSNOW extends SSOBase{
	SSOBase obj=new SSOBase();
	String yesmlsUid = "79";
	String yesmlsPwd = "NHSP";

	@FindBy(xpath = "(//a[text()='Sign In'])[1]")
	static WebElement CommonSignIn;
	@FindBy(xpath = "//a[@class='btn btn-orange']")
	static WebElement SignIn_button;
	@FindBy(xpath = "//input[@id='email']")
	static WebElement yesmlsUserName;
	@FindBy(xpath = "//input[@id='password']")
	static WebElement yesmlsPassword;
	@FindBy(xpath = "//button[@id='btn-login']")
	static WebElement yesmlsLogin;
	@FindBy(xpath = "//span[@id='user-name-menu']")
	static WebElement Welcome;
	@FindBy(xpath = "//meta[@name='ServerName']")
	static WebElement ServerName;
	@FindBy(xpath = "//img[@title='NewConstruction']")
	static WebElement NHSPOnDashboard;
	

	@Test(description="Validate the mlsNow Login")
	public void Validate_mlsNow_login() {
		PageFactory.initElements(driver, MLSNOW.class);
		String url="https://www.newhomesourceprofessional.com/mlsnow";
		driver.get(url);
		driver.manage().window().maximize();
		JsonUtility json=new JsonUtility("SSOData");
		System.out.println("Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO, "Server Name is "+ServerName.getAttribute("content"));
		logger.log(Status.INFO,"SAML Request ");
		CommonSignIn.click();
		logger.log(Status.INFO, "Clicked on SignIn link");
		System.out.println("Clicked on SignIn link");
		String actual=obj.SSO_Login_text(driver);
		System.out.println(json.getJsonData("SSO_text","ALL_SSO_Text"));
		String expectedtext=json.getJsonData("SSO_text","ALL_SSO_Text")+"voilà! You'll be signed in and ready to take action.";
		Assert.assertTrue(actual.equalsIgnoreCase(expectedtext),"Login text Does not match");
		logger.log(Status.INFO,"Login text = "+actual);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(SignIn_button));
		SignIn_button.click();
		logger.log(Status.INFO, "Clicked on SignIn button");
		System.out.println("Clicked on SignIn button");
		wait.until(ExpectedConditions.visibilityOf(yesmlsUserName));
		yesmlsUserName.sendKeys(yesmlsUid);
		logger.log(Status.INFO, "yesMls Username Entered");
		System.out.println("mlsNow Username Entered");
		wait.until(ExpectedConditions.visibilityOf(yesmlsPassword));
		yesmlsPassword.sendKeys(yesmlsPwd);
		logger.log(Status.INFO, "yesMls Password Entered");
		System.out.println("mlsNow Password Entered");
		yesmlsLogin.click();
		logger.log(Status.INFO, "yesMls Submit Button Clicked");
		System.out.println("yesMls Submit Button Clicked");
		wait.until(ExpectedConditions.visibilityOf(NHSPOnDashboard));
		NHSPOnDashboard.click();
		System.out.println("Selected NHS Pro From the Dashboard");
		ActionsUtility.staticWait(3);
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		try {
			wait.until(ExpectedConditions.visibilityOf(Welcome));
			String actualtext=Welcome.getText();
			System.out.println(actualtext);
			Assert.assertTrue(actualtext.contains("New Home"),"yesMls User Login Failed");
			logger.log(Status.INFO, "mlsNow User Login LoggedIn Sucessfully");
			System.out.println("mlsNow User Login LoggedIn Sucessfully");
			obj.clickOnMyAccount(driver);
			ActionsUtility.staticWait(3);
			logger.log(Status.INFO,"FirstName : "+obj.getFirstName(driver));
			logger.log(Status.INFO,"LastName : "+obj.getLastName(driver));
			logger.log(Status.INFO, "ZipCode : "+obj.getZipCode(driver));
			logger.log(Status.INFO,"Email : "+obj.getEmail(driver));
			logger.log(Status.INFO,"AgencyName : "+obj.getAgencyName(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
			if(obj.getAgencyName(driver).equals(""))
			{
				Assert.fail("Agency is missing");
			} 

		}
		catch(Exception e)
		{
			driver.findElement(By.xpath("//h1[text()='Welcome!']")).isDisplayed();
			logger.log(Status.INFO,"Confirm account detail pop up displayed");
			System.out.println("Confirm account detail pop up displayed");
			  logger.log(Status.INFO,"FirstName : "+obj.getFirstNameFromConfirmPopUp(driver)); 
			  logger.log(Status.INFO,"LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			  logger.log(Status.INFO, "ZipCode : "+obj.getZipCodeFromConfirmPopUp(driver));
			  logger.log(Status.INFO,"Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("First Name: "+obj.getFirstNameFromConfirmPopUp(driver));
			System.out.println("LastName : "+obj.getLastNameFromConfirmPopUp(driver));
			System.out.println("Email : "+obj.getEmailFromConfirmPopUp(driver));
			System.out.println("ZipCode "+obj.getZipCodeFromConfirmPopUp(driver));
			if(obj.getFirstName(driver).equals(""))
			{
				Assert.fail("FirstName is missing");
			}
			if(obj.getLastName(driver).equals(""))
			{
				Assert.fail("LastName is missing");
			}
			if(obj.getZipCode(driver).equals(""))
			{
				Assert.fail("ZipCode is missing");	
			}
			if(obj.getEmail(driver).equals(""))
			{
				Assert.fail("Email is missing");
			}
		} 
	}
}
