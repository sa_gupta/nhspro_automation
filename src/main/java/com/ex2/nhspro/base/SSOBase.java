package com.ex2.nhspro.base;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.GetScreenshot;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SSOBase extends BrowserFactory{
	
	
	
	@Parameters({"browser"})
	@BeforeMethod
	public void beforeStart(@Optional("chrome")String browser,Method m) {
		launchBrowser(browser);
		System.out.println("================================= Start Execution:- "+m.getName()+"===================================");
			logger = extent.createTest(m.getName());
	}

	@BeforeTest
	public void ssoReporting() throws UnknownHostException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/ExtentReports/SSO/"+ActionsUtility.nameWithDate()+".html");
		// Create an object of Extent Reports
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Name", InetAddress.getLocalHost().getHostName());
		extent.setSystemInfo("Environment", PropertyFileOperation.getPropertyValue("environment"));
		extent.setSystemInfo("User Name", System.getProperty("user.name"));
		htmlReporter.config().setDocumentTitle("ExtentReport for NHSPro");
		// Name of the report
		htmlReporter.config().setReportName("Execution Report");
		// Dark Theme
		htmlReporter.config().setTheme(Theme.DARK);

	}
	@AfterMethod
	public void tearDown(ITestResult result,Method m) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			// MarkupHelper is used to display the output in different colors
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
			// To capture screenshot path and store the path of the screenshot in the string
			// "screenshotPath"
			// We do pass the path captured by this method in to the extent reports using
			// "logger.addScreenCapture" method.
			// String Scrnshot=TakeScreenshot.captuerScreenshot(driver,"TestCaseFailed");
			String screenshotPath = GetScreenshot.getScreenShot(driver, result.getName());
			// To add it in the extent report
	//		logger.fail("Test Case Failed Snapshot is below " + logger.addScreenCaptureFromPath(screenshotPath));
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			logger.log(Status.PASS,
					MarkupHelper.createLabel(result.getName() + " Test Case PASSED", ExtentColor.GREEN));
			String screenshotPath = GetScreenshot.getScreenShot(driver, result.getName());
			// To add it in the extent report
//			logger.pass("Test Case Passed Snapshot is below " + logger.addScreenCaptureFromPath(screenshotPath));
		System.out.println("--------------------------------------End:- "+m.getName()+"----------------------------------------- ");
		driver.quit();
		extent.flush();
	}
	
	}
	
	
	public String SSO_Login_text(WebDriver driver)
	{
		String text=driver.findElement(By.xpath("(//div[@class='clearfix sso-login-container']/p)[1]")).getText();
		System.out.println(text);
		return text;
	}
	
	public void clickOnMyAccount(WebDriver driver)
	{
		Actions action =new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//span[@id='user-name-menu']"))).build().perform();
		driver.findElement(By.xpath("//a[text()='My Account']")).click();
	}
	public String getFirstName(WebDriver driver)
	{
		String firstname=driver.findElement(By.xpath("//input[@id='FirstName']")).getAttribute("value");
		return firstname;
	}
	
	public String getLastName(WebDriver driver)
	{
		String lastname=driver.findElement(By.xpath("//input[@id='LastName']")).getAttribute("value");
		return lastname;
	}
	public String getZipCode(WebDriver driver)
	{
		String zipCode=driver.findElement(By.xpath("//input[@id='ZipCode']")).getAttribute("value");
		return zipCode;
	}
	public String getEmail(WebDriver driver)
	{
		String email=driver.findElement(By.xpath("//input[@id='Username']")).getAttribute("value");
		return email;
	}
	public String getAgencyName(WebDriver driver)
	{
		String agencyName=driver.findElement(By.xpath("//input[@id='AgencyName']")).getAttribute("value");
		return agencyName;
	}
	
	public String getFirstNameFromConfirmPopUp(WebDriver driver)
	{
		return (String) ((JavascriptExecutor) driver).executeScript("return document.getElementById('FirstName').getAttribute('value');");
		
	}
	public String getLastNameFromConfirmPopUp(WebDriver driver)
	{
		return (String) ((JavascriptExecutor) driver).executeScript("return document.getElementById('LastName').getAttribute('value');");
		
	}
	public String getEmailFromConfirmPopUp(WebDriver driver)
	{
		return (String) ((JavascriptExecutor) driver).executeScript("return document.getElementById('email').getAttribute('value');");
		
	}
	public String getZipCodeFromConfirmPopUp(WebDriver driver)
	{
		return (String) ((JavascriptExecutor) driver).executeScript("return document.getElementById('ZipCode').getAttribute('value');");
		
	}
	
}
