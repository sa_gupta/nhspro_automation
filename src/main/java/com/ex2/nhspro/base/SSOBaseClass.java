package com.ex2.nhspro.base;


import java.net.MalformedURLException;
import java.net.URL;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.ex2.nhspro.utility.PropertyFileOperation;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SSOBaseClass {

	public  WebDriver driver;
	protected AppiumDriver appiumDriver;
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest parentTest;
	public static ExtentTest logger;
	private AppiumDriverLocalService service;
	private AppiumServiceBuilder builder;
	DesiredCapabilities cap;
	

	
	public void launchBrowser(String browser) {
		if (browser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
			driver = new ChromeDriver();
			DriverFactory.getInstance().setDriver(driver);
			driver = DriverFactory.getInstance().getDriver();
		} else if (browser.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("ie")) {
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
		} else if (browser.equalsIgnoreCase("edge")) {
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")),
				TimeUnit.SECONDS);
	}
	

//	public void dockerStart(String dockerstart) {
//		DesiredCapabilities cap=DesiredCapabilities.chrome();
//		URL url;
//		try {
//			url = new URL("http://localhost:4444/wd/hub");
//			driver=new RemoteWebDriver(url,cap);
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")),
//				TimeUnit.SECONDS);
//		
//	}

//	public void openAgentApplication() {
//		String agentUrl=getAgentBaseUrl();
//		System.out.println("URL launched is :-"+agentUrl);
//		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
//			driver.get(agentUrl);
//		}else
//		{
//			appiumDriver.get(agentUrl);
//			driver=appiumDriver;
//		}
//	}
	
//	public String getAgentBaseUrl() {
//		String environment = PropertyFileOperation.getPropertyValue("environment");
//		
//		String agentUrl = null;
//		if (environment.equalsIgnoreCase("sprint")) {
//			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_sprint");
//		} else if (environment.equalsIgnoreCase("stage")) {
//			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_stage");
//		} else if (environment.equalsIgnoreCase("production")) {
//			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_production");
//		}else if(environment.equalsIgnoreCase("hotfix")) {
//			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_hotfix");
//		}
//		return agentUrl;
//	}
//	
//	public String getCurrentUrl() {
//		return driver.getCurrentUrl();
//	}
//
//	public void openClientApplication() {
//		String environment = PropertyFileOperation.getPropertyValue("environment");
//		if (environment.equalsIgnoreCase("sprint")) {
//			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_sprint"));
//		} else if (environment.equalsIgnoreCase("stage")) {
//			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_stage"));
//		} else if (environment.equalsIgnoreCase("production")) {
//			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_production"));
//		}
//		else if (environment.equalsIgnoreCase("hotfix")) {
//			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_hotfix"));
//		}
//	}
//	
//
//	public void applicationOnMobile() {
//	cap = new DesiredCapabilities();
//		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, PropertyFileOperation.getPropertyValue("platformName"));
//		cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, PropertyFileOperation.getPropertyValue("platformVersion"));
//		cap.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");
//		cap.setCapability(MobileCapabilityType.DEVICE_NAME, PropertyFileOperation.getPropertyValue("deviceName"));
//		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator1");
//		cap.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
//		cap.setCapability("noReset", true);
//		cap.setCapability("locationServicesAuthorized", true);
//		cap.setCapability("autoAcceptAlerts", true);
//		cap.setCapability("autoGrantPermissions", true);
//		
////		cap.setCapability(MobileCapabilityType.d, value);
//		System.setProperty("webdriver.chrome.driver",
//
//				System.getProperty("user.dir")+"src/resources/java/Drivers/chromedriver.exe");
//
//		// WebDriverManager.chromedriver().setup();
//		URL url;
//		try {
//			url = new URL("http://0.0.0.0:4723/wd/hub");
//			appiumDriver = new AndroidDriver<MobileElement>(url, cap);
//			driver=appiumDriver;
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		appiumDriver.manage().timeouts().implicitlyWait(Integer.valueOf((PropertyFileOperation.getPropertyValue("implicitWait"))),
//				TimeUnit.SECONDS);
////		driver=(AndroidDriver)appiumDriver;
//		System.out.println("Execution on mobile");
//	
//	}

//	private void startServer() {
//		//Build the Appium service
//		builder = new AppiumServiceBuilder();
//		builder.withIPAddress("127.0.0.1");
//		builder.usingPort(4723);
//		builder.withCapabilities(cap);
//		builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
//		builder.withArgument(GeneralServerFlag.LOG_LEVEL,"error");
//		
//		//Start the server with the builder
//		service = AppiumDriverLocalService.buildService(builder);
//		service.start();
//	}	
}
