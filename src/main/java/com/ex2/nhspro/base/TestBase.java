package com.ex2.nhspro.base;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.ex2.nhspro.utility.PropertyFileOperation;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

public class TestBase {

	protected WebDriver driver;
	BrowserFactory bf=new BrowserFactory();
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest parentTest;
	public ExtentTest logger;
	protected AppiumDriver appiumDriver;
	private AppiumDriverLocalService service;
	private AppiumServiceBuilder builder;
	DesiredCapabilities cap;
	
	public void launchBrowserInstance(String browser) {
		WebDriver driverInstance = bf.launchBrowser(browser);
		DriverFactory.getInstance().setDriver(driverInstance);
		driver = DriverFactory.getInstance().getDriver();
	}
	
	public void openAgentApplication() {
		String agentUrl=getAgentBaseUrl();
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			driver.get(agentUrl);
		}else
		{
			appiumDriver.get(agentUrl);
			driver=appiumDriver;
		}
	}
	
	public String getAgentBaseUrl() {
		String environment = PropertyFileOperation.getPropertyValue("environment");
	//	String environment=System.getProperty("environment");
		String agentUrl = null;
		if (environment.equalsIgnoreCase("sprint")) {
			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_sprint");
		} else if (environment.equalsIgnoreCase("stage")) {
			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_stage");
		} else if (environment.equalsIgnoreCase("production")) {
			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_production");
		}else if(environment.equalsIgnoreCase("hotfix")) {
			agentUrl=PropertyFileOperation.getPropertyValue("agentUrl_hotfix");
		}
		return agentUrl;
	}
	

	public void openClientApplication() {
		String environment = PropertyFileOperation.getPropertyValue("environment");
	//	String environment=System.getProperty("environment");
		if (environment.equalsIgnoreCase("sprint")) {
			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_sprint"));
		} else if (environment.equalsIgnoreCase("stage")) {
			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_stage"));
		} else if (environment.equalsIgnoreCase("production")) {
			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_production"));
		}
		else if (environment.equalsIgnoreCase("hotfix")) {
			driver.get(PropertyFileOperation.getPropertyValue("clientUrl_hotfix"));
		}
	}
	
	public void applicationOnMobile() {
		cap = new DesiredCapabilities();
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, PropertyFileOperation.getPropertyValue("platformName"));
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, PropertyFileOperation.getPropertyValue("platformVersion"));
			cap.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, PropertyFileOperation.getPropertyValue("deviceName"));
			cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
			cap.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS,true);
			cap.setCapability("noReset", true);
			cap.setCapability("locationServicesAuthorized", true);
			cap.setCapability("autoAcceptAlerts", true);
			cap.setCapability("autoGrantPermissions", true);

			System.setProperty("webdriver.chrome.driver",

					System.getProperty("user.dir")+"src/resources/java/Drivers/chromedriver.exe");

			// WebDriverManager.chromedriver().setup();
			URL url;
			try {
				url = new URL("http://0.0.0.0:4723");
				appiumDriver = new AndroidDriver(url, cap);
				driver=appiumDriver;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			appiumDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(50));
//			driver=(AndroidDriver)appiumDriver;
			System.out.println("Execution on mobile");
		
		}
	
	
	
//	@AfterMethod
	public void tearDown(){
		DriverFactory.getInstance().removeDriver();
	}
	
	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}
	
}
