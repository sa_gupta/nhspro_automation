package com.ex2.nhspro.api;



import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ex2.nhspro.utility.PropertyFileOperation;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;




public class SRP_api {
	
	SRP_api srpApi;
	
	public SRP_api(){
	String environment=PropertyFileOperation.getPropertyValue("environment");
	if(environment.equalsIgnoreCase("sprint"))
	{
		RestAssured.baseURI=PropertyFileOperation.getPropertyValue("apiUrl_sprint");
		System.out.println(environment);
	}
	else if(environment.equalsIgnoreCase("stage")) {
		RestAssured.baseURI=PropertyFileOperation.getPropertyValue("apiUrl_stage");
		System.out.println(environment);
	}
	else {
		RestAssured.baseURI=PropertyFileOperation.getPropertyValue("apiUrl_production");
		System.out.println(environment);
	}
	}
	
	public String getSRPResponseForZipcode(String ZipCode) {
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET,"Search/Homes?partnerid=88&marketid=269&PostalCode="+ZipCode+"&SortBy=Random&SortSecondBy=None&countsonly=1");
	    System.out.println(response.getBody().asString());
		JsonPath jsonPath = response.jsonPath();
		System.out.println(jsonPath.getString("ResultCounts.HomeCount"));
		return String.valueOf(jsonPath.get("ResultCounts.Facets"));
	}
	
	public String getNoOfHomesWithSublocation(String SubLocation,String Value) {
		String path;
		RequestSpecification httpRequest = RestAssured.given();
		
		switch(SubLocation)
		{
		case "Zipcode":
			path="Search/Homes?partnerid=88&marketid=269&PostalCode="+Value+"&SortBy=Random&SortSecondBy=None&countsonly=1";
			break;
		case "NearByCity":
			path="Search/Homes?partnerid=88&marketid=269&City="+Value+"&SortBy=Random&SortSecondBy=None&countsonly=1";
			break;
		case "SchoolDistricts":
			path="Search/Homes?partnerid=88&marketid=269&districtids="+Value+"&SortBy=Random&SortSecondBy=None&countsonly=1";
			break;
		case "Counties":
			path="Search/Homes?partnerid=88&marketid=269&County="+Value+"&SortBy=Random&SortSecondBy=None&countsonly=1";
			break;
		default:
			path="Search/Homes?partnerid=88&marketid=269&SortBy=Random&SortSecondBy=None&countsonly=1";
		}
		Response response = httpRequest.request(Method.GET,path);
		JsonPath jsonPath = response.jsonPath();
	//	return jsonPath.getString("");
		return jsonPath.getString("ResultCounts.HomeCount");	
	}
	
	@BeforeClass
	public void beforeClas()
	{
		 srpApi=new SRP_api();
		 
		 
		// RequestSpecification request=RestAssured.given()
			//	 .baseUri(environment)
		 
	}
	
	
	@Test
	public void srpApi()
	{
		String response=srpApi.getSRPResponseForZipcode("73301");
		 System.out.println(response);
	}
	
	
	
	/*
	 * public static void main(String[] args) { SRP_api api=new SRP_api();
	 * api.getSRPResponseForZipcode("73301"); }
	 */
	
	
	
	//https://sprint-api.newhomesource.com/api/v2/Search/Homes?partnerid=88&marketid=269&PostalCode=76527&SortBy=Random&SortSecondBy=None&bed=2&bath=5&prlow=1000&prhigh=20000&sflow=100&sfhigh=20000&
	
	
	 
}
