package com.ex2.nhspro.utility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.Assert;

public class DatabaseConnectivityUtility {

	Connection conn;
	Statement smt;
	static ResultSet result;
	public String RequestId,CommentText,LeadPageAction,RequesttyepeCode,LeadPage,PlanId,SpecId,CommunityId,BuilderId;

	public void createDBConnection()

	{
		// databaseName=BHITransaction"
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String dbURL = "jdbc:sqlserver://devbuildsql5\\SQLFULL:1433;databaseName=BHITransaction";
			String user = "ritkumar";
			String pass = "rit@1234";
			conn = DriverManager.getConnection(dbURL, user, pass);
			System.out.println("Connection established");
			smt = conn.createStatement();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ResultSet executeLeadQuery(String emailId) {
		ActionsUtility.staticWait(6);
		try {
			result = smt.executeQuery("use bhitransaction\r\n" + "select top 1 * from [dbo].[request] r\r\n"
					+ "join [dbo].[request_item] ri on r.request_id = ri.request_id\r\n"
					+ "where partner_id = 88 and r.email_address='" + emailId + "'\r\n" + "order by r.request_id desc");
			while (result.next()) {
				RequestId=result.getString("request_id");
				CommentText=result.getString("comment_text");
				RequesttyepeCode=result.getString("request_type_code");
				LeadPage=result.getString("lead_page");
				LeadPageAction=result.getString("lead_page_action");
				PlanId=result.getString("plan_id");
				SpecId=result.getString("specification_id");
				CommunityId=result.getString("community_id");
				BuilderId=result.getString("builder_id");
				
			}
			return result;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public String get_RequestId() {
		System.out.println("In method");
		try {
			while (result.next()) {
				System.out.println("In while");
				System.out.println(result.getString("request_id"));
				System.out.println(result.getString("comment_text"));
				System.out.println(result.getString("lead_page_action"));
				return result.getString("request_id");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	//	return getValue("request_id");
	}





	public String get_planId() {
		return getValue("plan_id");
	}

	public String get_specId() {
		return getValue("specification_id");
	}

	public String getCommunityId() {
		return getValue("community_id");
	}

	public String getBuilder_id() {
		return getValue("builder_id");
	}

	private String getValue(String Column_name) {
		try {
			while (result.next()) {
				return result.getString(Column_name);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void validateValueFromDB(String BeforeLeadRequestId, String Expected_CommentText,
			String ExpectedRequestTypeCode, String Expected_LeadPage, String Expected_LeadPageAction,
			String Expected_HomeId, String Expected_BuilderId) {

		if (Integer.valueOf(BeforeLeadRequestId) < Integer.valueOf(RequestId))
			Assert.assertTrue(true, "Before Lead RequestId was:-" + BeforeLeadRequestId
					+ " and Request Id after lead is:-" + RequestId);
		else
			Assert.assertTrue(false, "Before Lead RequestId was:-" + BeforeLeadRequestId
					+ " and Request Id after lead is:-" + RequestId);
		/*logger.log(Status.PASS, "Before Lead RequestId was:-" + BeforeLeadRequestId + " and Request Id after lead is:-"
				+ RequestId);*/
		System.out.println("Before Lead RequestId was:-" + BeforeLeadRequestId + " and Request Id after lead is:-"
				+ RequestId);

		Assert.assertEquals(CommentText, Expected_CommentText);
	/*	logger.log(Status.PASS,
				"Expected CommentText:-" + Expected_CommentText + " and Actual CommentText:-" + database.CommentText);*/
		System.out.println(
				"Expected CommentText:-" + Expected_CommentText + " and Actual CommentText:-" + CommentText);

		Assert.assertEquals(LeadPage, Expected_LeadPage);
		/*logger.log(Status.PASS,
				"Expected LeadPage:-" + Expected_LeadPage + " and Actual LeadPage:-" + LeadPage);*/
		System.out.println(
				"Expected leadPage:-" + Expected_LeadPage + " and Actual leadPage:-" + LeadPage);

		Assert.assertEquals(LeadPageAction, Expected_LeadPageAction);
		/*log(Status.PASS, "Expected LeadPageAction:-" + Expected_LeadPageAction + " and Actual LeadPageAction:-"
				+ LeadPageAction);*/
		System.out.println("Expected LeadPageAction:-" + Expected_LeadPageAction + " and Actual CommentText:-"
				+ LeadPageAction);

		Assert.assertEquals(RequesttyepeCode, ExpectedRequestTypeCode);
	/*	logger.log(Status.PASS, "Expected Requesttypecode:-" + ExpectedRequestTypeCode + " and Actual RequestTypeCode:-"
				+ RequesttyepeCode);*/
		System.out.println("Expected Requesttypecode:-" + ExpectedRequestTypeCode + " and Actual RequestTypeCode:-"
				+ RequesttyepeCode);
		if (Expected_HomeId.startsWith("s")) {
			Assert.assertEquals(PlanId, null);
			Assert.assertEquals(CommunityId, null);
			Assert.assertEquals(SpecId, Expected_HomeId.replace("s", ""),
					"Expected SpecId:" + Expected_HomeId + " Actual SpecId" + SpecId);
		} else if (Expected_HomeId.startsWith("p")) {
			Assert.assertEquals(SpecId, null);
			Assert.assertEquals(CommunityId, null);
			Assert.assertEquals(PlanId, Expected_HomeId.replace("p", ""),
					"Expected PlanId:" + Expected_HomeId + " Actual PlanId" + PlanId);
		} else if (CommunityId != null) {
			Assert.assertEquals(PlanId, null);
			Assert.assertEquals(SpecId, null);
			Assert.assertEquals(CommunityId, Expected_HomeId,
					"Expected CommunityId:" + Expected_HomeId + " Actual CommunityId" + CommunityId);
/*			Assert.assertEquals(BuilderId, Expected_BuilderId,
					"Expected BuilderId:" + Expected_BuilderId + "Actual PlanId" + BuilderId);*/
		} else
			Assert.assertTrue(false, "PlanId,SpecId and CommunityID is blank");

	}
}	



