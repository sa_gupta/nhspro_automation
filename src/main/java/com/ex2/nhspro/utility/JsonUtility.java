package com.ex2.nhspro.utility;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JsonUtility {
	WebDriver driver;
	JSONParser parser = new JSONParser();

	JSONObject jsonObject;

	JSONObject locator;
	String JsonFile = null;
	Object obj;


	public JsonUtility(WebDriver driver, String fileName) {
		this.driver = driver;
		intializeJsonFile(fileName);
	}

	public JsonUtility(String FileName) {
		JSONParser parser = new JSONParser();

		try {
			obj = parser.parse(
					new FileReader(System.getProperty("user.dir") + "//src/resources/java/TestData/" + FileName + ".json"));
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void intializeJsonFile(String FileName) {
		// System.out.println("File name is:-"+FileName);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			JsonFile = System.getProperty("user.dir") + "/src/main/java/com/ex2/nhspro/desktop_or/" + FileName
					+ ".json";
		else
			JsonFile = System.getProperty("user.dir") + "/src/main/java/com/ex2/nhspro/mobile_or/" + FileName + ".json";
//		System.out.println("Json file"+JsonFile);
	}

	public WebElement getWebElement(String elementName) {
		WebElement pageElement = null;
		try {
			Object obj = parser.parse(new FileReader(JsonFile));
			jsonObject = (JSONObject) obj;
			
			pageElement = getElement(jsonObject.get(elementName));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return pageElement;
	}
	
	public WebElement getWebElement(String ParentKey,String elementName) {
		WebElement pageElement = null;
		try {
			Object obj = parser.parse(new FileReader(JsonFile));
			jsonObject = (JSONObject) obj;
			JSONObject jsonObject1=(JSONObject)jsonObject.get(ParentKey);
	//		jsonObject1.get("pricevalue");
			pageElement = getElement(jsonObject1.get(elementName));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return pageElement;
	}

	private WebElement getElement(Object object) {
		locator = (JSONObject) object;
		WebElement element = null;
		Set<String> elementLocator = locator.keySet();
		if (elementLocator.contains("id")) 
			element = driver.findElement(By.id((String) locator.get("id")));
		 else if (elementLocator.contains("name")) 
			element = driver.findElement(By.name((String) locator.get("name")));
		 else if (elementLocator.contains("linkText")) 
			element = driver.findElement(By.linkText((String) locator.get("linkText")));
		 else if (elementLocator.contains("partiallinkText")) 
			element = driver.findElement(By.partialLinkText((String) locator.get("partialText")));
		 else if (elementLocator.contains("xpath")) 
			element = driver.findElement(By.xpath((String) locator.get("xpath")));
		 else if (elementLocator.contains("css")) 
			element = driver.findElement(By.cssSelector((String) locator.get("css")));
		else if(elementLocator.contains("class"))
			element=driver.findElement(By.className((String) locator.get("className")));

		return element;

	}

	public String getJsonData(String parentKey,String dataNeedValue) {
		JSONObject jsonObject = (JSONObject) obj;
		JSONObject value=(JSONObject)jsonObject.get(parentKey);
	return (String)value.get(dataNeedValue);		
	}
}
