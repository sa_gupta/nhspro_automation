package com.ex2.nhspro.utility;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.TestBase;

public class BrokenLinkUtility {

	private WebDriver driver;
	private int invalidLinksCount = 0;
	private int validLinkCount = 0;
	private int linkWithouthrefValue = 0;
	List<String> validUrlList;
	List<String> invalidUrlList;
	List<String> listwithoutHref;
	String linkText, hrefTag;
	ExtentTest logger;
	String homepage;
	

	public BrokenLinkUtility(WebDriver driver,ExtentTest logger) {
		this.logger=logger;
		this.driver = driver;
	}

	public void validateInvalidLink() {
		try {
			if(driver.getCurrentUrl().contains("newhomesourceprofessional"))
			homepage =new TestBase().getAgentBaseUrl();
		
		else {
		homepage=driver.getCurrentUrl();
		}
			validUrlList = new ArrayList<String>();
			invalidUrlList = new ArrayList<String>();
			listwithoutHref = new ArrayList<String>();
			List<WebElement> anchorTagsList = driver.findElements(By.tagName("a"));
			System.out.println("Total no. of links are " + anchorTagsList.size());
		
			anchorTagsList.stream()
			.filter(s->isAttributeExist(s,"href"))
			.map(s->s.getAttribute("href"))
			.peek(ancorlinks->System.out.println(ancorlinks))
			.filter(i->i.equals(""))
			.filter(i->i.startsWith("javascript"))
			.filter(i->i==null)
			.filter(i->i.startsWith("mailto:"));
//			.map()
			
			for (WebElement anchorTagElement : anchorTagsList) {

				linkText = anchorTagElement.getText();
			//	System.out.println("Link txt is:-" + linkText);
				boolean attributeExist = isAttributeExist(anchorTagElement, "href");
				if (attributeExist) {
					hrefTag = anchorTagElement.getAttribute("href");
					
					if (hrefTag.equals("") || hrefTag.startsWith("javascript") || hrefTag == null||hrefTag.startsWith("mailto:")) {
						linkWithouthrefValue++;
						listwithoutHref.add(linkText);
						System.out.println("URL is either not configured for anchor tag or it is empty");
						// verifyURLStatus(hrefTag);
					} else {
						if(!hrefTag.startsWith(homepage)){
			                System.out.println("URL belongs to another domain, skipping it.");
			                continue;
			            }
						else
						verifyURLStatus(hrefTag);

					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println(e.getMessage());
		}
		System.out.println();
		System.out.println();
		System.out.println("Total valid url:-" + validLinkCount + " with list" + validUrlList.toString());
		System.out.println("Total invalid url:-" + invalidLinksCount + " with list" + invalidUrlList.toString());
		System.out.println("Total link without href:-" + linkWithouthrefValue + " with list" + listwithoutHref.toString());
		logger.log(Status.INFO, "Validated all links on page "+driver.getCurrentUrl());
	}

	private void verifyURLStatus(String urlLink) {
		try {
//			System.out.println("Verify:-"+urlLink);

			URL link = new URL(urlLink);
			HttpURLConnection httpConn = (HttpURLConnection) link.openConnection();
			// Set the timeout for 2 seconds
			httpConn.setConnectTimeout(2000);
			// connect using connect method
			httpConn.connect();
			// use getResponseCode() to get the response code.
			if (httpConn.getResponseCode() > 400) {
				System.out.println("Invaild Url:-" + urlLink + " - " + httpConn.getResponseMessage());
				invalidLinksCount++;
				invalidUrlList.add(urlLink);
			} else {
				System.out.println("Valid Url:-" + urlLink + " - " + httpConn.getResponseMessage());
				validLinkCount++;
				validUrlList.add(urlLink);

			}

			// Create a connection using URL object (i.e., link)

		} catch (MalformedURLException blankHref) {
			System.out.println("Blank href for:-" + urlLink);
		} catch (Exception e) {
			System.out.println("Link is:-" + linkText);
			e.printStackTrace();

			invalidLinksCount++;
		}

	}

	private boolean isAttributeExist(WebElement element, String Attribute) {
		Boolean result = false;
		try {
			String value = element.getAttribute(Attribute);
			if (value != null) {
				result = true;
			}
		} catch (Exception e) {
		}

		return result;
	}
}
