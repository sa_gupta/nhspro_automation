package com.ex2.nhspro.utility;

import java.util.HashMap;
import java.util.Map;

public class GetCommonData extends Initializations {

//==================================================================== HomePage Data ===============================================	

	public static HashMap<String, String> getSearchData(String currentUrl) {
		HashMap<String, String> expectedSearchedData = new HashMap<String, String>();
		if (currentUrl.contains("showingnew"))
			expectedSearchedData.put("title", homeData.getJsonData("SearchBox", "title_showingnew"));
		else
			expectedSearchedData.put("title", homeData.getJsonData("SearchBox", "title_pro"));
		expectedSearchedData.put("squareFeetDropdownSelectedValue",
				homeData.getJsonData("SearchBox", "squareFeetDropdownSelectedValue"));
		expectedSearchedData.put("allNewHomesElement_selection", homeData.getJsonData("SearchBox", "selectedLabel"));
		expectedSearchedData.put("minPriceDropdownSelectedValue",
				homeData.getJsonData("SearchBox", "minPriceDropdownSelectedValue"));
		expectedSearchedData.put("inventoryHomesSelection", homeData.getJsonData("SearchBox", "nonselectionLabel"));
		expectedSearchedData.put("searchbutton", "true");
		expectedSearchedData.put("searchfield", "true");
		expectedSearchedData.put("BedsDropdownSelectedValue",
				homeData.getJsonData("SearchBox", "BedsDropdownSelectedValue"));

		expectedSearchedData.put("BathDropdownSelectedValue",
				homeData.getJsonData("SearchBox", "BathDropdownSelectedValue"));
		expectedSearchedData.put("maxPriceDropdownSelectedValue",
				homeData.getJsonData("SearchBox", "maxPriceDropdownSelectedValue"));
		expectedSearchedData.put("labels", homeData.getJsonData("SearchBox", "labels"));
		return expectedSearchedData;
	}

	public static HashMap<String, String> getBannerDetails() {
		HashMap<String, String> expectedBannerDetails = new HashMap<>();
		expectedBannerDetails.put("title", homeData.getJsonData("BannerInfo", "title"));
		expectedBannerDetails.put("description", homeData.getJsonData("BannerInfo", "description"));
		return expectedBannerDetails;
	}

	public static HashMap<String, String> getGuideSectionDetails() {
		HashMap<String, String> expectedGuideDetails = new HashMap<>();
		expectedGuideDetails.put("title", homeData.getJsonData("GuideInfo", "title"));
		expectedGuideDetails.put("video", "" + true);
		return expectedGuideDetails;
	}

	public static HashMap<String, String> getHomebuildersData(String currentUrl) {
		HashMap<String, String> expectedbuildersDetails = new HashMap<>();
		if (currentUrl.contains("showingnew"))
			expectedbuildersDetails.put("title", homeData.getJsonData("BuilderInfo", "title_showingnew"));
		else
			expectedbuildersDetails.put("title", homeData.getJsonData("BuilderInfo", "title_pro"));
		expectedbuildersDetails.put("noOfBuilders", "20");
		return expectedbuildersDetails;
	}

	public static Map<?, ?> getStartFreshDetails() {
		HashMap<String, String> expectedStartFreshDetails = new HashMap<>();
		expectedStartFreshDetails.put("title", homeData.getJsonData("StartFreshInfo", "title"));
		expectedStartFreshDetails.put("mainVideo", "true");
		expectedStartFreshDetails.put("videoCount", "4");
		return expectedStartFreshDetails;
	}

	public static Map<?, ?> getLearnHowToSellDetails() {
		HashMap<String, String> expectedLearnHowToSellDetails = new HashMap<>();
		expectedLearnHowToSellDetails.put("title", homeData.getJsonData("HomeLearnGuideInfo", "title"));
		expectedLearnHowToSellDetails.put("noOfBuilders", "4");
		return expectedLearnHowToSellDetails;
	}

//==================================================== SRP Data ============================================================
	
	public static HashMap<String, Object> expectedgetFilterData(){
		HashMap<String, Object> expectedFilterValue = new HashMap<>();
		expectedFilterValue.put("locationData", homeData.getJsonData("Search_data", "Matching_Searched_data"));
		expectedFilterValue.put("filtersValue", srpData.getJsonData("FilterSection", "DefaultValue"));
		expectedFilterValue.put("ShareSearchlinkexistence", "true");
		expectedFilterValue.put("RessetFilterlinkExistence", "true");
		expectedFilterValue.put("SaveButtonExistence", "true");	
		return expectedFilterValue;		
	}
}
