package com.ex2.nhspro.utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.ExtentFactory;
import io.appium.java_client.functions.ExpectedCondition;

public class ActionsUtility {
	WebDriver driver;
	WebDriverWait wait;

	public ActionsUtility(WebDriver driver, ExtentTest logger) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(30));
	}

	public void clickOnElement(WebElement element, String Log) {
		retryingFindClick(element);
		checkPageIsReady();
		printLog(Log);
	}
	public void waitForAttributeAvailablility(WebElement element,String attribute,String value) {
		wait.until(ExpectedConditions.attributeContains(element, attribute, value));
	}
	
	public String getAttributeValue(WebElement element,String attributeName) {
		retryTillExist(element);
		return element.getAttribute(attributeName);
	}

	public void typeSlowOnElement(WebElement element, String textToEnter, String Log) {
		retryTillExist(element);
		for (int i = 0; i < textToEnter.length(); i++) {
			String s = new StringBuilder().append(textToEnter.charAt(i)).toString();
			element.sendKeys(s);
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		printLog(Log);
	}

	public void clearandType(WebElement element, String textToEnter, String Log) {
		wait.until(ExpectedConditions.elementToBeClickable(element)).clear();
		element.sendKeys(textToEnter);
		printLog(Log);
	}

	public void normalTypeOnElement(WebElement element, String textToEnter, String Log) {
		retryTillExist(element);
		element.sendKeys(textToEnter);
		printLog(Log);
	}

	public String getTextData(WebElement element) {
		  retryTillExist(element);	 
		retryUntilTextExist(element);
	
		String GetValue=element.getText();
		return GetValue;
	}

	private void retryUntilTextExist(WebElement element) {
		wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return element.getText().length() != 0;
            }
        });
		
	}

	public boolean isElementExistOnThePage(WebElement element, String Log) {
		
		boolean existence = wait.until(ExpectedConditions.visibilityOf(element)).isDisplayed();
		printLog(Log);
		return existence;
	}

	public static void staticWait(int waitInSec) {
		try {
			Thread.sleep(waitInSec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void waitForVisibility(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public  void waitforElementClickable( WebElement element) {
		int attempts = 0;
        while(attempts < 3) {
     
            try {
            	wait.until(ExpectedConditions.elementToBeClickable(element));
                break;
                
            } catch(StaleElementReferenceException e) {
            }
            catch(ElementClickInterceptedException e) {
            	
            }
            attempts++;
        }
		
	}
	
	public List<String> getAllDropdownValues(WebElement element){
		Select select=new Select(element);
		return select.getOptions().stream().map(dropdown->getTextData(dropdown)).collect(Collectors.toList());
	}

	public void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};

	}

	public static String nameWithDate() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh-mm-ss");
		String format = dateFormat.format(date);
		return format;
	}

	public  void hovertoElement(WebDriver driver, WebElement element) {
		retryTillExist(element);
		new Actions(driver).moveToElement(element).build().perform();
	}

	public String getDigitsOnly(String Value) {
		String digitsOnly = "";
		char[] charArray = Value.toCharArray();
		for (char ch : charArray) {
			if (Character.isDigit(ch)) {
				digitsOnly = digitsOnly + ch;
			}
		}
		return digitsOnly;
	}
	
	public String getFirstSelectedValueFromDropdown(WebElement element) {
		Select select=new Select(element);
		return select.getFirstSelectedOption().getText();
	}

	public void uploadFileWithRobot(String imagePath) {
		 StringSelection stringSelection = new StringSelection(imagePath);
	        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	        clipboard.setContents(stringSelection, null);
		
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
		robot.delay(250);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(150);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public void pageRefresh() {
		driver.navigate().refresh();
	}

	public void selectValueFromDropdown(WebElement element,String valueToBeSelected,String log){
		Select select=new Select(element);
		select.selectByValue(valueToBeSelected);
		printLog(log);
		
	}
	public void waitForTextExistence(WebElement webElement, String text) {
		WebDriverWait wait1=new WebDriverWait(driver,Duration.ofSeconds(30));
		wait1.until(ExpectedConditions.textToBePresentInElement(webElement, text));

	}



	public boolean isButtonDisabled(WebElement element) {
		boolean status;
		if (element.isEnabled()) {
			status = false;
			printLog("Element is enabled");
		} else {
			status = true;
			printLog("Element is disable");
		}
		return status;

	}

	public void checkPageIsReady() {
JavascriptExecutor js = (JavascriptExecutor) driver;
		for (int i = 0; i < 25; i++) {
			staticWait(1);
			if (js.executeScript("return document.readyState").toString().equals("complete")) {
				break;
			}
		}
	}

	
	public boolean retryingFindClick(WebElement element) {
        boolean result = false;
        int attempts = 0;
        while(attempts < 3) {
     
            try {
            	wait.until(ExpectedConditions.visibilityOf(element));
            	wait.until(ExpectedConditions.elementToBeClickable(element));
                element.click();
                result = true;
          
                break;
                
            } catch(StaleElementReferenceException e) {
            }
            catch(ElementClickInterceptedException e) {
            	
            }
            attempts++;
        }
        return result;
}
	public boolean retryTillExist(WebElement element) {
		 boolean result = false;
	        int attempts = 0;
	        while(result=false) {
	            try {
	            	wait.until(ExpectedConditions.visibilityOf(element));
	                element.isDisplayed();
	                result = true;
	                break;
	            } catch(StaleElementReferenceException e) {
	            	System.out.println("Ignoring StaleElementReferenceException");
	            	staticWait(1);
	            }
	            catch(NoSuchElementException e) {
	            	System.out.println("Not exist on the page");
	            }
	            attempts++;
	        }
	        return result;
	}
	
	
	public static void printLog(String Log) {
		ExtentFactory.getInstance().getExtent().log(Status.INFO, Log);
		System.out.println(Log);
	}
}
