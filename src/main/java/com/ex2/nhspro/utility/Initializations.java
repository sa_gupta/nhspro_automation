package com.ex2.nhspro.utility;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.ex2.nhspro.actions.CommonOperations_actions;
import com.ex2.nhspro.actions.CommonPage_actions;
import com.ex2.nhspro.actions.CommunityDetailPage_actions;
import com.ex2.nhspro.actions.HomeDetailPage_actions;
import com.ex2.nhspro.actions.HomePage_actions;
import com.ex2.nhspro.actions.LocationHandlerPage_actions;
import com.ex2.nhspro.actions.ManageMyClientActivityPage_actions;
import com.ex2.nhspro.actions.MyAccountPage_actions;
import com.ex2.nhspro.actions.MyClients_actions;
import com.ex2.nhspro.actions.MyConsumerPortal_actions;
import com.ex2.nhspro.actions.SRPFilter_actions;
import com.ex2.nhspro.actions.SRPPage_actions;
import com.ex2.nhspro.actions.SRPPhotoview_actions;
import com.ex2.nhspro.actions.SaveListing_actions;
import com.ex2.nhspro.actions.SavedSearches_actions;
import com.ex2.nhspro.actions.WebinarPage_actions;
import com.ex2.nhspro.actions.Widget_actions;
import com.ex2.nhspro.api.SRP_api;
import com.ex2.nhspro.base.ExtentFactory;
import com.ex2.nhspro.base.TestBase;


public class Initializations extends TestBase {

	protected  CommonPage_actions commonPage;
	protected  HomePage_actions homePage;
	protected  SRPPage_actions srpPage;
	protected  SavedSearches_actions savedSearchesPage;
	protected  HomeDetailPage_actions homeDetailPage;
	protected  CommunityDetailPage_actions communityDetailPage;
	protected  SaveListing_actions saveListingPage;
	protected  BrokenLinkUtility brokenLink; 
	protected  LocationHandlerPage_actions locationHandler;
	protected  DatabaseConnectivityUtility database;
	protected  MyClients_actions myclient;
	protected  MyAccountPage_actions myaccount;
	protected  MyConsumerPortal_actions myconsumerportal;
	protected  ManageMyClientActivityPage_actions managemyclientactivity;
	protected  SRPFilter_actions filter;
	protected  String ClientNameforSaveListing;
	protected  CommonOperations_actions commonOperation;
	protected  Widget_actions widget;
	protected  SRPPhotoview_actions photoView;
	protected  WebinarPage_actions webinar;
	String property_value;
	public String propertyBrowser = PropertyFileOperation.getPropertyValue("mode");
	public String environment = PropertyFileOperation.getPropertyValue("environment");
//	public String environment = System.getProperty("environment");
	
	List<Object> objectElements;
	
	protected static SRP_api srp_api;

	protected static JsonUtility homeData, searchData, commonData, myaccountData, myconsumerportalData, myclientData, srpData, title,leadData, webinarData;

	@BeforeSuite(groups= {"smokeTest","regressionTest","HappyPath"})
	public void reportingFolderCreation() {
		FolderCreationUtility.createFolderIfNotExist(
				System.getProperty("user.dir") + "//ExtentReports/Script/sprint/" + PropertyFileOperation.getPropertyValue("sprint"));
		FolderCreationUtility.createFolderIfNotExist(
				System.getProperty("user.dir") + "//ExtentReports/Script/stage/" + PropertyFileOperation.getPropertyValue("sprint"));
		FolderCreationUtility.createFolderIfNotExist(System.getProperty("user.dir")
				+ "//ExtentReports/Script/production/" + PropertyFileOperation.getPropertyValue("sprint"));
		FolderCreationUtility.createFolderIfNotExist(System.getProperty("user.dir")
				+ "//ExtentReports/Script/hotfix/" + PropertyFileOperation.getPropertyValue("sprint"));
		FolderCreationUtility.createFolderIfNotExist(
				System.getProperty("user.dir") + "//ExtentReports/SSO/" + PropertyFileOperation.getPropertyValue("sprint"));
	}

	
	@BeforeClass(groups= {"smokeTest","regressionTest","HappyPath"})
	@Parameters({ "browser" })
	public void intializeData(@Optional("chrome") String browser ) {
		property_value = PropertyFileOperation.getPropertyValue("mode");
		if (property_value.equalsIgnoreCase("desktop")) {
			launchBrowserInstance(browser);
		} else {
			applicationOnMobile();
		}
	}
	@BeforeMethod(groups= {"smokeTest","regressionTest","HappyPath"})
	public void before(Method m) {
		System.out.println("================================= Start Execution:- " + m.getName()+ "===================================");
		logger = extent.createTest(m.getName());
		ExtentFactory.getInstance().setExtent(logger);
		logger.log(Status.INFO, "Environment selected is :- "+environment);
		System.out.println("Environment selected is :- "+environment);
		initialize();
		intializeAllTestData();
		intializeApi();
		database = new DatabaseConnectivityUtility();
		ClientNameforSaveListing = srpData.getJsonData("SaveListing", "ClientName");
	}
	@AfterClass(groups= {"smokeTest","regressionTest","HappyPath"})
	public void afterExecution() {
		driver.quit();
	}	
	


	@BeforeTest(groups= {"smokeTest","regressionTest","HappyPath"})
	public void startReport() throws UnknownHostException {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/ExtentReports/Script/" + environment
				+ "/" + PropertyFileOperation.getPropertyValue("sprint") + "/" + propertyBrowser + "_" + ActionsUtility.nameWithDate()
				+ ".html");
		// Create an object of Extent Reports
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Name", InetAddress.getLocalHost().getHostName());
		extent.setSystemInfo("Environment", PropertyFileOperation.getPropertyValue("environment"));
		extent.setSystemInfo("User Name", System.getProperty("user.name"));
		htmlReporter.config().setDocumentTitle("ExtentReport for NHSPro");
		// Name of the report
		htmlReporter.config().setReportName("Execution Report");
		// Dark Theme
		htmlReporter.config().setTheme(Theme.DARK);

	}

	@AfterMethod(groups= {"smokeTest","regressionTest","HappyPath"})
	public void tearDown(ITestResult result, Method m) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			// MarkupHelper is used to display the output in different colors
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
			// To capture screenshot path and store the path of the screenshot in the string
			// "screenshotPath"
			// We do pass the path captured by this method in to the extent reports using
			// "logger.addScreenCapture" method.
			// String Scrnshot=TakeScreenshot.captuerScreenshot(driver,"TestCaseFailed");
			String screenshotPath = GetScreenshot.getScreenShot(driver, result.getName());
			// To add it in the extent report
			logger.fail("Test Case Failed Snapshot is below " + logger.addScreenCaptureFromPath(screenshotPath));
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.ORANGE));
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			logger.log(Status.PASS,
					MarkupHelper.createLabel(result.getName() + " Test Case PASSED", ExtentColor.GREEN));
			// String screenshotPath = GetScreenshot.getScreenShot(driver,
			// result.getName());
			// To add it in the extent report
			// logger.pass("Test Case Passed Snapshot is below " +
			// logger.addScreenCaptureFromPath(screenshotPath));
		}

		System.out.println("--------------------------------------End:- " + m.getName()
				+ "----------------------------------------- ");
		System.out.println();
		extent.flush();
	//	driver.quit();
	//	System.out.println("In After count"+count++);
	}
	
	public void intializeApi() {
		srp_api=new SRP_api();
	}

	public void intializeAllTestData() {
		homeData = new JsonUtility("HomepageData");
		commonData = new JsonUtility("CommonData");
		myaccountData = new JsonUtility("MyAccountData");
		myconsumerportalData = new JsonUtility("MyConsumerPortalData");
		myclientData = new JsonUtility("MyClientData");
		srpData = new JsonUtility("SRPData");
		title= new JsonUtility("UrlandTitles");
		leadData=new JsonUtility("LeadData");
		webinarData=new JsonUtility("WebinarData");
	}
	private void initialize() {
		objectElements=new ArrayList<Object>();

			commonPage=new CommonPage_actions(driver,logger);  //0
			objectElements.add(commonPage);
			homePage=new HomePage_actions(driver,logger);    //1
			objectElements.add(homePage);
			srpPage=new SRPPage_actions(driver, logger);  //2
			objectElements.add(srpPage);
			savedSearchesPage=new SavedSearches_actions(driver, logger); //3
			objectElements.add(savedSearchesPage);
			homeDetailPage =new HomeDetailPage_actions(driver,logger); //4
			objectElements.add(homeDetailPage);
			communityDetailPage=new CommunityDetailPage_actions(driver,logger); //5
			objectElements.add(communityDetailPage);
			saveListingPage=new SaveListing_actions(driver, logger);//6
			objectElements.add(saveListingPage);
			
			brokenLink=new BrokenLinkUtility(driver,logger); //7
			objectElements.add(brokenLink);
			locationHandler=new LocationHandlerPage_actions(driver, logger); //8
			objectElements.add(locationHandler);
			myclient=new MyClients_actions(driver,logger); //9
			objectElements.add(myclient);
			myaccount=new MyAccountPage_actions(driver,logger); //10
			objectElements.add(myaccount);
			myconsumerportal=new MyConsumerPortal_actions(driver,logger); //11
			objectElements.add(myconsumerportal);
			managemyclientactivity=new ManageMyClientActivityPage_actions(driver,logger); //12 
			objectElements.add(managemyclientactivity);
			filter=new SRPFilter_actions(driver, logger); //13
			objectElements.add(filter);
			commonOperation=new CommonOperations_actions(driver,logger,objectElements); 
			widget=new Widget_actions(driver,logger); //14
			objectElements.add(widget);
			photoView=new SRPPhotoview_actions(driver, logger); //15
			objectElements.add(photoView);
			webinar= new WebinarPage_actions(driver, logger);
			objectElements.add(webinar);
			}

}
