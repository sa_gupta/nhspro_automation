package com.ex2.nhspro.actions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class HomeDetailPage_actions {

	WebDriver driver;
	JsonUtility jsonData;
	ExtentTest logger;
	ActionsUtility elementAction;

	public HomeDetailPage_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "HomeDetail");
		elementAction = new ActionsUtility(driver, logger);
	}

	public void clickOnPrintReportButtonOnHomeDetail() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("PrintReport"),
					"Clicked in Home Detail Print Report Button");
		}else {
		elementAction.clickOnElement(jsonData.getWebElement("PrintReport"),
				"Clicked in Home Detail Print Report Button");
		}
	}

	public void clickOnBackToSearchResultOnHomeDetail() {
		elementAction.clickOnElement(jsonData.getWebElement("BackToSearchResult"),
				"Clicked on BackToSearchResult From Home Detail Page");
	}

	public void clickOn_SaveListing() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("Main_SaveListing_btn"), "Clicked on Saved Listing from home details page");
		}else {
		elementAction.waitForLoad(driver);
		elementAction.clickOnElement(jsonData.getWebElement("Main_SaveListing_btn"),
				"Clicked on Saved Listing from home details page");
		}
	}

	public void clickOn_PrintReport() {
		elementAction.waitForLoad(driver);
		elementAction.clickOnElement(jsonData.getWebElement("PrintReport"),
				"Click on Print Report button on details page");
	}

	public void clickOnOkBtn_PrintReport() {
		elementAction.clickOnElement(jsonData.getWebElement("PrintPDF_OK_btn_client"),
				"Click on OK button for print report");
	}

	public String getHomeId() {
		String currentUrl = driver.getCurrentUrl();
		String[] split = currentUrl.split("/");
		return split[split.length - 1];
	}

	public void clickOnOkButton() {
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("OkButton"), "Clicked on Ok Button from Home Detail page");
	}

	public void clickOnConfirmOk() {
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("confirmOk"),
				"Clicked on Confirm Ok Button from Home Detail page");
	}

	public void clickOnRequestToRegister_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Main_RequestToRegister_btn"),
				"Clicked on Request to Register Client button");
	}
	
	public void enterMessageInContactToBuilderTextBox(String Message) {
		ActionsUtility.staticWait(3);
		elementAction.normalTypeOnElement(jsonData.getWebElement("ContactToBuilder", "Message_textArea"), Message, "Entered Message in contact to builder textArea");
	}
	
	public void enterMessageInContactToBuilderMessageLightBox(String Message) {
		ActionsUtility.staticWait(3);
		elementAction.normalTypeOnElement(jsonData.getWebElement("ContactToBuilder", "Message_LightBox"), Message, "Entered Message in contact to builder textArea");
	}
	
	public void enterMessageInRequestAnApprointment(String Message) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("ContactToBuilder", "Request_an_appointment_TextArea"), Message, "Entered Message in Request an Appointment textArea");
	}
	
	public void clickOnRequest_an_appointment_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("ContactToBuilder","Request_an_appointment_button"),
				"Clicked on Request an Appointment button");
	}
	
	public void clickOnContactAgentSales_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("ContactToBuilder","ContactSalesAgent_btn"),
				"Clicked on Contact Agent Sales button");
	}
	
	public void clickOnContactAgentSales_btnInMessageLightBox() {
		elementAction.clickOnElement(jsonData.getWebElement("ContactToBuilder","ContactSalesAgent_btnInMessageLightBox"),
				"Clicked on Contact Agent Sales button");
	}
	
	public void clickOnNextListing()
	{
		elementAction.clickOnElement(jsonData.getWebElement("DetailHeader","NextListing"),
				"Clicked on the Next Listing button");
	}
	public void clickOnPrevious()
	{
		elementAction.clickOnElement(jsonData.getWebElement("DetailHeader","Previous"),
				"Clicked on the Previous button");
	}
	
	public void validatePreviousAndNextListingUrls(String currentUrl,String nextListingUrl)
	{
		if(currentUrl!=nextListingUrl)
		{
			Assert.assertTrue(true,"User is unable to redirect to the Previous/next listing");
			System.out.println("Previous/Next Listing functionality verified");
		}
		else
		{
			Assert.assertFalse(false,"User is unable to redirect to the Previous/next listing");
		}
	}
	

	public void validateJumpLinks() {
		List<WebElement> jumplinks = driver.findElements(By.xpath("//ul[@id='jump-links']/li[not(@style)]/a"));
		List<WebElement> litags = driver.findElements(By.xpath("//ul[@id='jump-links']/li"));
		jumplinks.stream().forEach(s -> {
			elementAction.clickOnElement(s, "Clicked on " + s.getText());
			litags.stream().forEach(t -> {
				if (elementAction.getAttributeValue(t, "class").equalsIgnoreCase("item acfffftive")) {
					Assert.assertTrue(true, "");
					System.out.println(s.getText() + " is selected");
				} else
					Assert.assertFalse(false);
			});
		});
	}
}
