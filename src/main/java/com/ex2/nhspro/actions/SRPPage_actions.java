package com.ex2.nhspro.actions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SRPPage_actions extends Initializations{
	JsonUtility jsonData;
	ExtentTest logger;
	WebDriver driver;
	Actions action;
	ActionsUtility elementAction;
	JavascriptExecutor js;

	public SRPPage_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "SRPPage");
		action = new Actions(driver);
		elementAction = new ActionsUtility(driver, logger);
		js = (JavascriptExecutor) driver;
	}

//################################################### Grids #########################################################################	

	
	public String getAdTitle()
	{
		return elementAction.getTextData(jsonData.getWebElement("AdTitle"));
	}
	public String getAdDescription()
	{
		return elementAction.getTextData(jsonData.getWebElement("AdDescription"));
	}
	public List<WebElement> getAdCountOnGrid()
	{
		return driver.findElements(By.xpath("//td[@class='srp-grid-native-ads-row']"));
	}
	
	public void AdtextVerification(String messageText)
	{
		if(messageText!=" ")
		{
			Assert.assertTrue(true,"No Ad Avaiable");
		}
	}
	
	public void clickOnSponserBy()
	{
		elementAction.clickOnElement(jsonData.getWebElement("AdSponsoredBy"), "Click on SponserBy");
	}
	public String gethrefOfAd()
	{
		return elementAction.getAttributeValue(jsonData.getWebElement("AdSponsoredBy"), "href");
	}
	
	public void clickOnHome(int homeNo) {
		WebElement element;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			element = driver.findElement(By.xpath("//table/tbody/tr[" + homeNo + "]/td[2]/a"));
		else
//			element = driver
//					.findElement(By.xpath("(//a[contains(@aria-label,\"pro-mobile-details-page\")])[" + homeNo + "]"));
			element = driver.findElement(By.xpath(
					"//body/main[@id='main-content']/section[@id='results']/div[@id='pro-mobile-list-view']/div[@id='pro-mobile-listview-results']/div[@id='pro-mobile-listview-results-items']/a["
							+ homeNo + "]/div[1]"));
		elementAction.clickOnElement(element, "Clicked on " + homeNo);
		ActionsUtility.staticWait(3);
	}

	public int getNoOfHomesOnPage() {
		return driver.findElements(By.xpath("//table/tbody/tr[@role='row']")).size();
	}



//Filters

	public void clickOnFilter_link() {
		elementAction.clickOnElement(jsonData.getWebElement("Filter_link"), "Click on Filter link");
		elementAction.waitForVisibility(jsonData.getWebElement("Filter_title"));
	}

	public void enterSearchedData_in_location_field(String textToSearch) {
		elementAction.clearandType(jsonData.getWebElement("Location_field"), textToSearch,
				"Enter " + textToSearch + " in Search field");
	}

	public String getSearchLocationText() {
		ActionsUtility.staticWait(5);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		elementAction.waitForVisibility(jsonData.getWebElement("Location_field"));
		String getEnteredText = js.executeScript("return document.getElementById('LocationSearchTextBox').value")
				.toString();
		ActionsUtility.printLog("Showing " + getEnteredText + " in Search field");
		return getEnteredText;
	}

	public List<Integer> getListOfBeds() {
		List<Integer> bedsList = new ArrayList<Integer>();
		List<WebElement> bedsElements = driver.findElements(By.xpath("//table/tbody/tr/td[9]"));
		for (WebElement webElement : bedsElements) {
			bedsList.add(Integer.valueOf(webElement.getText()));
		}
		return bedsList;
	}

	public void selectHome(int noOfHome) {
		List<WebElement> listOfhomes;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			listOfhomes = driver.findElements(By.xpath("//tbody/tr/td/label"));
		} else {
			listOfhomes = driver.findElements(By.xpath("//div[@id='pro-mobile-listview-results-items']/a/div/label"));
		}
		for (int i = 0; i < noOfHome; i++) {
			elementAction.clickOnElement(listOfhomes.get(i), "Clicked on " + i + 1 + " home");
		}
		System.out.println("Selected home no-" + noOfHome);
		logger.log(Status.INFO, "Selected home no-" + noOfHome);

	}

	public void selectParticularHome(int homeNo) {
	//	elementAction.waitforElementClickable(jsonData.getWebElement("SelectAll_checkbox"));
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) 
		{
			elementAction.clickOnElement(driver.findElement(By.xpath("(//div[@id='pro-mobile-listview-results-items']//label)[" + homeNo + "]")),
					"Clicked on home No - " + homeNo);
		}
		else {
		elementAction.clickOnElement(
				driver.findElement(
						By.xpath("(//tr[@role='row']/td//label[@class='k-checkbox-label'])[" + homeNo + "]")),
				"Clicked on home No - " + homeNo);
		}
	}

	public List<String> getIdOfHomes(int noOfHome) {
		List<String> idOfHome = new ArrayList<String>();
		List<WebElement> homeElements;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			homeElements = driver.findElements(By.xpath("//div[@id='pro-mobile-listview-results-items']/a"));
		else
			homeElements = driver.findElements(By.xpath("//table/tbody/tr/td/a"));
		for (int i = 0; i < noOfHome; i++) {
			String allUrls = homeElements.get(i).getAttribute("href");
			String[] split = allUrls.split("/");
			System.out.println(split[split.length - 1]);
			idOfHome.add(split[split.length - 1]);
		}
		return idOfHome;
	}

	public String getIdOfPerticularHome_InList(int homeNo) {
		WebElement homeElement;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			homeElement = driver.findElement(By.xpath("//div[@id='pro-mobile-listview-results-items']/a"));
		else
			homeElement = driver.findElement(By.xpath("(//table/tbody/tr/td[3]/a)[" + homeNo + "]"));
		elementAction.waitforElementClickable(homeElement);
		String allUrls = elementAction.getAttributeValue(homeElement, "href");
		String[] split = allUrls.split("/");
		String idOfHome = split[split.length - 1];
		System.out.println(split[split.length - 1]);
		return idOfHome;
	}

	public List<String> getIdOfHomesInPhotoView(int noOfHomes) {
		List<String> idOfHome = new ArrayList<String>();
		List<WebElement> homeElements;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			homeElements = driver.findElements(By.xpath("//div[@id='pro-mobile-listview-results-items']/a"));
		} else {
			homeElements = driver.findElements(
					By.xpath("//div[@class='results-photo-item']//p[contains(@class,'item-title')]//a[1]"));
		}
		for (int i = 0; i < noOfHomes; i++) {
			String allUrls = homeElements.get(i).getAttribute("href");
			String[] split = allUrls.split("/");
			System.out.println(split[split.length - 1]);
			idOfHome.add(split[split.length - 1]);
		}
		return idOfHome;
	}

// ======================================================== Save Listing Popup ================================================

	public void clickOn_Main_SaveListing() {
		elementAction.clickOnElement(jsonData.getWebElement("SaveListing_main_pro"),
				"Clicked on the Save Listing  Button");
	}

	public boolean isSavdListing_popup_exist() {
		return jsonData.getWebElement("SaveListing_title").isDisplayed();
	}

	public void clickOnPreviewEmail_link() {
		elementAction.clickOnElement(jsonData.getWebElement("PreviewtheEmail_link"), "Clicked on Preview Email link");
	}

	public boolean isPreviewEmail_popup_exist() {
		return jsonData.getWebElement("PreviewEmail_title").isDisplayed();
	}

	public void clickOnCopyEmailToClipBoard() {
		elementAction.clickOnElement(jsonData.getWebElement("CopyClipBoard_btn"),
				"Clicked on Copy to Clip board button");
	}

	public void clickOnSend_button_on_PreviewEmail_popup() {
		elementAction.clickOnElement(jsonData.getWebElement("Send_btn"), "Clicked on Send button");
	}

	public void clickOnSaveandEmail_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("SaveListing_SaveandEmail_btn"),
				"Click on Save and Email button");
	}

	public void clickOnSaveandEmailofSaveSearch() {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("SaveandEmail_btn"),
					"Click on Save and Email button of save search");
		}else {
		elementAction.clickOnElement(jsonData.getWebElement("SaveAndEmailOfSaveSearch"),
				"Click on Save and Email button of save search");
		}
	}

	public void clickOnYes_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Yes_btn"), "Clicked on Yes button");
	}

	public void clickOnNo_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("No_btn"), "Clicked on No button");
	}

//Copy to Clipboard popup
	public boolean isCopyToClipboard_title_exist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("EditMessagePopup_title"),
				"Edit popup  exist on the page");
	}

	public void clickOn_EditMessageCopied_Ok_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Ok_btn"), "Clicked on Ok button");
	}

	public void clickOnOkbuttonforConfirmation() {
		elementAction.clickOnElement(jsonData.getWebElement("Ok_btn"), "Click on confirmation Ok button");
	}
// ------------------------------------------------------------ End of Save Listing Popup ---------------------------------------------

// ====================================================== Print Report ==============================================================   
	public void clickOnPrintReportButton() {
		elementAction.clickOnElement(jsonData.getWebElement("Print_Report"), "Clicked on the Print Report Button");
	}

	public boolean isClientVersion_btn_exist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("Client_Version"),
				"Clicked on the Client Version Print Report Button");
	}

	public boolean isAgentVersion_btn_exist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("Agent_Version"),
				"Clicked on the Agent Version Print Report Button");
	}

	public void clickOnAgentVersionPrintReport() {
		elementAction.clickOnElement(jsonData.getWebElement("Agent_Version"),
				"Clicked on the Agent Version Print Report Button");
	}

	public void clickOnClientVersionPrintReport() {
		ActionsUtility.staticWait(5);
	    js.executeScript("window.scrollBy(0,1000)");
		elementAction.clickOnElement(jsonData.getWebElement("Client_Version"),
				"Clicked on the Client Version Print Report Button");
	}

// ----------------------------------------------------- End of Print Report ----------------------------------------------

// ====================================================== Sorting ============================================================

	public String getDefaultSelectedSortingValue() {
		return elementAction.getFirstSelectedValueFromDropdown(jsonData.getWebElement("SortBy_dropdown"));
	}

	public List<String> getAlldropdownValues() {
		Select select = new Select(jsonData.getWebElement("SortBy_dropdown"));
		List<String> collect = select.getOptions().stream().map(s -> elementAction.getTextData(s))
				.collect(Collectors.toList());
		return collect;
	}

	public void selectSortByValue(String valuetoSelected) {
		Select select = new Select(jsonData.getWebElement("SortBy_dropdown"));
		select.selectByVisibleText(valuetoSelected);
		((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
		elementAction.waitforElementClickable(driver.findElement(By.xpath("//tbody/tr[10]//a")));
ActionsUtility.printLog(valuetoSelected + " selected sucessfully");
	}

	public boolean verifyColumnIsSelcted(String selectedValue) {
		List<WebElement> allHomeColumnsInSRP = getAllHomeColumnsInSRP();
		boolean isSelected = false;
		if (selectedValue.startsWith("Plan"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(0));
		else if (selectedValue.contains("Community"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(1));
		else if (selectedValue.contains("City"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(2));
		else if(selectedValue.contains("Zip"))
			isSelected=attributeValidation(allHomeColumnsInSRP.get(3));
		else if (selectedValue.contains("Builder"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(4));
		else if (selectedValue.contains("Status"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(5));
		else if (selectedValue.contains("Price"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(6));
		else if (selectedValue.contains("Sq.Ft"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(7));
		else if (selectedValue.contains("Beds"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(8));
		else if (selectedValue.contains("Bath"))
			isSelected = attributeValidation(allHomeColumnsInSRP.get(9));
		return isSelected;

	}

	private boolean attributeValidation(WebElement element) {
		elementAction.waitForVisibility(element);
		return element.getCssValue("color").equalsIgnoreCase("rgba(245, 130, 32, 1)"); // it is for orange as code
																						// #f58220
	}

	public List<WebElement> verifyListIsSortedAccordingToSelectedValues(String selectedValue, String selectedTab) {
//		elementAction.waitforElementClickable(jsonData.getWebElement("SelectAll_checkbox"));
		elementAction.waitforElementClickable(driver.findElement(By.xpath("(//tbody/tr/td/label)[1]")));
		List<WebElement> columnListData = null;
		if(selectedTab.equals("HomeTab"))
			columnListData = getListDataOfSelectedValuesforPro(selectedValue);
		else
			columnListData = getListOfDataOfSelectedValueFromCommunity(selectedValue);
		return columnListData;
	}

	public List<WebElement> getAllHomeColumnsInSRP() {
		return driver.findElements(By.xpath("//thead//th/a"));
	}

	private List<WebElement> getListDataOfSelectedValuesforPro(String selectedValue) {
		List<WebElement> columnListData = null;
		if (selectedValue.startsWith("Plan"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[3]/a"));
		else if (selectedValue.contains("Community"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[4]"));
		else if (selectedValue.contains("City"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[5]"));
		else if (selectedValue.contains("Zip"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[6]"));
		else if (selectedValue.contains("Builder"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[7]"));
		else if (selectedValue.contains("Status"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[8]"));
		else if (selectedValue.contains("Price"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[9]"));
		else if (selectedValue.contains("Sq.Ft"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[10]"));
		else if (selectedValue.contains("Beds"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[11]"));
		else if (selectedValue.contains("Bath"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[12]"));
		return columnListData;
	}

	private List<WebElement> getListOfDataOfSelectedValueFromCommunity(String selectedValue) {
		List<WebElement> columnListData = null;
		if (selectedValue.contains("Community"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[3]/a"));
		else if (selectedValue.contains("homes"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[4]"));
		else if (selectedValue.contains("City"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[5]"));
		else if (selectedValue.contains("Zip"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[6]"));
		else if (selectedValue.contains("Builder"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[7]"));
		else if (selectedValue.contains("Price"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[8]"));
		else if (selectedValue.contains("Sq.Ft"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[9]"));
		else if (selectedValue.contains("Beds"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[10]"));
		else if (selectedValue.contains("Bath"))
			columnListData = driver.findElements(By.xpath("//tbody/tr/td[11]"));
		return columnListData;
	}

	public void sortingVerification(String SelectedValue, List<String> actualList) {
		if (!SelectedValue.startsWith("Plan")) {
//			System.out.println("Actual is:-" + actualList);
			if (SelectedValue.contains("A-Z") || SelectedValue.contains("Low - High")|| SelectedValue.contains("Low to High")) {
				if (SelectedValue.contains("Beds") || SelectedValue.contains("Bath")) {
					List<Integer> integerList = actualList.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
					List<Integer> sortedList = integerList.stream().sorted().collect(Collectors.toList());
					Assert.assertEquals(integerList, sortedList);
					ActionsUtility.printLog("Assertion Passed for "+SelectedValue+":- Actual--"+integerList+" Expected :-"+sortedList);
				} else {
					List<String> expectedResult = actualList.stream().sorted().collect(Collectors.toList());
					Assert.assertEquals(actualList, expectedResult);
					ActionsUtility.printLog("Assertion Passed for "+SelectedValue+":- Actual--"+actualList+" Expected :-"+expectedResult);
				}
			} else if (SelectedValue.contains("Z-A") || SelectedValue.contains("High - Low")
					|| SelectedValue.contains("High to Low")) {
				if (SelectedValue.contains("Beds") || SelectedValue.contains("Bath")||SelectedValue.contains("Sq.Ft")) {
					List<Integer> integerList = actualList.stream().map(s -> Integer.parseInt(s))
							.collect(Collectors.toList());
					List<Integer> sortedList = integerList.stream().sorted(Comparator.reverseOrder())
							.collect(Collectors.toList());
					Assert.assertEquals(integerList, sortedList);
					ActionsUtility.printLog("Assertion Passed for "+SelectedValue+":- Actual--"+integerList+" Expected :-"+sortedList);					
				}			
				else {
					List<String> expectedResult = actualList;
					System.out.println("Expected is:-" + expectedResult);
					Assert.assertEquals(actualList, expectedResult);
					ActionsUtility.printLog("Assertion Passed:- Actual--"+actualList+" Expected :-"+expectedResult);
				}
			}
		}
	}

	public void sortingBasedOnRange(String SelectedValue, List<String> actualResult) {
		List<String> leftValue = new ArrayList<String>();
		List<String> rightValue = new ArrayList<String>();
		List<String> sortedValue = null;
		if (SelectedValue.contains("A-Z") || SelectedValue.contains("Low - High")
				|| SelectedValue.contains("Low to High")) {
			for (String range : actualResult) {
				if (range.contains("N/A"))
					leftValue.add("N/A");
				else if (range.startsWith("-"))
					leftValue.add(range);
				else {
					String[] split = range.split("-");
					leftValue.add(split[0].trim());
				}
			}
			if (SelectedValue.startsWith("Price")) {
				leftValue = pricevalues(leftValue);
				sortedValue = leftValue.stream().sorted().collect(Collectors.toList());
				Assert.assertEquals(leftValue, sortedValue);
			} else {
				if (leftValue.stream().anyMatch(s -> s.contains("-"))) {
					Assert.assertEquals(leftValue, leftValue.stream().sorted().collect(Collectors.toList()));
				} else {
					List<Integer> listWithoutSort = leftValue.stream().map(s -> Integer.parseInt(s))
							.collect(Collectors.toList());
					List<Integer> sortedList = listWithoutSort.stream().sorted().collect(Collectors.toList());
					Assert.assertEquals(listWithoutSort, sortedList);
				}
			}

		} else if (SelectedValue.contains("Z-A") || SelectedValue.contains("High - Low")
				|| SelectedValue.contains("High to Low")) {
			for (String range : actualResult) {
				if (range.contains("N/A"))
					rightValue.add("N/A");
				else if (range.startsWith("-"))
					rightValue.add(range);
				else {
					if (!range.contains("-"))
						rightValue.add(range);
					else {
						String[] split = range.split("-");
						rightValue.add(split[1].trim());
					}
				}
			}
		}
		if (SelectedValue.startsWith("Price")) {
			List<Float> priceList = pricevalues(rightValue).stream().map(s -> Float.parseFloat(s))
					.collect(Collectors.toList());
			List<Float> sorted = priceList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
			Assert.assertEquals(priceList, sorted);
		} else {

			if (leftValue.stream().anyMatch(s -> s.contains("-"))) {
//				listWithoutSort.stream().sorted().collect(Collectors.toList());
				Assert.assertEquals(rightValue,
						rightValue.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));
			} else {
				List<Integer> listWithoutSort = rightValue.stream().map(s -> Integer.parseInt(s.replace(",", "")))
						.collect(Collectors.toList());
				List<Integer> sortedList = listWithoutSort.stream().sorted(Comparator.reverseOrder())
						.collect(Collectors.toList());
				Assert.assertEquals(listWithoutSort, sortedList);
			}
			// sortedValue=rightValue.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
			// Assert.assertEquals(rightValue, sortedValue);
		}

	}

	private List<String> pricevalues(List<String> Price) {
		List<String> priceChanged = new ArrayList<String>();
		String valueAfterConversion;
		for (String eachPriceValue : Price) {
			if (eachPriceValue.contains("K")) {
				valueAfterConversion = String
						.valueOf(Float.parseFloat(eachPriceValue.replace("K", "").replace("$", "")) * 1000);
			} else if (eachPriceValue.contains("M")) {
				valueAfterConversion = String
						.valueOf(Float.parseFloat(eachPriceValue.replace("M", "").replace("$", "")) * 1000000);
			} else
				valueAfterConversion = eachPriceValue;
			priceChanged.add(valueAfterConversion);
		}
		return priceChanged;
	}

	public List<WebElement> getAllPhotoViewSortingOptions() {
		return driver.findElements(By.xpath("//legend[text()='Sort by:']/following-sibling::a[not(@style='display: none;')]"));
	}
	
	public void clickOnPriceSorting() {
		elementAction.clickOnElement(jsonData.getWebElement("SortBy_Price"), "Click on Price Sorting");
	}

	public List<String> getPriceList() {
		List<String> priceList = new ArrayList<String>();
		List<WebElement> priceElements = driver.findElements(
				By.xpath("//div[@class='results-photo-item']//ul[@class='item-features']/li[contains(text(),'$')]"));
		for (WebElement homePrice : priceElements) {
			priceList.add(elementAction.getTextData(homePrice).trim());
		}
		return priceList;
	}

	public List<WebElement> getAllSortingElementsInPhotoView(String selectedValue) {
		List<WebElement> actualData = null;
		if (selectedValue.equals("City"))
			actualData = driver
					.findElements(By.xpath("//div[@class='results-photo-item']//p[@class='item-comm-info']"));
		else if (selectedValue.equals("Price"))
			actualData = driver.findElements(By
					.xpath("//div[@class='results-photo-item']//ul[@class='item-features']/li[contains(text(),'$')]"));
		else if (selectedValue.equals("Community") || (selectedValue.equals("Home")))
			actualData = driver
					.findElements(By.xpath("//div[@class='results-photo-item']//p[contains(@class,'item-title')]/a"));
		return actualData;
	}

	public void verifyBasedOnClicked(String selectedValue, String selectionMode) {
		List<WebElement> allSortingElementsIPhotoView = getAllSortingElementsInPhotoView(selectedValue);
		List<String> allSortingElementsInPhotoViewText = null;
		List<String> sortedValues = null;
		
		if (selectedValue.equals("City")) {
			List<String> allCities = allSortingElementsIPhotoView.stream()
					.map(s -> elementAction.getTextData(s).split("\n")[1].split(",")[0]).collect(Collectors.toList());
			sortedValues = getSortedValues(selectedValue + selectionMode, allCities);
			ActionsUtility.printLog("Expected:- " + sortedValues);
			Assert.assertEquals(allCities, sortedValues);
			logger.log(Status.PASS, "Actual list is :-" + allCities + " and Expected:- " + sortedValues);
		}
		else if (selectedValue.equalsIgnoreCase("Price")) {
			List<Integer> allSortingPriceElementsInPhotoViewText = new ArrayList<Integer>();
			List<Integer> sorted;
			allSortingElementsInPhotoViewText = allSortingElementsIPhotoView.stream().map(s -> elementAction.getTextData(s))
					.collect(Collectors.toList());
			for (String eachPrice : allSortingElementsInPhotoViewText) {
				System.out.println(eachPrice);
				if (eachPrice.contains("-"))
					allSortingPriceElementsInPhotoViewText.add(Integer.valueOf(eachPrice.split("-")[1].trim()
							.replace(",", "").replace("$", "").replace("Starting at", "").replace("\n", "")));
				else 
					allSortingPriceElementsInPhotoViewText.add(Integer.valueOf(eachPrice.trim().replace(",", "")
							.replace("$", "").replace("Starting at", "").replace("\n", "")));
				
			}
			if (selectedValue.contains("Low to High"))
				sorted = allSortingPriceElementsInPhotoViewText.stream().sorted().collect(Collectors.toList());
			else
				sorted = allSortingPriceElementsInPhotoViewText.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
			Assert.assertEquals(allSortingPriceElementsInPhotoViewText, sorted);
			ActionsUtility.printLog("Assertion Passed:-"+"Expected list is"+sorted+"Actual sorted list is"+allSortingPriceElementsInPhotoViewText);
		} else if (selectedValue.equals("Home") || selectedValue.equals("Community")) {
			allSortingElementsInPhotoViewText = allSortingElementsIPhotoView.stream().map(s -> s.getText())
					.collect(Collectors.toList());
			sortedValues = getSortedValues(selectedValue + selectionMode, allSortingElementsInPhotoViewText);
			Assert.assertEquals(allSortingElementsInPhotoViewText, sortedValues);
		}
		logger.log(Status.PASS,"Actual list is :-" + allSortingElementsInPhotoViewText + " and Expected:- " + sortedValues);
	}

	public List<String> getSortedValues(String SelectedValue, List<String> listReceived) {
		if (SelectedValue.contains("Low to High"))
			return listReceived.stream().sorted().collect(Collectors.toList());
		else
			return listReceived.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
	}

//------------------------------------------------- End Sorting --------------------------------------------------

	public void switchTowindow() {
		Set<String> allwindow = driver.getWindowHandles();
		Iterator<String> itr = allwindow.iterator();
		while (itr.hasNext()) {
			driver.switchTo().window(itr.next());
		}
		ActionsUtility.staticWait(3);
	}

//===================================================Save Search ====================================================

	public void clickOnSaveSearch_btn() {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("Save_Search_button"),"Clicked on the Save Search button");
			elementAction.waitForVisibility(jsonData.getWebElement("Save_Search_title"));
			}
		else {
		elementAction.clickOnElement(jsonData.getWebElement("SaveSearch", "Save_Search_button"),
				"Clicked on Save Search button");
		elementAction.waitForVisibility(jsonData.getWebElement("SaveSearch", "Save_Search_title"));
		}
	}

	public void enterNameOfSearcNameThisSearch_field(String SearchNamedToSave) {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clearandType(jsonData.getWebElement("NameThisSearch_field"), SearchNamedToSave,
					"Entered " + SearchNamedToSave + " on Name field");
		}
		else {
		elementAction.clearandType(jsonData.getWebElement("SaveSearch", "NameThisSearch_field"), SearchNamedToSave,
				"Entered " + SearchNamedToSave + " on Name field");
//		elementAction.typeSlowOnElement(jsonData.getWebElement("SaveSearch", "NameThisSearch_field"), SearchNamedToSave,
//				"Entered " + SearchNamedToSave + " on Name field");
		}
	}

	public void selectClientFromClientPopup(String ClientName) {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("SearchClient_OnSearchForClient_popup"),
					"Clicked on Search Client field");
		}
		elementAction.clickOnElement(jsonData.getWebElement("SaveSearch", "SearchClient_field"),
				"Clicked on Search Client field");
		List<WebElement> listOfClients = getListOfClients();
		for (WebElement webElement : listOfClients) {
			String actualClientName = elementAction.getTextData(webElement);
			System.out.println(actualClientName);
			if (actualClientName.equalsIgnoreCase(ClientName)) {
				webElement.click();
				break;
			} else if (actualClientName.equalsIgnoreCase("Search for a Client")) {
				webElement.click();
				elementAction.clickOnElement(
						jsonData.getWebElement("SaveSearch", "SearchClient_OnSearchForClient_popup"),
						"Click on Enter Client Name field");
				List<WebElement> listOfCLientFromSearchforClientPopup = driver
						.findElements(By.xpath("//div[@class='chosen-drop']//li"));
				for (WebElement Element : listOfCLientFromSearchforClientPopup) {
					String actualClientNameFromList = elementAction.getTextData(Element);
					System.out.println(actualClientNameFromList);
					if (actualClientNameFromList.equalsIgnoreCase(ClientName)) {
						Element.click();
						break;
					}

				}
			}
		}

	}
	
	//need to work after completion of java learning
	public void selectClientFromClientPopup_update(String ClientName) {
		elementAction.clickOnElement(jsonData.getWebElement("SaveSearch", "SearchClient_field"),
				"Clicked on Search Client field");
		List<WebElement> listOfClients = getListOfClients();
		listOfClients.stream().forEach(element->{
			String actualClientName = elementAction.getTextData(element);
			System.out.println(actualClientName);
			if (actualClientName.equalsIgnoreCase(ClientName)) {
				element.click();
				
			}
		});
	}

	private List<WebElement> getListOfClients() {
		return driver.findElements(
				By.xpath("//p[@class='title']//following-sibling::p//div//div[@class='chosen-drop']//li"));
	}

	public void clickOnSave_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("SaveSearch", "Save_btn"), "Clicked on Save button");
	}
//------------------------------------------------ End of Save Search ------------------------------------------------------------

//=============================================================== BreadCrumb ======================================================

	public void verifyBreadCrumb(String ResultLabel, String State, String Area, String City, String count) {
		Assert.assertEquals(jsonData.getWebElement("BreadCrumb", "ResultLabel").getText(), ResultLabel);
		Assert.assertEquals(jsonData.getWebElement("BreadCrumb", "State").getText(), State);
		Assert.assertEquals(jsonData.getWebElement("BreadCrumb", "Location_Area").getText(), Area);
		Assert.assertEquals(jsonData.getWebElement("BreadCrumb", "City").getText(), City);
		Assert.assertEquals(jsonData.getWebElement("BreadCrumb", "Bredcrumb_home_count").getText(), count + " homes");
	}

	public String getHomeCountFromSRP() {
		String homeCount;
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			homeCount = elementAction.getTextData(jsonData.getWebElement("Bredcrumb_home_count"));
			}
		else {
		homeCount = elementAction.getTextData(jsonData.getWebElement("BreadCrumb", "Bredcrumb_home_count"));
		}
		logger.log(Status.INFO, "HomeCount On SRP breadcrumb:-" + homeCount);
		System.out.println("HomeCount On SRP breadcrumb:-" + homeCount);
		return homeCount;
		}

	public String waitForHomeCount(String homecount) {
//		elementAction.waitForTextExistence(jsonData.getWebElement("BreadCrumb", "Bredcrumb_home_count"), homecount);
		String homeCount = elementAction.getTextData(jsonData.getWebElement("BreadCrumb", "Bredcrumb_home_count"));
		logger.log(Status.INFO, "HomeCount On SRP breadcrumb:-" + homeCount);
		System.out.println("HomeCount On SRP breadcrumb:-" + homeCount);
		return homeCount;
	}

//	public String focusOnFirstHome_Community_OfGrid() {
//		elementAction.waitForVisibility(jsonData.getWebElement("FirstGridHome"));
//		action.moveToElement(jsonData.getWebElement("FirstGridHome")).build().perform();
//		WebElement firstElementtext = jsonData.getWebElement("FirstGridHome");
//		logger.log(Status.INFO, "Focus on the First Home/Community of grid");
//		System.out.println("Grid home/community text is " + firstElementtext.getText());
//		return firstElementtext.getText();
//
//	}

	public void hoverToHome(String homeNo) {
		WebElement home = driver.findElement(By.xpath("//table/tbody/tr[" + homeNo + "]/td[4]"));
		elementAction.waitforElementClickable(home);
		action.moveToElement(home).build().perform();
	}

//	public String focusOnHomePreview() {
//		ActionsUtility.staticWait(2);
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		String getEnteredText = js.executeScript("return document.getElementById('preview-home-link').innerHTML")
//				.toString();
//		logger.log(Status.INFO, "Focus on the Home Preview");
//		System.out.println("PreView text is " + getEnteredText);
//		return getEnteredText;
//
//	}
//
//	public String focusOnCommunityPreview() {
//		ActionsUtility.staticWait(3);
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		String getEnteredText = js.executeScript("return document.getElementById('preview-comm-link').innerHTML")
//				.toString();
//		logger.log(Status.INFO, "Focus on the Community Preview");
//		System.out.println("PreView text is " + getEnteredText);
//		return getEnteredText;
//
//	}
	public String focusOnSecondHome_Community_OfGrid() {
		ActionsUtility.staticWait(2);
		elementAction.waitForVisibility(jsonData.getWebElement("SecondGridHome"));
		action.moveToElement(jsonData.getWebElement("SecondGridHome")).build().perform();
	String firstElementtext = elementAction.getTextData(jsonData.getWebElement("SecondGridHome"));
		logger.log(Status.INFO, "Focus on the First Home/Community of grid");
		System.out.println("Grid home/community text is " + firstElementtext);
		return firstElementtext;
	}

	public String getHomePreview_Title() {
		ActionsUtility.staticWait(5);
		return elementAction.getTextData(jsonData.getWebElement("HomePreview_update"));

	}

	public String getCommunityPreview_Title() {
		ActionsUtility.staticWait(5);
		return elementAction.getTextData(jsonData.getWebElement("CommunityPreview"));
	}

	public String focusOnSecondHomePreview() {
		ActionsUtility.staticWait(3);
		WebElement firstElementtext = jsonData.getWebElement("SecondHomePreview");
		String text = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;",
				firstElementtext);
		logger.log(Status.INFO, "Focus on Preview of Home/Community");
		System.out.println("Preview text of home/community is " + text);
		return text;
	}

	public void closeSRPEnageClientPopUp() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")
				&& driver.getCurrentUrl().contains("newhomesourceprofessional")) {
			try {
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				/// jsonData.getWebElement("CloseSRPpopup").click();
				elementAction.clickOnElement(jsonData.getWebElement("CloseSRPpopup"),
						"Close Engage your clients SRP popup");
			} catch (Exception e) {
				e.printStackTrace();
				driver.manage().timeouts().implicitlyWait(
						Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
				System.out.println("Popup is not available");
			}
		}

		driver.manage().timeouts().implicitlyWait(
				Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
	}
	
	public void closeBatchProcessingPopUp()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")
				&& driver.getCurrentUrl().contains("newhomesourceprofessional"))
		{
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 if(driver.findElements(By.xpath("//w-div//span")).size() > 0);
			 {
				 elementAction.clickOnElement(jsonData.getWebElement("CloseBatchProcessionPopup"),
							"Close Batch Processing popup");
			  }
		}
	}
	

	public void closeSignMeUpPopUp() {
		ActionsUtility.staticWait(3);
		if (PropertyFileOperation.getPropertyValue("environment").equalsIgnoreCase("production")
				&& driver.getCurrentUrl().contains("newhomesourceprofessional")) {
			try {
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				elementAction.clickOnElement(jsonData.getWebElement("SignUp_popup"), "click on sign me up");
			} catch (Exception e) {
				driver.manage().timeouts().implicitlyWait(
						Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
			}
		}

	}

	public void openSaveListing_perClient(String clientName) {
		List<WebElement> listingClient;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			listingClient = driver.findElements(By.xpath("//span[@data-id='Open-Client-SavedListing-Name']"));
		} else {
			listingClient = driver.findElements(By.xpath("//div[@id='saveListingNav']//li/a"));
		}
		for (int i = 0; i < listingClient.size(); i++) {
			WebElement perClientWithCount = listingClient.get(i);
			String perClientWithCountText = perClientWithCount.getText();
			System.out.println(perClientWithCountText);
			if (perClientWithCountText.contains(clientName)) {
				perClientWithCount.click();
				break;
			}

		}
	}

	public void clickOnOk_button() {
		elementAction.clickOnElement(jsonData.getWebElement("Ok_btn"), "Clicked on OK button");
	}

// ==================================================== ShowingNewPage =================================================
	public void clickOnOk_saveListing() {
		elementAction.clickOnElement(jsonData.getWebElement("OK_btn_saveListing"), "Click on OK button");
	}

	public void clickOnOk_PrintReport() {
		elementAction.clickOnElement(jsonData.getWebElement("OK_btn_printReport"),
				"Click on OK button of Print Report");
	}

	public void clickOnSearchIcon() {
		elementAction.clickOnElement(jsonData.getWebElement("SearchIcon"), "Clicked Searched icon");
	}

	public void selectClientTheFirstCheckbox() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("ClientFirstCheckBox"), "Selected the First Checkbox");
		} else {
	//		elementAction.waitforElementClickable(jsonData.getWebElement("SelectAll_checkbox"));
			elementAction.clickOnElement(jsonData.getWebElement("First_Checkbox"), "Selected first checkbox");
		}
	}

	public void clickOnOkPrintReportForClient() {
		elementAction.clickOnElement(jsonData.getWebElement("PrintOKClient"), "Click On OK Button");
	}

	public boolean isOK_btn_exist() {
		ActionsUtility.staticWait(10);
        boolean displayed = jsonData.getWebElement("PrintOKClient").isDisplayed();
		return displayed;
	}

	public int breadcrumhomecounttext() {
		elementAction.waitForVisibility(jsonData.getWebElement("HomeCount"));
		String elementtext = jsonData.getWebElement("HomeCount").getText();
		String[] list = elementtext.split("\\s");
		int homecount = Integer.parseInt(list[0]);
		System.out.println("Count is " + homecount);
		return homecount;
	}
//======================================================= Pagination ====================================================================

	public String getDefaultPaginationValue() {
		return elementAction.getFirstSelectedValueFromDropdown(jsonData.getWebElement("Pagination_dropdown"));
	}

	public void clickOnPaginationNoListView(String paginationNo) {
		WebElement PaginationNo = driver.findElement(By.xpath(
				"//div[@id='gridContent']//ul[@class='k-pager-numbers k-reset']/li/*[text()='" + paginationNo + "']"));
		elementAction.clickOnElement(PaginationNo, "Clicked on " + paginationNo + " in pagination");
		elementAction.waitForLoad(driver);
	//	elementAction.waitforElementClickable(jsonData.getWebElement("SelectAll_checkbox"));
	}

	public void clikcOnPaginationNoOnPhotoView(String paginationNo) {
		WebElement PaginationNo = driver
				.findElement(By.xpath("//div[@id='photoView-pager']//ul[@class='k-pager-numbers k-reset']/li/*[text()='"
						+ paginationNo + "']"));
		elementAction.clickOnElement(PaginationNo, "Clicked on Pagination No " + paginationNo);
	//	elementAction.waitforElementClickable(jsonData.getWebElement("SelectAll_checkbox"));
	}

	public void selectItemsPerPage(String chooseByValue) {
		elementAction.selectValueFromDropdown(jsonData.getWebElement("Pagination_dropdown"), chooseByValue,
				chooseByValue + " value selected from dropdown ");
		elementAction.waitForVisibility(jsonData.getWebElement("First_Checkbox"));
		ActionsUtility.staticWait(10);
	}

	public void clickOnHomeFromSRPPhotoView(int home) {
		WebElement homeNo;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			homeNo = driver.findElement(By.xpath("(//a[@class='preview-home-link txt-o-f prv-link'])[" + home + "]"));
		} else
			homeNo = driver.findElement(By.xpath("(//div[@id='pro-mobile-listview-results-items']/a)[" + "]"));
		elementAction.clickOnElement(homeNo, "Clicked on home No" + home);
	}

	public void clickOnCommunityFromSRPPhotoView(int CommunityNo) {
		List<WebElement> elements = driver.findElements(By.xpath("//div[@class='results-item item-comm']/p//a"));
		elementAction.clickOnElement(elements.get(CommunityNo),
				"Clicked on \"" + elements.get(CommunityNo).getText() + "\" Community");
	}

// ######################## Share Search Methods #################################

	public void clickOnShareSearch() {
		elementAction.clickOnElement(jsonData.getWebElement("ShareSearch"), "Clicked on the Share Search Link");
	}

	public boolean isShareSearchPopupExist() {
		return jsonData.getWebElement("ShareSearchPopUp").isDisplayed();
	}

	public void verification_Of_ShareSearchFunctionlity() {

		if (isShareSearchPopupExist()) {
			elementAction.clickOnElement(jsonData.getWebElement("ShareSearchPopUp"), "Clicked on the Share Search URL");
			Set<String> allwindow = driver.getWindowHandles();
			Iterator<String> itr = allwindow.iterator();
			while (itr.hasNext()) {
				driver.switchTo().window(itr.next());
			}
			System.out.println("Share Search Url on ShowingNew--> " + driver.getCurrentUrl());
			Assert.assertTrue(driver.getCurrentUrl().contains("showingnew"),
					"Share Search Functionality Does Not Verified");
			System.out.println("Share Search Functionality Verified");
		} else {
			System.out.println("Share SearchPopup doesn't display");
			Assert.fail("Share SearchPopup doesn't display");
		}
	}

	public void closeShareSearchPopup() {
		elementAction.clickOnElement(jsonData.getWebElement("ShareSearch_Close_icon"), "Closed shared search popup");
	}

// ####################### End ########################################################################

// ================================================ Preview ===========================================

	public void clickOn_RequestToRegisterClient_btn_Preview() {
		scrollOnPage();
		elementAction.clickOnElement(jsonData.getWebElement("Preview", "RequestToRegister_button"),
				"Clicked on Request to Register Client");
	}

	public void clickOn_RequestToRegister_from_popup() {
		elementAction.clickOnElement(jsonData.getWebElement("RequestToRegister_poup"),
				"Clicked on Request to Register Client button from popup");
	}

	public void clickOn_SaveListing_btn_Preview() {
		scrollOnPage();
	//	 js.executeScript("window.scrollBy(0,1000)");
	//	js.executeScript("arguments[0].scrollIntoView();",jsonData.getWebElement("Preview", "SaveListing_btn"));
		elementAction.clickOnElement(jsonData.getWebElement("Preview", "SaveListing_btn"),"Saved Button Element is not Clicked");
	//	new Actions(driver).click(jsonData.getWebElement("Preview", "SaveListing_btn")).build().perform();
	}

	public void clickOn_SaveSearch_Preview_link() {
		elementAction.clickOnElement(jsonData.getWebElement("PreviewOfSavesearch"),
				"Clicked on Save Search Preview Link");
	}
	
	public void hovorOnPreviewHome()
	{
		WebElement preview=driver.findElement(By.xpath("//div[@id='previewItem']"));
		action.moveToElement(preview).build().perform();
	
	}
	
	public void scrollOnPage()
	{
		 for(int sec=0;;sec++) { if(sec>=5) { break; }
		  js.executeScript("window.scrollBy(0,400)",""); ActionsUtility.staticWait(1);
		  }
	}

	public void clickOn_PrintReport_btn_Preview() {
		
		scrollOnPage();
	//	 js.executeScript("window.scrollBy(0,1000)");
	//	js.executeScript("arguments[0].scrollIntoView(true);", jsonData.getWebElement("Preview", "PrintReport"));
		 elementAction.clickOnElement(jsonData.getWebElement("Preview", "PrintReport"),"Print Report Button Element is not Clicked");
			
	//	new Actions(driver).click(jsonData.getWebElement("Preview", "PrintReport")).build().perform();
	}

	public void clickOnContactTheBuilderTabFromPreview() {
		elementAction.clickOnElement(jsonData.getWebElement("Preview", "ContactTheBuilder_tab"),
				"Click on Contact The Builder tab");
	}

	public void enterTextInContactBuilderTextArea(String textInContactTextArea) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("Preview", "ContactBuilder_txtArea"),
				textInContactTextArea, "Entered " + textInContactTextArea);
	}

	public void clickOn_ContactBuilderAgent() {
		elementAction.clickOnElement(jsonData.getWebElement("Preview", "ContactBuilderAgent_btn"),
				"Clicked on Contact Builder Agent button");
	}

	public void selectNewClientAndEnterDetailInSavedSearch(String FirstName, String LastName, String Phone,
			String Email) {
		elementAction.clickOnElement(jsonData.getWebElement("SaveSearch", "SearchClient_field"),
				"Clicked on Search Client field");
		elementAction.clickOnElement(jsonData.getWebElement("NewClient"), "Clicked on New Client");
		elementAction.normalTypeOnElement(jsonData.getWebElement("FirstName_editField"), FirstName,
				"FirstName Entered");
		elementAction.normalTypeOnElement(jsonData.getWebElement("LastName_editfield"), LastName, "LastName Entered");
		elementAction.normalTypeOnElement(jsonData.getWebElement("Email_editfield"), Email, "Email Entered");
		elementAction.normalTypeOnElement(jsonData.getWebElement("PhoneNo_editfield"), Phone, "Phone No Entered");
	}
	
	public void clickOnContactAgentSales_btnPreview() {
		elementAction.clickOnElement(jsonData.getWebElement("Preview", "ContactAgentSales_btn"),"Clicked on Conatct Agent Sales Button");
		String successMessage = jsonData.getWebElement("Preview", "SuccessMessageAfterContactSalesAgentButtonClicked").getText();
		Assert.assertEquals(successMessage, "Click here to send another message.", "Click here to send another message doesn't appear");
	}
	
	public void clickOn_RequestToRegisterClient_btn_BuilderPreview() {
		elementAction.clickOnElement(jsonData.getWebElement("Preview","RequestToRegisterClient_btn_Builder"),"Clicked on Request to Register Client from Conatct The builder preview ");
		
	}
	
	public void clickOnContactMeFormTabPreview() {
		elementAction.clickOnElement(jsonData.getWebElement("Preview","ContactMeFormTabPreview"),"clicked On the Contact Me Form Preview");
	}
	public void enterTextInContactMeTextArea(String textInContactTextArea) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("Preview", "ContactMe_txtArea"),
				textInContactTextArea, "Entered " + textInContactTextArea);
	}
	public void clickOnSend_button_on_ContactMe_Preview() {
		elementAction.clickOnElement(jsonData.getWebElement("Preview","ContactMe_Send_Button"), "Clicked on the send button");
		String successMessage = jsonData.getWebElement("Preview", "SuccessMessageAfterContactMeButtonClicked").getText();
		Assert.assertEquals(successMessage, "Click here to send another message.", "Click here to send another message doesn't appear");
	}
	

//==================================================== Map View =======================================================

	public boolean isDrawInMapOptionExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("MapView", "DrawOnMap_icon"),
				"Checking for Draw in map option");
	}

	public boolean isSearchWithInMapExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("MapView", "SearchWithInMap"),
				"Checking for Search in map option");
	}

	public List<String> getMapOptions() {
		return driver.findElements(By.xpath("//div[@class=\"gmnoprint\"]/div[@class='gm-style-mtc']/button")).stream()
				.map(s -> s.getText()).collect(Collectors.toList());
	}

	public boolean isZoomInButtonExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("MapView", "ZoomIn"),
				"Check for existence of Zoom in button");
	}

	public boolean isZoomOutButtonExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("MapView", "ZoomOut"),
				"Check for existence of Zoom out button");
	}

	public void clickOnCommunity(int n) {
		driver.findElement(By.xpath("//tbody/tr["+n+"]/td[3]/a[1]")).click();
		
	}

	public void clickOn_CommunityStatus(String listingType) {
		if(listingType.equalsIgnoreCase("normal")){
			elementAction.clickOnElement(jsonData.getWebElement("MoreFilter", "Normal"), "clicked on the normal community status on more filter section");
		}
		else if(listingType.equalsIgnoreCase("comingSoon")) {
			elementAction.clickOnElement(jsonData.getWebElement("MoreFilter", "ComingSoon"), "clicked on the coming soon status on more filter section");
		}
		else if(listingType.equalsIgnoreCase("notBilled")) {
			ActionsUtility.staticWait(5);
//			elementAction.clickOnElement(jsonData.getWebElement("MoreFilter", "All_Builders"), "Clicked on the All builders scroll down on More Filter section");
			driver.findElement(By.xpath("//button[@id='BuilderName']")).click();
			driver.findElement(By.xpath("//button[@id='BuilderName']/..//li[@id='93']")).click();
			System.out.println("Success");
		}
		else
		     System.out.println("select correct listing type");
		
	}

	public void clickOn_MoreFilterButton() {
		ActionsUtility.staticWait(5);
		elementAction.clickOnElement(jsonData.getWebElement("MoreFilter","MoreButton"), "Clicked on the \"More\" Button");
	}

	public void clickOn_UpdateButtonUnderMoreFilterSection() {
        elementAction.clickOnElement(jsonData.getWebElement("MoreFilter", "UpdateButton"), "Clicked on the Update Button");
		
	}

}