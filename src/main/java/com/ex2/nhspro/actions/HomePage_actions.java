package com.ex2.nhspro.actions;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class HomePage_actions {

	JsonUtility jsonData;
	ExtentTest logger;
	WebDriver driver;
	ActionsUtility elementAction;

	public HomePage_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "HomePage");
		elementAction = new ActionsUtility(driver, logger);
	}

	
	public boolean isBackgroundImageExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("backgroundImage"), "Checking for background Image exist on the homepage");
	}
	
	
	public HashMap<String, String> getSearchBoxElementsExistOnThePage() {
		HashMap<String,String> hasMap=new HashMap<String,String>();
		List<String> labels=driver.findElements(By.xpath("//p[@id='HomesTypeField']/label")).stream().map(s->s.getText()).collect(Collectors.toList());
		hasMap.put("title",elementAction.getTextData(jsonData.getWebElement("SearchBox","Search_label")).replace("\n", " "));
		hasMap.put("labels", labels.toString());
		hasMap.put("allNewHomesElement_selection",jsonData.getWebElement("SearchBox","All_New_Homes_label").getCssValue("border-bottom"));
		hasMap.put("inventoryHomesSelection", jsonData.getWebElement("SearchBox", "Inventory_Homes").getCssValue("border-bottom"));
		hasMap.put("searchfield",""+elementAction.isElementExistOnThePage(jsonData.getWebElement("SearchBox","Search_field"), "Search field exist on the page"));
		hasMap.put("searchbutton",""+elementAction.isElementExistOnThePage(jsonData.getWebElement("SearchBox","Search_icon"), "Search button exist on the page"));
		hasMap.put("minPriceDropdownSelectedValue",elementAction.getFirstSelectedValueFromDropdown(jsonData.getWebElement("SearchBox","MinPrice_dropdown")));
		hasMap.put("maxPriceDropdownSelectedValue",elementAction.getFirstSelectedValueFromDropdown(jsonData.getWebElement("SearchBox","MaxPrice_dropdown")));
		hasMap.put("BedsDropdownSelectedValue",elementAction.getFirstSelectedValueFromDropdown(jsonData.getWebElement("SearchBox","Beds_dropdown")));
		hasMap.put("BathDropdownSelectedValue",elementAction.getFirstSelectedValueFromDropdown(jsonData.getWebElement("SearchBox","Baths_dropdown")));
		hasMap.put("squareFeetDropdownSelectedValue",elementAction.getFirstSelectedValueFromDropdown(jsonData.getWebElement("SearchBox","SquareFeet_dropdown")));
		return hasMap;
	}
	
	public HashMap<String, String> getBannerDetails() {
		HashMap<String,String> hasMap=new HashMap<String,String>();
		hasMap.put("title", elementAction.getTextData(jsonData.getWebElement("bannerinfo", "title")));
		hasMap.put("description", elementAction.getTextData(jsonData.getWebElement("bannerinfo", "bannerText")));
		return hasMap;
	}
	public void clickOnSignUpForWebinarLink() {
		elementAction.clickOnElement(jsonData.getWebElement("SignUpforUpcomingWebinar"), "Clicked On Sign up for an upcoming webinar");
	}
	
	public String getWebinarPageTitle() {
		return elementAction.getTextData(jsonData.getWebElement("WebinarPageTitle"));
	}
	
	
	public boolean isSignUpForWbinarLinkExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("SignUpforUpcomingWebinar"), "Checking for Sign up for an upcoming webinar link");
	}
	
	public boolean isWhatsNewTodayLinkExist() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("SeeWhatsNewTodayPage","SeeWhatsNewTodayLink"), "Checking for the see what's New Today link");
		}else {
			return elementAction.isElementExistOnThePage(jsonData.getWebElement("SeeWhatsNewTodayPage","SeeWhatsNewTodayLink"), "Checking for the see what's New Today link");
		}
	}
	
	public void clickOnWhatsNewTodayLink() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
		elementAction.clickOnElement(jsonData.getWebElement("SeeWhatsNewTodayPage","SeeWhatsNewTodayLink"), "Clicking on See What's New Today Link");
		}else {
			elementAction.clickOnElement(jsonData.getWebElement("SeeWhatsNewTodayPage","SeeWhatsNewTodayLink"), "Clicking on See What's New Today Link");	
		}
	}
	
	public boolean isSeeAllQuickMoveInsLinkExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("SeeWhatsNewTodayPage","SeeAllQuickMoveInsLink"), "Checking for the See All Quick Move-Ins link");
	}
	
	public boolean isSeeAllNewCommunitiesLinkExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("SeeWhatsNewTodayPage","SeeAllNewCommunitiesLink"), "Checking for the See All New Communities link");
	}
	
	public boolean isSeeAllFeaturedCommunitiesLinkExist() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("SeeWhatsNewTodayPage","SeeAllFeaturedCommunities"), "Checking for the See All Featured Communities link");
	}
	
	
	public String getNavigationToConsumerPortallink() {
		return elementAction.getTextData(jsonData.getWebElement("SignInToNavigateConsumer"));
	}
	
	public HashMap<String, String>getsGuideSectionData() {
		HashMap<String, String> hashMap=new HashMap<>();
		hashMap.put("title", elementAction.getTextData(jsonData.getWebElement("GuideVideo", "title")));
		hashMap.put("video", ""+elementAction.isElementExistOnThePage(jsonData.getWebElement("GuideVideo", "video"), "Checking for video"));
		return hashMap;
	}
	
	public HashMap<String, String> getHomeBuilderSectionData() {
		HashMap<String, String> hashMap=new HashMap<>();
		hashMap.put("title", elementAction.getTextData(jsonData.getWebElement("Builders", "title")));
		hashMap.put("noOfBuilders", ""+driver.findElements(By.xpath("//ul[@id='home-brands']/li/img")).size() );
		return hashMap;
	}
	
	public void validateAgentsSection() {
			for(int i=1;i<=4;i++)
			{
			elementAction.isElementExistOnThePage(driver.findElement(By.xpath("(//div[@id='testimonialContainer']/div/div//img)["+i+"]")), "Checking for Imge of Agent existence");
			elementAction.isElementExistOnThePage(driver.findElement(By.xpath("(//div[@id='testimonialContainer']/div/div//figcaption/strong)["+i+"]")), "Checking for Agent Name");
			elementAction.isElementExistOnThePage(driver.findElement(By.xpath("(//div[@id='testimonialContainer']/div/div/div/h2)["+i+"]")), "Checking for agent title");
			elementAction.isElementExistOnThePage(driver.findElement(By.xpath("(//div[@id='testimonialContainer']/div/div/div/p)["+i+"]")), "Checking for agent description");
			if(i==4)
				break;
			elementAction.clickOnElement(jsonData.getWebElement("Agents","rightnavigation_icon"), "Click on right Navigation");	
			}

	}
	
	public HashMap<String, String> getLearnHowToSellData() {
		HashMap<String, String> hashMap=new HashMap<>();
		hashMap.put("title", elementAction.getTextData(jsonData.getWebElement("HowToSellInfo", "title")));
		hashMap.put("noOfBuilders", ""+driver.findElements(By.xpath("//div[@id='home-learn-guide']//a")).size() );
		return hashMap;
	}
	
	
	public HashMap<String, String> getStartFreshSection() {
		HashMap<String, String> hashMap=new HashMap<>();
		hashMap.put("title", elementAction.getTextData(jsonData.getWebElement("StartFresh", "title")));
		hashMap.put("mainVideo", ""+elementAction.isElementExistOnThePage(jsonData.getWebElement("StartFresh", "mainvideo"),"Checking for main video for StartFresh"));
		hashMap.put("videoCount", ""+driver.findElements(By.xpath("//div[@id='home-start-fresh']/div/div/a")).size() );
		return hashMap;
	}
	
	
	public void enterSearchData_field(String SearchData) {
		WebElement searchField = jsonData.getWebElement("SearchBox","Search_field");
		searchField.clear();
//		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
//			ActionsUtility.staticWait(3);
//		}
		elementAction.typeSlowOnElement(searchField, SearchData, "Enter " + SearchData + " in Search field");
	}

	public void clickOnSearch_icon() {
		elementAction.waitForLoad(driver);
		elementAction.clickOnElement(jsonData.getWebElement("SearchBox","Search_icon"), "Clicked in Search icon");
		elementAction.waitForLoad(driver);
	}

	public String select_minprice(String minPrice) {
		String text = "";
		WebElement MinPrice = jsonData.getWebElement("SearchBox","MinPrice_dropdown");
		elementAction.waitForVisibility(MinPrice);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			ActionsUtility.staticWait(5);
			WebElement bathroomCount = driver.findElement(By.xpath("//body/main[@id='main-content']/div[@id='home-search']/form[@id='pro-search-box']/div[1]/div[2]/div[3]/ul[1]/li[2]/label[1]"));
			bathroomCount.click();
			Actions action = new Actions(driver);
			action.dragAndDropBy(MinPrice, 5, 50).release().build().perform();
			ActionsUtility.staticWait(1);
			text = driver.findElement(By.xpath("/html[1]/body[1]/main[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/p[1]/span[1]")).getText();
			System.out.println(text);
		} else {
			elementAction.clickOnElement(MinPrice, "Clicked on MinPrice element");
			ActionsUtility.staticWait(1);
			Select s1 = new Select(MinPrice);
			s1.selectByValue(minPrice);
		}
		logger.log(Status.INFO, "Price is selected");
		System.out.println("Price is selected");
		return text;
	}

	public String getSelectedMinPrice() {
		String value = jsonData.getWebElement("MinPrice_value").getText();
		return value;
	}

	public String select_maxprice(String maxPrice) {
		String text = null;
		elementAction.waitForVisibility(jsonData.getWebElement("SearchBox","MaxPrice_dropdown"));
		WebElement MaxPrice = jsonData.getWebElement("SearchBox","MaxPrice_dropdown");
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			Actions action = new Actions(driver);
			action.dragAndDropBy(MaxPrice, -50, -50).release().build().perform();
			ActionsUtility.staticWait(1);
			text = elementAction.getTextData(jsonData.getWebElement("MaxPrice_value"));
			System.out.println(text);
		} else {

			MaxPrice.click();
			ActionsUtility.staticWait(1);
			Select s1 = new Select(jsonData.getWebElement("SearchBox","MaxPrice_dropdown"));
			s1.selectByValue(maxPrice);
		}
		return text;
	}

	public void select_beds(String noOfBeds) {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {

			driver.findElement(By.xpath("(//label[text()='" + noOfBeds + "+'])[1]")).click();
			logger.log(Status.INFO, "Clicked on the Bedrooms");
			System.out.println("Clicked on the Bedrooms");
		} else {
			elementAction.waitForVisibility(jsonData.getWebElement("SearchBox","Beds_dropdown"));
			jsonData.getWebElement("SearchBox","Beds_dropdown").click();
			ActionsUtility.staticWait(1);
			Select s1 = new Select(jsonData.getWebElement("SearchBox","Beds_dropdown"));
			s1.selectByValue(noOfBeds);
		}
	}

	public void select_bath(String noOfbath) {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(driver.findElement(By.xpath("(//label[text()='" + noOfbath + "+'])[2]")),
					"Clicked on the Bathrooms");
		} else {
			elementAction.waitForVisibility(jsonData.getWebElement("SearchBox","Baths_dropdown"));
			elementAction.clickOnElement(jsonData.getWebElement("SearchBox","Baths_dropdown"), "Clicked on BathRoom dropdown");
			ActionsUtility.staticWait(1);
			new Select(jsonData.getWebElement("SearchBox","Baths_dropdown")).selectByValue(noOfbath);
		}
	}

	/****************************** Square feet Methods *************/
	public String select_squareFeet(String sqtFeet) {
		String mobilesqtfeet = null;
		elementAction.waitForVisibility(jsonData.getWebElement("SearchBox","SquareFeet_dropdown"));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			Actions action = new Actions(driver);
			action.dragAndDropBy(jsonData.getWebElement("SearchBox","SquareFeet_dropdown"), -10, 0).release().build().perform();
			ActionsUtility.staticWait(1);
			String text = elementAction.getTextData(jsonData.getWebElement("SliderSquarefeetMobile"));
			mobilesqtfeet = text.replace("+", "");
			System.out.println(mobilesqtfeet);
		} else {
			elementAction.clickOnElement(jsonData.getWebElement("SearchBox","SquareFeet_dropdown"), "Clicked on Square Feet dropdown");
			ActionsUtility.staticWait(1);
			elementAction.selectValueFromDropdown(jsonData.getWebElement("SearchBox","SquareFeet_dropdown"), sqtFeet, "Select "+sqtFeet+" from square feet dropdown");
		}
		return mobilesqtfeet;
	}

	/**************************************** End *******************************/

	public void weAreSorry() {
		if (driver.getTitle().contains("Error")) {
			Assert.fail("Home/Community is not available");
		}
		List<WebElement> element = driver.findElements(By.xpath("//h1[contains(text(),'We')]"));
		if (element.size() > 0) {
			Assert.fail("Home/Community is not available");
		}
	}
	
	
	
	
}
