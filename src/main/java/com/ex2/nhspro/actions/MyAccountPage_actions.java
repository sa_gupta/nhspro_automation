package com.ex2.nhspro.actions;



import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class MyAccountPage_actions {
	
		WebDriver driver;
//		WebDriverWait wait;
		JsonUtility jsonData;
		ExtentTest logger;
		JavascriptExecutor js;
		ActionsUtility elementAction;
		public MyAccountPage_actions(WebDriver driver, ExtentTest logger) {
			this.logger = logger;
			this.driver = driver;
			jsonData = new JsonUtility(driver, "MyAccount");
			js = (JavascriptExecutor) driver;
			elementAction=new ActionsUtility(driver, logger);
		}
		public void myAccount_titletext() {
			elementAction.waitForVisibility(jsonData.getWebElement("MyAccount_title"));
			logger.log(Status.INFO, "User Redirect to "+jsonData.getWebElement("MyAccount_title").getText()+" Page");
			System.out.println("User Redirect to "+jsonData.getWebElement("MyAccount_title").getText()+" Page");
		}
		public void clickOnSaveButton()
		{
			// This  will scroll down the page by  1000 pixel vertical		
	        js.executeScript("window.scrollBy(0,1000)");
	        elementAction.clickOnElement(jsonData.getWebElement("Save_Button"), "Clicked On the Save Button");		}
		public String sucessMessagetext()
		{
			return elementAction.getTextData(jsonData.getWebElement("Success_Message"));
		}
		public void clickOnOkButton()
		{
			elementAction.clickOnElement(jsonData.getWebElement("OK_Button"), "Clicked On the OK Button");
			}
		
		public void selectEmailCheckbox()
		{
			ActionsUtility.staticWait(2);
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("Zipcode"));
		ActionsUtility.staticWait(1);
		boolean email=jsonData.getWebElement("Email_checkbox").isSelected();
		System.out.println(" Is Element Selected "+email);
		if(email==true)
			System.out.println("Email Checkbox is already selected");
		else
			elementAction.clickOnElement(jsonData.getWebElement("Email_Checkbox_text"), "Select the Email Checkbox");	
		}
		
		public void selectTextCheckbox()
		{
		ActionsUtility.staticWait(1);
		boolean text=jsonData.getWebElement("Text_checkbox").isSelected();
		System.out.println(" Is Element Selected "+text);
		if(text==true)
			System.out.println("Text checkbox is already selected");
		else
		{
			elementAction.clickOnElement(jsonData.getWebElement("Text_Checkbox_text"), "Select the Text Checkbox");		}
		}
		public void selectNewsletterCheckbox()
		{
		ActionsUtility.staticWait(1);
		boolean text=jsonData.getWebElement("Newsletter_checkbox").isSelected();
		System.out.println(" Is Element Selected "+text);
		if(text==true)
			System.out.println("Newsletter checkbox is already selected");
		else
			elementAction.clickOnElement(jsonData.getWebElement("Newsletter_Checkbox_text"), "Select the Text Checkbox");	
		}
		
		public void enterMyAccountDetails(String FirstName,String LastName,String BrokerageName,String Email,String BusinessPhone,String PhoneNumber,String Zipcode)
		{
			elementAction.clearandType(jsonData.getWebElement("First_name"), FirstName, "Entered First Name as "+FirstName);
			elementAction.clearandType(jsonData.getWebElement("Last_name"), LastName, "Entered First Name as "+LastName);
			elementAction.clearandType(jsonData.getWebElement("Email_address"), Email, "Entered Email as "+Email);
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			{
				 WebElement element=jsonData.getWebElement("Business_Phone");
				 element.clear();
				ActionsUtility.staticWait(2);
				js.executeScript("arguments[0].value='"+BusinessPhone+"';",element);
				
			}
			else {
			elementAction.clearandType(jsonData.getWebElement("Business_Phone"), BusinessPhone, "Entered BusinessPhone as "+BusinessPhone);
			}
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			{
				 WebElement element=jsonData.getWebElement("Mobile_Phone");
				 element.clear();
				ActionsUtility.staticWait(2);
				js.executeScript("arguments[0].value='"+PhoneNumber+"';",element);
			}
			else
			{
			elementAction.clearandType(jsonData.getWebElement("Mobile_Phone"), PhoneNumber, "Entered Mobile Phone as "+PhoneNumber);
			}
			elementAction.clearandType(jsonData.getWebElement("Brokerage_Name"), BrokerageName, "Entered BrokerageName as "+BrokerageName);
			/*
			 * jsonData.getWebElement("Zipcode").clear();
			 * jsonData.getWebElement("Zipcode").click();
			 * js.executeScript("document.getElementById('ZipCode').value='"+Zipcode+"';");
			 */

			ActionsUtility.staticWait(3);
		//	elementAction.typeSlowOnElement(jsonData.getWebElement("Zipcode"), FirstName, "Entered Zipcode as "+Zipcode);
				
			if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			{
				//((AppiumDriver) driver).hideKeyboard();
			}
		}

}
