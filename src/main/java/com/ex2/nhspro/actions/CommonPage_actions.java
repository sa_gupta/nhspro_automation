package com.ex2.nhspro.actions;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.base.SSOBaseClass;
import com.ex2.nhspro.base.TestBase;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;
import static org.testng.Assert.assertTrue;

public class CommonPage_actions{

	WebDriver driver;
	JsonUtility jsonData;
	ExtentTest logger;
	ActionsUtility elementAction;

	public CommonPage_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "CommonPage");
		elementAction=new ActionsUtility(driver, logger);
	}
	
	public void clickOnLogo() {
		elementAction.clickOnElement(jsonData.getWebElement("Logo"), "Clicked on Logo to navigate homepage");
	}

	  public void hoverToSaveListing() {
		  if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
		  elementAction.hovertoElement(driver, jsonData.getWebElement("Saved_Listings"));
		  }
		  else {
			  clickOnSideMenu_mobile();
	elementAction.clickOnElement(jsonData.getWebElement("Saved_Listings"), "Clicked on Saved Listing from Menu option");
		  }
	  }
	  public void clickOnTrustBuilder_Link()
	  {
		  elementAction.hovertoElement(driver, jsonData.getWebElement("Resources"));
		  elementAction.clickOnElement(jsonData.getWebElement("TrustBuilderPage"), "Clicked on the Trust Builder Link");
	  }
	  public void clickOnWebinarLink()
	  {
		  if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			  clickOnSideMenu_mobile();
			  elementAction.clickOnElement(jsonData.getWebElement("Resources"), "Clicked on the Resource"); 
		  }
		  else {
		  elementAction.hovertoElement(driver, jsonData.getWebElement("Resources"));
		  }
		  elementAction.clickOnElement(jsonData.getWebElement("Webinar"), "Clicked on the Webinar Page");
	  }
	  
	  public void clickOnTrainingVideo()
	  {
		  elementAction.hovertoElement(driver, jsonData.getWebElement("Resources"));
		  elementAction.clickOnElement(jsonData.getWebElement("Training_Videos"), "Clicked on the Training Videos");
	  }
	  
	  public void clickOnHowToSell()
	  {
		  elementAction.hovertoElement(driver, jsonData.getWebElement("Resources"));
		  elementAction.clickOnElement(jsonData.getWebElement("How_to_Sell_New"), "Clicked on How_to_Sell_New");
	  }
	  public void clickOnSaveListing() {
		  if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
				clickOnSideMenu_mobile();
			}
		  elementAction.clickOnElement(jsonData.getWebElement("Saved_Listings"), "Clicked on Saved Listing from Menu option");	  
	  }
	  
	  public void clickOnViewAll_SaveListing() {
		  elementAction.clickOnElement(jsonData.getWebElement("SaveListing_ViewAll"), "Clicked on View All of Save Listing");
	  }
	  
	  public void openParticularSaveListing() {
		 hoverToSaveListing();		 
	  }
	
	public void click_On_SavedSearches() {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			clickOnSideMenu_mobile();
			elementAction.clickOnElement(jsonData.getWebElement("Saved_Searches"), "Clicked on Saved Searches from Menu option");
		}
		else
		{
			boolean staleElement = true;
			while (staleElement) {
				try {
					elementAction.waitForVisibility(jsonData.getWebElement("Saved_Searches"));
					elementAction.clickOnElement(jsonData.getWebElement("Saved_Searches"), "Clicked on Saved Searches from Menu option");
					staleElement = false;
				} catch (StaleElementReferenceException e) {
					staleElement = true;
				}
			}
		
		}
	}
	public void clickOnSingIn_link() {

		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			clickOnSideMenu_mobile();
		}
		elementAction.clickOnElement(jsonData.getWebElement("SignIn_link"), "Clicked on SignIn link from Menu option");
	}

	public void enterEmailAddress(String emailAdd) {
		elementAction.typeSlowOnElement(jsonData.getWebElement("EmailAddress_field"), emailAdd, "Enter " + emailAdd + " in Email field");
	}

	public void clickOnSubmit_btn() {
		ActionsUtility.staticWait(2);
		elementAction.clickOnElement(jsonData.getWebElement("submit_btn"), "Click on Submit button");
		ActionsUtility.staticWait(2);
	}

	public void enterPassowrd_in_Password_field(String password) {
			ActionsUtility.staticWait(3);
		elementAction.normalTypeOnElement(jsonData.getWebElement("Password_field"), password, "Entered " + password + " in Password field");
	}
	
	public void enterFirstName_in_FirstName_field(String firstName) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("FirstName_field"), firstName, "Entered " + firstName + " in FirstName field");
	}	
	public void enterLastName_in_LastName_field(String lastName) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("LastName_field"),lastName,"Entered " + lastName + " in LastName field");
	}
	
	public void enterZipCode_in_ZipCode_field(String zipCode) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("ZipCode_field"),zipCode,"Entered " + zipCode + " in ZipCode field");
	}
	
	public void enterLicense_in_License_field(String license) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("License_field"),license,"Entered " + license + " in License field");
	}
	
	public void enterPhone_in_Phone_field(String phone) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("Phone_field"),phone,"Entered " + phone + " in Phone field");
	}
	
	public void enterConfirmPassword_in_ConfirmPassword_field(String confirmPassword) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("ConfirmPassword_field"),confirmPassword,"Entered " + confirmPassword + " in ConfirmPassword field");
	}
	
	public void clickOnSignIn_button() {
		elementAction.clickOnElement(jsonData.getWebElement("Sign_In_btn"), "Click on SignIn button");
	}
	
	public void clickOnCreateAccount_button() {
		elementAction.clickOnElement(jsonData.getWebElement("Create_Account_btn"), "Click on Create Account button");
	}
	
	public void clickOnSave_button() {
		elementAction.clickOnElement(jsonData.getWebElement("Save_btn"), "Click on Save button");
	}

	public boolean isUserLoggedIn() {
		ActionsUtility.staticWait(2);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			clickOnSideMenu_mobile();
		}
		boolean status;
		String  WelcomeMsg=elementAction.getTextData(jsonData.getWebElement("Welcome_afterLogin"));
		System.out.println(WelcomeMsg+" is showing after login");
		logger.log(Status.INFO, WelcomeMsg+" is showing after login");
		WebElement element=jsonData.getWebElement("Welcome_afterLogin");
		if (element.isDisplayed())
			status = true;
		else
			status = false;
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")){
			jsonData.getWebElement("Home_Search").click();
		}
		return status;
	}

	public void clickOnWelcomeProfile() {
		WebElement Welcome_afterLogin = jsonData.getWebElement("Welcome_afterLogin");
		elementAction.waitForVisibility( Welcome_afterLogin);

	}

	public void loginIntoApplication(String emailId, String Password) {
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
		clickOnSingIn_link();
		enterEmailAddress(emailId);
//		clickOnSubmit_btn();
		enterPassowrd_in_Password_field(Password);
		clickOnSignIn_button();
		
		 boolean LoginStatus=isUserLoggedIn();
		 Assert.assertTrue(LoginStatus,"User not logged In");
		 
		System.out.println("User is logged In");
		ActionsUtility.staticWait(2);
	}
	
	public void loginIfNot(String emailId,String Password) {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		TestBase base=new TestBase();
		try {
		if(elementAction.isElementExistOnThePage(jsonData.getWebElement("Welcome_afterLogin"), "User is logged in") && driver.getCurrentUrl().contains("professional")) {
			base.openAgentApplication();
		}
		}catch(Exception e) {
			loginIntoApplication(emailId, Password);			
		}
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
	}
	
	public void loginIfNotInShowingNew(String emailId) {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
		if(elementAction.isElementExistOnThePage(jsonData.getWebElement("Welcome_afterLogin"), "User is logged in") && driver.getCurrentUrl().contains("showingnew")) {
			clickOnSearchNewHomes_link();
		}
		}catch(Exception e) {
			clickOnSingIn_link();
			enterEmailAddress(emailId);
			clickOnSubmit_btn();
			boolean LoginStatus=isUserLoggedIn();
			Assert.assertTrue(LoginStatus,"User not logged In");
		}
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
	}

	private void clickOnSideMenu_mobile() {
		ActionsUtility.staticWait(5);
		boolean staleElement = true;
		while (staleElement) {
			try {
				elementAction.waitForVisibility(jsonData.getWebElement("SideMenu"));
				elementAction.clickOnElement(jsonData.getWebElement("SideMenu"), "Open side menu");
				staleElement = false;
			} catch (StaleElementReferenceException e) {
				staleElement = true;
			}
		}
		ActionsUtility.staticWait(2);
	}
	
	public void closeSideMenu_mobile()
	{
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("CloseMenu"), "Close Menu Clicked");
	}
	public void focusOnFacebook_icon() {
		Actions action=new Actions(driver);
		action.moveToElement(jsonData.getWebElement("Facebook_icon")).build().perform();
	}
	public void clickOnContactUs_link() {
		elementAction.clickOnElement(jsonData.getWebElement("Contact_Us"), "Click on Contact Us link");
	}
	public void clickOnAccessibilityStatement() {
		elementAction.clickOnElement(jsonData.getWebElement("Accessibility_Statement"), "Click on Accessibility link");
	}
	public void clickOnPrivacy_link() {
		elementAction.clickOnElement(jsonData.getWebElement("Privacy"), "Click on Privacy link");
	}
	public void clickOnCookie_link() {
		elementAction.clickOnElement(jsonData.getWebElement("Cookie"),"Click on Cookie link");
	}
	public void clickOnAboutUs_link() {
		elementAction.clickOnElement(jsonData.getWebElement("About_Us"), "Click on About Us link");
	}
	public void clickOnHelp_link() {
		elementAction.clickOnElement(jsonData.getWebElement("Help"),"Click on Help link");
	}
	public void clickOnTerms_link() {
		elementAction.clickOnElement(jsonData.getWebElement("Terms"), "Click on Terms link");
	}
	public void clickOnDoNotSellInfo_link() {
		elementAction.clickOnElement(jsonData.getWebElement("DoNoSell"), "Click on Do not sell my Info link");
	}
	public void clickOnMyClientsLink()
	{
		  if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
				clickOnSideMenu_mobile();			
			}
		  elementAction.clickOnElement(jsonData.getWebElement("My_Clients"), "Click on My Clients link");
	}
	public void focusOnWelcome_icon() {
		Actions action=new Actions(driver);
		action.moveToElement(jsonData.getWebElement("Welcome_afterLogin")).build().perform();
		logger.log(Status.INFO, "Focus on User Profile");
		System.out.println("Focus on User Profile");
	}
	public void clickOnMyAccount()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
			clickOnSideMenu_mobile();
			elementAction.clickOnElement( jsonData.getWebElement("Welcome_afterLogin"), "Clicked on the User Profile");  	 
		}
		else
		{
		focusOnWelcome_icon();
		}
		elementAction.clickOnElement( jsonData.getWebElement("My_Account"), "Clicked on My Account"); 
	}
	
	
	
	public void clickOnMyConsumerPortal()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
			clickOnSideMenu_mobile();
			elementAction.clickOnElement(jsonData.getWebElement("Welcome_afterLogin"),  "Clicked on the User Profile");
		}
		else {
			focusOnWelcome_icon();
		elementAction.clickOnElement(jsonData.getWebElement("ShowingNew_link"), "Clicked on ShowingNew.com Link");
		}
	}
	
	public void clickOnWidget()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
			clickOnSideMenu_mobile();
			elementAction.clickOnElement(jsonData.getWebElement("Welcome_afterLogin"),  "Clicked on the User Profile");
			elementAction.clickOnElement(jsonData.getWebElement("ShowingNew_link"), "Clicked on ShowingNew.com Link");
		}
		else
			focusOnWelcome_icon();
		elementAction.clickOnElement(jsonData.getWebElement("Widget"), "Clicked on Widget");
	}
	
//ShowingNew page
	
	public void clickOnSearchNewHomes_link() {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) 
			clickOnSideMenu_mobile();
		elementAction.clickOnElement(jsonData.getWebElement("SearchNewHomes_link"), "Click on Search New Homes link");
	}
	
	public void clickOnManagMyClientActivity()
	{
		focusOnWelcome_icon();
		elementAction.clickOnElement(jsonData.getWebElement("ManageMyClientActivityLink"), "Clicked on Manage My Client Activity Link");
		}
	

	public void openNewTab()
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.open()");
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		System.out.println("New tab has been opened");
		logger.log(Status.INFO, "New tab has been opened");
	}
	
	public void switchtoWindow()
	{
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
	}
	
	public void logout()
	{
		focusOnWelcome_icon();
		elementAction.clickOnElement(jsonData.getWebElement("Logout"), "Clicked on SignOut");
	  	
	}
	public void logoutIflogin()
	{
		List<WebElement> elements = driver.findElements(By.xpath("//span[@id='user-name-menu']"));
		if(elements.size()>0)
		{
			logout();
		}
	  	
	}
	
	public void clickOnHomes_tab() {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) 
		{
			
		}
		else {
		while(!jsonData.getWebElement("Tabs","HomeTabSelection").getAttribute("class").contains("active"))
			elementAction.clickOnElement(jsonData.getWebElement("Tabs","HomeTab"), "Clicked on homes tab");
		Assert.assertTrue(isHomeTabSelected());
		}
	}
	
	public void clickOnView(String View) {
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) 
		{
			
		}
		else {
		WebElement viewElement=jsonData.getWebElement("Views",View+"View");
		while(!viewElement.getAttribute("class").contains("active"))
			elementAction.clickOnElement(viewElement, "Clicked on "+View);
		}
	}

	public boolean isHomeTabSelected() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.waitForAttributeAvailablility(jsonData.getWebElement("Tabs","HomeTabSelection"), "value", "View Communities");
		}else {
		elementAction.waitForAttributeAvailablility(jsonData.getWebElement("Tabs","HomeTabSelection"), "class", "active");
	//	isTabSelected(jsonData.getWebElement("Homes_tab"));
		}
		return true;
	}
	public boolean isCommunitiesSelected() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.waitForAttributeAvailablility(jsonData.getWebElement("Tabs","CommunityTabSelection"), "value", "View Homes");
		}else {
		ActionsUtility.staticWait(3);
		elementAction.waitForAttributeAvailablility(jsonData.getWebElement("Tabs","CommunityTabSelection"), "class", "active");
//		isTabSelected(jsonData.getWebElement("Communities_tab"));
		}
		return true;
	}

	public void clickOnCommunities_tab() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			assertTrue(jsonData.getWebElement("CommunityTab").isDisplayed());
			elementAction.clickOnElement(jsonData.getWebElement("CommunityTab"), "Click on Communities tab");
		} else {
			while (!jsonData.getWebElement("Tabs", "CommunityTabSelection").getAttribute("class").contains("active"))
				elementAction.clickOnElement(jsonData.getWebElement("Tabs", "CommunityTab"),
						"Click on Communities tab");
			isCommunitiesSelected();
		}
	}

		public void register_a_newclient(String firstName,String lastName,String phone)
		{
			enterFirstName_in_FirstName_field(firstName);
			enterLastName_in_LastName_field(lastName);
			ActionsUtility.staticWait(1);
			enterPhone_in_Phone_field(phone);
			ActionsUtility.staticWait(1);
			clickOnSave_button();
		}

		public void scroll(double length , double width) {
			JavascriptExecutor js =(JavascriptExecutor) driver;
	        js.executeScript("window.scrollBy("+length+","+width+")");
		}
		
}
