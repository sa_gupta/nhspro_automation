package com.ex2.nhspro.actions;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class Widget_actions {
	
	JsonUtility jsonData;
	ExtentTest logger;
	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	ActionsUtility elementAction;
	JavascriptExecutor js;

	public Widget_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "widget");
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		action = new Actions(driver);
		elementAction = new ActionsUtility(driver, logger);
		js = (JavascriptExecutor) driver;
	}

	public String getWidgetTitleText() {
		System.out.println(jsonData.getWebElement("Widget_Page_title").getText());
		return jsonData.getWebElement("Widget_Page_title").getText();
	}
	
	public void enterLocationAndClickOnSearchIcon(String Location,String MinPrice,String MaxPrice,String Bedrooms,String Bathrooms,String Sqtfeet)
	{
		WebElement searchbox=jsonData.getWebElement("Widget_Search_Box");
		js.executeScript("arguments[0].scrollIntoView();", searchbox);
		searchbox.clear();
		searchbox.sendKeys(Location);	
		selectMinPrice(MinPrice);
		selectMaxPrice(MaxPrice);
		selectBedroom(Bedrooms);
		selectBathroom(Bathrooms);
		selectSqtFeet(Sqtfeet);
		clickOnSearchIcon();
	}
	
	public void clickOnSearchIcon()
	{
		elementAction.clickOnElement(jsonData.getWebElement("Widget_Search_icon"), "Clicked On the SearchIcon");
	}
	
	public void selectMinPrice(String MinPrice)
	{
		WebElement minPrice=jsonData.getWebElement("MinPrice_Filter");
		ActionsUtility.staticWait(1);
		Select s1 = new Select(minPrice);
		s1.selectByValue(MinPrice);
	}
	
	public void selectMaxPrice(String MaxPrice)
	{
		WebElement maxPrice=jsonData.getWebElement("MaxPrice_Filter");
		maxPrice.click();
		ActionsUtility.staticWait(1);
		Select s1 = new Select(maxPrice);
		s1.selectByValue(MaxPrice);
	}
	
	public void selectBedroom(String Bedrooms)
	{
		WebElement Bedroom=jsonData.getWebElement("Beds_Filter");
		Bedroom.click();
		ActionsUtility.staticWait(1);
		Select s1 = new Select(Bedroom);
		s1.selectByValue(Bedrooms);
	}
	public void selectBathroom(String bathrooms)
	{
		WebElement Bathrooms=jsonData.getWebElement("Bathrooms_Filter");
		Bathrooms.click();
		ActionsUtility.staticWait(1);
		Select s1 = new Select(Bathrooms);
		s1.selectByValue(bathrooms);
	}
	public void selectSqtFeet(String sqtfeet)
	{
		WebElement SqtPrice=jsonData.getWebElement("SqFeet_Filter");
		SqtPrice.click();
		ActionsUtility.staticWait(1);
		Select s1 = new Select(SqtPrice);
		s1.selectByValue(sqtfeet);
	}

}
