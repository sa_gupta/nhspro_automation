package com.ex2.nhspro.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.Initializations;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class CommunityDetailPage_actions extends Initializations{
	
	WebDriver driver;
	JsonUtility jsonData;
	ExtentTest logger;
	ActionsUtility elementAction;
	JavascriptExecutor js;
	SRPPage_actions srpPage;
	CommonPage_actions commonPage;

	public CommunityDetailPage_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "CommunityDetailPage");
		js = (JavascriptExecutor) driver;
		elementAction=new ActionsUtility(driver, logger);
		srpPage=new SRPPage_actions(driver,logger);
		commonPage=new CommonPage_actions(driver,logger);
	}
	
	public void clickOnPrintReportButtonOnCommunityDetail() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("PrintReport"),
					"Clicked in Community Detail Print Report Button");
		} else {
			elementAction.clickOnElement(jsonData.getWebElement("PrintReport"),
					"Clicked in Community Detail Print Report Button");
		}
	}
	public void clickOnBackToSearchResultOnCommunityDetail() {
		elementAction.clickOnElement(jsonData.getWebElement("BackToSearchResult"),"Clicked on BackToSearchResult From Community Detail Page");
	}
	
	public void ClickOn_Agent_SaveListing() {
		// TODO Auto-generated method stub
		elementAction.clickOnElement(jsonData.getWebElement("Agent_Main_SaveListing_btn"),"Clicked on Saved Listing from Community Detail page");
	}
	
	public void clickOn_SaveListing() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(jsonData.getWebElement("Main_SaveListing_btn"),"Clicked on Saved Listing from Community Detail page");
		}else {
		elementAction.waitForLoad(driver);
		elementAction.clickOnElement(jsonData.getWebElement("Main_SaveListing_btn"),"Clicked on Saved Listing from Community Detail page");
		elementAction.waitForLoad(driver);
		}
	}
	
	public String getHomeId() {
		String currentUrl = driver.getCurrentUrl();
		String[] split = currentUrl.split("/");
		return split[split.length-1];
	}
	public void clickOnOkButton()
	{
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("OkButton"), "Clicked on Ok Button from Community Detail page");
	}
   public void clickOnConfirmOk()
   {
	   elementAction.clickOnElement(jsonData.getWebElement("confirmOk"), "Clicked on Confirm Ok Button from Community Detail page");
	   }
	public void clickOnBackToSearchResultFromCommunityDetail() {
		elementAction.clickOnElement(jsonData.getWebElement("BackToSearchResult"),"Clicked on BackToSearchResult From Community Detail Page");
	}
	
	public void scrollTo_Home_SavedListing_In_CommunityDetailPage()
	{
		try {
			if (gridHomeCount()>0) {
				js.executeScript("arguments[0].scrollIntoView(true);", jsonData.getWebElement("Homes_SavedListing_Button"));
			}
		}catch(Exception e)
		{
			System.out.println("There are no homes in the grid");
		}
		
		
	}
	public void click_On_Preview_RequestToRegister_Client_Button()
	{
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("Preview_Request_To_Register_Client_Button"), "Clicked on Preview Request To Register Client Button from Community Detail page");
	}
	public void click_On_RequestToRegister_Client_Popup_Button()
	{
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("RequestToRegisterButton_popup"), "Clicked on Preview Request To Register Client pop up Button from Community Detail page");
	}
	
	public void click_On_Preview_Saved_Listing_Button()
	{
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("Preview_SavedListing_Button"), "Clicked on Preview Saved Listing Button from Community Detail page");
	}
	
	public void click_On_Preview_Print_Report_Button()
	{
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("Preview_PrintReport_Button"), "Clicked on Preview Print Button from Community Detail page");
	}
	
	public void click_On_Above_Home_grid_Print_Report_Button()
	{
		ActionsUtility.staticWait(3);
		js.executeScript("arguments[0].scrollIntoView(false);", jsonData.getWebElement("Homes_Print_Report"));
		elementAction.clickOnElement(jsonData.getWebElement("Homes_Print_Report"), "Clicked on Print Button Home Grid from Community Detail page");
	}
	
	public void click_On_Home_Grid_SavedListing_In_CommunityDetailPage()
	{
		ActionsUtility.staticWait(3);
		js.executeScript("arguments[0].scrollIntoView(false);", jsonData.getWebElement("Homes_SavedListing_Button"));
		elementAction.clickOnElement(jsonData.getWebElement("Homes_SavedListing_Button"), "Clicked on Saved Listing Home Grid Button from Community Detail page");
		
	}
	
	
	public int gridHomeCount()
	{
		return driver.findElements(By.xpath("//tbody/tr")).size();
	}

	public void Save_Listing(int n, String listingType) {
	    srpPage.clickOnCommunity(n);
	    String s = listingDetail();
		boolean isBilled = (listingType=="notBilled") ? !checkBilledStatus(s) : checkBilledStatus(s);
		boolean isLimitedAvailability = checkLimitedAvailabilityStatus(s);
		if(isBilled && isLimitedAvailability) {
			ClickOn_Agent_SaveListing();
		}else
		{
            driver.navigate().back();
            commonPage.clickOnCommunities_tab();
            Save_Listing(++n, listingType);
		}
		
	}
	
	/*This function generates a string which contains info about 
	listings billed and Limited Availability Status*/
	public String listingDetail() {
		ActionsUtility.staticWait(5);
		WebElement details = driver.findElement(By.xpath("//body/script[11]"));
		String s = (String)((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;", details);
		System.out.println(s);
		return s;
	}
	
	public boolean checkBilledStatus(String s) {
		if(s.contains("\"IsBilled\":true"))
			return true;
		else
			return false;
	}
	
	public boolean checkLimitedAvailabilityStatus(String s) {
		if(s.contains("\"IsLimitedAvailability\":false"))
			return true;
		else
			return false;
	}
}

