package com.ex2.nhspro.actions;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class WebinarPage_actions {
	
	JsonUtility jsonData;
	ExtentTest logger;
	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	ActionsUtility elementAction;
	JavascriptExecutor js;

	public WebinarPage_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "webinar");
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		action = new Actions(driver);
		elementAction = new ActionsUtility(driver, logger);
		js = (JavascriptExecutor) driver;
	}
	
	public String getWebinarTitleText() {
		String webinarTitleText = jsonData.getWebElement("Webinar_Page", "Webinar_Page_Title").getText();
		System.out.println(webinarTitleText);
		return webinarTitleText;
	}
	
	public String getDetailPageWebinarTitleText() {
		String detailPageWebinarTitleText = jsonData.getWebElement("Webinar_Page", "Webinar_Detail_Page_Title").getText();
		System.out.println(detailPageWebinarTitleText);
		return detailPageWebinarTitleText;
	}
	
	public String getRegisterationFormTitleText() {
		//ActionsUtility.staticWait(5);
		String registerationFormTitleText = jsonData.getWebElement("Webinar_Page", "Webinar_Registration_Form_Title").getText();
		System.out.println(registerationFormTitleText);
		return registerationFormTitleText;
	}
	
	public WebElement getFirstWebinarImage() {
		WebElement i1 = jsonData.getWebElement("Webinar_Page", "First_Webinar_Image");
		return i1;
	}
	
	public WebElement getRegisterNowButton() {
		WebElement registerNowButton = jsonData.getWebElement("Webinar_Page", "Register_Now_Button");
		return registerNowButton;
	}
	
	public WebElement getDetailWebinarPageRegisterNowButton() {
		WebElement registerNowDetailPageButton = jsonData.getWebElement("Webinar_Page", "Detail_Page_Register_Now_Button");
		return registerNowDetailPageButton;
	}
	
	public WebElement getTestingWebinar() {
		WebElement testingWebinar = jsonData.getWebElement("Webinar_Page", "Testing_Webinar");
		return testingWebinar;
	}
	
	public void getAllWebinar() {
		try {
		driver.findElement(By.xpath("//button[contains(text(),'View All')]")).click();
		System.out.println("Clicked on the View All button");
		} 
		catch(Exception e) {
			System.out.println("There are less than 7 webinars so view all button is not present");
		}
	}

	public WebElement getRegisterNowButtonWebinarDetail() {
		WebElement registerNowButton = jsonData.getWebElement("Webinar_Page", "Register_Now_Button_Detail_Page");
		return registerNowButton;
	}
	
	public String getRegistrationFormFirstName() {
		String firstName = jsonData.getWebElement("Webinar_Page", "First_Name").getAttribute("value");
		return firstName;
	}
	
	public String getRegistrationFormLastName() {
		String LastName = jsonData.getWebElement("Webinar_Page", "Last_Name").getAttribute("value");
		return LastName;
	}
	
	public String getRegistrationFormEMail() {
		String EMail = jsonData.getWebElement("Webinar_Page", "E_Mail").getAttribute("value");
		return EMail;
	}

	public WebElement registerationFormCheckbox() {
		WebElement checkBox = jsonData.getWebElement("Webinar_Page","Registeration_Form_Checkbox");
		return checkBox;
	}
	
	public String getSuccessMessageText() {
		String successMessage = jsonData.getWebElement("Webinar_Page", "Success_Message").getText();
		return successMessage;
	}
}
