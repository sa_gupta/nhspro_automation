package com.ex2.nhspro.actions;

import static org.testng.Assert.assertTrue;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SaveListing_actions {

	JsonUtility jsonData;
	ExtentTest logger;
	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	ActionsUtility elementAction;
	JavascriptExecutor js;

	public SaveListing_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "SaveListing");
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		action = new Actions(driver);
		elementAction = new ActionsUtility(driver, logger);
		js = (JavascriptExecutor) driver;
	}

	public List<String> getIdOfHomes(int noOfHome) {
//		elementAction.retryTillExist(jsonData.getWebElement("SelectAll_checkbox"));
		List<String> idOfHome = new ArrayList<String>();
		List<WebElement> homeElements;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			homeElements = driver.findElements(By.xpath("//div[@id='pro-mobile-list-view']/a"));
		} else if (driver.getCurrentUrl().contains("showingnew.com")) {
			homeElements = driver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
		} else {
			homeElements = driver.findElements(By.xpath("//table/tbody/tr/td[5]/a")); // TODO: change for showingnew
																						// data
		}
		for (int i = 0; i < noOfHome; i++) {
			String allUrls=elementAction.getAttributeValue(homeElements.get(i), "href");
//			String allUrls = homeElements.get(i).getAttribute("href");
			String[] split = allUrls.split("/");
			idOfHome.add(split[split.length - 1]);
		}
		return idOfHome;
	}
	public List<String> getIdOfHomesFromSavedListing(int noOfHome) {
//		elementAction.retryTillExist(jsonData.getWebElement("SelectAll_checkbox"));
		List<String> idOfHome = new ArrayList<String>();
		List<WebElement> homeElements;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			homeElements = driver.findElements(By.xpath("//div[@id='pro-mobile-list-view']/a"));
		} else if (driver.getCurrentUrl().contains("showingnew.com")) {
			homeElements = driver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
		} else {
			homeElements = driver.findElements(By.xpath("//table/tbody/tr/td[5]/a")); // TODO: change for showingnew
																					// data
		}
		for (int i = 0; i < noOfHome; i++) {
			String allUrls=elementAction.getAttributeValue(homeElements.get(i), "href");
//			String allUrls = homeElements.get(i).getAttribute("href");
			 String[] segments = allUrls.split("/");
		        String number = segments[segments.length - 2];
		        System.out.println(number);
		        String regex = number.split("clientId=")[0];
			    String result = regex.replace("?", "");
			    System.out.println(result);
			    idOfHome.add(result);
		        System.out.println(idOfHome);
		//	String[] split = allUrls.split("/");
		//	idOfHome.add(split[split.length - 2]);
		}
		return idOfHome;
	}
	
	
	public String getHomeIdFromSavedListingPage(List<String> allUrls) {
	    String url = allUrls.get(0);
	    String regex = url.split("clientId=")[0];
	    String result = regex.replace("?", "");
	    System.out.println(result);
	    return result;
	}

	public int getCountOfHome() {
//		elementAction.retryTillExist(jsonData.getWebElement("SelectAll_checkbox"));
		List<WebElement> homeelements;
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			homeelements = driver.findElements(By.xpath("//table/tbody/tr/td/label[@class='k-checkbox-label']"));
		} else {
			homeelements = driver.findElements(By.xpath("//div[@id='pro-mobile-list-view']/a//div/label[contains(@data-id,'label')]"));
		}
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
		return homeelements.size();
	}

//	public void clickOnSelectAll_checkbox() {
//		elementAction.clickOnElement(jsonData.getWebElement("SelectAll_checkbox"), "Click on select all checkbox");
//	}

	public void clickOnDelete_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Delete_btn"), "Clicked on Delete button");
	}

//Mobile
	public boolean clickOnClientOnSaveListingPage(String clientName) {
		boolean flag = false;
		List<WebElement> clientElements;
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			clientElements = driver.findElements(By.xpath("//span[@data-id='Open-Client-SavedListing-Name']"));
		else
			clientElements = driver.findElements(By.xpath("//div[@id='saveListingNav']//a"));
		for (WebElement singleClient : clientElements) {
			String eachClient =elementAction.getTextData(singleClient);
			if (eachClient.startsWith(clientName)) {
				elementAction.clickOnElement(singleClient, "Clicked on " + clientName);
				flag = true;
				ActionsUtility.staticWait(5);
				break;
			}
		}
		}catch(Exception e) {
			System.out.println("Client is not showing in hover");
		}
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
		return flag;
	}

	public void updateclickOnClientOnSaveListingPage(String clientName) {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			openClientListingOnMobile(clientName);
		}

	}

	private void openClientListingOnMobile(String clientName) {
		List<WebElement> clientElements;
		clientElements = driver.findElements(By.xpath("//span[@id='Open-Client-SavedListing-Name']"));
		for (WebElement singleClient : clientElements) {
			String eachClient = singleClient.getText();
			System.out.println(eachClient);
			if (eachClient.startsWith(clientName)) {
				elementAction.clickOnElement(singleClient, "Clicked on " + clientName);
				ActionsUtility.staticWait(5);
				break;
			}
		}

	}

	public void clickOnClientOnSaveSearchPage(String clientName) {

		List<WebElement> clientElements = driver.findElements(By.xpath("//div[@id='table-saved-searches']//a//div/span"));
		for (WebElement singleClient : clientElements) {
			String eachClient = singleClient.getText();
			System.out.println(eachClient);
			if (eachClient.startsWith(clientName)) {
				elementAction.clickOnElement(singleClient, "Clicked on " + clientName);
				ActionsUtility.staticWait(5);
				break;
			}
		}
	}
	
	public void clickOnDeleteOnConfirmationPopp() {
		elementAction.clickOnElement(jsonData.getWebElement("Delete_confirmation_popup"), "Click On Delete from confirmation popup");
	}

	public void clearSaveListing() {
		ActionsUtility.staticWait(3);
		System.out.println("Total homes are"+getCountOfHome());
		if (getCountOfHome() > 0) {
//			clickOnSelectAll_checkbox();
			for(int i=1;i<=getCountOfHome();i++) {
				selectParticluarHome(i);
			}
				clickOnDelete_btn();
				clickOnDeleteOnConfirmationPopp();		
			
		}
	}

	public void clickOnCommunityTab_saveListing() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			assertTrue(jsonData.getWebElement("Communities_tab").isDisplayed());
			elementAction.clickOnElement(jsonData.getWebElement("Communities_tab"), "Click on Communities tab");

		} else {
			elementAction.clickOnElement(jsonData.getWebElement("Communities_tab"),
					"Clicked on community tab of save listing");
		}
	}
	
	public void clickOnBack_link() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
		driver.navigate().back();
		}
		else {
		elementAction.clickOnElement(jsonData.getWebElement("Back_link"), "Click on Back link");
		}
	}
	
	public void selectParticluarHome(int homeNo) {
	//	elementAction.retryTillExist(jsonData.getWebElement("SelectAll_checkbox"));
	//	wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("SelectAll_checkbox")));
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			WebElement homeCheckBox = driver.findElement(By.xpath("(//div[@id='pro-mobile-list-view']/a//div/label[contains(@data-id,'label')])["+homeNo+"]"));
			elementAction.clickOnElement(homeCheckBox, "Selected home no:- "+homeNo);
		}
		else
		{
		WebElement homeCheckBox = driver.findElement(By.xpath("(//table/tbody/tr//label[@class='k-checkbox-label'])["+homeNo+"]"));
		elementAction.clickOnElement(homeCheckBox, "Selected home no:- "+homeNo);
		}
	}
	
	public void clickOnSend_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Send_btn"), "Clicked on Send button");
	}
	
	public void clickOnNoForEditEmail_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Send_popup", "No_btn"), "Clicked on No button for Edit Email popup");
	}
	public void clickOnYesForEditEmail_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Send_popup", "Yes_btn"), "Clicked on No button for Edit Email popup");
	}
	
	public void clickOnSendfromPopup() {
		elementAction.clickOnElement(jsonData.getWebElement("Send_popup", "Send_btn"), "Click on Send from popup");
	}
	
	public void clickOnOkForDone_popup() {
		elementAction.clickOnElement(jsonData.getWebElement("Send_popup","OK_Done_btn"), "Clicked on Ok button for Done popup");
	}
	
	public String getSentInfo(int homeNo) {
		WebElement sentStatus = driver.findElement(By.xpath("//table/tbody/tr["+homeNo+"]/td[2]"));
		return sentStatus.getText();
	}
	
	public void performSendOperation(String Status) {
		if(Status.equalsIgnoreCase("Yes"))
			clickOnSendfromPopup();
		clickOnNoForEditEmail_btn();
		clickOnOkForDone_popup();
	}

	public void openPreviewBasedOnSendStatus(String Status) {
		if(Status.equalsIgnoreCase("Yes"))
			clickOnSendfromPopup();
		clickOnYesForEditEmail_btn();
	}
	
	public void clickOnPrintReport_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("PrintReport_btn"), "Clicked on Print Report button");
	}
	
	public void hoverToHome(int homeNo) {
		WebElement home = driver.findElement(By.xpath("//table/tbody/tr["+homeNo+"]/td[5]/a"));
		wait.until(ExpectedConditions.elementToBeClickable(home));
		action.moveToElement(home).build().perform();
	}
	
	public void hoverToCommunity(int homeNo) {
		WebElement home = driver.findElement(By.xpath("//table/tbody/tr["+homeNo+"]/td[5]/a"));
		wait.until(ExpectedConditions.elementToBeClickable(home));
		action.moveToElement(home).build().perform();
	}
	
	public void clickOnSendAgain()
	{
		if(driver.findElements(By.xpath("//button[@class='swal2-confirm styled']")).size()>0)
		{
			elementAction.clickOnElement(jsonData.getWebElement("SendAgain"), "Clicked on Send Again button");
		}
	}
	
	
	 //----------------------------- Preview -------------------------------------------------------------
    public void clickOn_RequestToRegisterClient_btn_Preview() {
    	js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("Preview", "RequestToRegister_button"));
   	 elementAction.clickOnElement(jsonData.getWebElement("Preview", "RequestToRegister_button"), "Clicked on Request to Register Client");
    }
    
    public void clickOn_RequestToRegister_from_popup() {
   	 elementAction.clickOnElement(jsonData.getWebElement("RequestToRegister_poup"), "Clicked on Request to Register Client button from popup");
    }
    
    public void clickOn_Send_btn_Preview() {
   	 elementAction.clickOnElement(jsonData.getWebElement("Preview", "Send_btn"), "Clicked on Send from Preview");
    }
    
    public void clickOn_PrintReport_btn_Preview() {
   	 elementAction.clickOnElement(jsonData.getWebElement("Preview", "PrintReport"), "Clicked on Print Report from Preview");
    }
	
    public String getCssValueOfSendButton()
    {
    	String value=jsonData.getWebElement("Send_btn").getCssValue("background-color");
    	System.out.println(value);
    	return value;
    }
    
    public String getCssValueOfPrintReportButton()
    {
    	String value=jsonData.getWebElement("PrintReport_btn").getCssValue("background-color");
    	System.out.println(value);
    	return value;
    }
    
    public String getCssValueOfDeleteButton()
    {
    	String value=jsonData.getWebElement("Delete_btn").getCssValue("background-color");
    	System.out.println(value);
    	return value;
    }

}
