package com.ex2.nhspro.actions;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.aventstack.extentreports.ExtentTest;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SRPPhotoview_actions {

	JsonUtility jsonData;
	ExtentTest logger;
	WebDriver driver;
	Actions action;
	ActionsUtility elementAction;
	JavascriptExecutor js;

	public SRPPhotoview_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "SRPPage");
		action = new Actions(driver);
		elementAction = new ActionsUtility(driver, logger);
		js = (JavascriptExecutor) driver;
	}
	
// ============================================== Main ============================================================
	
	public boolean isMainButtonsExistOnPage() {
		boolean buttonExistence;
		buttonExistence=elementAction.isElementExistOnThePage(jsonData.getWebElement("PhotoView", "SaveListing_main"), "----------- Checking for Save/Email Listing button -------------");
		buttonExistence=elementAction.isElementExistOnThePage(jsonData.getWebElement("PhotoView","PrintReport_main"), "------------ Checking for Print Report button -------------------");
		return buttonExistence;
	}
	
	
	
	public void slectHomeInPhotoView(int noOfHomes) {
		List<WebElement> homeElements = driver
				.findElements(By.xpath("//p[contains(@class,'item-title')]/label[@class='k-checkbox-label']"));
		for (int i = 0; i < noOfHomes; i++) {
			elementAction.clickOnElement(homeElements.get(i), "Select " + i + " home");
		}
	}

	public String getIdOfHome_photoView(int homeNo) {
		WebElement homeElement;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			homeElement = driver.findElement(By.xpath("//div[@id='pro-mobile-listview-results-items']/a"));
		else
			homeElement = driver.findElement(By.xpath("((//div[@class='results-photo-item'])[" + homeNo
					+ "]//p[contains(@class,'item')]//span/a)[1]"));
		elementAction.waitforElementClickable(homeElement);
		String allUrls = homeElement.getAttribute("href");
		String[] split = allUrls.split("/");
		String idOfHome = split[split.length - 1];
		System.out.println(split[split.length - 1]);
		return idOfHome;
	}

	public void clickOnSaveListingforAnyHome_photoView(int homeNo) {
		WebElement saveListing = driver.findElement(
				By.xpath("(//div[@class='results-photo-item'])[" + homeNo + "]//a[contains(text(),'Listing')]"));
		elementAction.clickOnElement(saveListing, "Clicked on Save Listing home No " + homeNo);
	}

	public void clickOn_PrintReport_HomeTab_PhotoView(int homeNo) {
		WebElement saveListing = driver.findElement(
				By.xpath("(//div[@class='results-photo-item'])[" + homeNo + "]//a[contains(text(),'Print Report')]"));
		elementAction.clickOnElement(saveListing, "Clicked on Print Reporthome No " + homeNo);
	}

	public void clickOn_RequestToRegister_HomeTab_PhotoView(int homeNo) {
		WebElement saveListing = driver.findElement(
				By.xpath("(//div[@class='results-photo-item'])[" + homeNo + "]//a[contains(text(),'Request')]"));
		elementAction.clickOnElement(saveListing, "Clicked on Request to Register Client of home No " + homeNo);
	}
	
	public List<String> getAllColumnsOnPhotoView(){
		return driver.findElements(By.xpath("//legend[text()='Sort by:']/following-sibling::a[not(@style)]")).stream().map(s->s.getText()).collect(Collectors.toList());
	}
}
