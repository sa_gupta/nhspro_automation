package com.ex2.nhspro.actions;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SRPFilter_actions {

	JsonUtility jsonData;
	ExtentTest logger;
	WebDriver driver;
	Actions action;
	ActionsUtility elementAction;

	public SRPFilter_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "SRPPage");
		action = new Actions(driver);
		elementAction = new ActionsUtility(driver, logger);
	}

//----------------------------------------- Location Filter -----------------------------------
	public void clickOnLocationField() {
		elementAction.clickOnElement(jsonData.getWebElement("Location_field"), "Click on Location field");
	}

	public void clickNearByCities() {
		elementAction.clickOnElement(jsonData.getWebElement("LocationFilter", "Nearby_Cities"),
				"Click on Nearby Cities option");
	}

	public void selectCity_from_NearBy(String cityName) {
		WebElement NearByCity = driver
				.findElement(By.xpath("//div[@id='CitiesRadioFacets']//li//span[contains(text(),'" + cityName + "')]"));
		elementAction.clickOnElement(NearByCity, "Clicked on " + cityName);
	}

	public void clickZipcode() {
		elementAction.clickOnElement(jsonData.getWebElement("LocationFilter", "Zip_Code"), "Click on Zip Code option");
	}

	public void selectZipCode_from_NearBy(String ZipCode) {
		WebElement NearByCity = driver.findElement(
				By.xpath("//div[@id='PostalCodeRadioFacets']//li//span[contains(text(),'" + ZipCode + "')]"));
		elementAction.clickOnElement(NearByCity, "Clicked on " + ZipCode);
	}

	public void clickSchoolDistricts() {
		elementAction.clickOnElement(jsonData.getWebElement("LocationFilter", "School_Districts"),
				"Click on School Districts option");
	}

	public void selectSchool_from_SchoolDistricts(String School) {
		WebElement SchoolValue = driver.findElement(
				By.xpath("//div[@id='SchoolDistrictsRadioFacets']//li//span[contains(text(),'" + School + "')]"));
		elementAction.clickOnElement(SchoolValue, "Clicked on " + SchoolValue);
	}

	public String getIdOfTheDistrict(String SchoolDistrictName) {
		WebElement schoolDistrict = driver
				.findElement(By.xpath("//div[@id='SchoolDistrictsRadioFacets']//li/label/span[text()='"
						+ SchoolDistrictName + "']/parent::label/preceding-sibling::input"));
		return schoolDistrict.getAttribute("data-key");
	}

	public void clickCounties() {
		elementAction.clickOnElement(jsonData.getWebElement("LocationFilter", "Counties"), "Click on Counties option");
	}

	public void selectCounty_from_Counties(String CountyValue) {
		WebElement County = driver.findElement(
				By.xpath("//div[@id='CountiesRadioFacets']//li//span[contains(text(),'" + CountyValue + "')]"));
		elementAction.clickOnElement(County, "Clicked on " + CountyValue);
	}

	public void clickOnUpdate_btn_forlocation() {
		elementAction.clickOnElement(jsonData.getWebElement("LocationFilter", "Update_btn"), "Click on Update button");
	}

//------------------------------------------ Price Filter --------------------------------------
	public void clickOnPrice_filter() {
		elementAction.clickOnElement(jsonData.getWebElement("PriceFilter", "Price_title"), "Open Price filter");
	}

	public void clickOnBedsFilter() {
		elementAction.clickOnElement(jsonData.getWebElement("BedsandBathFilter", "Bath_BedLabel"), "Open Beds filter");
	}

	public String getPriceValue() {
		return elementAction.getTextData(jsonData.getWebElement("PriceFilter", "Price_selected"));

	}

	/*
	 * public void enterMinandMaxPrice(String min, String max) {
	 * elementAction.normalTypeOnElement(jsonData.getWebElement("PriceFilter",
	 * "MinPrice"), min, "Entered " + min + " value");
	 * elementAction.normalTypeOnElement(jsonData.getWebElement("PriceFilter",
	 * "MaxPrice"), max, "Entered " + max + " value");
	 * 
	 * }
	 */

	/*
	 * public void clickOnUpdatePrice_btn() {
	 * elementAction.clickOnElement(jsonData.getWebElement("PriceFilter",
	 * "UpdatePrice_btn"), "Click on Update button"); }
	 */

	// -------------------------------
	// Bedroom-------------------------------------------------

	public String getSelectedBedrooms() {
		if (jsonData.getWebElement("BedsandBathFilter", "BedRoom_value").getCssValue("display")
				.equalsIgnoreCase("none")) {
			return String.valueOf(0);
		} else
			return elementAction.getTextData(jsonData.getWebElement("BedsandBathFilter", "BedRoom_value"));
	}

	public String getExactSelectedBedrooms() {
		String Beds = getSelectedBedrooms();
		String[] split = Beds.split(" ");
		return split[0];
	}



	public void enterMinandMaxPrice(String min, String max) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("PriceFilter", "MinPrice"), min,
				"Entered " + min + " value");
		elementAction.normalTypeOnElement(jsonData.getWebElement("PriceFilter", "MaxPrice"), max,
				"Entered " + max + " value");

	}

	public void clickOnUpdatePrice_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("PriceFilter", "UpdatePrice_btn"),
				"Click on Update button");
	}

	public void verifyMin_Max_PriceInMobile(String minprice, String maxprice) {
		elementAction.clickOnElement(jsonData.getWebElement("Filter_link"), "Clicked on the Filter On");
		String minPrice = elementAction.getTextData(jsonData.getWebElement("PriceFilter", "Price_min"));
		String maxPrice = elementAction.getTextData(jsonData.getWebElement("PriceFilter", "Price_max"));
		System.out.println("Min price on SRP " + minPrice + " Max price on SRP " + maxPrice);
		Assert.assertTrue(minPrice.contains(minprice), "Min Price does not matched");
		Assert.assertTrue(maxPrice.contains(maxprice), "Max Price does not matched");
	}

//------------------------------- Bedroom  -------------------------------------------------	 

	/*
	 * public String getSelectedBedrooms() { return
	 * elementAction.getTextData(jsonData.getWebElement("BedsandBathFilter",
	 * "BedRoom_value"));
	 * 
	 * }
	 * 
	 * public String getExactSelectedBedrooms() { String Beds =
	 * getSelectedBedrooms(); String[] split = Beds.split(" "); return split[0]; }
	 */

	public void verifyBedroomsAndColorInMobile(String SelectedHome) {
		elementAction.clickOnElement(jsonData.getWebElement("Filter_link"), "Clicked on the Filter On");
		WebElement element = driver
				.findElement(By.xpath("//ul[@id='bedrooms-ammount-facet']/li/label[text()='" + SelectedHome + "+']"));
		String color = element.getCssValue("color");
		String bgColor = element.getCssValue("background-color");
		System.out.println("Color is " + color + " And Bg Color is " + bgColor);
		Assert.assertTrue(color.contains("rgba(255, 255, 255, 1)"), "Color does not matched");
		Assert.assertTrue(bgColor.contains("rgba(0, 29, 49, 1)"), "Background Color does not matched");
	}

	public void verifyBathroomsAndColorInMobile(String SelectedHome) {
		elementAction.clickOnElement(jsonData.getWebElement("Filter_link"), "Clicked on the Filter On");
		WebElement element = driver
				.findElement(By.xpath("//ul[@id='bathrooms-ammount-facet']/li/label[text()='" + SelectedHome + "+']"));
		String color = element.getCssValue("color");
		String bgColor = element.getCssValue("background-color");
		System.out.println("Color is " + color + " And Bg Color is " + bgColor);
		Assert.assertTrue(color.contains("rgba(255, 255, 255, 1)"), "Color does not matched");
		Assert.assertTrue(bgColor.contains("rgba(0, 29, 49, 1)"), "Background Color does not matched");
	}

	
	  public void selectBedrooms(String bedroom) { 
		  List<WebElement>bedroomsElement; 
		  if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
	  bedroomsElement = driver.findElements(By.xpath("//legend[text()='Bedrooms']/following-sibling::div/label")); 
		  else
	  bedroomsElement =driver.findElements(By.xpath("//ul[@id='bedrooms-ammount-facet']//label"));
	  for (WebElement webElement : bedroomsElement)
	  { 
		  String bedroomText =webElement.getText(); 
		  if (bedroomText.contains(bedroom)) {
	  elementAction.clickOnElement(webElement, "Selected " + bedroomText +" in bedroom");
	  break; 
	  } 
		  }
	  
	  }
	 

	// ------------------------------- Bathrooms
	// -------------------------------------------------

//	public String getSelectedBathrooms() {
//		if (jsonData.getWebElement("BedsandBathFilter", "BathRoom_value").getCssValue("display")
//				.equalsIgnoreCase("none")) {
//			return String.valueOf(0);
//		}else
//		return jsonData.getWebElement("BedsandBathFilter", "BathRoom_value").getText();
//	}
//
//	public void selectBathrooms(String Bathroom) {
//		List<WebElement> bedroomsElement;
//		if (prop.getPropertyValue("mode").equalsIgnoreCase("desktop"))
//			bedroomsElement = driver.findElements(By.xpath("//legend[text()='Bathrooms']/following-sibling::label"));
//		else
//			bedroomsElement = driver.findElements(By.xpath("//ul[@id='bathrooms-ammount-facet']//label"));
//		for (WebElement webElement : bedroomsElement) {
//=======
//	 }

	// ------------------------------- Bathrooms
	// End-------------------------------------------------

	public String getSelectedBathrooms() {
		return jsonData.getWebElement("BedsandBathFilter", "BathRoom_value").getText();
	}

	public void selectBathrooms(String Bathroom) {
		List<WebElement> bedroomsElement;
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			bedroomsElement = driver.findElements(By.xpath("//legend[text()='Bathrooms']/following-sibling::div/label"));
		else
			bedroomsElement = driver.findElements(By.xpath("//ul[@id='bathrooms-ammount-facet']//label"));
		for (WebElement webElement : bedroomsElement) {
			String bathroomText = webElement.getText();
			if (bathroomText.contains(Bathroom)) {
				elementAction.clickOnElement(webElement, "Selected " + bathroomText + " in bathroom");
				break;
			}
		}
	}



	public void clickOnUpdateBedBath_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("BedsandBathFilter", "Apply_btn"),
				"Click on ApplyFilter button");
		ActionsUtility.staticWait(3);
	}

	// ******************************** Square Feet Filter
	// ***************************************
	public String getSquareFeet() {
		return jsonData.getWebElement("SquareFeetFilter", "SquareFeet_value").getText();
	}

	public String getExactSquareFeet() {
		String SqtText = getSquareFeet();
		String replaceText = SqtText.replace(",", "");
		String[] split = replaceText.split(" ");
		System.out.println(split[0]);
		return split[0];
	}

	public void verifySqaureFeetInMobile(String sqtFromHome) {
		elementAction.clickOnElement(jsonData.getWebElement("Filter_link"), "Clicked on the Filter On");
		String text = elementAction.getTextData(jsonData.getWebElement("SquareFeetFilter", "SquareFeetTextInMobile"));
		String sqtvalueOnSRP = text.replace(",", "");
		System.out.println(sqtvalueOnSRP);
		Assert.assertTrue(sqtFromHome.contains(sqtvalueOnSRP), "SquareFeet does not matched");
	}

//********************************* End Square feet Method ************************************

	public void clickOnResetFilter() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
		elementAction.clickOnElement(jsonData.getWebElement("ResetFilters"), "Clicked on Reset filter");
		ActionsUtility.staticWait(5);
		elementAction.retryTillExist(jsonData.getWebElement("BreadCrumb","Bredcrumb_home_count"));
//		elementAction.waitForTextExistence(jsonData.getWebElement("BreadCrumb","Bredcrumb_home_count"),jsonData.getWebElement("BreadCrumb","Bredcrumb_home_count").getText());
		elementAction.retryTillExist(jsonData.getWebElement("First_Checkbox"));
		}
		else {
			elementAction.clickOnElement(jsonData.getWebElement("ResetFilters"), "Clicked on Reset filter");
			ActionsUtility.staticWait(5);
		}
	}

//Mobile filter
	public void clickOnApply_btn() {
		elementAction.clickOnElement(jsonData.getWebElement("Apply_btn"), "Clicked on Apply button");

	}
	
	public String getSearchLocationText() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		elementAction.waitForVisibility(jsonData.getWebElement("Location_field"));
		String getEnteredText = js.executeScript("return document.getElementById('LocationSearchTextBox').value")
				.toString();
		logger.log(Status.INFO, "Showing " + getEnteredText + " in Search field");
		System.out.println("Showing " + getEnteredText + " in Search field");
		return getEnteredText;
	}
	public HashMap<String,Object> getFilterData(){
		List<String> allFiltersDefaultValue=driver.findElements(By.xpath("//div[@class='facetTab']/p[1]")).stream().map(s->s.getText()).collect(Collectors.toList());
		HashMap<String,Object> hashMap=new HashMap<>();
		hashMap.put("filtersValue", allFiltersDefaultValue.toString());
		hashMap.put("locationData", getSearchLocationText());
		hashMap.put("SaveButtonExistence", elementAction.isElementExistOnThePage(jsonData.getWebElement("SaveSearch","Save_Search_button"), "Checking for existence of Save button on SRP")+"");
		hashMap.put("ShareSearchlinkexistence", elementAction.isElementExistOnThePage(jsonData.getWebElement("ShareSearch"), "Checking for existence of Share Search link on SRP")+"");
		hashMap.put("RessetFilterlinkExistence", elementAction.isElementExistOnThePage(jsonData.getWebElement("ResetFilters"), "Checking for existence of Reset filter link on SRP")+"");
		return hashMap;
	}
	
	

	 public HashMap<String,String> getBreadCrumbData(String SearchedData){
		 HashMap<String,String> hashMap=new HashMap<>();
		 hashMap.put("ResultLabel", elementAction.getTextData(jsonData.getWebElement("BreadCrumb", "ResultLabel")));
		 hashMap.put("State", elementAction.getTextData(jsonData.getWebElement("BreadCrumb","State")));
		 if(SearchedData.contains("Area"))
			 hashMap.put("Area", elementAction.getTextData(jsonData.getWebElement("BreadCrumb","Location_Area")));
		hashMap.put("CityorZip", elementAction.getTextData(jsonData.getWebElement("BreadCrumb","City")));
		 hashMap.put("Count", elementAction.getTextData(jsonData.getWebElement("BreadCrumb","Bredcrumb_home_count")));
		 return hashMap;
	 }

}
