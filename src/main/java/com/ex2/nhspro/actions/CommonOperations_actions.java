package com.ex2.nhspro.actions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.BrokenLinkUtility;
import com.ex2.nhspro.utility.DatabaseConnectivityUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class CommonOperations_actions  {

	JsonUtility jsonData;
	ActionsUtility elementAction;
	WebDriver driver;
	
	protected  CommonPage_actions commonPage; //0
	protected  HomePage_actions homePage; //1
	protected  SRPPage_actions srpPage; //2
	protected  SavedSearches_actions savedSearchesPage; //3
	protected  HomeDetailPage_actions homeDetailPage; //4
	protected  CommunityDetailPage_actions communityDetailPage; //5
	protected  SaveListing_actions saveListingPage; //6
	protected BrokenLinkUtility brokenLink;   //7
	protected  LocationHandlerPage_actions locationHandler;  //8
	protected DatabaseConnectivityUtility database;
	protected  MyClients_actions myclient;
	protected  MyAccountPage_actions myaccount;
	protected  MyConsumerPortal_actions myconsumerportal;
	protected  ManageMyClientActivityPage_actions managemyclientactivity;
	protected  SRPFilter_actions filter;
	protected String ClientNameforSaveListing;
	

	

	public CommonOperations_actions(WebDriver driver,ExtentTest logger,List<Object> objectElements) {
		this.commonPage=(CommonPage_actions)objectElements.get(0);
		this.homePage=(HomePage_actions)objectElements.get(1);
		this.srpPage=(SRPPage_actions)objectElements.get(2);
		this.savedSearchesPage=(SavedSearches_actions)objectElements.get(3);
		this.homeDetailPage=(HomeDetailPage_actions)objectElements.get(4);
		this.communityDetailPage=(CommunityDetailPage_actions)objectElements.get(5);
		this.saveListingPage=(SaveListing_actions)objectElements.get(6);
		this.brokenLink = (BrokenLinkUtility)objectElements.get(7);
		this.locationHandler=(LocationHandlerPage_actions)objectElements.get(8);
		this.myclient=(MyClients_actions) objectElements.get(9);
		this.myaccount=(MyAccountPage_actions) objectElements.get(10);
		this.myconsumerportal=(MyConsumerPortal_actions) objectElements.get(11);
		this.managemyclientactivity=(ManageMyClientActivityPage_actions) objectElements.get(12);
        this.filter=(SRPFilter_actions)objectElements.get(13);
        this.driver=driver;
		elementAction = new ActionsUtility(driver, logger);
//		System.out.println(elementAction);
		jsonData=new JsonUtility(driver, "CommonOperations");
	}

	public void navigateToSaveListingData(String clientName) {
		commonPage.hoverToSaveListing();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			ActionsUtility.staticWait(5);
			saveListingPage.clickOnClientOnSaveListingPage(clientName);
		} else {
			boolean isClientExist = saveListingPage.clickOnClientOnSaveListingPage(clientName);
			if (isClientExist == false) {
				commonPage.clickOnMyClientsLink();
				myclient.clickOnSaveListingFromClient(clientName);
			}
		}
	}
	
	public void clearSaveListingHomeTabData(String clientName) {
		navigateToSaveListingData(clientName);
		commonPage.clickOnHomes_tab();
		saveListingPage.clearSaveListing();
	}
	public void clearSaveListingCommunityTabData(String clientName) {
		navigateToSaveListingData(clientName);
		commonPage.clickOnCommunities_tab();
		saveListingPage.clearSaveListing();
	}
	
	public void clearCompleteSaveListingData(String clientName) {
		navigateToSaveListingData(clientName);
		commonPage.clickOnHomes_tab();
		saveListingPage.clearSaveListing();
		commonPage.clickOnCommunities_tab();
		saveListingPage.clearSaveListing();
	}

//Comment popup
	public boolean isCommentPopupAvailable() {
		return elementAction.isElementExistOnThePage(jsonData.getWebElement("Commont_title"),"Comment popup exist on the page");
	}
	
	public String getLastComment()
	{
		List<WebElement> elements= driver.findElements(By.xpath("//div[@class='n-scrolling-msg']"));
		int lastMessage=elements.size();
		String lastComment=driver.findElement(By.xpath("(//div[@class='n-scrolling-msg'])["+lastMessage+"]")).getText();
		return lastComment;
	}

	public void clickOnCommentIconFromSaveListing(int homeNo) {
		WebElement home = null;
		String CurrentUrl = driver.getCurrentUrl();
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
			home = driver.findElement(By.xpath("(//span[contains(@class,'icon-comment')])[" + homeNo + "]"));
		else {
			if (CurrentUrl.contains("newhomesource"))
				home = driver.findElement(By.xpath("//table/tbody/tr[" + homeNo + "]/td[13]"));
			else if (CurrentUrl.contains("showingnew"))
				home = driver.findElement(By.xpath("//table/tbody/tr[" + homeNo + "]/td[11]"));
		}
		elementAction.clickOnElement(home, "Clicked on " + homeNo + " home");

	}

	public void enterOnCommentBox(String TypeCommentBox) {
		
		elementAction.normalTypeOnElement(jsonData.getWebElement("Notes_textarea"), TypeCommentBox,TypeCommentBox + " typed successfully");
	}

	public void clickOnAddButton() {
		elementAction.clickOnElement(jsonData.getWebElement("Add_btn"), "Click on Add button");
	}

	public String getCommentFromCommentBox() {
		List<WebElement> listOfComments = driver.findElements(By.xpath("//body/main[@id='main-content']/div[@id='TelerikWindow-wrapper']/div[@id='TelerikWindow']/div[2]/div[2]/div[1]/div[1]/div[1]"));
		return listOfComments.get(listOfComments.size() - 1).getText();
	}

	public void clickOnClose_icon() {
		elementAction.clickOnElement(jsonData.getWebElement("Close_popup"), "Closed popup");
	}

	
	
	public void performCopyToClipboardOperationFromPreview() {
		srpPage.clickOnCopyEmailToClipBoard();
		Assert.assertTrue(srpPage.isCopyToClipboard_title_exist(), "Edit popup confirmation popup not exist"); // bug
		srpPage.clickOn_EditMessageCopied_Ok_btn();
		srpPage.clickOnOkbuttonforConfirmation();
	}
	
	public void CommunitytabSelection(String currentUrl) {
		if(!driver.getCurrentUrl().equalsIgnoreCase(currentUrl)) {
			driver.navigate().to(currentUrl);	
		}
		ActionsUtility.staticWait(2);
		commonPage.clickOnCommunities_tab();
	}
	
	public void HomeTabtabSelection(String currentUrl) {
		if(!driver.getCurrentUrl().equalsIgnoreCase(currentUrl)) {
			driver.navigate().to(currentUrl);	
		}
		ActionsUtility.staticWait(2);
		commonPage.clickOnHomes_tab();
	}
//	
//	public void loginOrNavigate(String currentUrl) {
//		if(currentUrl.equals("")) {
//			commonPage.loginIfNot(commonData.getJsonData(environment, "EmailId_agent"),commonData.getJsonData(environment, "Password"));
//			homePage.enterSearchData_field(homeData.getJsonData("Search_data", "Matching_Searched_data"));
//			homePage.clickOnSearch_icon();
//			currentUrl=driver.getCurrentUrl();
//		}
//		else
//			driver.navigate().to(currentUrl);			
//	}

	public List<String> getPaginationValuesOnListView() {
		return elementAction.getAllDropdownValues(jsonData.getWebElement("Pagination", "list_Pagination_dropdown"));
	}
	
	public List<String> getPaginationValuesOnPhotoView() {
		return elementAction.getAllDropdownValues(jsonData.getWebElement("Pagination", "photo_Pagination_dropdown"));
	}

	public void clickOnScheduleTourButton() {
		ActionsUtility.staticWait(3);
		// TODO Auto-generated method stub
		elementAction.clickOnElement(jsonData.getWebElement("Schedule_Tour_Button"), "Clicked on the ScheduleTour Button");
	}

	public void clickOnPrintReportMessageLightBox() {
		ActionsUtility.staticWait(3);
		// TODO Auto-generated method stub
		elementAction.clickOnElement(jsonData.getWebElement("Print_Report_Button"), "Clicked on the PrintReport Button");
	}
	public void clickOnNoThanksOnNotificationPopUp()
	{
		ActionsUtility.staticWait(3);
		elementAction.clickOnElement(jsonData.getWebElement("NoThanks"), "Clicked on the No Thanks Link");
	}

}
