package com.ex2.nhspro.actions;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class SavedSearches_actions {

	JsonUtility JsonOR;
	ExtentTest logger;
	WebDriver driver;
	WebDriverWait wait;
	ActionsUtility elementAction;
	public SavedSearches_actions(WebDriver driver, ExtentTest logger) {
		this.logger=logger;
		this.driver=driver;
		JsonOR=new JsonUtility(driver, "SavedSearches");
		wait=new WebDriverWait(driver, Duration.ofSeconds(30));
		elementAction=new ActionsUtility(driver, logger);
}
	
	public boolean isSavedSearches_headerExistOnPage() {
		return JsonOR.getWebElement("").isDisplayed();
	}
	public boolean isSend_btn_existOnPage() {
		return JsonOR.getWebElement("").isDisplayed();
	}
	public boolean isDelete_btn_ExistOnPage() {
		return JsonOR.getWebElement("").isDisplayed();
	}
	public boolean is_Send_btn_disable() {
		return elementAction.isButtonDisabled(JsonOR.getWebElement("Send_btn"));
	}
	public boolean is_Delete_btn_disable() {
		return elementAction.isButtonDisabled(JsonOR.getWebElement("Delete_btn"));
	}

	
	public void clickOnSelectAll_checkbox() {
		elementAction.clickOnElement(JsonOR.getWebElement("SelectAll_checkbox"), "Clicked on Select All checkbox");
	}
	
	public void clickOnSend_btn() {
		elementAction.clickOnElement(JsonOR.getWebElement("Send_btn"), "Clicked on Send button in Save Searches");
	}
	
	public void clickOn_Delete_btn() {
		elementAction.clickOnElement(JsonOR.getWebElement("Delete_btn"), "Clicked on Delete button in Save Search page");
		wait.until(ExpectedConditions.visibilityOf(JsonOR.getWebElement("Delete_btn_popup")));
		logger.log(Status.INFO, "Clicked on Delete button in Save Searches page");
	}
	
	public void select_Perticular_checkbox(String SearchName) {
//		wait.until(ExpectedConditions.visibilityOf(JsonOR.getWebElement("Saved_Seaches_heading")));
		WebElement element;
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
			element = driver.findElement(By.xpath("//table/tbody/tr/td/a[text()='"+SearchName+"']/parent::td/preceding-sibling::td"));
		else
			element = driver.findElement(By.xpath("//span[text()='"+SearchName+"']/parent::h1/preceding-sibling::label"));
		elementAction.clickOnElement(element, SearchName+" checkbox is selected");
	}
	
	public void clickOn_Perticular_SearchName(String SearchName) {
		WebElement savedSearchName;
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			savedSearchName=driver.findElement(By.xpath("//h1/span[text()='"+SearchName+"']"));
			//savedSearchName=driver.findElement(By.xpath("//div/label/h1/span[text()='"+SearchName+"']"));
		}
		else {
			savedSearchName=driver.findElement(By.xpath("//table/tbody/tr/td/a[text()='"+SearchName+"']"));
		}
		elementAction.clickOnElement(savedSearchName, "Clicked on "+SearchName);		
	}
	
	public void clickOn_Perticualr_Rename(String SearchName) {
		driver.findElement(By.xpath("//table/tbody/tr/td/a[text()='"+SearchName+"']/parent::td/following-sibling::td[1]/span")).click();
	}
	
	public String getAssignedTo_PerticularSavedSearch(String SearchName) {
	return	driver.findElement(By.xpath("//table/tbody/tr/td/a[text()='"+SearchName+"']/parent::td/following-sibling::td[2]")).getText();
	}
	
	public String getSentValue_Perticular_SavedSearch(String SearchName) {
		return	driver.findElement(By.xpath("//table/tbody/tr/td/a[text()='"+SearchName+"']/parent::td/following-sibling::td[3]")).getText();
	}
	
	public String getLastView_Perticualr_SavedSearch(String SearchName) {
		return	driver.findElement(By.xpath("//table/tbody/tr/td/a[text()='"+SearchName+"']/parent::td/following-sibling::td[4]")).getText();
	}
	
	public void click_On_Delete_btn_from_Delete_popup() {
		JsonOR.getWebElement("Delete_btn_popup").click();
		logger.log(Status.INFO, "Clicked on Delete button from delete popup");
		System.out.println("Clicked on Delete button from delete popup");
	}
	
	public String getHomeCountFromSavedSearches() {
		   String homeCount = elementAction.getTextData(JsonOR.getWebElement("Bredcrumb_home_count"));
		   logger.log(Status.INFO,"HomeCount On Saved Search breadcrumb:-"+homeCount);
			System.out.println("HomeCount On Saved Search breadcrumb:-"+homeCount);
			return homeCount;
	}
	
	public void clickOnCommentInSaveSearches() {
		WebElement commentIcon;
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
			commentIcon = driver.findElement(By.xpath("//table/tbody/tr/td[2]/a"));
		}else {
			commentIcon=driver.findElement(By.xpath("(//div[@id='feedback-section'])[1]//span[@title='Comment']"));		
		elementAction.clickOnElement(commentIcon, "Clicked on Comment icon from Saved Searched");
		}
	}
	
	public void clickOnSendButtonInSavedSearchPage()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
		elementAction.clickOnElement(JsonOR.getWebElement("SendbuttonInSaveSearchPage"), "Clicked on the send button");
		}else {
			elementAction.clickOnElement(JsonOR.getWebElement("SendbuttonInSaveSearchPage"), "Clicked on the send button");
		}
	}
	
	public void clickOnUpdateSearchButtonFromSaveSearch()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop")) {
		elementAction.clickOnElement(JsonOR.getWebElement("UpdateSearchButton"), "Clicked on the update search button");
		}
		else{
			elementAction.clickOnElement(JsonOR.getWebElement("Update_Save_Search"), "Clicked on the update search button");
		}
	}

	public void clickOnUpdateAndEmailButtonFromSaveSearch()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(JsonOR.getWebElement("Update_And_Email_Button"), "Clicked on the update & email button");
			}
		else {
		elementAction.clickOnElement(JsonOR.getWebElement("UpdateAndEmailButton"), "Clicked update & email button");
		}
	}
	public void clickOnNoButtonOfEditSearchPopUp()
	{  
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
		elementAction.clickOnElement(JsonOR.getWebElement("NoButtonEditSearchPopUp"), "Clicked on no button of edit search popup");
		}
		else {
		elementAction.clickOnElement(JsonOR.getWebElement("NoButtonEditSearchPopUp"), "Clicked on no button of edit search popup");
		}
	}
	
	public void clickOnSendButtonInTemplateOfEditSearchPopUp()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			elementAction.clickOnElement(JsonOR.getWebElement("SendButtonInTemplate"), "Clicked on Send button of Edit Email Save Search Pop Up");	
		}else {
		elementAction.clickOnElement(JsonOR.getWebElement("SendButtonInTemplate"), "Clicked on Send button of Edit Email Save Search Pop Up");
		}
	}
	
	public boolean verifySaveSearchName(String SaveSearchesName)
	{
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		boolean status=false;
		 List<WebElement> list;
		 if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			 list = driver.findElements(By.xpath("//div[@class='saved-searches']/h1/span"));
		 }
		 else {
		 list=driver.findElements(By.xpath("//tr[@role='row']//td[2]/a"));
		 }
		    for(WebElement element:list)
		    {
		    	if(element.getText().contains(SaveSearchesName)) {
		    		System.out.println("Save Search Name "+SaveSearchesName+"Matched" );
		    		status=true;
		    		break;
		    	}
		    }
		    driver.manage().timeouts().implicitlyWait(Integer.parseInt(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
			return status;
	}
	
	public void verifySaveSearchDeletedOrNot(boolean status,String message)
	{
		 if(status==false)
		    {
		    	System.out.println("Save Search Successfully deleted from "+ message);
		    }
		    else
		    {
		    	System.out.println("Save Search not deleted");
		    	Assert.fail("Save Search Not Deleted.");
		    }
	}
	
	public void clickOnRenameIcon(String SearchName)
	{
		ActionsUtility.staticWait(2);
		driver.findElement(By.xpath("(//a[text()='"+SearchName+"']/following::span)[1]")).click();
	}
	
	public void renameSavedSearch(String RenameSearch)
	{
		WebElement editSearchInputBox=JsonOR.getWebElement("Rename_editfield");
		wait.until(ExpectedConditions.visibilityOf(editSearchInputBox));
		editSearchInputBox.clear();
		editSearchInputBox.sendKeys(RenameSearch);
		elementAction.clickOnElement(JsonOR.getWebElement("Update_btn"), "Clicked on the update search button");
		
	}
	
	public void onlyUpdateSavedSeachFromSavedSeachPage(String RenameSearch)
	{
		WebElement editSearchInputBox=JsonOR.getWebElement("EditSearchField");
		wait.until(ExpectedConditions.visibilityOf(editSearchInputBox));
		editSearchInputBox.clear();
		editSearchInputBox.sendKeys(RenameSearch);
		elementAction.clickOnElement(JsonOR.getWebElement("Update"), "Clicked on the update button");
		
	}
	public String getNameofFirstItemIntheDropDownOfSavedSearch()
	{
		System.out.println(JsonOR.getWebElement("FirstItemInSavedSearchDropDown").getText());
		return JsonOR.getWebElement("FirstItemInSavedSearchDropDown").getText();
		
	}
	public String getSearchNameFromBreadCrum()
	{
		System.out.println(JsonOR.getWebElement("SearchNameBreadcrum").getText());
		return JsonOR.getWebElement("SearchNameBreadcrum").getText();
		
	}
	public String getSaveSearchNameFromUpdateSearchPopUp()
	{
		clickOnUpdateSearchButtonFromSaveSearch();
		WebElement editSearchInputBox=JsonOR.getWebElement("EditSearchField");
		wait.until(ExpectedConditions.visibilityOf(editSearchInputBox));
		String SearchName=editSearchInputBox.getAttribute("value");
		elementAction.clickOnElement(JsonOR.getWebElement("CloseUpdateSearchPopup"), "Clicked on Close Update Search Popup");
		System.out.println(SearchName);
		return SearchName;
	}
	
	public void clickOnEditSearchIcon()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("EditSearchIcon"), "Clicked on Edit Search Icon");
		
	}
	
	public void clickOnDropDownAndSelectSearch()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("FirstItemInSavedSearchDropDown"), "Clicked on the drop down");
		elementAction.clickOnElement(JsonOR.getWebElement("SelectSearchFromDropDown"), "Selected the search from the drop down");
	     ActionsUtility.staticWait(3);
	}
	
	public void clickOnComment()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("Comment"), "Clicked on comment");
	}
	
	
	  public void EnterTextOnCommentBox(String Message) {
	  elementAction.normalTypeOnElement(JsonOR.getWebElement("TextArea"), Message, "Enter message "+Message);
	  }
	 
	
	public void clickOnAdd()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("Add"), "Clicked on Add");
	}
	public void clickOnLike()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("Like"), "Clicked on like");
	}
	public void clickOnDislike()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("Dislike"), "Clicked on dis-like");
	}
	public void clickOnCloseOfCommentPopUp()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("Close"), "Clicked on close pop up");
	}
	
	
	public String getLikeImagePath()
	{
		WebElement Like=JsonOR.getWebElement("Like");
		wait.until(ExpectedConditions.visibilityOf(Like));
		String LikeImagePath=Like.getCssValue("background-image");
		System.out.println("Like Image Path -->>"+LikeImagePath);
		return LikeImagePath;
	}
	public String getDisLikeImagePath()
	{
		WebElement DisLike=JsonOR.getWebElement("Dislike");
		wait.until(ExpectedConditions.visibilityOf(DisLike));
		String DisLikeImagePath=DisLike.getCssValue("background-image");
		System.out.println("DisLike Image Path -->>"+DisLikeImagePath);
		
		return DisLikeImagePath;
	}
	
	public void getAttributeValueOfPreview()
	{
		String value;
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			value=JsonOR.getWebElement("PreviewtheEmail_link").getAttribute("class");
		}
		else {
		value=JsonOR.getWebElement("Previewlink").getAttribute("class");
		}
		if(value.equalsIgnoreCase("disable-preview"))
		{
			System.out.println("Preview Link is Disabled");
		}
		else
		{
			Assert.fail("Preview Link is Enabled for Me");
		}
				
	}
	
	public void getAttributeValueOfUpdateAndEmailButton()
	{
		String value;
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			value=JsonOR.getWebElement("Update_And_Email_Button").getAttribute("class");
		}
		else{
			value=JsonOR.getWebElement("UpdateAndEmailButton").getAttribute("class");
		}
		if(value.contains("disabled"))
		{
			System.out.println("Update And Email Button is Disabled");
		}
		else
		{
			Assert.fail("Update And Email Button is Enabled for Me");
		}
				
	}
	
	public void clickOnSendButtonOfSavedSearchResultPage()
	{
		elementAction.clickOnElement(JsonOR.getWebElement("SendButtonOnSavedSearchResultPage"), "Clicked on send button");
	}
	
}
