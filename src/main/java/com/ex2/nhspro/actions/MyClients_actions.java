package com.ex2.nhspro.actions;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;


public class MyClients_actions {

	WebDriver driver;
	WebDriverWait wait;
	JsonUtility jsonData;
	ExtentTest logger;
	ActionsUtility elementAction;
	JavascriptExecutor js;


	public MyClients_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		js = (JavascriptExecutor) driver;
		jsonData = new JsonUtility(driver, "MyClient");
		wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		elementAction = new ActionsUtility(driver, logger);
	}

	public void clickOnNewClientButton() {
		wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("NewClient_btn")));
		WebElement NewClient = jsonData.getWebElement("NewClient_btn");
		ActionsUtility.staticWait(2);
		elementAction.clickOnElement(NewClient, "Clicked On the New Client Button");
	}

	public String EnterValuesInAddNewClientForm(String FirstName, String LastName, String Phone, String Message) {
		elementAction.normalTypeOnElement(jsonData.getWebElement("FirstName_editField"), FirstName, "FirstName Entered");
		elementAction.normalTypeOnElement(jsonData.getWebElement("LastName_editfield"), LastName, "LastName Entered");

		String prefix = "QA";
		Random rand = new Random();
		int random = rand.nextInt(10000);
		String email = prefix + random + "@newhomesource.com";
		elementAction.normalTypeOnElement(jsonData.getWebElement("Email_editfield"), email, "Email Entered");
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
			 WebElement element=jsonData.getWebElement("PhoneNo_editfield");
			 element.clear();
			ActionsUtility.staticWait(2);
			js.executeScript("arguments[0].value='"+Phone+"';",element);
			
		}
		else {
		elementAction.normalTypeOnElement(jsonData.getWebElement("PhoneNo_editfield"), Phone, "Phone no Entered");
		}
		elementAction.normalTypeOnElement(jsonData.getWebElement("Notes_field"), Message, "Message no Entered");
		ActionsUtility.staticWait(2);
		elementAction.clickOnElement(jsonData.getWebElement("Add_btn"), "Clicked On Add Button");
		return email;
	}

	public String AddClientSuccessMessage() {
		wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("AddClientSucessMsg")));
		String text = jsonData.getWebElement("AddClientSucessMsg").getText();
		logger.log(Status.INFO, "Get the Success Message Text");
		System.out.println("Get the Success Message Text");
		return text;
	}

	public void clickOnAddNewClientSucessOkButton() {
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			ActionsUtility.staticWait(3);
		}
		wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("OkButton")));
		jsonData.getWebElement("OkButton").click();
		logger.log(Status.INFO, "Clicked On the Add New Client Success Message Ok Button");
		System.out.println("Clicked On the Add New Client Success Message Ok Button");
	}

	public void clickOnSaveListingFromClient(String clientName) {
		// table/tbody/tr/td[2][contains(text(),'Ritesh
		// Kumar')]/parent::tr//a[contains(@href,'savedlistings')]
		List<WebElement> clientsListing = driver.findElements(By.xpath("//table/tbody/tr/td[2][contains(text(),'"
				+ clientName + "')]/parent::tr//a[contains(@href,'savedlistings')]"));
		elementAction.clickOnElement(clientsListing.get(0), "Clicked on Save Listing");
	}

	public void deleteClient(String ClientName) {
		ActionsUtility.staticWait(2);
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			/*
			 * List<WebElement> l1 = driver.findElements(By.xpath("//span[text()='"+
			 * ClientName+"']/preceding-sibling::input[@class='checkbox pro-client-checkbox']"
			 * )); //span[text()='ABC Test']/ancestor::div[@class='client']/input for
			 * (WebElement element : l1) { Actions action=new Actions(driver);
			 * action.moveToElement(element).click().build().perform(); //
			 * js.executeScript("arguments[0].scrollIntoView(true);", element); //
			 * element.click();
			 * 
			 * wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement(
			 * "Delete_btn"))); jsonData.getWebElement("Delete_btn").click();
			 * wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement(
			 * "DeleteConfirm"))); jsonData.getWebElement("DeleteConfirm").click();
			 * ActionsUtility.staticWait(2);
			 * 
			 * }
			 */
		} else {
			
			elementAction.clickOnElement(jsonData.getWebElement("FirstCheckBox"), "Clicked on First Checkbox");
			elementAction.clickOnElement(jsonData.getWebElement("Delete_btn"), "Clicked on Delete button");
			elementAction.clickOnElement(jsonData.getWebElement("DeleteConfirm"), "Clicked on Delete Confirm button");
			ActionsUtility.staticWait(2);
		}
	//	logger.log(Status.INFO, "Client Deleted Sucessfully");
	//	System.out.println("Client Deleted Sucessfully");

	}
	
	public void clickOnClientonMobile(String ClientName)
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
		driver.findElement(By.xpath("(//div[@id='table-clients']/a/div[@class='client']/span[text()='"+ClientName+"'])[1]")).click();
		}
	}
	public void clickOnMobileBack()
	{
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
		elementAction.clickOnElement(jsonData.getWebElement("Back"),"Clicked on back on client detail page on mobile");
	}
	
	public boolean isClientExist(String EmailId) {
		boolean isdisplayed;
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
		  isdisplayed=driver.findElement(By.xpath("//span[text()='"+EmailId+"']")).isDisplayed();
		  clickOnMobileBack();
		}
		else
		{
		try {
			isdisplayed = driver.findElement(By.xpath("//tr[@role='row']//td[5][text()='"+EmailId+"']")).isDisplayed();
		}
		catch(Exception e) {
			isdisplayed=false;
		}
		driver.manage().timeouts().implicitlyWait(Integer.parseInt(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
		}
		return isdisplayed;
	}
	
	public void deleteClientIfExist(String EmailId) {
		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			elementAction.clickOnElement(driver.findElement(By.xpath("//tr[@role='row']//td[4][text()='"+EmailId+"']/preceding-sibling::td[3]/label")),"Click on Checkbox");
			elementAction.clickOnElement(jsonData.getWebElement("Delete_btn"), "Clicked on Delete button");
			elementAction.clickOnElement(jsonData.getWebElement("DeleteConfirm"), "Clicked on Delete Confirm button");
		}
		catch(Exception e) {
			System.out.println("Client not created");
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
		}

	}
	public boolean isDisplayed(WebElement element)
	{
		return element.isDisplayed();
	}
	
	public void verifyButtonsAvailableOnMyClientPage()
	{
		boolean newclientbutton=isDisplayed(jsonData.getWebElement("NewClient_btn"));
		boolean Deletebutton=isDisplayed(jsonData.getWebElement("Delete_btn"));
		Assert.assertTrue(newclientbutton, "Add New Client Button Is Not Available");
		Assert.assertTrue(Deletebutton, "Delete Button Is Not Available");
		if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
		
		}
		else
		{
			boolean Exportbutton=isDisplayed(jsonData.getWebElement("Export_btn"));
			boolean Importbutton=isDisplayed(jsonData.getWebElement("Import_btn"));
			Assert.assertTrue(Exportbutton, "Export Button Is Not Available");
			Assert.assertTrue(Importbutton, "Import Button Is Not Available");
		}
		System.out.println("On My Client Page Buttons Are Available");
	}
	
	public void exportClients()
	{
		elementAction.clickOnElement(jsonData.getWebElement("SelectAll_checkbox"), "Selected All The Checkboxs");
		elementAction.clickOnElement(jsonData.getWebElement("Export_btn"), "Clicked On The Export Button");
		elementAction.clickOnElement(jsonData.getWebElement("SelectAll_checkbox"), "UnSelected All The Checkboxs");
	}
	
	 public File getLatestFilefromDir(String dirPath){
		    File dir = new File(dirPath);
		    File[] files = dir.listFiles();
		    if (files == null || files.length == 0) {
		        return null;
		    }

		    File lastModifiedFile = files[0];
		    for (int i = 1; i < files.length; i++) {
		       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
		           lastModifiedFile = files[i];
		       }
		    }
		    return lastModifiedFile;
		    } 
	
	 
	 public void clickOnEditIcon(String ClientName)
	 {
		 if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			WebElement client= driver.findElement(By.xpath("(//span[text()='"+ClientName+"'])[1]"));
			client.click();
			elementAction.clickOnElement(jsonData.getWebElement("EditIcon"), "Clicked On Edit Icon");
			} 
		 else {
		WebElement Edit= driver.findElement(By.xpath("(//td[text()='"+ClientName+"']/following::span[text()='Edit'])[1]"));
		Edit.click();
		 }
		 logger.log(Status.INFO, "Clicked On Edit Sucessfully");
			System.out.println("Clicked On Edit Sucessfully");
	 }
	 
	 public void EditClientInformation(String Email,String FirstName, String LastName, String Phone, String Message) {
		    wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("Email_editfield")));
		    jsonData.getWebElement("Email_editfield").clear();
			jsonData.getWebElement("Email_editfield").sendKeys(Email);
			logger.log(Status.INFO, "Email Entered");
			System.out.println("Email Entered");
			wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("FirstName_editField")));
			jsonData.getWebElement("FirstName_editField").clear();
			jsonData.getWebElement("FirstName_editField").sendKeys(FirstName);
			logger.log(Status.INFO, "FirstName Entered");
			System.out.println("FirstName Entered");
			wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("LastName_editfield")));
			jsonData.getWebElement("LastName_editfield").clear();
			jsonData.getWebElement("LastName_editfield").sendKeys(LastName);
			logger.log(Status.INFO, "LastName Entered");
			System.out.println("LastName Entered");
			wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("PhoneNo_editfield")));
			jsonData.getWebElement("PhoneNo_editfield").clear();
			jsonData.getWebElement("PhoneNo_editfield").click();
			ActionsUtility.staticWait(2);
			jsonData.getWebElement("PhoneNo_editfield").sendKeys(Phone);
			logger.log(Status.INFO, "Entered Phone Number");
			System.out.println("Entered Phone Number");
			ActionsUtility.staticWait(1);
			wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("Notes_field")));
			jsonData.getWebElement("Notes_field").clear();
			jsonData.getWebElement("Notes_field").sendKeys(Message);
			logger.log(Status.INFO, "Notes Entered");
			System.out.println("Notes Entered");
			ActionsUtility.staticWait(2);
			wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("SaveButton")));
			jsonData.getWebElement("SaveButton").click();
			logger.log(Status.INFO, "Clicked On Save Button");
			System.out.println("Clicked On Save Button");
		}
	 
	 public void backMyClientPageInMobile()
	 {
		  if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) {
			  elementAction.clickOnElement(jsonData.getWebElement("Back"), "Clicked On Back Link");
			 } 
	 }
	 public String notesMessageOnMyClient(String ClientName)
	 {
		 String Message=null;
		 if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) 
		 {
		 Message= jsonData.getWebElement("NotesMessage").getText();
		System.out.println("Message is "+Message);
		 }
		 else
		 {
			 Message=driver.findElement(By.xpath("//td[text()='"+ClientName+"']/following-sibling::td/span[@class='icon-notes help-tip']")).getAttribute("tooltip");
			 System.out.println(Message);
		 }
		return Message;
		
	 }
	 
	 public void clickOnNotes(String ClientName,String Message)
	 {
		 if (PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile")) 
		 {
			 wait.until(ExpectedConditions.visibilityOf(jsonData.getWebElement("Notes_field")));
				jsonData.getWebElement("Notes_field").clear();
				jsonData.getWebElement("Notes_field").sendKeys(Message);
				logger.log(Status.INFO, "Notes Entered");
				wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("SaveButton")));
				jsonData.getWebElement("SaveButton").click();
				logger.log(Status.INFO, "Clicked On Save Button");
				System.out.println("Clicked On Save Button");
		 }
		 else
		 {
		 driver.findElement(By.xpath("(//td[text()='"+ClientName+"']/following::span[@class='icon-notes help-tip'])[1]")).click();
		 jsonData.getWebElement("NotesTextArea").clear();
		 jsonData.getWebElement("NotesTextArea").sendKeys(Message);
		 ActionsUtility.staticWait(1);
		 elementAction.clickOnElement(jsonData.getWebElement("SaveNotes"), "Clicked On Save Notes Button");
		 }
	 }
	 
	 public void clickOnImport()
	 {
		 ActionsUtility.staticWait(5);
		 elementAction.clickOnElement(jsonData.getWebElement("Import_btn"), "Clicked Import Button"); 
	 }
	 public void ClickOnChooseButton()
	 {
		 ActionsUtility.staticWait(1);
		 Actions action = new Actions(driver);
		 action.moveToElement(jsonData.getWebElement("ChooseFile")).click().build().perform();
	 }
	 public void uploadClientCSVFile()
	 {
		 String filePath=System.getProperty("user.dir")+"\\src\\resources\\java\\MyClients\\My Clients.csv";
		driver.findElement(By.xpath("//input[@id=\"clients\"]")).sendKeys(filePath);
	//	 elementAction.uploadFileWithRobot(filePath);
		 elementAction.clickOnElement(jsonData.getWebElement("SubmitButton"), "Clicked On Submit Button");
	 }
	 
	 public String importedClient()
	 {
		 return jsonData.getWebElement("ImportedClients").getText();
		 
	 }
	 
	 public String notImportedClient()
	 {
		 return jsonData.getWebElement("ClientsNotImported").getText();
		 
	 }
	 public void closeImportPopUp()
	 {
		 elementAction.clickOnElement(jsonData.getWebElement("Close"), "Clicked On Close Icon");
	 }
	 
	 public void deleteAllClient()
	 {
		 elementAction.clickOnElement(jsonData.getWebElement("SelectAll_checkbox"), "Selected All Clients");
		 elementAction.clickOnElement(jsonData.getWebElement("Delete_btn"), "Clicked On Delete Button");
		 elementAction.clickOnElement(jsonData.getWebElement("DeleteConfirm"), "Clicked on Delete Confirm button");
		  driver.navigate().refresh();
		//  ActionsUtility.staticWait(30);
		  List<WebElement>L1= driver.findElements(By.xpath("//tr[@role='row']"));
		  for(int i=0;i<=L1.size();i++)
			 {
			  System.out.println("Total number of clients to be deleted "+L1.size());
			  elementAction.clickOnElement(jsonData.getWebElement("SelectAll_checkbox"), "Selected All Clients");
				 elementAction.clickOnElement(jsonData.getWebElement("Delete_btn"), "Clicked On Delete Button");
				 elementAction.clickOnElement(jsonData.getWebElement("DeleteConfirm"), "Clicked on Delete Confirm button");
				 driver.navigate().refresh();
				 ActionsUtility.staticWait(10);
		 if(driver.findElements(By.xpath("//tr[@role='row']")).size()==0)
		 {
			 System.out.println("Deleted All Clients");
			 break;
		 }
		 }
		
	 }
	 
	 public void deleteAllClient_Update(){
		 List rows;
		 driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		 do {
			 elementAction.clickOnElement(jsonData.getWebElement("SelectAll_checkbox"), "Selected All Clients");
			 elementAction.clickOnElement(jsonData.getWebElement("Delete_btn"), "Clicked On Delete Button");
			 elementAction.clickOnElement(jsonData.getWebElement("DeleteConfirm"), "Clicked on Delete Confirm button");
			 driver.navigate().refresh();
			rows=driver.findElements(By.xpath("//tr[@role='row']"));

		 }while(rows.size()!=0);
		 driver.manage().timeouts().implicitlyWait(Integer.parseInt(PropertyFileOperation.getPropertyValue("implicitWait")), TimeUnit.SECONDS);
		 }
	 
	 
}
