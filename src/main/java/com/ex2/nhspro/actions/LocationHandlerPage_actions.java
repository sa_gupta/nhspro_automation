package com.ex2.nhspro.actions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;

public class LocationHandlerPage_actions {

	WebDriver driver;
	JsonUtility jsonData;
	ExtentTest logger;
	ActionsUtility elementAction;
	WebDriverWait wait;
	public LocationHandlerPage_actions(WebDriver driver,ExtentTest logger) {
		this.driver=driver;
		this.logger=logger;
		jsonData=new JsonUtility(driver, "LocationHandlerPage");
		wait=new WebDriverWait(driver, Duration.ofSeconds(30));
		elementAction=new ActionsUtility(driver, logger);
	}
	
	public void enterSearchData_field(String SearchData) {
		elementAction.clearandType(jsonData.getWebElement("Search_field"), SearchData, "Entered "+SearchData+" in Search field");
		}
	
	public void clickOnSearch_icon() {
		elementAction.clickOnElement(jsonData.getWebElement("Search_icon"), "Clicked in Search field");
		ActionsUtility.staticWait(5);
	}
	
	public boolean isUserOn_Location_HandlerPage() {
		wait.until(ExpectedConditions.urlContains("locationhandler"));
		boolean islocationHandlerExist=elementAction.isElementExistOnThePage(jsonData.getWebElement("Try_your_search"), "User is on location handler page");
		return islocationHandlerExist;
	}
}
