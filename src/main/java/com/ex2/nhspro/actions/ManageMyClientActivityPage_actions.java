package com.ex2.nhspro.actions;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;

public class ManageMyClientActivityPage_actions {

	WebDriver driver;
	WebDriverWait wait;
	JsonUtility jsonData;
	ExtentTest logger;
	CommonPage_actions commonPage;
	HomePage_actions homePage;
	SRPPage_actions srpPage;
	ActionsUtility action;
	JavascriptExecutor js;

	

	public ManageMyClientActivityPage_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "ManageMyClientActivityPage");
		wait=new WebDriverWait(driver, Duration.ofSeconds(30));
		commonPage= new CommonPage_actions(driver,logger);
		homePage=new HomePage_actions(driver,logger);
		srpPage=new SRPPage_actions(driver,logger);
		action=new ActionsUtility(driver, logger);
		js = (JavascriptExecutor) driver;
		
	}
    
	public void switchtoWindowAndLogin(String email,String Location)
	{
		String original=driver.getWindowHandle();
		Set<String> allwindow=driver.getWindowHandles();
		Iterator<String> itr=allwindow.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
		commonPage.clickOnSingIn_link();
		commonPage.enterEmailAddress(email);
		commonPage.clickOnSubmit_btn();
		ActionsUtility.staticWait(2);
		homePage.enterSearchData_field(Location);
		homePage.clickOnSearch_icon();
		commonPage.clickOnView("List");
		commonPage.clickOnCommunities_tab();
		ActionsUtility.staticWait(3);
		srpPage.selectClientTheFirstCheckbox();
		ActionsUtility.staticWait(1);
		srpPage.clickOn_Main_SaveListing();
		srpPage.clickOnOk_saveListing();
		ActionsUtility.staticWait(2);
		clickOnSavedListingClient();
		ActionsUtility.staticWait(2);
		commonPage.clickOnCommunities_tab();
		focusOnNone();
		clickOnLike();
		ActionsUtility.staticWait(3);
		driver.switchTo().window(original);
		commonPage.clickOnManagMyClientActivity();
		
	}
	
	public void clickOnSavedListingClient()
	{
		
		wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("SavedListings")));
		js.executeScript("arguments[0].click();", jsonData.getWebElement("SavedListings"));
		logger.log(Status.INFO, "Clicked On the Saved Listings");
		System.out.println("Clicked On the Saved Listings");
		
	}
	public void clickOnLike()
	{
		ActionsUtility.staticWait(3);
		wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("Like")));
		js.executeScript("arguments[0].click();", jsonData.getWebElement("Like"));
		logger.log(Status.INFO, "Clicked On Like");
		System.out.println("Clicked On Like");
	}
	
	public void focusOnNone()
	{
		Actions action=new Actions(driver);
		action.moveToElement(jsonData.getWebElement("None")).build().perform();
	}
	
	public int getNotificationCount()
	{
		
		driver.navigate().refresh();
		String notificationcount=jsonData.getWebElement("NotificationIcon").getText();
		 System.out.println("Total number of Notification is "+notificationcount);
		 if(notificationcount.equalsIgnoreCase(""))
			 return 0;
		 else
			 return Integer.valueOf(notificationcount);
	}

    public void markAsRead()
    {
    	ActionsUtility.staticWait(5);
    	action.clickOnElement(jsonData.getWebElement("SelectAllCheckbox"), "Selected All Checkboxes");
    	action.clickOnElement(jsonData.getWebElement("MarkAsRead"), "Clicked On Mark as Read");
 
    }
}
