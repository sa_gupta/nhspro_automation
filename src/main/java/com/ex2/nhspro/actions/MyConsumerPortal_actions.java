package com.ex2.nhspro.actions;

import static org.testng.Assert.assertEquals;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.AppiumDriver;

import org.apache.commons.io.output.BrokenOutputStream;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.ex2.nhspro.utility.ActionsUtility;
import com.ex2.nhspro.utility.JsonUtility;
import com.ex2.nhspro.utility.PropertyFileOperation;

public class MyConsumerPortal_actions {
	
	WebDriver driver;
	WebDriverWait wait;
	JsonUtility jsonData;
	ExtentTest logger;
	JavascriptExecutor js;
	ActionsUtility elementaction;

	public MyConsumerPortal_actions(WebDriver driver, ExtentTest logger) {
		this.logger = logger;
		this.driver = driver;
		jsonData = new JsonUtility(driver, "MyConsumerPortal");
		wait=new WebDriverWait(driver, Duration.ofSeconds(30));
		js = (JavascriptExecutor) driver;
		elementaction= new ActionsUtility(driver,logger);
	}
	public void enterMyConsumerPortalAccountDetails(String FirstName,String LastName,String Designation,String emailddress,String OfficeName,String License,String Brokeragewebsite,String Phone)
	{
	elementaction.clearandType(jsonData.getWebElement("First_name"), FirstName, "Entered First Name");
	elementaction.clearandType(jsonData.getWebElement("Last_name"), LastName, "Entered Last Name");
	elementaction.clearandType(jsonData.getWebElement("Designation"), Designation, "Entered Designation");
	elementaction.clearandType(jsonData.getWebElement("Email_address"), emailddress, "Entered Email Address");
	if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
	{
		 WebElement element=jsonData.getWebElement("Phone");
		 element.clear();
		ActionsUtility.staticWait(2);
		js.executeScript("arguments[0].value='"+Phone+"';",element);
	}
	else {
	elementaction.clearandType(jsonData.getWebElement("Phone"), Phone, "Entered Phone");
	}
	elementaction.clearandType(jsonData.getWebElement("OfficeName"), OfficeName, "Entered OfficeName");
	elementaction.clearandType(jsonData.getWebElement("License"), License, "Entered License");
	elementaction.clearandType(jsonData.getWebElement("BrokerWebsite"), Brokeragewebsite, "Entered BrokerWebsite");
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
			//((AppiumDriver) driver).hideKeyboard();
		}
	}
	
	public void clickOnSaveButton()
	{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("SaveButton"));
		wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("SaveButton")));
elementaction.clickOnElement(jsonData.getWebElement("SaveButton"), "Clicked On the Save Button");
	}
	public String sucessMessagetext()
	{
		return elementaction.getTextData(jsonData.getWebElement("SuccessMessage"));
	}
	public void clickOnOkButton()
	{
		elementaction.clickOnElement(jsonData.getWebElement("OK_Button"), "Clicked On the OK Button");
	}
	public void enterTextInEditor(String Editor)
	{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("ConsumerPortalEditor"));
		elementaction.normalTypeOnElement(jsonData.getWebElement("ConsumerPortalEditor"), Editor,  "Entered Text In Editor");
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("mobile"))
		{
			//((AppiumDriver) driver).hideKeyboard();
		}
	}
	
	public void EditSiteUrl(String Url)
	{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("EditShowingNew_link"));
		elementaction.clickOnElement(jsonData.getWebElement("EditShowingNew_link"), "Clicked On Edit Link");
		wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("SaveEditUrlButton")));
		elementaction.clearandType(jsonData.getWebElement("SiteUrl"), Url, "Entered Url");
		elementaction.clickOnElement(jsonData.getWebElement("SaveEditUrlButton"), "Save Edit Url button");
		clickOnOkButton();
	}
	
	public void uploadHeadShotImage()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("HeadshotLabel"));
		String filePath=System.getProperty("user.dir")+"\\src\\resources\\java\\ConsumerPortalImages\\Images1.png";
		System.out.println(filePath);
		if(jsonData.getWebElement("DeleteHeadshot").isDisplayed())
		{
			elementaction.clickOnElement(jsonData.getWebElement("DeleteHeadshot"), "Deleted Headshot Image");
			elementaction.clickOnElement(jsonData.getWebElement("UploadHeadShot"), "UploadHead shot image");
		    elementaction.uploadFileWithRobot(filePath);
		    elementaction.clickOnElement(jsonData.getWebElement("CropHeadshot"), "CropHead shot ");
		    elementaction.clickOnElement(jsonData.getWebElement("CropHeadshotImage"), "CropHeadshotImage");
		}
		else
		{
			elementaction.clickOnElement(jsonData.getWebElement("UploadHeadShot"), "UploadHead shot image");
			    elementaction.uploadFileWithRobot(filePath);
			elementaction.clickOnElement(jsonData.getWebElement("CropHeadshot"), "Cropped Head shot");
			elementaction.clickOnElement(jsonData.getWebElement("CropHeadshotImage"), "CropHeadshotImage");
		}
		 logger.log(Status.INFO, "Croped Headshot Image");
			System.out.println("Croped Headshot Image");
	    wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("HeadshotSucessOkButton")));
	    String headshotSuccessmessagetext=jsonData.getWebElement("HeadshotSuccessMessageText").getText();
	    assertEquals(headshotSuccessmessagetext, "The image was updated.");
	    elementaction.clickOnElement( jsonData.getWebElement("HeadshotSucessOkButton"), "Headshot Image Uploaded Successfully");
		}
	}
	
	public void uploadBrokerageImages()
	{
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("BrokerageLabel"));
		String filePath2=System.getProperty("user.dir")+"\\src\\resources\\java\\ConsumerPortalImages\\Images2.png";
		if(jsonData.getWebElement("DeleteBrokerageLogo").isDisplayed())
		{
			 wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("DeleteBrokerageLogo")));
			 jsonData.getWebElement("DeleteBrokerageLogo").click();
			 logger.log(Status.INFO, "Deleted Brokerage Image");
			 System.out.println("Deleted Brokerage Image");
			 wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("UploadBrokerageLogo")));
		     jsonData.getWebElement("UploadBrokerageLogo").click();
		     elementaction.uploadFileWithRobot(filePath2);
		     wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("CropHeadshot")));
		     jsonData.getWebElement("CropHeadshot").click();
		     wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("CropBrokerageLogo")));
		     jsonData.getWebElement("CropBrokerageLogo").click();
		}
		else
		{
			    wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("UploadBrokerageLogo")));
			    jsonData.getWebElement("UploadBrokerageLogo").click();
			    elementaction.uploadFileWithRobot(filePath2);
			    wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("CropHeadshot")));
			    jsonData.getWebElement("CropHeadshot").click();
			    wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("CropBrokerageLogo")));
			    jsonData.getWebElement("CropBrokerageLogo").click();
		}
		logger.log(Status.INFO, "Croped Brokerage Image");
		System.out.println("Croped Brokerage Image");
	    wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("OK_Button")));
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    String brokerageSuccessmessagetext=jsonData.getWebElement("BrokerageSuccessMessageText").getText();
	    assertEquals(brokerageSuccessmessagetext, "The image was updated.");
	    jsonData.getWebElement("OK_Button").click();
	    logger.log(Status.INFO, "Brokerage Logo Uploaded Successfully");
		System.out.println("Brokerage Logo Uploaded Successfully");
		}
	}
	
	public void selectBackgroundImage()
	{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("BrokerageLabel"));
		 wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("ChooseDefaultImage")));
		 jsonData.getWebElement("ChooseDefaultImage").click();
		 wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("SelectBackgroundImage")));
		 jsonData.getWebElement("SelectBackgroundImage").click();
		 logger.log(Status.INFO, "Selected Home Page Background Image");
			System.out.println("Selected Home Page Background Image");
		 wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("OK_Button")));
		 String HomepagebackgroundSuccessmessagetext=jsonData.getWebElement("HomeBackgroungImageSuccessMessageText").getText();
		    assertEquals(HomepagebackgroundSuccessmessagetext, "The image was updated.");
		    jsonData.getWebElement("OK_Button").click();
		    logger.log(Status.INFO, "Home Page Background Image Uploaded Successfully");
			System.out.println("Home Page Background Image Uploaded Successfully");
	}
	
	public void validateImages()
	{
		elementaction.pageRefresh();
		ActionsUtility.staticWait(3);
		if(PropertyFileOperation.getPropertyValue("mode").equalsIgnoreCase("desktop"))
		{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("HeadshotLabel"));
		boolean deleteheadshot=jsonData.getWebElement("DeleteHeadshot").isDisplayed();
		boolean deletebrokerage=jsonData.getWebElement("DeleteBrokerageLogo").isDisplayed();
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("BrokerageLabel"));
		boolean deletehomebackground=jsonData.getWebElement("DeleteHomePageBackground").isDisplayed();
		if(deleteheadshot==true & deletebrokerage==true & deletehomebackground==true)
		{
			logger.log(Status.INFO, "All the Images Uploaded Successfully");
			System.out.println("All the Images Uploaded Successfully");
		}
		else
		{
			Assert.fail("Images are not uploaded");
		}
	}
		else
		{
			js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("BrokerageLabel"));
			boolean deletehomebackground=jsonData.getWebElement("DeleteHomePageBackground").isDisplayed();
			if(deletehomebackground==true)
			{
				logger.log(Status.INFO, "Background images is Uploaded Successfully");
				System.out.println("Background images is Uploaded Successfully");
			}
			else
			{
				Assert.fail("Background images is not uploaded");
			}
		}
	}
	
	public void clickShowingAgentLink()
	{
		wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("ShowingNew_link")));
		jsonData.getWebElement("ShowingNew_link").click();
	}
	
	public void clickOnWidget()
	{
		elementaction.clickOnElement(jsonData.getWebElement("Widget_link"), "Clicked on Widget link");
	}
	
	public void clickOnPreview()
	{
		js.executeScript("arguments[0].scrollIntoView();", jsonData.getWebElement("Preview"));
		wait.until(ExpectedConditions.elementToBeClickable(jsonData.getWebElement("Preview")));
		jsonData.getWebElement("Preview").click();
		logger.log(Status.INFO, "Clicked On the Preview Button");
		System.out.println("Clicked On the Preview Button");
	}
	
	public void loginSiteManager(String Email,String Password)
	{
		elementaction.clearandType(jsonData.getWebElement("SitemangerEmail"), Email, "Entered Email");
		elementaction.clearandType(jsonData.getWebElement("SitemangerPassword"), Password, "Entered Password");
		jsonData.getWebElement("SitemangerSignIn").click();
		
	}
	public void clickOnRestrictUrl()
	{
		jsonData.getWebElement("SitemangerRestrictUrlLink").click();
	}
	
	
	public void switchtopreviewPage()
	{
		Set<String> allList=driver.getWindowHandles();
		Iterator<String> itr=allList.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
		}
	}
	
	public List<WebElement> listOfRestrictUrls()
	{
		return driver.findElements(By.xpath("//ul[@class='flex wrap ac rsn-readonly-ul']//li"));
	}
	
	public void printAllRestrictedUrls(List<WebElement>restrict,List<String>alltext)
	{
		 List<WebElement> Manual;
		 if(driver.findElements(By.xpath("//li[@class='flex ac']/input[@type='text']")).size()>0)
		    {
		     Manual=driver.findElements(By.xpath("//li[@class='flex ac']/input[@type='text']"));
		    for(WebElement ele:Manual)
		    {
		    	alltext.add(ele.getAttribute("value"));
		    }
		    }
		    else
		    {
		    	System.out.println("No Item in user defined URLS ");
		    }
		
		for(WebElement ele:restrict)
		{
			alltext.add(ele.getText());
		  
		}
		for(String str:alltext)
		{
			System.out.println(str);
		}
		
	}
	
	public String returnShowingNewUrl()
	{
		  String[] str=driver.getCurrentUrl().split("/");
		  System.out.println(str[0]+"//"+str[2]+"/"); 
		  return str[0]+"//"+str[2]+"/"; 
	}
	
	public String getKeyActiveText()
	{
		return jsonData.getWebElement("ActiveKey").getText();
	}
	
}
